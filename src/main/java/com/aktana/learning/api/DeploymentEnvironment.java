package com.aktana.learning.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides special access control to the current run-time value of 
 * AktanaServiceConfiguration.theCustomerEnvironmentName, which is defined
 * in the aktana-service.yml file.
 * 
 * The purpose of this class is not to restrict direct access to that value, e.g. via 
 * 		String theCustomerEnvironmentName = AktanaService.getInstance().getConfiguration().getTheCustomerEnvironmentName();
 * but instead to form a compile-time linkage of all usage of this important configuration parameter.
 * 
 * The theCustomerEnvironmentName config parameter is vital to the way Aktana handles
 * customer-specific customization of our product.
 * 
 * There are only two callers of this class at this time (07Feb2016):
 * 		FlexibleAttributesBo	- relates to \src\main\resources\custom\XXXXXXX\CustomMetadata.json
 * 		SEConfiguratorBo		- relates to \src\main\resources\custom\XXXXXXX\SeConfigDeploymentMetadata.json
 */
public class DeploymentEnvironment {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeploymentEnvironment.class);
	
	private static final String THE_DEFAULT_ENVIRONMENT = "dev";
	
	// It is not really necessary to maintain an "official" list of "theCustomerEnvironmentName"
	// values here.  This has been done more for educational purposes for training 
	// new Aktana ENG/DEV staff about the key role that the aktana-service.yml file's
	// "theCustomerEnvironmentName" plays in Aktana's approach to handling customer-specific
	// database schema and associated "flexible attributes" aka CustomerMetadata, and
	// customization of DSE configuration parameter default values.
	private static final List<String> DEPLOYMENT_ENVIRONMENTS = new ArrayList<String>(Arrays.asList(
			"dev",
			"qa",
			"preuat",
			"uat",
			"prod"
	));
	
	private static String THE_ENVIRONMENT = null;	// the value of "theCustomerEnvironmentName"

	private DeploymentEnvironment() {	}
	
	public static String getEnvironment() {
		// lazy-initialization, static style
		if (THE_ENVIRONMENT == null) {
			LOGGER.warn("*** Someone is calling DeploymentEnvironment.getEnvironment(), but no one has called DeploymentEnvironment.setEnvironment(xx) yet !!!");
			THE_ENVIRONMENT = THE_DEFAULT_ENVIRONMENT;
		}
		return THE_ENVIRONMENT;
	}

	protected static void setEnvironment(String env) {
		if (!isKnownCustomer(env)) {
			LOGGER.warn("*** TheDeploymentEnvironmentName " +env+ " is not a Known value, please verify and if correct add to DeploymentEnvironment");
		}
		if (THE_ENVIRONMENT != null && !THE_ENVIRONMENT.equals(env)) {
			LOGGER.error("*** DeploymentEnvironment has already been set to "+THE_ENVIRONMENT
					+", but someone is now trying to reset it to "+env);
		} 
		else if (env==null || env.trim().isEmpty()) {
			throw new RuntimeException("Attempted to set DeploymentEnvironment to null/empty value");
		}
		else {
			LOGGER.info("*** DeploymentEnvironment has been specified: "+env);
			THE_ENVIRONMENT = env;
		}
	}
	
	private static boolean isKnownCustomer(String env) {
		return DEPLOYMENT_ENVIRONMENTS.contains(env);
	}
	
}