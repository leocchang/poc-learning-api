/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* Implementation of the ValidPeriodDTO annotation.
* Checks that the IExternallyIdentifiableDTO object's getStartDate() is <= getEndDate().
* Returns false IFF both dates are valid, but endDate < startDate.
*/
public class ObjectUIDValidator {

	// alpha-numeric plus period, dash, underscore
	private static final String VALID_OBJECT_UID_CHARS = "a-zA-Z0-9\\.\\-\\_";
	private static final String COMPOUND_OBJECT_UID_SEPARATOR_CHAR = "~";
	
	private static final Pattern invalidPrimaryObjectUIDPattern;
	private static final Pattern primaryObjectUIDPattern;
	private static final Pattern compoundObjectUIDPattern;
	
	static {
		invalidPrimaryObjectUIDPattern = Pattern.compile("[^" + VALID_OBJECT_UID_CHARS + "]");
		primaryObjectUIDPattern = Pattern.compile("[" + VALID_OBJECT_UID_CHARS + "]*");
		compoundObjectUIDPattern = Pattern.compile("[" + VALID_OBJECT_UID_CHARS + COMPOUND_OBJECT_UID_SEPARATOR_CHAR +"]*");
	}

	public boolean isValid(String objectUID, boolean isCompound) {
		Matcher matcher;
		
		// will be checked in some other validator
		if (objectUID == null){
			return true;
		}
		
		if (isCompound) { //0018000000uQOc5AAG~a008000000G35pGAAR~~VISIT~VISIT_DETAIL~2016-07-01~2016-09-30~STR_MR
			matcher = compoundObjectUIDPattern.matcher(objectUID);
		}
		else {
			matcher = primaryObjectUIDPattern.matcher(objectUID);
		}
		return matcher.matches();
	}
	
	/**
	 * Returns String containing the first invalid character found within the specified
	 * primaryOjectUID value. Returns null if no invalid char is found
	 */
	public String findFirstInvalidCharInPrimaryObjectUID(String primaryObjectUID) {
		Matcher matcher = invalidPrimaryObjectUIDPattern.matcher(primaryObjectUID);
		String invalidChar = null;
        if (matcher.find()) {
            invalidChar = matcher.group();
        }
		return invalidChar;
	}

}
