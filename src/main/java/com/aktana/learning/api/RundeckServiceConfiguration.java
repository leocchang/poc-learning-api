package com.aktana.learning.api;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RundeckServiceConfiguration {
	
	@JsonProperty
    @NotNull
    private String key;

    @JsonProperty
    @NotNull
    private String url;

    @JsonProperty
    @NotNull
    private Map<String,String> paths;


	/**
     * Returns the key used to connect to Rundeck API
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the key to connect to Rundeck API
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    

    /**
     * Returns the authentication endpoint to connect to Rundeck
     * @return the authEndpoint
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the auth endpoint to connect to Rundeck
     * @param authEndpoint
     */
    public void setUrl(String url) {
        this.url = url;
    }
    

    /**
     * Returns a map of job paths
     * @return the paths
     */
    public Map<String,String> getPaths() {
        return paths;
    }

    /**
     * Sets the  map of job paths
     * @param paths
     */
    public void setJobs(Map<String,String> paths) {
        this.paths = paths;
    }

}
