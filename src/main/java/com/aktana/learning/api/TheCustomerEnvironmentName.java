package com.aktana.learning.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides special access control to the current run-time value of 
 * AktanaServiceConfiguration.theCustomerEnvironmentName, which is defined
 * in the aktana-service.yml file.
 * 
 * The purpose of this class is not to restrict direct access to that value, e.g. via 
 * 		String theCustomerEnvironmentName = AktanaService.getInstance().getConfiguration().getTheCustomerEnvironmentName();
 * but instead to form a compile-time linkage of all usage of this important configuration parameter.
 * 
 * The theCustomerEnvironmentName config parameter is vital to the way Aktana handles
 * customer-specific customization of our product.
 * 
 * There are only two callers of this class at this time (07Feb2016):
 * 		FlexibleAttributesBo	- relates to \src\main\resources\custom\XXXXXXX\CustomMetadata.json
 * 		SEConfiguratorBo		- relates to \src\main\resources\custom\XXXXXXX\SeConfigDeploymentMetadata.json
 */
public class TheCustomerEnvironmentName {

	private static final Logger LOGGER = LoggerFactory.getLogger(TheCustomerEnvironmentName.class);
	
	private static final String THE_DEFAULT_CUSTOMER = "DEFAULT";
	
	// It is not really necessary to maintain an "official" list of "theCustomerEnvironmentName"
	// values here.  This has been done more for educational purposes for training 
	// new Aktana ENG/DEV staff about the key role that the aktana-service.yml file's
	// "theCustomerEnvironmentName" plays in Aktana's approach to handling customer-specific
	// database schema and associated "flexible attributes" aka CustomerMetadata, and
	// customization of DSE configuration parameter default values.
	private static final List<String> KNOWN_CUSTOMERS = new ArrayList<String>(Arrays.asList(
			"DEFAULT",
			"EMPTY",
			"V3DEMO_EN",	// ??
			"PFIZERJA",		// Pfizer Japan, 9 Products: PRV,VAT,TRA,TOV,CAD,ELQ,ZLT,LYR,CEL,WHC-G
			"SUNOVIONUS",	// Sunovion US, 2 Products: Aptiom, Latuda, Brovana
			"PFIZERUS",		// PfizerUS, 3 Products: Chantix, Xeljanz, Lyrica, Bosulif
			"AZJA",			// AstraZeneca Japan: NEX, SYM
			"BMSUS",		// Bristol-Myers Squibb: ? products
			"MSDUK",		// MSD UK, ("Merck Sharp & Dohme", or just "Merck" within US): JANUVIA
			"GSKJA",		// GSK Japan: ???
			"LILLYJA",		// Lilly Japan: Stratera?
			"MSDFR",			// MSD France
			"MERCKSERONOJA"		// Merck-Serono (the "MS" in "MSD) Japan
			));
	
	private static String THE_CUSTOMER = null;	// the value of "theCustomerEnvironmentName"

	private TheCustomerEnvironmentName() {	}
	
	public static String getName() {
		// lazy-initialization, static style
		if (THE_CUSTOMER == null) {
			LOGGER.warn("*** Someone is calling TheCustomerEnvironmentName.getName(), but no one has called TheCustomer.setName(xx) yet !!!");
			THE_CUSTOMER = THE_DEFAULT_CUSTOMER;
		}
		return THE_CUSTOMER;
	}

	protected static void setName(String name) {
		if (!isKnownCustomer(name)) {
			LOGGER.warn("*** TheCustomerEnvironmentName " +name+ " is not a Known value, please verify and if correct add to TheCustomerEnvironmentName");
		}
		if (THE_CUSTOMER != null && !THE_CUSTOMER.equals(name)) {
			LOGGER.error("*** TheCustomerEnvironmentName has already been set to "+THE_CUSTOMER
					+", but someone is now trying to reset it to "+name);
		} 
		else if (name==null || name.trim().isEmpty()) {
			throw new RuntimeException("Attempted to set TheCustomerEnvironmentName to null/empty value");
		}
		else {
			LOGGER.info("*** TheCustomerEnvironmentName has been specified: "+name);
			THE_CUSTOMER = name;
		}
	}
	
	private static boolean isKnownCustomer(String name) {
		return KNOWN_CUSTOMERS.contains(name);
	}
	
}
