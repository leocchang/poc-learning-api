package com.aktana.learning.api;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AktanaSubscriptionServiceConfiguration {
	
	// can be veeva-default, veeva-pfizer
	@JsonProperty
    @NotNull
    private String type;
	

	@JsonProperty
    @NotNull
    private String username;

    @JsonProperty
    @NotNull
    private String password;

    @JsonProperty
    private String securityToken = "";

    @JsonProperty
    @NotNull
    private String authEndpoint;

    @JsonProperty    
    @NotNull    
    private Boolean publishEnabled;

    @JsonProperty    
    @NotNull
    private Integer veevaMaxAccountsPerList;
        
    @JsonProperty
    @NotNull
    private String veevaSuggestionListName = "DSE Touchpoints";

    // number of days to publish from the currentDate
    @JsonProperty
    @NotNull
    private Integer publishDays;
        
    @JsonProperty    
    @NotNull
    private String pfizerSpecificAccountListName = "DSE Visits";
    
        
    
    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
     * Returns the username used to connect to SalesForce
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username to connect to SalesForce
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the password used to connect to SalesForce
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password to be used in the connection
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the security token. If not defined sets to an empty string.
     * @return the securityToken
     */
    public String getSecurityToken() {
        return securityToken;
    }

    /**
     * Sets the security token if exists
     * @param securityToken
     */
    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    /**
     * Returns the authentication endpoint to connect to SalesForce
     * @return the authEndpoint
     */
    public String getAuthEndpoint() {
        return authEndpoint;
    }

    /**
     * Sets the auth endpoint to connect to SalesForce
     * @param authEndpoint
     */
    public void setAuthEndpoint(String authEndpoint) {
        this.authEndpoint = authEndpoint;
    }

	public Boolean getPublishEnabled() {
		return publishEnabled;
	}

	public void setPublishEnabled(Boolean publishEnabled) {
		this.publishEnabled = publishEnabled;
	}

	public String getVeevaSuggestionListName() {
		return veevaSuggestionListName;
	}

	public void setVeevaSuggestionListName(String veevaSuggestionListName) {
		this.veevaSuggestionListName = veevaSuggestionListName;
	}

	public Integer getVeevaMaxAccountsPerList() {
		return veevaMaxAccountsPerList;
	}

	public void setVeevaMaxAccountsPerList(Integer veevaMaxAccountsPerList) {
		this.veevaMaxAccountsPerList = veevaMaxAccountsPerList;
	}

	public Integer getPublishDays() {
		return publishDays;
	}

	public void setPublishDays(Integer publishDays) {
		this.publishDays = publishDays;
	}

	public String getPfizerSpecificAccountListName() {
		return pfizerSpecificAccountListName;
	}

	public void setPfizerSpecificAccountListName(
			String pfizerSpecificAccountListName) {
		this.pfizerSpecificAccountListName = pfizerSpecificAccountListName;
	}
    
}
