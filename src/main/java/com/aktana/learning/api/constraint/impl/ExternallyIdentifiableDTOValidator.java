/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.constraint.impl;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.aktana.learning.api.constraint.ValidExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.validation.ObjectUIDValidator;

/**
* Implementation of the ValidPeriodDTO annotation.
* Checks that the IExternallyIdentifiableDTO object's getStartDate() is <= getEndDate().
* Returns false IFF both dates are valid, but endDate < startDate.
*/
public class ExternallyIdentifiableDTOValidator implements ConstraintValidator<ValidExternallyIdentifiableDTO, IExternallyIdentifiableDTO> {
	
	private ObjectUIDValidator objectUIDValidator;
	
	@Override
	public void initialize(ValidExternallyIdentifiableDTO constraintAnnotation) {
		objectUIDValidator = new ObjectUIDValidator();
	}

	@Override
	public boolean isValid(IExternallyIdentifiableDTO externallyIdentifiableDTO, ConstraintValidatorContext context) {
		return objectUIDValidator.isValid(externallyIdentifiableDTO.getObjectUID(), externallyIdentifiableDTO.isCompoundObjectUID());
	}

}
