/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.constraint;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.aktana.learning.api.constraint.impl.DateStringValidator;
import com.aktana.learning.common.util.datetime.PureDate;

@Target( { METHOD, FIELD, PARAMETER, LOCAL_VARIABLE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@NotEmpty(message="Date must be specified")
@Pattern(regexp=PureDate.AKTANA_PURE_DATE_REGEX_PATTERN, message="Date must have format "+PureDate.AKTANA_PURE_DATE_FORMAT)
@Constraint(validatedBy = DateStringValidator.class)
@Documented
public @interface ValidDateString {

	String message() default "Invalid value for date";
	
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String fieldName();
}
