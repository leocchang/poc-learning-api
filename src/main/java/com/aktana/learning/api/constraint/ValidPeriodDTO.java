/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.constraint;

import com.aktana.learning.api.constraint.impl.PeriodDTOValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

/**
 * Validation annotation that can be applied to any DTO that implements IPeriodDTO.
 * Checks that the IPeriodDTO object's getStartDate() is <= getEndDate().
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PeriodDTOValidator.class)
@Documented
public @interface ValidPeriodDTO {

	String message() default "startDate must be less than or equal endDate";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	// no "value" is needed
}