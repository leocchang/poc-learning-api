/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.constraint.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.aktana.learning.api.constraint.ValidDateString;
import com.aktana.learning.common.util.datetime.PureDate;

/**
 * Implementation of the ValidDateString annotation. 
 * Checks that the String contains a valid date, in Aktana PureDate format.
 */
public class DateStringValidator implements ConstraintValidator<ValidDateString, String> {
	
	public void initialize(ValidDateString constraintAnnotation) {
	}

	public boolean isValid(String yyyy_mm_dd,
			ConstraintValidatorContext constraintContext) {
		
			// check that the date string is valid by attempt to construct a PureDate from it
			PureDate date = null;
			try {
				date = new PureDate(yyyy_mm_dd);
			}
			catch(Exception e) {
			}
			return date != null;
	}

}
