/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.constraint.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.aktana.learning.api.constraint.ValidPeriodDTO;
import com.aktana.learning.api.dto.IPeriodDTO;
import com.aktana.learning.common.util.datetime.PureDate;

/**
 * Implementation of the ValidPeriodDTO annotation.
 * Checks that the IPeriodDTO object's getStartDate() is <= getEndDate().
 * Returns false IFF both dates are valid, but endDate < startDate.
 */
public class PeriodDTOValidator implements ConstraintValidator<ValidPeriodDTO, IPeriodDTO> {

	@Override
	public void initialize(ValidPeriodDTO constraintAnnotation) {
	}

	@Override
	public boolean isValid(IPeriodDTO periodDTO, ConstraintValidatorContext context) {
		PureDate startDate;
		PureDate endDate;
		try {
			startDate = new PureDate(periodDTO.getStartDate());
			endDate = new PureDate(periodDTO.getEndDate());
		}
		catch(Exception e) {
			// Note: Other validations will already have been performed on the IPeriodDTO
			// object's startDate and endDate.  Therefore, if either date is not valid, 
			// return true here to avoid triggering an unnecessary and confusing error message 
			// about startDate must be <= endDate
			return true;
		}
		return startDate.daysBetween(endDate) >= 0;
	}

}
