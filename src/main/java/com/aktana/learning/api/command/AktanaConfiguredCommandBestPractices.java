package com.aktana.learning.api.command;

import java.util.Map.Entry;
import java.util.TreeMap;

import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;

import com.aktana.learning.api.AktanaService;
import com.aktana.learning.api.AktanaServiceConfiguration;
import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.AktanaServiceModule;
import com.aktana.learning.common.config.AktanaTimezoneConfiguration;
import com.aktana.learning.common.util.deployment.ManifestInfo;
import com.aktana.learning.common.util.deployment.ManifestUtility;
import com.codahale.metrics.MetricRegistry;
import com.google.inject.Injector;
import com.hubspot.dropwizard.guice.GuiceBundle;

public class AktanaConfiguredCommandBestPractices {

	private GuiceBundle  guiceBundle = null;
	
	private SessionFactory sessionFactory = null;
	
	/**
	 * This method performs several DropWizard ConfiguredCommand "best practices", 
	 * that help to ensure that the calling ConfiguredCommand sub-class behaves
	 * equally-well in both "normal" and "bootstrap" mode.
	 * 
	 * This method should be called from the Aktana ConfiguredCommand sub-class's 
	 * overridden run(Bootstrap<AktanaServiceConfiguration>,Namespace,AktanaServiceConfiguration) method,
	 * and this should be done before attempting any application operations.
	 * 
	 * @param bootstrap
	 * @param namespace
	 * @param configuration
	 */
    protected void initRun(
    		Bootstrap<AktanaServiceConfiguration> bootstrap,
			Namespace namespace, 
			AktanaServiceConfiguration configuration) {    	
        
    	String command = namespace.getString("command");
    	TreeMap<String,Object> commandLineArgs = new TreeMap<String,Object>();
    	for (Entry<String,Object> entry : namespace.getAttrs().entrySet()) {
    		String argName = entry.getKey();
    		Object argValue = entry.getValue();
    		if (argValue != null) {
    			commandLineArgs.put(argName, argValue);
    		}
    	}
        System.out.println("\n****************************** "+command+" **********************************");
        System.out.println("*** Customer Env:  "+configuration.getTheCustomerEnvironmentName());
        System.out.println("*** database Url:  "+configuration.getDatabaseConfiguration().getUrl());
        System.out.println("*** Command line:  "+commandLineArgs);
        ManifestInfo manifestInfo = ManifestUtility.getInfoFromManifest(AktanaService.class);
        if (manifestInfo != null) {
        	System.out.println("*** "+manifestInfo.getImplementationTitle());
        	System.out.println("*** "+manifestInfo.getImplementationVersion());
        	System.out.println("*** "+manifestInfo.getSrcRevision());
        	System.out.println("*** "+manifestInfo.getSrcBranch());
        }
       System.out.println("*******************************************************************************\n");

    	AktanaTimezoneConfiguration.setUTC();
    	
        AktanaService.getInstance().setConfiguration(configuration);
        
        guiceBundle = GuiceBundle.<AktanaServiceConfiguration>newBuilder()
      	      .addModule(new AktanaServiceModule())
      	      .setConfigClass(AktanaServiceConfiguration.class)
      	      .enableAutoConfig("com.aktana.api.command", "com.aktana.api.resources", "com.aktana.api.managed")
      	      .build();

       bootstrap.addBundle(guiceBundle);        
        
        
       // bootstrap.addBundle(AktanaService.getInstance().getHibernateBundle());
        
        MetricRegistry registry = new MetricRegistry();

        Environment environment = new Environment("AktanaServiceCommand",  bootstrap.getObjectMapper().copy(), null, registry, this.getClass().getClassLoader());
        
        try {
			bootstrap.run(configuration, environment);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //SessionFactory sessionFactory = AktanaService.getInstance().getHibernateBundle().getSessionFactory();
        //AktanaServiceDaos daos = AktanaServiceDaos.getInstance();
        //daos.setSessionFactory(sessionFactory); //sets the session factory to all DAOs
        // should be injected by GUICE
        
        Injector injector = guiceBundle.getInjector();
        
        sessionFactory = injector.getInstance(SessionFactory.class);
        
        //SessionFactory sessionFactory = AktanaServiceDaos.getInstance().getSessionFactory();
        
        Session session = sessionFactory.openSession();
        ManagedSessionContext.bind(session);    	
    }
        
    public SessionFactory getSessionFactory() {
    	return sessionFactory;
    }
    
    public GuiceBundle getGuiceBundle() {
    	return guiceBundle;
    }
 
}
