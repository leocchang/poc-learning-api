package com.aktana.learning.api.command;

import java.io.FileReader;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;

import com.aktana.learning.api.AktanaServiceConfiguration;
import com.aktana.learning.api.resource.HibernateSessionHelper;
import com.aktana.learning.common.util.sql.ScriptRunner;

public class AktanaCustomizeCommand extends ConfiguredCommand<AktanaServiceConfiguration> {
	 private static final Logger LOGGER = LoggerFactory.getLogger(AktanaCustomizeCommand.class);
	 private static String CUSTOM_DEPLOY_SCRIPT = "upgradeCustomMetadata.sql";
	 
	protected AktanaCustomizeCommand(String name, String description) {
		super(name, description);
	}

    public AktanaCustomizeCommand() {
        super("customizedb", "Aktana Customize DB");
    }

    
    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);        
    }	    

    
    private String findDeploymentScript(String prefix) {
    	// first look for the dbscript file in classpath
    	String filename = "/" + prefix + "/customization/generated/custom/" + CUSTOM_DEPLOY_SCRIPT; //@SPECIAL KNOWLEDGE: path
    	try {
    		URL url = AktanaCustomizeCommand.class.getResource(filename);
    		if (url==null || url.getPath()==null || url.getPath().trim().isEmpty()) {
        		LOGGER.info("Filename not found in classpath: " + filename);
    			return null;
    		} else {
	    		String fullPath = url.getPath();
	    		return fullPath;
    		}    		
    	} catch (Exception e) {
    		LOGGER.info("Filename not found in classpath: " + filename);
    	}    	
    	
    	return null;
    }
	
	private void runScript(SessionFactory factory, final String scriptFile) throws Exception
	{
		HibernateSessionHelper helper = null;		
		helper = new HibernateSessionHelper(factory);
		final Session session = helper.getSessionFactory().openSession();
	    try
	    {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);

	        session.doWork(new Work() {
	                 @Override
                     public void execute(Connection conn) throws SQLException {
			        	  ScriptRunner scrun = new ScriptRunner(conn, true, true);
			        	  Reader reader;
						  try {
								reader = new FileReader(scriptFile);

					        	scrun.runScript(reader);

						  } catch (Exception e) {
								e.printStackTrace();
								throw new SQLException("Error executing script: "  + scriptFile);
						 }
			        	  
                     }
	              });
	        	  helper.commitTransaction(session);        		
	       }
	       catch(HibernateException e)
	       {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
	       }
	       finally
	       {
	            session.close();
	            ManagedSessionContext.unbind(helper.getSessionFactory());
	            helper=null;
	       }            
   }		

    
	@Override
	protected void run(Bootstrap<AktanaServiceConfiguration> bootstrap,
			Namespace namespace, AktanaServiceConfiguration configuration)
			throws Exception {
		// TODO Auto-generated method stub
		// Perform DropWizard ConfiguredCommand "best practices", in a standard manner for 
    	// and AktanaService-registered ConfiguredCommand.
    	AktanaConfiguredCommandBestPractices bestPractices = new AktanaConfiguredCommandBestPractices();
     	bestPractices.initRun(bootstrap, namespace, configuration);     	
     	String prefix = configuration.getTheCustomerEnvironmentName().toLowerCase();
     	SessionFactory factory = bestPractices.getSessionFactory(); 	
	}

}
