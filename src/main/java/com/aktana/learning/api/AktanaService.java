/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api;

import com.aktana.learning.common.config.AktanaTimezoneConfiguration;    
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.filter.CorsHeadersFilter;
import com.aktana.learning.common.learningS3.LearningS3Connector;
import com.aktana.learning.common.rundeck.RundeckConnector;
import com.aktana.learning.common.swagger.AktanaApiFilterImpl;

//import com.aktana.service.pub.resources.ActionResource;

import com.aktana.learning.common.util.deployment.ManifestInfo;
import com.aktana.learning.common.util.deployment.ManifestUtility;
import com.aktana.learning.api.bo.ApiInfoBo;
import com.aktana.learning.api.bo.special.SysParameterBo;
import com.aktana.learning.api.command.AktanaCustomizeCommand;
import com.aktana.learning.api.health.PullServicesHealthChecker;
import com.aktana.learning.api.health.PushServicesHealthChecker;
import com.aktana.learning.api.resource.HibernateSessionHelper;
import com.aktana.learning.api.resource.LearningBuildResource;
import com.aktana.learning.api.resource.LearningConfigResource;
import com.aktana.learning.api.resource.LearningFileResource;
import com.aktana.learning.api.resource.LearningParamResource;
import com.aktana.learning.api.resource.LearningRunResource;
import com.aktana.learning.api.resource.LearningVersionResource;
import com.aktana.learning.api.resource.ApiInfoResource;
import com.aktana.learning.api.resource.DSEResource;
import com.aktana.learning.api.resource.S3Resource;
import com.aktana.learning.api.resource.LearningSimulationResource;
import com.aktana.learning.api.resource.LearningObjectListResource;
import com.aktana.learning.api.resource.PingResource;
import com.aktana.learning.api.resource.RepEngagementCalculationResource;
import com.aktana.learning.api.resource.AktanaUserResource;
import com.aktana.learning.api.resource.OptimalLearningParamsResource;
import com.aktana.learning.api.resource.CustomerConfigurationsResource;

import com.aktana.learning.api.resource.TokenGeneratorResource;

import com.aktana.learning.api.resource.exceptionmapper.AktanaExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.GenericExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.HibernateConstraintViolationExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.InvalidEntityExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.JsonMappingExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.JsonParseExceptionMapper;

import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.google.common.collect.ImmutableList;
import com.hubspot.dropwizard.guice.GuiceBundle;
import javax.ws.rs.client.Client;
import org.glassfish.jersey.server.ResourceConfig;
import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.FilterFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider;
import com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON;
import com.wordnik.swagger.jaxrs.listing.ResourceListingProvider;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.reader.ClassReaders;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BooleanConverter;
import org.apache.commons.beanutils.converters.DoubleConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.StringConverter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.servlet.DispatcherType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.ExceptionMapper;

import java.net.URI;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import com.roskart.dropwizard.jaxws.BasicAuthentication;
import com.roskart.dropwizard.jaxws.EndpointBuilder;
import com.roskart.dropwizard.jaxws.JAXWSBundle;
import com.aktana.learning.api.resource.soap.*;
import com.aktana.learning.api.security.AktanaApiAuthFeature;
import com.aktana.learning.api.security.AktanaApiBasicAuthenticator;


/** Service Implementation based on v2 */
public class AktanaService extends Application<AktanaServiceConfiguration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaService.class);

    private static AktanaService _instance;

    private AktanaServiceConfiguration _configuration;

    private GuiceBundle<AktanaServiceConfiguration> guiceBundle;    
    
    public static AktanaService getInstance()
    {
        if (_instance == null)
            _instance = new AktanaService();

        return _instance;
    }


    private AktanaService()
    {

    }

    public AktanaServiceConfiguration getConfiguration()
    {
        return _configuration;
    }


    public void setConfiguration(AktanaServiceConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    


	// JAX-WS Bundle
    private JAXWSBundle jaxWsBundle = new JAXWSBundle("/api/soap");	//@SPECIAL KNOWLEDGE
    
    
    //private final AktanaSoapBundle soapBundle = new AktanaSoapBundle();

    @Override
    public void initialize(Bootstrap<AktanaServiceConfiguration> bootstrap) {

        //bootstrap.setName("AktanaService");

        // set JRE, java, and joda default timezones all to UTC
        AktanaTimezoneConfiguration.setUTC();

		// swagger-ui to view the apis
        bootstrap.addBundle(new AssetsBundle("/assets/swagger-ui", "/api-browser", "index.html"));	//@SPECIAL KNOWLEDGE

        bootstrap.addBundle(new MigrationsBundle<AktanaServiceConfiguration>() {
			@Override
			public DataSourceFactory getDataSourceFactory(
					AktanaServiceConfiguration configuration) {
				return configuration.getDatabaseConfiguration();
			}
        });
 
        guiceBundle = GuiceBundle.<AktanaServiceConfiguration>newBuilder()
        	      .addModule(new AktanaServiceModule())
        	      .setConfigClass(AktanaServiceConfiguration.class)
        	      .enableAutoConfig("com.aktana.learning.api.command", "com.aktana.learning.api.resources", "com.aktana.learning.api.managed")		//@SPECIAL KNOWLEDGE
        	      .build();

        bootstrap.addBundle(guiceBundle);        

        // moved to AkatanServiceModule
        //bootstrap.addBundle(hibernateBundle);

        // bootstrap.addBundle(soapBundle);
        bootstrap.addBundle(jaxWsBundle);
        
        // bootstrap.addBundle(new ViewBundle());

        bootstrap.addCommand(new AktanaCustomizeCommand());	//@SPECIAL KNOWLEDGE
        
        //TODO: Do we need log bundle?
    }

    //public HibernateBundle<AktanaServiceConfiguration> getHibernateBundle() {
    //    return this.hibernateBundle;
    //}
        
    @Override
    public void run(AktanaServiceConfiguration configuration, Environment environment) throws Exception {
    	
        //Set  configuration to singleton instance
        setConfiguration(configuration);
          
        environment.jersey().setUrlPattern("/api/*");

        //uncomment log incoming requests and responses
        //environment.jersey().property(ResourceConfig.PROPERTY_CONTAINER_REQUEST_FILTERS, LoggingFilter.class.getName());
        //environment.jersey().property(ResourceConfig.PROPERTY_CONTAINER_RESPONSE_FILTERS, LoggingFilter.class.getName());        
        
        //Set date format to ISO8601
        environment.getObjectMapper().setDateFormat(new ISO8601DateFormat());

        // make BeanUtils set the default to be null when the value is null
        ConvertUtils.register(new IntegerConverter(null), Integer.class);
        ConvertUtils.register(new DoubleConverter(null), Double.class);
        ConvertUtils.register(new BooleanConverter(null), Boolean.class);
        ConvertUtils.register(new StringConverter(null), String.class);
        
        //Set session factory for generic dao
        // SessionFactory in AktanaServiceDaos is set by Guice - AktanaServiceModule
        // This is done to maintain backward compatibility. 
        //
        // AktanaServiceDaos daos = AktanaServiceDaos.getInstance();
        // daos.setSessionFactory(hibernateBundle.getSessionFactory());

         //initialize metadata
         initializeMetadata();

        // construct urls for the pull services
        String pullservicesUrl = configuration.getPullservicesUrl();
        URI baseURI = getBaseURI(pullservicesUrl);
 
        // lookup information about the AktanaService release from the .JAR manifest
        // Note that manifestInfo will be null if AktanaService is being run from local /build.
        ManifestInfo manifestInfo = ManifestUtility.getInfoFromManifest(AktanaService.class);
        
        // Update AktanaDeploymentHistory if this is a new deployment
        boolean isNewVersion = recordDeploymentIfNewVersion(manifestInfo);
        
        System.out.println("\n****************************** AktanaService **********************************");
        System.out.println("*** Customer Env:  "+configuration.getTheCustomerEnvironmentName());
        System.out.println("*** Deployment Env:  "+configuration.getDeploymentEnvironment());
        System.out.println("*** Database Url:  "+configuration.getDatabaseConfiguration().getUrl());
        System.out.println("*** DSE Service Url:  "+configuration.getDSEServiceConfiguration().getUrl());
        System.out.println("*** Rundeck Service Url:  "+configuration.getRundeckServiceConfiguration().getUrl());
        System.out.println("*** LearningS3 bucket:  "+configuration.getLearningS3ServiceConfiguration().getBucketName());
        System.out.println("*** API baseURI :  "+baseURI);
        if (isNewVersion) {
        	System.out.println("*** This is a new deployment of:");
        	System.out.println("*** \t"+manifestInfo.getImplementationTitle());
        	System.out.println("*** \t"+manifestInfo.getImplementationVersion());
        	System.out.println("*** \t"+manifestInfo.getSrcRevision());
        	System.out.println("*** \t"+manifestInfo.getSrcBranch());
        }
        System.out.println("*******************************************************************************\n");
       
        //create Jersey client for the pull services
        final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
                .using(environment)
                .build("jersey");

        // initialize DSE connection
        DSEConnector.getInstance().initializeConnection(configuration.getDSEServiceConfiguration());
        
        // initialize Rundeck connection
        RundeckConnector.getInstance().initializeConnection(configuration.getRundeckServiceConfiguration());
               
        // initialize LearningS3 connection
        LearningS3Connector.getInstance().initializeConnection(configuration.getLearningS3ServiceConfiguration());
        
    	//@SPECIAL KNOWLEDGE: below is where all SOAP services are built/registered
        // jaxWsBundle.publishEndpoint(
        //        new EndpointBuilder("/simple", new SimpleService()));        
        AktanaApiBasicAuthenticator   aktanaBasicApiAuthenticator = new AktanaApiBasicAuthenticator();   
        BasicAuthentication jaxwsAuthenticator = new BasicAuthentication(aktanaBasicApiAuthenticator, "AktanaRealm");

        jaxWsBundle.publishEndpoint(new EndpointBuilder("/v1.0/token", new TokenService()));        
   
       // environment.jersey().register(new AuthDynamicFeature(
       //        new OAuthCredentialAuthFilter.Builder<UserPrincipalDTO>()
        //                .setAuthenticator(new AktanaApiAuthenticator())
       //                 .setPrefix("Bearer")
       //                 .buildAuthFilter()));
        // environment.jersey().register(new AuthValueFactoryProvider.Binder<>(UserPrincipalDTO.class));
        // AktanaApiAuthenticator aktanaAuthenticator = new AktanaApiAuthenticator();    	
        // environment.jersey().register(new OAuthProvider<UserPrincipalDTO>(aktanaAuthenticator, "restricted"));
        
        environment.jersey().register(new AktanaApiAuthFeature());
        
        //Add all resources here		//@SPECIAL KNOWLEDGE: below is where all end-points are registered
        environment.jersey().register(new ApiInfoResource());
        environment.jersey().register(new PingResource());
               
        environment.jersey().register(new TokenGeneratorResource());
        environment.jersey().register(new AktanaUserResource());
        
        environment.jersey().register(new LearningConfigResource());
        environment.jersey().register(new LearningVersionResource());
        environment.jersey().register(new LearningParamResource());
        environment.jersey().register(new LearningBuildResource());
        environment.jersey().register(new LearningRunResource());
        environment.jersey().register(new LearningSimulationResource());
        environment.jersey().register(new LearningObjectListResource());
        environment.jersey().register(new LearningFileResource());
        environment.jersey().register(new DSEResource());
        environment.jersey().register(new S3Resource());
        environment.jersey().register(new OptimalLearningParamsResource());
        environment.jersey().register(new RepEngagementCalculationResource());
        environment.jersey().register(new CustomerConfigurationsResource());

        
        configureExceptionMappers(environment);
        
        // Swagger Resource
        environment.jersey().register(new ApiListingResourceJSON());

        // Swagger providers
        environment.jersey().register(new ApiDeclarationProvider());
        environment.jersey().register(new ResourceListingProvider());

        environment.healthChecks().register("pushservices", (HealthCheck) new PushServicesHealthChecker("pushservices"));
        environment.healthChecks().register("pullservices", (HealthCheck) new PullServicesHealthChecker("pullservices", client, baseURI));

        // Swagger Scanner, which finds all the resources for @Api Annotations
        ScannerFactory.setScanner(new DefaultJaxrsScanner());

        // Add the reader, which scans the resources and extracts the resource information
        ClassReaders.setReader(new DefaultJaxrsApiReader());

        FilterFactory.setFilter(new AktanaApiFilterImpl());
        
        // Set the swagger config options
        SwaggerConfig config = ConfigFactory.config();
        config.setApiVersion("1.0.1");

		// for testing using swagger-ui
        config.setBasePath("/");      

        // CORS filter
        // TODO: Useful for development and to be able to use swagger-ui but not sure if someone has a strong objection against leaving it here
        environment.servlets().addFilter("CorsHeadersFilter", new CorsHeadersFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }

    public static void main(String[] args) throws Exception {
        AktanaService.getInstance().run(args);
    }

    /**
     * Loads all models from <code>com.aktana.persistence.v2.model</code> package which will be returned
     * in a collections of classes. It collects ALL Entity classes only.
     *
     * This method should be used to create the <code>HibernateBundle</code>.
     *
     * @return A collection of Entity Classes.
     */
    public static ImmutableList<Class<?>> getHibernateMappings() {
        Reflections reflections = new Reflections("com.aktana.learning.persistence.models");	//@SPECIAL KNOWLEDGE
        Set<Class<? extends Object>> allClasses = reflections.getTypesAnnotatedWith(Entity.class);
        
        Reflections reflections2 = new Reflections("com.aktana.learning.persistence");	//@SPECIAL KNOWLEDGE
        Set<Class<? extends Object>> daoClasses = reflections2.getTypesAnnotatedWith(MappedSuperclass.class);
        allClasses.addAll(daoClasses);

        return ImmutableList.copyOf(allClasses);
    }


    /**
     * Method to remove all default exception mappers and register our own methods
     * @param environment
     * @param jerseyFilter
     */
    private void configureExceptionMappers(Environment environment) {
        ResourceConfig jrConfig = environment.jersey().getResourceConfig();

        Set<Object> dwSingletons = jrConfig.getSingletons();
        List<Object> singletonsToRemove = new ArrayList<Object>();

        for (Object s : dwSingletons) {
            if (s instanceof ExceptionMapper && s.getClass().getName().startsWith("io.dropwizard.jersey.")) {	//@SPECIAL KNOWLEDGE
                singletonsToRemove.add(s);
            }
        }

        for (Object s : singletonsToRemove) {
            jrConfig.getSingletons().remove(s);
        }
        
        environment.jersey().register(new GenericExceptionMapper());
        environment.jersey().register(new AktanaExceptionMapper());
        environment.jersey().register(new InvalidEntityExceptionMapper());
        environment.jersey().register(new JsonParseExceptionMapper());
        environment.jersey().register(new JsonMappingExceptionMapper());
        environment.jersey().register(new HibernateConstraintViolationExceptionMapper());
    }


    private URI getBaseURI(String url) {
        return UriBuilder.fromUri(url).build();
    }

    
    /*
     * If manifestInfo is not null, and if its srcVersion and/or srcBranchSimple
     * are different from the most-recently recorded deployment,
     * then insert a new row into AktanaDeploymentHistory to record the
     * new srcVersion and srcBranchSimple, and return true.
     * 
     * Otherwise return false.
     */
	private boolean recordDeploymentIfNewVersion(ManifestInfo manifestInfo) {
		boolean isNewVersion = false;
		if (manifestInfo != null) {

			HibernateSessionHelper helper = null;
			SessionFactory factory = guiceBundle.getInjector().getInstance(
					SessionFactory.class);
			helper = new HibernateSessionHelper(factory);
			final Session session = helper.getSessionFactory().openSession();
			try {
				helper.configureSession(session);
				ManagedSessionContext.bind(session);
				helper.beginTransaction(session);

				try {
					ApiInfoBo apiInfoBo = new ApiInfoBo();
					isNewVersion = apiInfoBo.recordDeploymentIfNew(
							manifestInfo.getSrcRevision(),
							manifestInfo.getSrcBranchSimple());

					helper.commitTransaction(session);
				} catch (Exception e) {
					helper.rollbackTransaction(session);
					helper.<RuntimeException> rethrow(e);
				}
			} finally {
				session.close();
				ManagedSessionContext.unbind(helper.getSessionFactory());
				helper = null;
			}
		}
		return isNewVersion;
	}
	   
		
	
	private void initializeMetadata() {
		HibernateSessionHelper helper = null;		
		SessionFactory factory = guiceBundle.getInjector().getInstance(SessionFactory.class);		
		helper = new HibernateSessionHelper(factory);
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
            
            try {
            	SysParameterBo.refreshCache(true);
            	//SEConfiguratorBo.initializeMetadata();
         		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	} 
	


}
