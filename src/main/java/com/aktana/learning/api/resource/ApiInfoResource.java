/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.io.File; 
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.aktana.learning.api.bo.ApiInfoBo;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.aktana.learning.api.AktanaService;
import com.aktana.learning.api.TheCustomerEnvironmentName;
import com.aktana.learning.api.bo.special.SysParameterBo;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.common.rundeck.RundeckConnectionException;
import com.aktana.learning.common.rundeck.RundeckConnector;
import com.aktana.learning.common.util.AktanaParameter;
import com.aktana.learning.common.util.deployment.ManifestInfo;
import com.aktana.learning.common.util.deployment.ManifestUtility;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import io.dropwizard.hibernate.UnitOfWork;

import com.aktana.learning.common.learningS3.LearningS3ConnectionException;
import com.aktana.learning.common.learningS3.LearningS3Connector;

/** Build Status Information based on v2 implementation */
@Path("/info")
@Produces(MediaType.TEXT_HTML)
@Api(value = "/v1.0/info", description = "Show Environment Information", position=3)
public class ApiInfoResource {

	private ApiInfoBo apiInfoBo = new ApiInfoBo();
	

    @GET
    @UnitOfWork
	@ApiOperation(value = "Show LearningService Basic Information", notes = "Show LearningService Basic Information", response = String.class,position=10)    
    public String buildInfo(			
    		@ApiParam(name="p", value="persist sysParameter values if/as needed elsewhere (default=false)", required=false)
    		@QueryParam("p") Boolean doRepersistAsNeeded,
    		@ApiParam(name="v", value="verbose output (default=false)", required=false)
    		@QueryParam("v") Boolean verboseMode
    		) {
    	
    	String databaseURL = AktanaService.getInstance().getConfiguration().getDatabaseConfiguration().getUrl();
    	
    	// This is ensure database is operating OK, and to trigger refreshing of cached SysParameters.
    	try {
    		SysParameterBo.refreshCache(doRepersistAsNeeded);   		
    	}
    	catch (Exception e) {
    		throw new GenericValidationException("Database is not accessible: "+databaseURL);	    								
    	}


        StringBuilder output = new StringBuilder("<html>");
        output.append("<head><style>table, td, th { border: 1px solid #ddd; text-align: center;}table {border-collapse: collapse;}th, td {padding: 2px;}</style></head>");
        output.append("<body>");

    	String customerDefaultTimezoneId = SysParameterBo.getStringValue("defaultTimezoneId");
    	String nowInCustomerDefaultTimezoneString = null;
    	if (customerDefaultTimezoneId==null) customerDefaultTimezoneId="UTC";
		try {
			DateTimeZone customerDateTimeZone = DateTimeZone.forID(customerDefaultTimezoneId);
			DateTime nowUTC = new DateTime();
	    	DateTime nowInCustomerDefaultTimezone = nowUTC.withZone(customerDateTimeZone);
	    	nowInCustomerDefaultTimezoneString = nowInCustomerDefaultTimezone.toString("h:mm a, EEEEEEEEE MMM dd, yyyy")
	    			+" ("+customerDefaultTimezoneId+")<br>";
		}
		catch (Exception e) {
		}
     	
    	// machine
    	String computername = "Unknown";
		try {
			computername = InetAddress.getLocalHost().getHostName();
		} 
			catch (UnknownHostException e1) {
		}

    	output.append("<h2>LearningService ").append(computername).append("/api/info -- "+nowInCustomerDefaultTimezoneString+"</h2>");

       	// theCustomerEnvironmentName
    	String theCustomerEnvironmentName = TheCustomerEnvironmentName.getName();
    	output.append("The configured customer environment name: <strong>"+theCustomerEnvironmentName+"</strong><br>");
 
      	// database in use
    	output.append("<br>Database connection:&nbsp&nbsp&nbsp<strong>"+databaseURL+"</strong>");
     	
    	// Branch/commit version   	
    	try {
            ManifestInfo manifestInfo = ManifestUtility.getInfoFromManifest(AktanaService.class);
            if (manifestInfo != null) {
            	output.append("<br>");
				output.append("SRC Revision: <strong>"+manifestInfo.getSrcRevision()+"</strong><br>");
				output.append("SRC Branch&nbsp&nbsp: <strong>"+manifestInfo.getSrcBranch()+"</strong><br>");		            	            	
				output.append("Impl Title&nbsp&nbsp: "+manifestInfo.getImplementationTitle()+"<br>");
				output.append("Impl Version: "+manifestInfo.getImplementationVersion()+"<br>");	            		            	
            }
    	}
    	catch (Exception e) {
    		output.append("Problem getting manifest info<br>");
    	}
    	
    	// Last Database migration
     	String lastMigrationInfo = apiInfoBo.getMostRecentLiquibaseMigrationInfo();
    	if (lastMigrationInfo != null) {
    	  	output.append("<br>Last db migration: "+lastMigrationInfo+"<br>");
    	}

    	// Recent Deployments history
    	int numDeploymentsToList = 5;
    	List<String> recentDeployments = apiInfoBo.getMostRecentDeploymentsInfo(numDeploymentsToList);
    	if (recentDeployments!=null && recentDeployments.size()>0) {
    	  	output.append("Last "+numDeploymentsToList+" deployments:");
    	  	for (String deploymentInfo : recentDeployments) {
    	  	  	output.append("<br><span style=\"margin-left:5em\">"+deploymentInfo+"</span>");
    	  	}
    	  	output.append("<br>");
    	}
        
    	String runningPath = "";
    	File runningPathFile = null;
    	try {    		
    		String path = System.getProperty("java.class.path");
    		String[] patharr = path.split(System.getProperty("path.separator"));
    		   		
    		runningPathFile = new File(patharr[0]);
    		runningPath = runningPathFile.getAbsolutePath();
    		    		
        	output.append("<br>Path: "+runningPath+"<br>");
		} catch (Exception e) {
			output.append("<br>Problem getting source control info from "+e.getMessage()+"<br>");
		}
    	
    	// JVM Memory usage
    	long totalMemoryBytes = Runtime.getRuntime().totalMemory();
    	long maxMemoryBytes = Runtime.getRuntime().maxMemory();
    	long freeMemoryBytes = Runtime.getRuntime().freeMemory();
    	long bytesPerMB = 1048576;
    	output.append("JVM memory, in MB:"
    			+"  Total="+(totalMemoryBytes/bytesPerMB)
    			+"  Free="+(freeMemoryBytes/bytesPerMB)
    			+"  Max="+(maxMemoryBytes/bytesPerMB)
    			+"<br><br>");
    	
    	// learning container
    	String failRed = "<span style=\"color:red;\">FAIL</span>";
    	String okGreen = "<span style=\"color:green;\">OK</span>";
    	
    	String s3Live = failRed;
		String bucketName = LearningS3Connector.getInstance().getConfig().getBucketName();
    	try {
    		s3Live = LearningS3Connector.getInstance().callTestS3().equals("1")
    				? okGreen + " Bucket: " + bucketName + " exists" : failRed + " Bucket: " + bucketName + " does no exist" ;
    		System.out.println(LearningS3Connector.getInstance().callTestS3());
    	} catch (LearningS3ConnectionException e) {
    		s3Live = failRed+" error "+ e.getClass()+": "+e.getMessage();		
    	}
    	output.append("<br /><b>Learning S3</b><br />&nbsp;url=").append("<br />&nbsp;health="+s3Live).append("<br />");

    	// DSE service ping
    	String ping = failRed;
    	try {
    		ping = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_PING);
    		ping = (ping != null && ping.contains("OK")?okGreen:(failRed+": "+ping));
		} catch (DSEConnectionException e) {
			ping = failRed+" error: "+e.getMessage();	 
		} catch (Exception e) {
			ping = failRed+" error "+ e.getClass()+": "+e.getMessage();		
		}
    	
    	// DSE service info
    	String info = failRed;
    	String infoContent = "";
    	try {
    		infoContent = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_INFO);
    		info = okGreen;
		} catch (DSEConnectionException e) {
			info = failRed;
			infoContent = e.getMessage();	 
		} catch (Exception e) {
			info = failRed;
			infoContent =  e.getClass()+": "+e.getMessage();	
		}
    	
    	//String toggle = "function toggleDseInfoDiv() { var x = document.getElementById(\"dseInfoDiv\"); if (x.style.display === \"none\") { x.style.display = \"block\"; } else { x.style.display =\"none\"; }}";
    	
    	//output.append(toggle+"<br><h2>DSEService ping=").append(ping).append("</h2><span onclick=\"toggleDseInfoDiv()\">DSE Info...</span>").append("<div id=\"dseInfoDiv\">"+info+"</div>");
    	output.append("<br /><b>DSE API</b><br />&nbsp;url=").append(DSEConnector.getInstance().getConfig().getUrl()).append("<br />&nbsp;ping=").append(ping).append("<br/>&nbsp;info=").append(info).append("<br /><div id=\"dseInfoDiv\" style=\"border-style: double; overflow: auto; max-height: 100px; \">"+infoContent+"</div>");
    	
    	
    	// RUndeck  info
    	String rdinfo = failRed;
    	try {
    		rdinfo = RundeckConnector.getInstance().callGet(RundeckAPIEnum.RUNDECK_API_INFO);
    		rdinfo =  okGreen+": "+rdinfo;
		} catch (RundeckConnectionException e) {
			rdinfo = failRed+": "+e.getMessage();	 
		} catch (Exception e) {
			rdinfo = failRed+" "+ e.getClass()+": "+e.getMessage();	
		}
    	output.append("<br/><b>Rundeck API</b><br />&nbsp;url=").append(RundeckConnector.getInstance().getConfig().getUrl());
    	
    	Map<String, String> pathMap = RundeckConnector.getInstance().getConfig().getPaths();
    	output.append("<br />&nbsp;jobs=");
    	for(String key: pathMap.keySet()) {
    		output.append("<br />&nbsp;&nbsp;&nbsp;").append(key).append("=").append(pathMap.get(key));
    	}
    	output.append("<br />&nbsp;info="+rdinfo).append("<br/><br />");
    	
     	// SysParameters
    	output.append("All SysParameters:<ul>");
    	List<AktanaParameter> allSysParams = SysParameterBo.getAllParamsLike(".*");
    	Collections.sort(allSysParams);
        for (AktanaParameter dseArgParam : allSysParams) {
        	String name = dseArgParam.getName();
        	Object value = dseArgParam.getValue();
        	// hide the value of "apitokenSecret" param
        	if (name.equals("apitokenSecret")) {
        		if (value.equals("AktanaSecret!"))
        			value = "<i>(Aktana standard)</i>";
        		else 
           			value = "<i>(Customer-specific value)</i>";
        	}
            output.append("<li>").append(name).append(" = <b>").append(value);
            if (!dseArgParam.isRegistered()) {
            	output.append("\t\t<font color=\"red\">(Not a current/registered sysParameter)</font>");
            }
            output.append("</b>");
        }
       	output.append("</ul>");
   	


    	output.append("</body></html>");

    	return output.toString();
	}
    
}