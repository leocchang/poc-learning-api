/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningBuildBo;
import com.aktana.learning.api.bo.LearningRunBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.LearningRunDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningRun;


/** LearningRun Resource */
@Path("/v1.0/LearningRun")
@Api(value = "/v1.0/LearningRun", description = "Operations on LearningRun",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningRunResource extends AbstractResourceSupport<LearningRunDTO, LearningRun, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningRunResource.class);

    /**
     * @param type
     */
    public LearningRunResource() {
        super(LearningRunResource.class, LearningRunDTO.class, (Class) LearningRunBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningRuns", notes = "List all the LearningRuns", response = LearningRunDTO.class, responseContainer = "List",position=10)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		List<LearningRunDTO> list = listObjects();

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningRunUID}")
	@ApiOperation(value = "Read LearningBuild", notes = "Lookup an LearningRun", response = LearningRunDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 404, message = "LearningBuild does not exist", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningRunUID", value="unique id for the LearningRun", required=true)
			@PathParam("learningRunUID") String id
			) {
		LearningRunDTO acct = readObject(id);

		String json = toJSONString(acct);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
/*
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
	@ApiOperation(value = "Create LearningBuild", notes = "Create a new LearningBuild", response = LearningRunDTO.class,position=20)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response saveLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		@ApiParam(name="newObj", value="new LearningBuild to create", required=true)
		@Valid LearningRunDTO newObj) {

		LearningRunDTO resp = saveObject(newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @UnitOfWork
    @Timed(name = "put-requests")
    @Path("/{learningBuildUID}")
	@ApiOperation(value = "Update LearningBuild", notes = "Update an LearningBuild", response = LearningRunDTO.class,position=40)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
	public Response updateLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true)
			@PathParam("learningBuildUID") String id,
			@ApiParam(name = "obj", value = "LearningBuild to update", required = true)
			@Valid LearningRunDTO obj) {

		validateObjectUID(id, obj);

		LearningRunDTO resp = updateObject(id, obj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

    }
    */



  /*
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningBuildUID}")
	@ApiOperation(value = "Delete LearningBuild", notes = "Delete an LearningBuild", response = LearningRunDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true)
			@PathParam("learningBuildUID") String id) {

		LearningRunDTO resp = removeObject(id);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    */
    
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningRunUID}")
	@ApiOperation(value = "Delete LearningRun", notes = "Delete a LearningRun", response = LearningRunDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningRun(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningRunUID", value = "unique id for the LearningRun", required = true) 
			@PathParam("learningRunUID") String id) {

		//LearningRunDTO resp = removeObject(id);

    	LearningRunBo bo = (LearningRunBo) this.createBoInstance();

    	
    	LearningRunDTO resp = bo.deleteLearningRunWithUIDwrapper(id);
    	
		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningRunUID}/file")
	@ApiOperation(value = "Get list of files of the run", notes = "Get LearningFiles from LearningRun", response = LearningFileInfoDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getRunFiles(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningRunUID", value = "unique id for the LearningRun", required = true) @PathParam("learningRunUID") String learningRunUID) {

		LearningRunBo bo = (LearningRunBo) this.createBoInstance();

		List<LearningFileInfoDTO> respList = bo.getFiles(learningRunUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningRunUID}/score")
	@ApiOperation(value = "Get scores of the run", notes = "Get LearningScoresDTO from LearningRun", response = LearningScoreDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getRunScores(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningRunUID", value = "unique id for the LearningRun", required = true) @PathParam("learningRunUID") String learningRunUID,
			@ApiParam(name = "page", value = "Page number", required = false) @QueryParam("page") Integer page,
			@ApiParam(name = "rows", value = "Number of rows", required = false) @QueryParam("rows") Integer rows) {

		LearningRunBo bo = (LearningRunBo) this.createBoInstance();

		List<LearningScoreDTO> respList = bo.getScores(learningRunUID, page, rows);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}
	
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningRunUID}/accounts/{accountUID}/score")
	@ApiOperation(value = "Get scores of the run for an account", notes = "Get LearningScoresDTO from LearningRun", response = LearningScoreDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getAccountRunScores(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningRunUID", value = "unique id for the LearningRun", required = true) @PathParam("learningRunUID") String learningRunUID,
			@ApiParam(name = "accountUID", value = "unique id for an account", required = true) @PathParam("accountUID") String accountUID) {

		LearningRunBo bo = (LearningRunBo) this.createBoInstance();
		List<LearningScoreDTO> respList = bo.getAccountScores(learningRunUID, accountUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}
	
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningRunUID}/reps/{repUID}/score")
	@ApiOperation(value = "Get scores of the run for a Rep", notes = "Get LearningScoresDTO from LearningRun", response = LearningScoreDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getRepRunScores(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningRunUID", value = "unique id for the LearningRun", required = true) @PathParam("learningRunUID") String learningRunUID,
			@ApiParam(name = "repUID", value = "unique id for a rep", required = true) @PathParam("repUID") String repUID,
			@ApiParam(name = "page", value = "Page number", required = false) @QueryParam("page") Integer page,
			@ApiParam(name = "rows", value = "Number of rows", required = false) @QueryParam("rows") Integer rows) {

		LearningRunBo bo = (LearningRunBo) this.createBoInstance();

		List<LearningScoreDTO> respList = bo.getRepScores(learningRunUID, repUID, page, rows);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}


}
