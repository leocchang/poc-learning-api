package com.aktana.learning.api.resource;

import com.aktana.learning.api.dto.SimpleTokenRequestDTO;
import com.aktana.learning.api.security.AktanaAccessToken;
import com.aktana.learning.api.security.AktanaAimPermissionEnum;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.useradmin.AktanaUserDTO;
import com.aktana.learning.api.dto.useradmin.LoginDTO;
import com.aktana.learning.api.security.TokenGenerationHelper;
import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import java.util.Set;


/**
 * This whole endpoint should be removed once the UI is fully integrated with AIM.
 */

@Path("/v1.0/User")
@Api(value = "/v1.0/User", description = "Operations on Aktana User", position=10)
public class AktanaUserResource  {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaUserResource.class);
    
 
    private TokenGenerationHelper helper;
    
    public AktanaUserResource() {
    	 helper = new TokenGenerationHelper();
    }

    
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Login", notes = "Login", response = LoginDTO.class,position=10)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})    
    public final Response login(@ApiParam(name="loginRequest", value="parameters required to login", required=true) @Valid SimpleTokenRequestDTO tokenRequest)
    {
        LOGGER.info("Verifying user access...");

        AktanaAccessToken token = helper.validateAimRequest(tokenRequest);
		AktanaUserDTO aktanaUserDTO = helper.getUserFromToken(token);
        
        
        LoginDTO dto = new LoginDTO();
    	dto.setResult("User not found");
    	dto.setSessionId("");
    	dto.setToken("");
    	dto.setUserId("");
    	dto.setUserName("");
    	dto.setUserDisplayName("");
    	if (aktanaUserDTO != null) {
    	    LOGGER.info(" Login successful, starting session for user {}", tokenRequest.getUsername());

    	    dto.setResult("Success");
    	    dto.setToken(token.getToken());
    	    dto.setUserId(aktanaUserDTO.getUserId());
    	    dto.setUserName(aktanaUserDTO.getUserName());
    	    dto.setEmail(aktanaUserDTO.getEmail());
    	    dto.setUserDisplayName(aktanaUserDTO.getUserDisplayName());
    	    dto.setRoleId(getRoleId(aktanaUserDTO.getAimPermissions()));
    	    dto.setAimPermissions(aktanaUserDTO.getAimPermissions());
    	    dto.setRefreshToken(token.getRefreshToken());
    	}
		return Response.status(HttpStatus.OK_200).entity(dto).build();
    }


    // As this roleId is temporary before UI fully integrate with AIM. We simply hard code 0-3 so that we can remove AktanaUserRoleEnum.
    private String getRoleId (Set<String> permissions) {
    	if (permissions == null) {
    		return "0";
		}
    	return "0";
	}
    
}
