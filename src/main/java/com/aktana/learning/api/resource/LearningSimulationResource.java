/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningSimulationBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.MessageInfoDTO;
import com.aktana.learning.api.dto.SimulationSegmentDTO;
import com.aktana.learning.api.dto.SimulationAccountSegmentDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.SimulationSegmentSequenceInfoDTO;
import com.aktana.learning.api.dto.SimulationAccountSequenceInfoDTO;
import com.aktana.learning.api.dto.SimulationAccountDTO;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

//import com.aktana.learning.persistence.models.impl.LearningRun; 
import com.aktana.learning.persistence.models.impl.SimulationSegment; 


/** LearningSimulation Resource */
@Path("/v1.0/LearningSimulation")
@Api(value = "/v1.0/LearningSimulation", description = "Operations on LearningSimulation",position=130) 
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningSimulationResource extends AbstractResourceSupport<SimulationSegmentDTO, SimulationSegment, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningSimulationResource.class);

    /**
     * @param type
     */
    public LearningSimulationResource() {
        super(LearningSimulationResource.class, SimulationSegmentDTO.class, (Class) LearningSimulationBo.class);
    }
   
    
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/segment")
	@ApiOperation(value = "Get list of segments", notes = "List all the segments per learningBuildUID", response = SimulationSegmentDTO.class, responseContainer = "List",position=20)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response listSegmentPerBuildUID(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID) {
    	    	
    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    	
    	List<SimulationSegmentDTO> list = bo.listSegmentsWithBuildId(learningBuildUID);
 
		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }  
     
    
   
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/segment/{segmentUID}")
	@ApiOperation(value = "Get segment", notes = "List all the segments per learningBuildUID segmentUID pair", response = SimulationSegmentDTO.class,position=30)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response getSegmentPerBuildUIDperSegmentUID(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID,
    	@ApiParam(name = "segmentUID", value = "unique id for the segment", required = true) @PathParam("segmentUID") String segmentUID) {

    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
		
		SimulationSegmentDTO dto = bo.getSegmentWithBuildIdAndSegmentId(learningBuildUID, segmentUID);	
		
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
    
    
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/segment/{segmentUID}/accounts")
	@ApiOperation(value = "Get segment", notes = "List all the segments per learningBuildUID segmentUID pair", response = SimulationAccountSegmentDTO.class, responseContainer = "List",position=40)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response listAccountSegmentPerBuildUIDperSegmentUID_(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID,
    	@ApiParam(name = "segmentUID", value = "unique id for the segment", required = true) @PathParam("segmentUID") String segmentUID,
		@ApiParam(name = "searchString", value = "partial string search for accountName") @QueryParam("searchString") String searchString,    	
		@ApiParam(name = "page", value = "Page number (default is 1)") @QueryParam("page") Integer page,
		@ApiParam(name = "rows", value = "Number of rows (default is 100)") @QueryParam("rows") Integer rows){
    	
    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    	
		List<SimulationAccountSegmentDTO> list = bo.listAccountSegmentsWithBuildIdAndSegmentId(learningBuildUID, segmentUID, searchString, page, rows);
    	
		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
   
    
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/account/{accountUID}/sequence")
	@ApiOperation(value = "Get sequence", notes = "get sequence level message data per learningBuildUID accountUID pair", response = SimulationAccountSequenceInfoDTO.class, responseContainer = "List",position=50)
    @ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response getSequencePerBuildUIDperAccountUID_(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID,
    	@ApiParam(name = "accountUID", value = "unique id for the account", required = true) @PathParam("accountUID") String accountUID) {

    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    			    	
		SimulationAccountSequenceInfoDTO simulationAccountSequenceInfoDTO = bo.listSequenceWithBuildIdAndAccountId(learningBuildUID, accountUID);
		
		String json = toJSONString(simulationAccountSequenceInfoDTO);
		
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/message/{messageUID}/messageInfo")
	@ApiOperation(value = "Get sequence", notes = "get message information from messageUID", response = MessageInfoDTO.class, responseContainer = "List",position=50)
    @ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response getMessageInfoPerMessageUID(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "messageUID", value = "unique id for the message", required = true) @PathParam("messageUID") String messageUID) {

    		LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    			    	
		MessageInfoDTO messageInfoDTO = bo.getMessageInfo(messageUID);
		
		String json = toJSONString(messageInfoDTO);
		
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/account/{accountUID}/message/{messageUID}/messageInfo")
	@ApiOperation(value = "Get sequence", notes = "get message information from messageUID", response = MessageInfoDTO.class, responseContainer = "List",position=50)
    @ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response getMessageInfoPerMessageUIDPerAccountUID(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    	@ApiParam(name = "accountUID", value = "unique id for the accountUID", required = true) @PathParam("accountUID") String accountUID,
    	@ApiParam(name = "messageUID", value = "unique id for the message", required = true) @PathParam("messageUID") String messageUID) {

    		LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    			    	
		MessageInfoDTO messageInfoDTO = bo.getMessageInfoPerAccount(messageUID, accountUID);
		
		String json = toJSONString(messageInfoDTO);
		
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 

    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/segment/{segmentUID}/sequence")
	@ApiOperation(value = "Get sequence", notes = "get sequence level data per learningBuildUID segmentUID pair", response = SimulationSegmentSequenceInfoDTO.class, responseContainer = "List",position=60)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response getSequencePerBuildUIDperSegmentUID_(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID,
    	@ApiParam(name = "segmentUID", value = "unique id for the segment", required = true) @PathParam("segmentUID") String segmentUID) {

    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
		
		SimulationSegmentSequenceInfoDTO simulationSegmentSequenceInfoDTO = bo.listSequenceWithBuildIdAndSegmentId(learningBuildUID, segmentUID);
		String json = toJSONString(simulationSegmentSequenceInfoDTO);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
    
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}/accounts")
	@ApiOperation(value = "Get accounts", notes = "List all the accounts per learningBuildUID", response = SimulationAccountDTO.class, responseContainer = "List",position=70)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
    public Response listAccountsPerBuildUIDperSegmentUID_(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
    	@ApiParam(name = "learningBuildUID", value = "unique id for the learning build", required = true) @PathParam("learningBuildUID") String learningBuildUID,
		@ApiParam(name = "searchString", value = "partial string search for accountName") @QueryParam("searchString") String searchString,    	
		@ApiParam(name = "page", value = "Page number (default is 1)") @QueryParam("page") Integer page,
		@ApiParam(name = "rows", value = "Number of rows (default is 100)") @QueryParam("rows") Integer rows){
    	
    	LearningSimulationBo bo = (LearningSimulationBo) this.createBoInstance();
    	
		List<SimulationAccountDTO> list = bo.listAccountsWithBuildUID(learningBuildUID, searchString, page, rows);
    	
		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    } 
    
}
