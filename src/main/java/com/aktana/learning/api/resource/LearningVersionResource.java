/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;  

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningSimulationBo;
import com.aktana.learning.api.bo.LearningVersionBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningVersionDTO;
import com.aktana.learning.api.dto.LearningVersionInfoDTO;
import com.aktana.learning.api.dto.AggRepDateLocationAccuracyDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningVersion;


/** LearningVersion Resource */
@Path("/v1.0/LearningVersion")
@Api(value = "/v1.0/LearningVersion", description = "Operations on LearningVersion",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningVersionResource extends AbstractResourceSupport<LearningVersionInfoDTO, LearningVersion, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningVersionResource.class);

    /**
     * @param type
     */
    public LearningVersionResource() {
        super(LearningVersionResource.class, LearningVersionInfoDTO.class, (Class) LearningVersionBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningVersions", notes = "List all the LearningVersions", response = LearningVersionInfoDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		List<LearningVersionInfoDTO> list = listObjects();

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningVersionUID}")
	@ApiOperation(value = "Read LearningVersion", notes = "Lookup an LearningVersion", response = LearningVersionInfoDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 404, message = "LearningVersion does not exist", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningVersion(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningVersionUID", value="unique id for the LearningVersion", required=true)
			@PathParam("learningVersionUID") String id
			) {
		LearningVersionInfoDTO acct = readObject(id);

		String json = toJSONString(acct);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    /*
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
	@ApiOperation(value = "Create LearningVersion", notes = "Create a new LearningVersion", response = LearningVersionInfoDTO.class,position=20)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response saveLearningVersion(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		@ApiParam(name="newObj", value="new LearningVersion to create", required=true)
		@Valid LearningVersionDTO newObj) {

		LearningVersionInfoDTO resp = saveObject(newObj.createLearningVersionInfoDTO());

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
*/
   
    
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningVersionUID}")
	@ApiOperation(value = "Delete LearningVersion", notes = "Delete an LearningVersion", response = LearningVersionInfoDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningVersion(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) 
			@PathParam("learningVersionUID") String id) {

    	
    	LearningVersionInfoDTO resp = removeObject(id);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
    
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningVersionUID}/anchorAggScores")
	@ApiOperation(value = "Lookup an agg scores for an anchor version", notes = "Lookup an agg scores for an anchor version", response = AggRepDateLocationAccuracyDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 404, message = "LearningVersion does not exist", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningVersionAggScores(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningVersionUID", value="unique id for the LearningVersion", required=true)
			@PathParam("learningVersionUID") String learningVersionUID) {
		
    	LearningVersionBo bo = new LearningVersionBo();
    	
    	AggRepDateLocationAccuracyDTO dto = bo.getAnchorAggScoresByVersion(learningVersionUID);
    	
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
    
   
}
