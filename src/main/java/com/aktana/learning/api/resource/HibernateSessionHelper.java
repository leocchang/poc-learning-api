package com.aktana.learning.api.resource;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

public class HibernateSessionHelper {
    private final SessionFactory sessionFactory;
    private boolean transactional = true;
    private boolean readonly = false;
    private CacheMode cachemode = CacheMode.NORMAL;
    private FlushMode flushmode = FlushMode.AUTO;


	public HibernateSessionHelper(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;    	        
    }

    public boolean isTransactional() {
		return transactional;
	}

	public void setTransactional(boolean transactional) {
		this.transactional = transactional;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	public CacheMode getCachemode() {
		return cachemode;
	}

	public void setCachemode(CacheMode cachemode) {
		this.cachemode = cachemode;
	}

	public FlushMode getFlushmode() {
		return flushmode;
	}

	public void setFlushmode(FlushMode flushmode) {
		this.flushmode = flushmode;
	}


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    
    public boolean hasBind() {
        return ManagedSessionContext.hasBind(getSessionFactory());    	
    }

    public void unbind() {
        ManagedSessionContext.unbind(getSessionFactory());    	
    }
    
    public void bind(Session session) {
    	ManagedSessionContext.bind(session);
    }
    
    /*    
    public void invoke(Object resource, String methodName, Object[] params) {
    	Method m = resource.getClass().getMethod(methodName, parameterTypes)
        final Session session = sessionFactory.openSession();
        try {
            configureSession(session);
            ManagedSessionContext.bind(session);
            beginTransaction(session);
            try {
                dispatcher.dispatch(resource, context);
                commitTransaction(session);
            } catch (Exception e) {
                rollbackTransaction(session);
                this.<RuntimeException>rethrow(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }
    */
    
    
    /* we need an invoke method with reflection to avoid repeating code
    public void dispatch(Object resource, HttpContext context) {
        final Session session = sessionFactory.openSession();
        try {
            configureSession(session);
            ManagedSessionContext.bind(session);
            beginTransaction(session);
            try {
                dispatcher.dispatch(resource, context);
                commitTransaction(session);
            } catch (Exception e) {
                rollbackTransaction(session);
                this.<RuntimeException>rethrow(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }
    */

    public void beginTransaction(Session session) {
        if (transactional) {
            session.beginTransaction();
        }
    }

    public Session openSession() {
    	return sessionFactory.openSession();
    }

    public Session openConfigureBindSession() {
    	Session session = sessionFactory.openSession();
    	configureSession(session);
    	bind(session);
    	return session;
    }

    
    public void closeUnbindSession(Session session) {
        session.close();
        ManagedSessionContext.unbind(sessionFactory);
    }
    
    public void configureSession(Session session) {
        session.setDefaultReadOnly(readonly);
        session.setCacheMode(cachemode);
        session.setFlushMode(flushmode);        
    }

    public void rollbackTransaction(Session session) {
        if (transactional) {
            final Transaction txn = session.getTransaction();
            if (txn != null && txn.isActive()) {
                txn.rollback();
            }
        }
    }

    public void commitTransaction(Session session) {
        if (transactional) {
            final Transaction txn = session.getTransaction();
            if (txn != null && txn.isActive()) {
                txn.commit();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <E extends Exception> void rethrow(Exception e) throws E {
        throw (E) e;
    }
}