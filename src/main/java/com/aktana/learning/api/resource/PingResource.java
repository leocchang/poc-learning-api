/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.aktana.learning.api.AktanaService;
import com.aktana.learning.api.bo.special.SysParameterBo;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.learningS3.LearningS3ConnectionException;
import com.aktana.learning.common.learningS3.LearningS3Connector;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.common.rundeck.RundeckConnector;

import javax.ws.rs.client.Client;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import io.dropwizard.hibernate.UnitOfWork;

/** Build Status Information based on v2 implementation */
@Path("/ping")
@Produces(MediaType.TEXT_HTML)
@Api(value = "/v1.0/ping", description = "Ping", position=3)
public class PingResource {

    public PingResource() {	
    }


    @GET
    @UnitOfWork
	@ApiOperation(value = "Ping", notes = "Ping", response = String.class,position=10)    
    public String buildInfo() {
    	
    	String databaseURL = AktanaService.getInstance().getConfiguration().getDatabaseConfiguration().getUrl();
    	
    	// This is ensure database is operating OK
    	try {
    		SysParameterBo.getStringValue("apitokenSecret");
    	}
    	catch (Exception e) {
    		throw new GenericValidationException("Database is not accessible: "+databaseURL);	    								
    	}

    	try {
    		RundeckConnector.getInstance().callGet(RundeckAPIEnum.RUNDECK_API_INFO);
		} catch (Exception e) {
			throw new GenericValidationException("Rundeck API is not accessible: "+RundeckConnector.getInstance().getConfig().getUrl()); 
		}
    	
    	try {
    		if (!LearningS3Connector.getInstance().callTestS3().equals("1")) {
    			throw new GenericValidationException("Bucket: %s does not exist", LearningS3Connector.getInstance().getConfig().getBucketName());
    		}		
    	} catch (LearningS3ConnectionException e) {
    		throw new GenericValidationException(e.getMessage());	
    	}
    	
    	try {
    		DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_PING);
		} catch (Exception e) {
			throw new GenericValidationException("DSE API is not accessible: "+DSEConnector.getInstance().getConfig().getUrl()); 
		}
    	

        StringBuilder output = new StringBuilder("<html>");
        output.append("<head></head>");
        output.append("<body>");
        output.append("OK");
    	output.append("</body></html>");

    	return output.toString();
	}
    

  

}