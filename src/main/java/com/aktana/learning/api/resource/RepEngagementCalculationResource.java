/*****************************************************************
*
* @author $Author: adiel.cohen $
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.persistence.models.impl.OptimalLearningParams;
import com.aktana.learning.persistence.models.impl.RepEngagementCalculation;
import com.aktana.learning.api.bo.RepEngagementCalculationBo;
import com.aktana.learning.api.dto.RepEngagementCalculationDTO;
import com.aktana.learning.api.dto.RepEngagementDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.EngagementFiltersForDSESettingsDTO;
import com.aktana.learning.api.dto.LearningConfigDraftDTO;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

//import com.aktana.learning.persistence.models.impl.LearningRun; 


/** RepEngagement Calculation Resource*/
@Path("/v1.0/RepEngagementCalculation")
@Api(value = "/v1.0/RepEngagementCalculation", description = "Operations on RepEngagement Calculation",position=200) 
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class RepEngagementCalculationResource extends AbstractResourceSupport<RepEngagementCalculationDTO, RepEngagementCalculation, String> {

   private static final Logger LOGGER = LoggerFactory.getLogger(RepEngagementCalculationResource.class);

   /**
    * @param type
    */
   public RepEngagementCalculationResource() {
       super(RepEngagementCalculationResource.class, RepEngagementCalculationDTO.class, (Class) RepEngagementCalculationBo.class);
   }
  
   
   @GET
   @UnitOfWork
   @Timed(name = "get-requests")
   @Path("/{configId}/info")
	@ApiOperation(value = "Get list of RepEngagement Params", notes = "List all the RepEngagement Params repTeams, territories, Years", response = RepEngagementCalculationDTO.class, responseContainer = "List",position=10)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
   public Response listRepEngagementCalculationParams(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		   @ApiParam(name = "configId", value = "unique id for the config", required = true) @PathParam("configId") String configId) {
   	    	
	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();
	   RepEngagementCalculationDTO list = bo.getRepEngagementCalculationParams(configId);

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }  
    
   @GET
   @UnitOfWork
   @Timed(name = "get-requests")
   @Path("/{configId}/calculation")
	@ApiOperation(value = "Get Reps performance", notes = "List all reps performance", response = RepEngagementDTO.class, responseContainer = "List",position=20)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
   public Response listRepsPerformance(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
	@ApiParam(name = "configId", value = "unique id for the config", required = true) @PathParam("configId") String configId,
	@ApiParam(name = "repTeamUID", value = "unique id for the repTeamUID", required = false) @QueryParam("repTeamUID") String repTeamUID,
	@ApiParam(name = "startYear", value = "start year", required = false) @QueryParam("startYear") String startYear,
	@ApiParam(name = "endYear", value = "end year", required = false) @QueryParam("endYear") String endYear,
	@ApiParam(name = "startMonth", value = "start Month", required = false) @QueryParam("startMonth") String startMonth,
	@ApiParam(name = "endMonth", value = "end Month", required = false) @QueryParam("endMonth") String endMonth,
	@ApiParam(name = "territoryUID", value = "territoryUID", required = false) @QueryParam("territoryUID") String territoryUID,
	@ApiParam(name = "suggestionType", value = "suggestionType", required = false) @QueryParam("suggestionType") String suggestionType){
   	    	
	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();
	   List<RepEngagementDTO> list = bo.getRepPerformance(configId, repTeamUID, startYear, startMonth, endYear, endMonth, territoryUID, suggestionType);

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }
   
   @POST
   @UnitOfWork
   @Timed(name = "post-requests")
   @Path("/engagementFiltersForDSESettings")
   @ApiOperation(value = "add or update EngagementFiltersForDSESettings", notes = "Post a set of filters for a dseConfig, suggestionType pair", response = EngagementFiltersForDSESettingsDTO.class,position=30)
   @ApiResponses(value = {
		   @ApiResponse(code = 200, message = "Successfully processed"),
		   @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
		   @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
   })
   public Response postFilters(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
		   @ApiParam(name = "newObj", value = "new LearningConfig to create", required = true) @Valid EngagementFiltersForDSESettingsDTO newObj) {

	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();	   
	   EngagementFiltersForDSESettingsDTO dto = bo.addEngagementFiltersForDSESettings(newObj);
	   String json = toJSONString(dto);
	   return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }
   
   @GET
   @UnitOfWork
   @Timed(name = "get-requests")
   @Path("/engagementFiltersForDSESettings")
   @ApiOperation(value = "Get EngagementFiltersForDSESettings", notes = "Get a set of filters for a dseConfig, suggestionType pair", response = EngagementFiltersForDSESettingsDTO.class, responseContainer = "List", position=40)
   @ApiResponses(value = {
		   @ApiResponse(code = 200, message = "Successfully processed"),
		   @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
		   @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
   })
   public Response getAllFilters(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {

	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();
	   List<EngagementFiltersForDSESettingsDTO> dto = bo.getAllEngagementFiltersForDSESettings();
	   String json = toJSONString(dto);

	   return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }
   
   @GET
   @UnitOfWork
   @Timed(name = "get-requests")
   @Path("/{seConfigId}/suggestionType/{suggestionType}/engagementFiltersForDSESettings")
   @ApiOperation(value = "Get EngagementFiltersForDSESettings", notes = "Get a set of filters for a dseConfig, suggestionType pair", response = EngagementFiltersForDSESettingsDTO.class,position=50)
   @ApiResponses(value = {
		   @ApiResponse(code = 200, message = "Successfully processed"),
		   @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
		   @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
   })
   public Response getFilters(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
		   @ApiParam(name = "seConfigId", value = "unique id for the config", required = true) @PathParam("seConfigId") String seConfigId,
		   @ApiParam(name = "suggestionType", value = "suggestionType", required = true) @PathParam("suggestionType") String suggestionType) {

	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();
	   EngagementFiltersForDSESettingsDTO dto = bo.getEngagementFiltersForDSESettingsDTO(Integer.parseInt(seConfigId), suggestionType);
	   String json = toJSONString(dto);

	   return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }
   
   
   @DELETE
   @UnitOfWork
   @Timed(name = "delete-requests")
   @Path("/{seConfigId}/suggestionType/{suggestionType}/engagementFiltersForDSESettings")
   @ApiOperation(value = "Delete EngagementFiltersForDSESettings", notes = "Delete a set of filters for a dseConfig, suggestionType pair", response = EngagementFiltersForDSESettingsDTO.class,position=60)
   @ApiResponses(value = {
		   @ApiResponse(code = 200, message = "Successfully processed"),
		   @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
		   @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
   })
   public Response deleteFilters(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
		   @ApiParam(name = "seConfigId", value = "unique id for the config", required = true) @PathParam("seConfigId") String seConfigId,
		   @ApiParam(name = "suggestionType", value = "suggestionType", required = true) @PathParam("suggestionType") String suggestionType) {

	   RepEngagementCalculationBo bo = (RepEngagementCalculationBo) this.createBoInstance();
	   EngagementFiltersForDSESettingsDTO dto = bo.deleteEngagementFiltersForDSESettingsDTO(Integer.parseInt(seConfigId), suggestionType);
	   String json = toJSONString(dto);
	   return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }
  
}


