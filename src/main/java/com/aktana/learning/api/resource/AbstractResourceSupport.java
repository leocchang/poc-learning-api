/*****************************************************************
 * 
 * AbstractResourceSupport<DTO, M, PK> is a base implementation for all resources. 
 * 
 * The AbstractResourceSupport is Generic implementation based on
 * <ul>
 * <li><DTO> (Data Transfer Object) the DTO Class of the corresponding resource representation </li>
 * <li><M> the Class of the corresponding Hibernate "model" (aka POJO) object</li>
 * <li><PK> the Class of the corresponding Hibernate "models" primary key</li>
 * </ul>
 * 
 * The base class provides the following capabilities that can be used or derived by the children. Basic CRUD
 * 
 * <ul>
 * 		<li>
 * 			CORE Methods listObjects, searchObjects, readObject, saveObject, updateObject, removeObject
 * 		</li>
 *		<li>
 * 			Support Methods to createBoInstance, initializeFixture, readObjectFixture, readFromFileAsString, validateObjectUID, validateStringsEqual
 * 		</li>
 * </ul>
 * 
 * <p>
 * The core methods delegate the implementation to a BusinessObject(Bo) that is registered with this resource at the time of construction OR 
 * responds back with a fixture (JSON) that is registered at the time of Construction.
 * </p>
 * 
 * <p>
 * For thread safety the Bo instances are created in each method instance prior to invoking them
 * </p>
 * 
 * <p>
 * The AbstractResourSupport is always derived and the relevant core methods are overridden to provide swagger documentation of each of the 
 * resource method.
 * </p>
 * 
 * <p>
 * The asbtract resource performs common validations that cross-cut across resource implementations. For example this validates that the
 * objectUID passed in the update method is the same as the objectUID in the ExternallyIdentifiableDTO interface. 
 * </p>
 * 
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *
 *****************************************************************/

package com.aktana.learning.api.resource;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;  
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.AbstractBo;
import com.aktana.learning.api.dto.AktanaFixtureDTO;
import com.aktana.learning.api.dto.AktanaSyncDTO;
import com.aktana.learning.api.dto.AktanaSyncResponseDTO;
import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.security.AktanaApiBasicAuthenticator;
import com.aktana.learning.common.exception.GenericValidationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;


/** Base class for all resources. Provides common functionality used by REST Resource.
 * 
 * DTO = the Class of the corresponding resource representation DTO  
 * M = the Class of the corresponding Hibernate "model" (aka POJO) object
 * PK = the Class of the corresponding Hibernate model object's Primary Key
 */
public abstract class AbstractResourceSupport<DTO, M, PK extends Serializable> {

	public static final String API_VERSION_1_0 = "v1.0";
	public static final String API_VERSION_3_0 = "v3.0";
	
	/** Location of fixtures used for samples */
    private final String fixtureLocation = "/samples/";

	/** Class type. */
	private final Class<?> type;

	/** DTO for resource representation class */
    private final Class<DTO> dtoClazz;

	/** Simple Class Name of the DTO class that represents the resource, minus the final "DTO" */
    private final String dtoName;

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** ObjectMapper user for JSON conversion */
    private ObjectMapper mapper;

	/** Fixture class that represents a JSON fixture to serve */
    private AktanaFixtureDTO<DTO> fixture;
        
    /** Business Object to interact with*/
    private Class<AbstractBo<DTO, M, PK>> boClazz;
    
    /** Authenticator to use for SOAP services - to support tokens passed as params */
    private Authenticator<BasicCredentials, UserPrincipalDTO>   aktanaBasicApiAuthenticator = new AktanaApiBasicAuthenticator();   

    // @Context
    private UriInfo uriInfo;
    
    /** soap does not seem to load uriInfo */
    private String apiVersion = null; 

    
    /**
     * Sole constructor for resources
     * 
     * @param type is the actual derived resource implementation class
     * @param dtoClazz is the class of the resource representation DTO 
     * @param boClazz is the class of the BO implementation to which the resource delegates actions
     */
	public AbstractResourceSupport(Class<?> type, Class<DTO> dtoClazz, Class<AbstractBo<DTO, M, PK>> boClazz) {
		// derived implementation type
		this.type = type;

		//  resource implementaion class
		this.dtoClazz = dtoClazz;

		// remove the last three characters (DTO) of the class (for example if ProductDTO, then it becomes Product)
		this.dtoName = this.dtoClazz.getSimpleName().substring(0, this.dtoClazz.getSimpleName().length()-3);

		// logger created for the derived class
		LOGGER = LoggerFactory.getLogger(this.type);

		// initialize the mapper to use for converting object to JSON
		mapper = new ObjectMapper();
		
		// read the fixtur (a JSON file) based on the dtoName and fixtureLocation and store in fixture
		fixture = initializeFixture();
		
		// Business Object (Bo) will be created using this prototype class and delegated to
		this.boClazz = boClazz;		
		
		
	}

	
	/*
	 * return the version of the API being invoked
	 * 
	 * currently - it can return v1.0 or v3.0 or unknown
	 */
	protected String getApiVersion() {		
		String version = "unknown";		
		if (uriInfo != null) {
			String basePath = uriInfo.getPath();
			String[] pathparts = basePath.split("/");
			
			if (pathparts.length > 0) {
				version = pathparts[0];
			} 			
		} else {
			if(apiVersion != null) {
				version = this.apiVersion;
			}
		}
				
		return version;		
	}
	
	/*
	 * set the version of API being invoked
	 * 
	 * a fix for userInfo not loading for SOAP
	 */
	protected void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	


	/**
	 * create an instance of boClazz for use by resource methods for thread safety
	 * 
	 * @return a Bo Implementation
	 */
	protected AbstractBo<DTO, M, PK> createBoInstance() {
		if (boClazz != null) {
			try {				
				AbstractBo<DTO, M, PK> instance = boClazz.newInstance();				
				instance.setApiVersion(getApiVersion());				
				return instance;
			} catch (InstantiationException e) {
				LOGGER.error("Unable to create BO instance", e);
			} catch (IllegalAccessException e) {
				LOGGER.error("Unable to create BO instance", e);				
			}
		} 		
		
		return null;
	}



	/**
	 * Core REST endpoint method for listing DTO objects.
	 *  
	 * @return a list of DTO objects.
	 */	
	protected List<DTO> listObjects() {
		List<DTO> list = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();
				
		if (bo == null || bo.getIsParameterAPIMock()) {
			list = fixture.getList();
		} else {
			list = bo.listLimitedObjects();
		}
		return list;
	}
	
	/**
	 * Core REST endpoint method for listing all DTO objects.
	 *  
	 * @return a list of DTO objects.
	 */	
	protected List<DTO> listAllObjects() {
		List<DTO> list = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();
				
		if (bo == null || bo.getIsParameterAPIMock()) {
			list = fixture.getList();
		} else {
			list = bo.listAllObjects();
		}
		return list;
	}
		
	
	
	/**
	 * Core Endpoint method for listing DTO objects. With support for transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected List<DTO> listObjectsWithTM(HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		List<DTO> list =  null;        		
            
            try {
            	list = listObjects();            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return list;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	
	
	/**
	 * Core REST endpoint method for listing DTO objects in a sync format used by client.
	 *  
	 * @return a list of DTO objects.
	 */	
	protected AktanaSyncDTO<DTO> synclistObjects() {
		List<DTO> list = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();
		
		if (bo == null || bo.getIsParameterAPIMock()) {
			list = fixture.getList();
		} else {
			// maximum is 10000 objects that can be synced
			list = bo.searchObjects(0, 10000, new ArrayList<String>(), new HashMap<String, Object>());
		}
		
		AktanaSyncDTO<DTO> dto = new AktanaSyncDTO<DTO>();		
		dto.setList(list);
		dto.setDelete(new ArrayList<String>());
		
		return dto;
	}
	
	
	
	/**
	 * Core Endpoint method for listing DTO objects. With support for transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected AktanaSyncDTO<DTO> synclistObjectsWithTM(HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		AktanaSyncDTO<DTO> list =  null;        		
            
            try {
            	list = synclistObjects();            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return list;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	

	
	/**
	 * Core REST endpoint method for searching DTO objects.
	 *  
	 * @return a list of DTO objects.
	 */	
	protected List<DTO> searchObjects(int offset, int limit, List<String> order_by, Map<String,Object> filters) {
		List<DTO> list = null;
		AbstractBo<DTO, M, PK> bo = createBoInstance();
		
		if (bo == null || bo.getIsParameterAPIMock()) {
			list = fixture.getList();
		} else {
			list = bo.searchObjects(offset, limit, order_by, filters);
		}
		return list;
	}

	
	/**
	 * Core Endpoint method for searching DTO objects. With support for manual transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected List<DTO> searchObjectsWithTM(int offset, int limit, List<String> order_by, Map<String,Object> filters, HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		List<DTO> list =  null;        		
            
            try {
            	list = searchObjects(offset, limit, order_by, filters);            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return list;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	
	
    protected Map<String, Object> constructFieldFilter(Map<String, Object> fieldMap, String fieldName, Object filter) {
    	
    	if (filter != null) {
    		fieldMap.put(fieldName, filter);
    	}

    	return fieldMap;
    }
	
	/**
	 * Core REST endpoint method for reading DTO object.
	 * 
	 * @param id objectUID to read
	 * 
	 * @return DTO object
	 */
	protected DTO readObject(String id) {
		DTO obj = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();
		
		if (bo == null || bo.getIsParameterAPIMock()) {
			obj = fixture.getRead();
		} else {
			obj = bo.readObject(id);
		}
		return obj;
	}
	
	
	
	
	


	/**
	 * Core Endpoint method for reading DTO objects. With support for manual transaction management. Required for SOAP endpoints
	 *  
	 * @return a DTO object.
	 */	
	protected DTO readObjectWithTM(String id, HibernateSessionHelper helper) {		
		// use default values
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		DTO obj =  null;        		
            
            try {
            	obj = readObject(id);            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return obj;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	
	
	/**
	 * Core REST endpoint method for saving an object.
	 * 
	 * @param obj object to save
	 * @return saved object
	 */
	protected DTO saveObject(DTO obj) {
		DTO resp = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();

		if (bo == null || bo.getIsParameterAPIMock()) {
			resp = fixture.getSave();
		} else {
			resp = bo.saveObject(obj);
		}
		return resp;
	}

	
	/**
	 * Core Endpoint method for saving DTO objects. With support for manual transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected DTO saveObjectWithTM(DTO obj, HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		DTO retobj =  null;        		
            
            try {
            	retobj = saveObject(obj);            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return retobj;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}

	
	
	/**
	 * Core REST endpoint method for syncing an object.
	 * 
	 * @param obj object to save
	 * @return saved object
	 */
	protected AktanaSyncResponseDTO syncObject(AktanaSyncDTO<DTO> syncobj) {
		AktanaSyncResponseDTO resp = null;

		AbstractBo<DTO, M, PK> bo = createBoInstance();

		if (bo == null || bo.getIsParameterAPIMock()) {
			resp = new AktanaSyncResponseDTO();
			resp.setStatus(1);
			resp.setMessage(null);
			resp.setUpdates(0);
			resp.setDeletes(0);
		} else {
			resp = bo.syncObject(syncobj);
		}
		
		return resp;
	}
	
		
	
	
	/**
	 * Core REST endpoint method for updating an object corresponding to the id
	 * 
	 * @param id objectUID of the object
	 * @param obj object to update
	 * @return updated object
	 */
	protected DTO updateObject(String id, DTO obj) {
		DTO resp = null;
		
		AbstractBo<DTO, M, PK> bo = createBoInstance();
		
		if (bo == null || bo.getIsParameterAPIMock()) {
			resp = fixture.getUpdate();
		} else {
			resp = bo.updateObject(id, obj);
		}
		return resp;
	}


	/**
	 * Core Endpoint method for updating DTO objects. With support for manual transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected DTO updateObjectWithTM(String id, DTO obj, HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		DTO retobj =  null;        		
            
            try {
            	retobj = updateObject(id, obj);            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return retobj;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	
	
	
	/**
	 * Core REST endpoint method for removing DTO object.
	 * 
	 * @param id objectUID to remove
	 * 
	 * @return DTO object
	 */
	protected DTO removeObject(String id) {
		DTO resp = null;
		
		AbstractBo<DTO, M, PK> bo = createBoInstance();
		
		if (bo == null || bo.getIsParameterAPIMock()) {
			resp = fixture.getDelete();
		} else {
			resp = bo.removeObject(id);
		}
		return resp;
	}

	
	
	/**
	 * Core Endpoint method for removing DTO objects. With support for manual transaction management. Required for SOAP endpoints
	 *  
	 * @return a list of DTO objects.
	 */	
	protected DTO removeObjectWithTM(String id, HibernateSessionHelper helper) {
		if (helper == null) {
			helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		}
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
    		DTO retobj =  null;        		
            
            try {
            	retobj = removeObject(id);            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return retobj;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
	}
	
	
	/**
	 * Support method to convert an object to JSON
	 * 
	 * @param obj object to convert
	 * 
	 * @return String JSON representation
	 */	
	protected String toJSONString(Object obj) {
		String json = null;

		try {
			json = mapper.writeValueAsString(obj);
        } catch (Exception e) {
        	LOGGER.error("error converting object list",e);
		}

		return json;
	}


	/**
	 * read the fixture (a JSON file) based on the dtoName and fixtureLocation and return it
	 * 
	 * @return a fixture corresponding to the DTO
	 */
	private AktanaFixtureDTO<DTO> initializeFixture() {
		String fixtureName = fixtureLocation + dtoName + "Fixture" + ".json";
		AktanaFixtureDTO<DTO> tempfixture = readObjectFixture(fixtureName);

		return tempfixture;
	}


	/**
	 * read the object fixture from file
	 * 
	 * @param fixtureName
	 * @return a fixture read from a file
	 */
	private AktanaFixtureDTO<DTO> readObjectFixture(String fixtureName) {
		JavaType type = mapper.getTypeFactory().constructParametricType(AktanaFixtureDTO.class, dtoClazz);
		AktanaFixtureDTO<DTO> resp = null;
		try {
			String json = readFromFileAsString(fixtureName);
			resp = mapper.readValue(json, type);
        } catch (Exception e) {
        	LOGGER.error("fixtureName not found:" + fixtureName,e);
		}

		return resp;
	}


	/**
	 * read a string from a file
	 * 
	 * @param fileName
	 * @return String
	 */	
	protected String readFromFileAsString(String fileName) {
		StringBuffer fileData = new StringBuffer(1000);
		String retString = null;
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader reader = null;
		try {
			inputStream = AbstractResourceSupport.class.getResourceAsStream(fileName);
			inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			char[] buf = new char[1024];
			int numRead = 0;
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
				buf = new char[1024];
			}
			retString = fileData.toString();
		} catch (IOException e) {
        	LOGGER.error("file could not be read:" + fileName,e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {

				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {

				}
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {

				}
			}
		}

		return retString;
	}

	/**
	 * Throws an exception if the objectUID does not match the DTO's objectUID
	 * 
	 * @param objectUID
	 * @param dto
	 */
	protected void validateObjectUID(String objectUID, IExternallyIdentifiableDTO dto) {
		validateStringsEqual(objectUID, "objectUID", dto.getObjectUID(), "the body's objectUID");
	}

	/**
	 * Throws an exception if the two Strings are not equals
	 * 
	 * @param string1  string to validate
	 * @param string1Name string to use in error response
	 * @param string2  string to validate 
	 * @param string2Name string to use in error response
	 */
	protected void validateStringsEqual(String string1, String string1Name, String string2, String string2Name) {
		if ( (string1==null && string2==null) 
				|| (string1!=null && string1.equals(string2))
				|| (string1!=null && string1.concat("~~").equals(string2))	// for MAIN-14656 back-compatibility: is valid if missing ~messageTopicUID~messageUID
				)   
		{
			return; // valid
		}
		throw new GenericValidationException(400, 0,
				"Requested %s %s does not match %s %s",
				string1Name, string1, string2Name, string2);
	}
	
		
	/**
	 * Support for authentication using token passed as params
	 * 
	 * @param token
	 * @return
	 * @throws AuthenticationException
	 */
	protected Optional<UserPrincipalDTO> authenticate(String token)
	{
		BasicCredentials credentials = new BasicCredentials("", token);

		Optional<UserPrincipalDTO> valid;
		try {
			valid = aktanaBasicApiAuthenticator.authenticate(credentials);
		} catch (AuthenticationException e) {
			LOGGER.debug("Unable to authenticate", e);			
    		throw new GenericValidationException(400, 0, "Invalid Token or Token Expired");	    					        	            							
		}

		return valid;		
	}


	protected ObjectMapper getJsonMapper() {
		return mapper;
	}


	protected void setJsonMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}
	
}
