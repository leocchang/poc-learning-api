/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningConfigBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import com.aktana.learning.api.dto.LearningFileDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.LearningConfigDTO;
import com.aktana.learning.api.dto.LearningConfigDraftDTO;
import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.api.dto.LearningParamElementDTO;
import com.aktana.learning.api.dto.LearningRunDTO;
import com.aktana.learning.api.dto.LearningVersionInfoDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningConfig;

import com.aktana.learning.api.dto.dse.MessageAlgorithmDTO;
import com.aktana.learning.api.dto.dse.TimeToEngageAlgorithmDTO;




/** LearningConfig Resource */
@Path("/v1.0/LearningConfig")
@Api(value = "/v1.0/LearningConfig", description = "Operations on LearningConfig", position = 130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningConfigResource extends AbstractResourceSupport<LearningConfigInfoDTO, LearningConfig, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LearningConfigResource.class);

	/**
	 * @param type
	 */
	public LearningConfigResource() {
		super(LearningConfigResource.class, LearningConfigInfoDTO.class, (Class) LearningConfigBo.class);
	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningConfigs", notes = "List all the LearningConfigs", response = LearningConfigInfoDTO.class, responseContainer = "List", position = 10)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response list(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "modelType", value = "Type of Model", required = false, allowableValues="MSO,REM,TTE,ANCHOR") @QueryParam("modelType") String type,
			@ApiParam(name = "page", value = "Page number (default is 1)") @QueryParam("page") Integer page,
			@ApiParam(name = "rows", value = "Number of rows (default is 100)") @QueryParam("rows") Integer rows){
		
		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		List<LearningConfigInfoDTO> list = bo.getConfigsByType(type, page, rows);
		
		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	/* @GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/configs")
	@ApiOperation(value = "List of LearningConfigs by Type", notes = "List all the LearningConfigs by Model Type", response = LearningConfigInfoDTO.class, position = 10)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getLearningConfigByModelType(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "modelType", value = "Type of Model", required = false, defaultValue = "MSO", allowableValues="MSO,REM,TTE") @QueryParam("modelType") String type,
			@ApiParam(name = "page", value = "Page number (default is 1)") @QueryParam("page") Integer page,
			@ApiParam(name = "rows", value = "Number of rows (default is 100)") @QueryParam("rows") Integer rows){
		
		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningConfigInfoDTO> configList = bo.getConfigsByType(type, page, rows);

		String json = toJSONString(configList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	} */

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}")
	@ApiOperation(value = "Read LearningConfig", notes = "Lookup an LearningConfig", response = LearningConfigInfoDTO.class, position = 30)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String id) {
		LearningConfigInfoDTO acct = readObject(id);

		String json = toJSONString(acct);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	/*
	 * @POST
	 * 
	 * @UnitOfWork
	 * 
	 * @Timed(name = "post-requests")
	 * 
	 * @ApiOperation(value = "Create LearningConfig", notes =
	 * "Create a new LearningConfig", response =
	 * LearningConfigInfoDTO.class,position=20)
	 * 
	 * @ApiResponses(value = {
	 * 
	 * @ApiResponse(code = 200, message = "Successfully processed"),
	 * 
	 * @ApiResponse(code = 400, message = "Invalid input",
	 * response=AktanaErrorResponseDTO.class),
	 * 
	 * @ApiResponse(code = 500, message = "Server error",
	 * response=AktanaErrorResponseDTO.class) }) public Response
	 * saveLearningConfig(@Auth @ApiParam(access="internal", required=false)
	 * UserPrincipalDTO user,
	 * 
	 * @ApiParam(name="newObj", value="new LearningConfig to create",
	 * required=true)
	 * 
	 * @Valid LearningConfigDTO newObj) {
	 * 
	 * LearningConfigInfoDTO resp =
	 * saveObject(newObj.createLearningConfigInfoDTO());
	 * 
	 * String json = toJSONString(resp);
	 * 
	 * return Response.ok(json, MediaType.APPLICATION_JSON).build(); }
	 */

	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Path("/draft")
	@ApiOperation(value = "Create LearningConfig Draft", notes = "Create a new LearningConfig draft", response = LearningConfigInfoDTO.class, position = 20)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response createLearningConfigDraft(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "newObj", value = "new LearningConfig to create", required = true) @Valid LearningConfigDraftDTO newObj) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO resp = bo.saveDraft(newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Path("/{learningConfigUID}/clone")
	@ApiOperation(value = "Clone LearningConfig with latest LearningVersion", notes = "Clone a LearningConfig with latest LearningVersion", response = LearningConfigInfoDTO.class, position = 20)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response cloneLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "newObj", value = "cloned LearningConfig properties", required = true) @Valid LearningConfigDTO newObj) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO resp = bo.clone(learningConfigUID, null, newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/clone")
	@ApiOperation(value = "Clone LearningConfig with specified LearningVersion", notes = "Clone a LearningConfig with specified LearningVersion", response = LearningConfigInfoDTO.class, position = 20)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response cloneLearningConfigWithVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "newObj", value = "cloned LearningConfig properties", required = true) @Valid LearningConfigDTO newObj) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO resp = bo.clone(learningConfigUID, learningVersionUID, newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Path("/{learningConfigUID}/params")
	@ApiOperation(value = "Clear Params and add new ones to latest draft version of LearningConfig", notes = "Delete any old params and add new Params to latest draft version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response resetParamsToConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "objlist", value = "List of parameters to update", required = true) @Valid List<LearningParamElementDTO> params) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, null, params, "~");

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/params")
	@ApiOperation(value = "Clear params and add new ones to version of LearningConfig ", notes = "Delete any old params and add new Params to a specific draft version of a LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response resetParamsToConfigAndVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "objlist", value = "List of parameters to update", required = true) @Valid List<LearningParamElementDTO> params) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, learningVersionUID, params, "~");

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/params")
	@ApiOperation(value = "Add Params to latest draft version of LearningConfig", notes = "Add Params to latest draft version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response addParamsToConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "objlist", value = "List of parameters to update", required = true) @Valid List<LearningParamElementDTO> params) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, null, params, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/params")
	@ApiOperation(value = "Add Params to version of LearningConfig ", notes = "Add Params to a specific draft version of a LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response addParamsToConfigAndVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "objlist", value = "List of parameters to update", required = true) @Valid List<LearningParamElementDTO> params) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, learningVersionUID, params, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@DELETE
	@UnitOfWork
	@Timed(name = "delete-requests")
	@Path("/{learningConfigUID}/params")
	@ApiOperation(value = "Delete all Params of latest draft version of LearningConfig", notes = "Delete all Params of latest draft version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response removeAllParamsOfConfig(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, null, null, "~");

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@DELETE
	@UnitOfWork
	@Timed(name = "delete-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/params")
	@ApiOperation(value = "Delete all Params of version of LearningConfig ", notes = "Delete all Params of a specific draft version of a LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response removeAllParamsOfConfigAndVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, learningVersionUID, null, "~");

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@DELETE
	@UnitOfWork
	@Timed(name = "delete-requests")
	@Path("/{learningConfigUID}/params/{learningParamUID}")
	@ApiOperation(value = "Delete specified Params of latest draft version of LearningConfig", notes = "Delete specified Params of latest draft version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response removeParamsOfConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningParamUID", value = "unique id for the LearningParam, format=paramName~paramIndex"
					+ "", required = true) @PathParam("learningParamUID") String learningParamUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, null, null, learningParamUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@DELETE
	@UnitOfWork
	@Timed(name = "delete-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/params/{learningParamUID}")
	@ApiOperation(value = "Delete specified Params of version of LearningConfig ", notes = "Delete specified Params of a specific draft version of a LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response removeParamsOfConfigAndVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "learningParamUID", value = "unique id for the LearningParam, format=paramName~paramIndex"
					+ "", required = true) @PathParam("learningParamUID") String learningParamUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.addParams(learningConfigUID, learningVersionUID, null, learningParamUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/params")
	@ApiOperation(value = "Get Params of the latest version of LearningConfig", notes = "Get Params from latest version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigParams(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.getParams(learningConfigUID, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/params")
	@ApiOperation(value = "Get Params for a version of LearningConfig ", notes = "Get Params for a specific version of LearningConfig", response = LearningParamDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigAndVersionParams(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningParamDTO> respList = bo.getParams(learningConfigUID, learningVersionUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}")
	@ApiOperation(value = "Update LearningConfig", notes = "Update a LearningConfig", response = LearningConfigInfoDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response updateLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String id,
			@ApiParam(name = "obj", value = "LearningConfig to update", required = true) @Valid LearningConfigDTO obj) {

		validateObjectUID(id, obj);

		LearningConfigInfoDTO resp = updateObject(id, obj.createLearningConfigInfoDTO());

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@DELETE
	@UnitOfWork
	@Timed(name = "delete-requests")
	@Path("/{learningConfigUID}")
	@ApiOperation(value = "Delete LearningConfig", notes = "Delete an LearningConfig", response = LearningConfigInfoDTO.class, position = 50)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response removeLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String id) {

		LearningConfigInfoDTO resp = removeObject(id);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/versions")
	@ApiOperation(value = "Get list of versions for LearningConfig", notes = "Add Params to latest draft version of LearningConfig", response = LearningVersionInfoDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getVersions(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningVersionInfoDTO> respList = bo.getVersions(learningConfigUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/publish")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Publish LearningConfig", notes = "Publish LearningConfig", response = LearningConfigInfoDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response publishLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO dto = bo.publish(learningConfigUID);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/unpublish")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Unpublish LearningConfig", notes = "Unpublish LearningConfig", response = LearningConfigInfoDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response unpublishLearningConfig(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO dto = bo.unpublish(learningConfigUID);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/build")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Build LearningConfig", notes = "Create a build of the LearningConfig", response = LearningBuildDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 409, message = "Rundeck Job Conflict", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response buildLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "deploy", value = "If true, deploy version on successful build", required = false, defaultValue = "false") @QueryParam("deploy") boolean deploy) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningBuildDTO dto = bo.build(learningConfigUID, null, deploy, true);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/build")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Build LearningConfig with specified LearningVersion", notes = "Create a build of the LearningConfig with specified LearningVersion", response = LearningBuildDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 409, message = "Rundeck Job Conflict", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response buildLearningConfigWithVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "deploy", value = "If true, deploy version on successful build", required = false, defaultValue = "false") @QueryParam("deploy") boolean deploy) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningBuildDTO dto = bo.build(learningConfigUID, learningVersionUID, deploy, true);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/rebuild")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Build LearningConfig", notes = "Create a build of the LearningConfig", response = LearningBuildDTO.class, responseContainer = "List", position = 41)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 409, message = "Rundeck Job Conflict", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response rebuildLearningConfigs(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "modelType", value = "Type of Model", required = false, allowableValues="MSO,TTE") @QueryParam("modelType") String modelType) {

		
		
		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		List<LearningBuildDTO> respList = bo.rebuild(modelType);
		String json = toJSONString(respList);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/deploy")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Deploy LearningConfig", notes = "Deploy LearningConfig", response = LearningBuildDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response deployLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningBuildDTO dto = bo.deploy(learningConfigUID, null, null);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/deploy")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Deploy LearningConfig with specified LearningVersion", notes = "Deploy LearningConfig with specified LearningVersion", response = LearningBuildDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response deployLearningConfigWithVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningBuildDTO dto = bo.deploy(learningConfigUID, learningVersionUID, null);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}
	
	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/builds/{learningBuildUID}/deploy")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Deploy LearningConfig with specified LearningVersion and LearningBuild", notes = "Deploy LearningConfig with specified LearningVersion and LearningBuild", response = LearningBuildDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response deployLearningConfigWithVersionAndBuild(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true) @PathParam("learningBuildUID") String learningBuildUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningBuildDTO dto = bo.deploy(learningConfigUID, learningVersionUID, learningBuildUID);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	
	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/undeploy")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Undeploy LearningConfig", notes = "Undeploy LearningConfig", response = LearningConfigInfoDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response undeployLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningConfigInfoDTO dto = bo.undeploy(learningConfigUID);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	
	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/score")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Score LearningConfig", notes = "Score LearningConfig", response = LearningRunDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 409, message = "Rundeck Job Conflict", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response scoreLearningConfig(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningRunDTO dto = bo.score(learningConfigUID, null);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@PUT
	@UnitOfWork
	@Timed(name = "put-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/score")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@ApiOperation(value = "Score LearningConfig with specified LearningVersion", notes = "Score LearningConfig with specified LearningVersion", response = LearningRunDTO.class, position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 409, message = "Rundeck Job Conflict", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response scoreLearningConfigWithVersion(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
		LearningRunDTO dto = bo.score(learningConfigUID, learningVersionUID);
		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}
	
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/builds")
	@ApiOperation(value = "Get list of builds of the latest version of LearningConfig", notes = "Get LearningBUilds from latest version of LearningConfig", response = LearningBuildDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigBuilds(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningBuildDTO> respList = bo.getBuilds(learningConfigUID, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/builds")
	@ApiOperation(value = "Get list of builds for a version of LearningConfig ", notes = "Get LearningBuilds for a specific version of LearningConfig", response =  LearningBuildDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigAndVersionBuilds(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningBuildDTO> respList = bo.getBuilds(learningConfigUID, learningVersionUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/runs")
	@ApiOperation(value = "Get list of runs of the latest version of LearningConfig", notes = "Get LearningRuns from latest version of LearningConfig", response = LearningRunDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigRuns(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningRunDTO> respList = bo.getRuns(learningConfigUID, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/runs")
	@ApiOperation(value = "Get list of runs for a version of LearningConfig ", notes = "Get LearningRuns for a specific version of LearningConfig", response = LearningRunDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigAndVersionRuns(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningRunDTO> respList = bo.getRuns(learningConfigUID, learningVersionUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/files")
	@ApiOperation(value = "Get list of files of the latest version of LearningConfig", notes = "Get LearningFiles from latest version of LearningConfig", response = LearningFileInfoDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigFiles(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningFileInfoDTO> respList = bo.getFiles(learningConfigUID, null);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningConfigUID}/versions/{learningVersionUID}/files")
	@ApiOperation(value = "Get list of files for a version of LearningConfig ", notes = "Get LearningFiles for a specific version of LearningConfig", response = LearningFileInfoDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getConfigAndVersionFiles(
			@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
			@ApiParam(name = "learningVersionUID", value = "unique id for the LearningVersion", required = true) @PathParam("learningVersionUID") String learningVersionUID) {

		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();

		List<LearningFileInfoDTO> respList = bo.getFiles(learningConfigUID, learningVersionUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	
//	@GET
//	@UnitOfWork
//	@Path("/{learningConfigUID}/crontab")
//	@ApiOperation(value = "Show Rundeck Job Basic Information by config and job type", notes = "Show Rundeck Job Basic Information by config and job type", response = CrontabDTO.class,position=100)    
//	public Response getJobDefinition(
//			@ApiParam(name = "learningConfigUID", value = "unique id for the LearningConfig", required = true) @PathParam("learningConfigUID") String learningConfigUID,
//			@ApiParam(name = "jobType", value = "Type of job", required = false, allowableValues="build,score") @QueryParam("jobType") String jobType) {
//
//		LearningConfigBo bo = (LearningConfigBo) this.createBoInstance();
//		
//		CrontabDTO dto = bo.getJobCrontab(learningConfigUID, jobType);
//
//		String json = toJSONString(dto);
//
//		return Response.ok(json, MediaType.APPLICATION_JSON).build();
//	}

}
