/*****************************************************************
 * 
 * InvalidEntityExceptionMapper is responsible to translate InvalidEntityException (or exceptions derived from InvalidEntityException) into error that is returned by the EndPoint.
 * 
 * InvalidEntityException is returned by Jackson when the JSON does not correspond to the DTO definition
 * 
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20346 $ on $Date: 2014-06-28 09:51:34 -0700 (Sat, 28 Jun 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.resource.exceptionmapper;


import java.util.ArrayList; 
import java.util.Iterator;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Provider
public class InvalidEntityExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());
   
    public InvalidEntityExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }

    public AktanaErrorResponseDTO getErrorResponse(ConstraintViolationException ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(400);
    	errorResponse.setMessage(ex.getMessage());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());
    	
    	//LOGGER.error("Invalid Entity Exception: ", ex);

    	if (ex.getConstraintViolations() != null) {        		
    		Iterator<ConstraintViolation<?>> iter = ex.getConstraintViolations().iterator();
    		
    		while (iter.hasNext()) {
    			ConstraintViolation<?> violation = iter.next();
    			
    			AktanaErrorDTO err = new AktanaErrorDTO();
    			
    		        			        			
    			if (violation.getLeafBean() != null) {
    				err.setMessage(violation.getMessage() + " was " + violation.getLeafBean().toString());
    			} else {
        			err.setMessage(violation.getMessage());        				
    			}
    			
    			err.setField(violation.getPropertyPath().toString());
    			err.setCode(0);
    			        			
    			errorResponse.getErrors().add(err);        			
    		}        		
    	}
    	
    	return errorResponse;
    }
    
    
    
    @Override
    public Response toResponse(ConstraintViolationException ex) {
        try {
        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);
        	
        	/*
        	if (ex.getErrors() != null) {
        		for(int i=0;i<ex.getErrors().size();++i) {
        			AktanaErrorDTO err = new AktanaErrorDTO();
        			
        			err.setMessage(ex.getErrors().get(i));
        			err.setField("");
        			err.setCode(0);
        			        			
        			errorResponse.getErrors().add(err);
        		}
        	}
        	*/
        	       	
//            _errorInfo.populate(ex);
//            String requestData = getRequestData(filter.getCachedRequest());
//            logException(ex, requestData);
//            notifyException(ex);

            // InvalidEntityExceptions are always mapped to 400 
        	return Response.status(400).entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON).build();        	
        } catch (Exception ex2) {
            throw new RuntimeException(ex2.toString());
        }
    }

}
