/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;  

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningFileBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningFileDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningFile;


/** LearningFile Resource */
@Path("/v1.0/LearningFile")
@Api(value = "/v1.0/LearningFile", description = "Operations on LearningFile",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningFileResource extends AbstractResourceSupport<LearningFileInfoDTO, LearningFile, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningFileResource.class);

    /**
     * @param type
     */
    public LearningFileResource() {
        super(LearningFileResource.class, LearningFileInfoDTO.class, (Class) LearningFileBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningFiles", notes = "List all the LearningFiles", response = LearningFileInfoDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		List<LearningFileInfoDTO> list = listObjects();

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningFileUID}")
	@ApiOperation(value = "Read LearningFile", notes = "Lookup an LearningFile", response = LearningFileDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 404, message = "LearningFile does not exist", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningFile(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningFileUID", value="unique id for the LearningFile", required=true)
			@PathParam("learningFileUID") String id
			) {
    	
    	LearningFileBo bo = (LearningFileBo) this.createBoInstance();
		LearningFileDTO dto = bo.readObjectWithData(id);

		String json = toJSONString(dto);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
/*
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
	@ApiOperation(value = "Create LearningFile", notes = "Create a new LearningFile", response = LearningFileDTO.class,position=20)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response saveLearningFile(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		@ApiParam(name="newObj", value="new LearningFile to create", required=true)
		@Valid LearningFileDTO newObj) {

		LearningFileDTO resp = saveObject(newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @UnitOfWork
    @Timed(name = "put-requests")
    @Path("/{learningFileUID}")
	@ApiOperation(value = "Update LearningFile", notes = "Update an LearningFile", response = LearningFileDTO.class,position=40)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
	public Response updateLearningFile(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningFileUID", value = "unique id for the LearningFile", required = true) 
			@PathParam("learningFileUID") String id,
			@ApiParam(name = "obj", value = "LearningFile to update", required = true)
			@Valid LearningFileDTO obj) {

		validateObjectUID(id, obj);
    	
		LearningFileDTO resp = updateObject(id, obj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

    }
    */
 
    
    
  /*  
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningFileUID}")
	@ApiOperation(value = "Delete LearningFile", notes = "Delete an LearningFile", response = LearningFileDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningFile(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningFileUID", value = "unique id for the LearningFile", required = true) 
			@PathParam("learningFileUID") String id) {

		LearningFileDTO resp = removeObject(id);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    */
    
   
}
