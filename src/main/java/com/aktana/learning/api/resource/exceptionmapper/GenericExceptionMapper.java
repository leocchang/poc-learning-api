/*****************************************************************
 * 
 * GenericExceptionMapper is responsible to translate GenericException (or exceptions derived from Exception) into error that is returned by the EndPoint.
 * 
 * 
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20346 $ on $Date: 2014-06-28 09:51:34 -0700 (Sat, 28 Jun 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.resource.exceptionmapper;

import java.util.ArrayList;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;


@Provider
public class GenericExceptionMapper implements ExceptionMapper<Exception> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    public GenericExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }

    public AktanaErrorResponseDTO getErrorResponse(Exception ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(500);
    	errorResponse.setMessage(ex.getMessage() + ex.toString());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());
    	
    	LOGGER.error("Exception thrown",ex);

    	return errorResponse;
    }
    
    
    @Override
    public Response toResponse(Exception ex) {
        try {
        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);
        	
        	       	
//            _errorInfo.populate(ex);
//            String requestData = getRequestData(filter.getCachedRequest());
//            logException(ex, requestData);
//            notifyException(ex);

            // We will map all Exceptions that are not caught into 500 as it is not expected
        	// If it is an expected response - AktanaException will be thrown
            return Response.status(500).entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON).build();        	
        } catch (Exception ex2) {
            throw new RuntimeException(ex2.toString());
        }
    }

}
