package com.aktana.learning.api.resource.exceptionmapper;

import java.util.ArrayList;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.xml.bind.UnmarshalException;

@Provider
public class UnmarshalExceptionMapper implements ExceptionMapper<UnmarshalException> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    public UnmarshalExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }
    
    public AktanaErrorResponseDTO getErrorResponse(UnmarshalException ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(400);
    	errorResponse.setMessage(ex.getMessage() + ex.toString());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());

    	return errorResponse;
    }

    @Override
    public Response toResponse(UnmarshalException ex) {
        try {
        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);
        	
        	       	
//            _errorInfo.populate(ex);
//            String requestData = getRequestData(filter.getCachedRequest());
//            logException(ex, requestData);
//            notifyException(ex);

        	// always return 400 for these types of exceptions
            return Response.status(400).entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON).build();        	
        } catch (Exception ex2) {
            throw new RuntimeException(ex2.toString());
        }
    }

}
