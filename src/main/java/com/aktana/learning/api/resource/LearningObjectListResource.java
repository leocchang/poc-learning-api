/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.LearningObjectListBo;
import com.aktana.learning.api.bo.LearningSimulationBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningObjectListDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

//import com.aktana.learning.persistence.models.impl.LearningRun; 
import com.aktana.learning.persistence.models.impl.LearningObjectList; 


/** LearningObjectList Resource */
@Path("/v1.0/LearningObjectList")
@Api(value = "/v1.0/LearningObjectList", description = "Operations on LearningObjectList",position=130) 
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningObjectListResource extends AbstractResourceSupport<LearningObjectListDTO, LearningObjectList, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LearningObjectListResource.class);

	/**
	 * @param type
	 */
	public LearningObjectListResource() {
		super(LearningObjectListResource.class, LearningObjectListDTO.class, (Class) LearningObjectListBo.class);
	}


	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningRunUID}/account")
	@ApiOperation(value = "Get list of learning objects", notes = "List all learning objects with object type 'Account'", response = LearningObjectListDTO.class, responseContainer = "List",position=10)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
	public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    	@ApiParam(name = "learningRunUID", value = "unique id for the learning run", required = true) @PathParam("learningRunUID") String learningRunUID, 
		@ApiParam(name = "page", value = "Page number (default is 1)") @QueryParam("page") Integer page,
		@ApiParam(name = "rows", value = "Number of rows (default is 100)") @QueryParam("rows") Integer rows){

		
		LearningObjectListBo bo = (LearningObjectListBo) this.createBoInstance();
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID",learningRunUID);
		map.put("objectType", "Account");
		
		List<LearningObjectListDTO> list = bo.listAccountsWithLearningRun(map, page, rows);
		
		
		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	} 

}
