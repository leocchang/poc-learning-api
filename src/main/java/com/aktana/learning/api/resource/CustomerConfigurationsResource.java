/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;  

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.CustomerConfigurationsBo;
import com.aktana.learning.api.bo.LearningConfigBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.CustomerConfigurationsDTO;
import com.aktana.learning.api.dto.LearningConfigDraftDTO;
import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.CustomerConfigurations;


/** LearningParam Resource */
@Path("/v1.0/CustomerConfigurations")
@Api(value = "/v1.0/CustomerConfigurations", description = "Operations on CustomerConfigurations",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CustomerConfigurationsResource extends AbstractResourceSupport<CustomerConfigurationsDTO, CustomerConfigurations, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerConfigurationsResource.class);

    /**
     * @param type
     */
    public CustomerConfigurationsResource() {
        super(CustomerConfigurationsResource.class, CustomerConfigurationsDTO.class, (Class) CustomerConfigurationsBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/configurationType/{configurationType}")
	@ApiOperation(value = "List customer configurations", notes = "List all the customer configurations by type", response = CustomerConfigurationsDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class) })	    
    public Response getConfigurations(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="configurationType", value="The configuration type", required=true) @PathParam("configurationType") String configurationType) {		
    	CustomerConfigurationsBo bo = new CustomerConfigurationsBo();    	
    	List<CustomerConfigurationsDTO> list = bo.getConfigurationsByType(configurationType);
		String json = toJSONString(list);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
	@POST
	@UnitOfWork
	@Timed(name = "post-requests")
	@Consumes(MediaType.TEXT_PLAIN + ";charset=utf-8")
	@Path("/configurationType/{configurationType}/configurationValue/{configurationValue}")
	@ApiOperation(value = "Create a new customer configuration", notes = "Add a customer configuration (value, type) pair", response = CustomerConfigurationsDTO.class, position = 40)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response postConfigurations(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="configurationType", value="The configuration type", required=true) @PathParam("configurationType") String configurationType,
    		@ApiParam(name="configurationValue", value="A value the configuraton type", required=true) @PathParam("configurationValue") String configurationValue) {
		
    	CustomerConfigurationsBo bo = new CustomerConfigurationsBo();	
    	CustomerConfigurationsDTO resp = bo.addCustomerConfiguration(configurationType, configurationValue);
		String json = toJSONString(resp);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
    
 
    
    
    
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/configurationType/{configurationType}/configurationValue/{configurationValue}")
	@ApiOperation(value = "Delete a customer configuration", notes = "Delete a customer configuration (value, type) pair", response = CustomerConfigurationsDTO.class,position=50)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class) })
    public Response deleteConfigurations(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="configurationType", value="The configuration type", required=true) @PathParam("configurationType") String configurationType,
    		@ApiParam(name="configurationValue", value="A value the configuraton type", required=true) @PathParam("configurationValue") String configurationValue) {
    	
    	CustomerConfigurationsBo bo = new CustomerConfigurationsBo();
    	CustomerConfigurationsDTO resp = bo.deleteCustomerConfiguration(configurationType, configurationValue);
		String json = toJSONString(resp);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
      
   
}
