/*****************************************************************
 * 
 * JsonParseExceptionMapper is responsible to translate JsonParseException (or exceptions derived from JsonParseException) into error that is returned by the EndPoint.
 * 
 * JsonParseException is returned by Jackson when the JSON string cannot be parsed
 * 
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20346 $ on $Date: 2014-06-28 09:51:34 -0700 (Sat, 28 Jun 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.resource.exceptionmapper;

import java.util.ArrayList;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Provider
public class JsonParseExceptionMapper implements ExceptionMapper<JsonParseException> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());
   
    public JsonParseExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }
    
    public AktanaErrorResponseDTO getErrorResponse(JsonParseException ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(400);
    	errorResponse.setMessage(ex.getMessage());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());

    	return errorResponse;
    }

    @Override
    public Response toResponse(JsonParseException ex) {
        try {
        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);
        	
        	       	
//            _errorInfo.populate(ex);
//            String requestData = getRequestData(filter.getCachedRequest());
//            logException(ex, requestData);
//            notifyException(ex);

        	// always return 400 for these types of exceptions
            return Response.status(400).entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON).build();        	
        } catch (Exception ex2) {
            throw new RuntimeException(ex2.toString());
        }
    }

}
