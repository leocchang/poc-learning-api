package com.aktana.learning.api.resource.exceptionmapper;


import java.util.ArrayList;  
import java.util.Iterator;

import javax.validation.ConstraintViolation;
import org.hibernate.exception.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Provider
public class HibernateConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());
  
    public HibernateConstraintViolationExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }

    public AktanaErrorResponseDTO getErrorResponse(ConstraintViolationException ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(400);
    	errorResponse.setMessage(ex.getMessage() + " " + ex.getSQLException().getMessage());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());

    	return errorResponse;
    }
    
    
    @Override
    public Response toResponse(ConstraintViolationException ex) {
        try {
        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);

            // InvalidEntityExceptions are always mapped to 400 
        	return Response.status(400).entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON).build();        	
        } catch (Exception ex2) {
            throw new RuntimeException(ex2.toString());
        }
    }

}
