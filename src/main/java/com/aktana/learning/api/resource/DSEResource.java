/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.api.dto.dse.AccountDTO;
import com.aktana.learning.api.dto.dse.MessageAlgorithmDTO;
import com.aktana.learning.api.dto.dse.MessageSetDTO;
import com.aktana.learning.api.dto.dse.ProductDTO;
import com.aktana.learning.api.dto.dse.RepActionTypeDTO;
import com.aktana.learning.api.dto.dse.RepDTO;
import com.aktana.learning.api.dto.dse.SEConfigDirectoryMessageDTO;
import com.aktana.learning.api.dto.dse.SEConfigGlobalParameterTypeDTO;
import com.aktana.learning.api.dto.dse.SEConfigStaticReferenceDataMessageDTO;
import com.aktana.learning.api.dto.dse.MessageDTO;
import com.aktana.learning.api.bo.DSEResourceBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.DSEParametersForREMDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;


/** DSE Resource */
@Path("/v1.0/DSE")
@Api(value = "/v3.0/DSE", description = "Operations on Atkana DSE", position=1000)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class DSEResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(DSEResource.class);

	@Path("/accounts")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all DSE Accounts", notes = "List all DSE Accounts", response = AccountDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getAccounts(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    			@ApiParam(name = "accountName", value = "Search account name") @QueryParam("accountName") String accountName,
    			@ApiParam(name = "page", value = "Page number") @QueryParam("page") Integer page,
    			@ApiParam(name = "rows", value = "Number of rows") @QueryParam("rows") Integer rows){
		
		try {
			// prepare string passed to DSE API call
			List<String> paginatedStringList = new ArrayList<String>();
    			if (page != null) paginatedStringList.add("page="+page);
			if (rows != null) paginatedStringList.add("rows="+rows);
			if (accountName != null) {
				// handle accountName's special character first (will have space in accountName)
				String accountNameEncoded;
				try {
					accountNameEncoded=URLEncoder.encode(accountName, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					throw new GenericValidationException(500, "UTF-8 encoding error");
				}
				accountNameEncoded = accountNameEncoded.replaceAll("\\+", "%20"); //URL encoder convert space to +, further convert + to %20
				paginatedStringList.add("accountName="+accountNameEncoded);
			}
			String paginatedString = paginatedStringList.size() > 0? "?"+String.join("&", paginatedStringList) : "";
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_ACCOUNT, "/paginated"+paginatedString);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/products")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all DSE Products", notes = "List all DSE Products", response = ProductDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getProducts(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_PRODUCT);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/configs")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all DSE Configs", notes = "List all DSE Configs", response = SEConfigDirectoryMessageDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getConfigs(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_DIRECTORY);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	    															    		
		}
    }
	
	
	
	@Path("/reps")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all DSE Reps", notes = "List all DSE Reps", response = RepDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getReps(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name = "start", value = "Start row number")
			@QueryParam("start") Integer start,
			@ApiParam(name = "rows", value = "Number of rows")
			@QueryParam("rows") Integer rows) {
		
    	try {
    		if(start == null || rows == null) {
    			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REP);
    		} else {
    			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REP, "?start="+start+"&rows="+rows);
    		}
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/referencedata")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "Get DSE reference data", notes = "Get DSE reference data used by DSE UI", response = SEConfigStaticReferenceDataMessageDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getReferenceData(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_REFERENCEDATA);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }

	
	@Path("/repactiontype")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "Get DSE reference data", notes = "Get DSE reference data used by DSE UI", response = RepActionTypeDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getRepActionType(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REPACTIONTYPE);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/messageset")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "Get DSE reference data", notes = "Get DSE reference data used by DSE UI", response = MessageSetDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getMessageSet(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_MESSAGESET);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/messagealgorithm")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "Get DSE reference data", notes = "Get DSE reference data used by DSE UI", response = MessageAlgorithmDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getMessageAlgorithm(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_MESSAGEALGORITHM);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	
	@Path("/message")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "Get DSE reference data", notes = "Get DSE reference data used by DSE UI", response = MessageDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public String getMessages(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		
    	try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_MESSAGE);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
  
	 
	@Path("/message/{messageUID}")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")    
	@ApiOperation(value = "Read message", notes = "Lookup a message", response = MessageDTO.class, position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String getMessage(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="messageUID", value="unique id of the Message to lookup", required=true)
			@PathParam("messageUID") String messageUID
			) {
		try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_MESSAGE_UID, "/"+messageUID);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/DSEConfig/{configId}/version/{versionId}/globalParameters/{parameterName}")
    @GET
    @UnitOfWork
    @Timed(name = "get-requests")    
	@ApiOperation(value = "Get the global parameter for specified DSE configuration", notes = "Used only by Aktana front end", response = Map.class, position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String getGlobalParameter(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name = "configId", value = "unique id of DSE config", required = true) @PathParam("configId") int configId,
    		@ApiParam(name = "versionId", value = "unique id of config version", required = true) @PathParam("versionId") int versionId,
        	@ApiParam(name = "parameterName", value = "the global parameter to fetch", required = true) @PathParam("parameterName") String parameterName) {

		try {
			return DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS,"/"+configId+"/version/"+versionId+"/globalParameters/"+parameterName);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }

	
	@Path("/DSEConfig/{configId}/version/{versionId}/globalParameters")
    @POST
    @UnitOfWork
    @Timed(name = "put-requests")    
	@ApiOperation(value = "Update the global parameter for specified DSE configuration", notes = "Used only by Aktana front end", response = Response.class, position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String putGlobalParameter(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name = "configId", value = "unique id of DSE config", required = true) @PathParam("configId") int configId,
    		@ApiParam(name = "versionId", value = "unique id of config version", required = true) @PathParam("versionId") int versionId,
    		@QueryParam("lang") String lang,
    		@QueryParam("userId") int userId,
    		Map<String,Object> paramsSpec) {

		try {
			Gson gson = new Gson(); 
			String params = gson.toJson(paramsSpec); 
			return DSEConnector.getInstance().callPost(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS, "/"+configId+"/version/"+versionId+"/globalParameters?userId="+userId, params);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
	@Path("/DSEConfig/{configId}/version/{versionId}/globalParameters/{parameterName}")
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")    
	@ApiOperation(value = "Delete the global parameter for specified DSE configuration", notes = "Used only by Aktana front end", response = Response.class, position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String deleteGlobalParameter(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name = "configId", value = "unique id of DSE config", required = true) @PathParam("configId") int configId,
    		@ApiParam(name = "versionId", value = "unique id of config version", required = true) @PathParam("versionId") int versionId,
    		@ApiParam(name="parameterName", value="the global parameter to fetch", required=true) @PathParam("parameterName") String paramName,
    		@QueryParam("userId") int userId,
    		@QueryParam("lang") String lang) {
		
		try {
			return DSEConnector.getInstance().callDelete(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS, configId+"/version/"+versionId+"/globalParameters/"+paramName+"?userId="+userId);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
    }
	
//    @Path("/AggGlobalParamsForRem")
//    @GET
//    @UnitOfWork
//    @Timed(name = "get-requests")    
//    @ApiOperation(value = "Get aggrigate global parameter values for DSE configurations", notes = "Used only by Aktana front end", response = DSEParametersForREMDTO.class, position=80)
//    @ApiResponses(value = {
//	    @ApiResponse(code = 200, message = "Successfully processed"),
//	    @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
//	    @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
//	})
//	public String getAggGlobalParamsForREM(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
//
//	DSEResourceBo bo = new DSEResourceBo();
//	DSEParametersForREMDTO responseMap = bo.getDSEConfigValuesForREM();
//	Gson gson = new GsonBuilder().serializeNulls().create();
//	String json = gson.toJson(responseMap);
//	return json;
//    }
}
