/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;  

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningParamBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningParam;


/** LearningParam Resource */
@Path("/v1.0/LearningParam")
@Api(value = "/v1.0/LearningParam", description = "Operations on LearningParam",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningParamResource extends AbstractResourceSupport<LearningParamDTO, LearningParam, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningParamResource.class);

    /**
     * @param type
     */
    public LearningParamResource() {
        super(LearningParamResource.class, LearningParamDTO.class, (Class) LearningParamBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningParams", notes = "List all the LearningParams", response = LearningParamDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		List<LearningParamDTO> list = listObjects();

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningParamUID}")
	@ApiOperation(value = "Read LearningParam", notes = "Lookup an LearningParam", response = LearningParamDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningParam(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningParamUID", value="compound unique id for the LearningParam, format=learningVersionUID~paramName~paramIndex", required=true)
			@PathParam("learningParamUID") String id
			) {
		LearningParamDTO acct = readObject(id);

		String json = toJSONString(acct);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
/*
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
	@ApiOperation(value = "Create LearningParam", notes = "Create a new LearningParam", response = LearningParamDTO.class,position=20)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response saveLearningParam(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		@ApiParam(name="newObj", value="new LearningParam to create", required=true)
		@Valid LearningParamDTO newObj) {

		LearningParamDTO resp = saveObject(newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @UnitOfWork
    @Timed(name = "put-requests")
    @Path("/{learningParamUID}")
	@ApiOperation(value = "Update LearningParam", notes = "Update an LearningParam", response = LearningParamDTO.class,position=40)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
	public Response updateLearningParam(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningParamUID", value = "compound unique id for the LearningParam, format=learningVersionUID~paramName~paramIndex", required = true) 
			@PathParam("learningParamUID") String id,
			@ApiParam(name = "obj", value = "LearningParam to update", required = true)
			@Valid LearningParamDTO obj) {

		validateObjectUID(id, obj);
    	
		LearningParamDTO resp = updateObject(id, obj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

    }
    
 
    
    
    
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningParamUID}")
	@ApiOperation(value = "Delete LearningParam", notes = "Delete an LearningParam", response = LearningParamDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningParam(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningParamUID", value = "compound unique id for the LearningParam, format=learningVersionUID~paramName~paramIndex"
					+ "", required = true) 
			@PathParam("learningParamUID") String id) {

		LearningParamDTO resp = removeObject(id);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
  */  
   
}
