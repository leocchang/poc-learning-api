package com.aktana.learning.api.resource;

import com.aktana.learning.api.dto.SimpleTokenRequestDTO;
import com.aktana.learning.api.security.AktanaAccessToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.TokenRequestDTO;
import com.aktana.learning.api.security.TokenGenerationHelper;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;


@Path("/v1.0/Token")
@Api(value = "/v1.0/Token", description = "Retrieve API Token", position=0)
public class TokenGeneratorResource {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenGeneratorResource.class);

    private TokenGenerationHelper helper;

    
    public TokenGeneratorResource() {    	
    	helper = new TokenGenerationHelper();    	
    }

    
    @POST
    @UnitOfWork
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Retrieve Token", notes = "Retrieve Token", response = String.class,position=10)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})    
    public final Response token(@ApiParam(name="tokenRequest", value="parameters required to generate an access token", required=true) @Valid TokenRequestDTO tokenRequest)
    {
        LOGGER.info("Verifying generic access ...");

        String compact = helper.validateTraditionalRequest(tokenRequest);
        
        return Response.ok(compact).build();
    }

    @POST
    @Path("/aim")
    @UnitOfWork
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieve Token", notes = "Retrieve Token", response = String.class,position=10)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully processed"),
            @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
            @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
    })
    public final Response aimToken(@ApiParam(name="tokenRequest", value="parameters required to generate an access token", required=true) @Valid SimpleTokenRequestDTO tokenRequest)
    {
        LOGGER.info("Verifying user access via AIM ...");

        AktanaAccessToken accessToken = helper.validateAimRequest(tokenRequest);

        return Response.ok(accessToken.getToken()).build();
    }
}
