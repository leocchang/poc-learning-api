/*****************************************************************
 *
 * @author $Author: adiel.cohen $
 * 
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;  

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.api.bo.LearningBuildBo;
import com.aktana.learning.api.bo.LearningSimulationBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

import com.aktana.learning.persistence.models.impl.LearningBuild;


/** LearningBuild Resource */
@Path("/v1.0/LearningBuild")
@Api(value = "/v1.0/LearningBuild", description = "Operations on LearningBuild",position=130)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LearningBuildResource extends AbstractResourceSupport<LearningBuildDTO, LearningBuild, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningBuildResource.class);

    /**
     * @param type
     */
    public LearningBuildResource() {
        super(LearningBuildResource.class, LearningBuildDTO.class, (Class) LearningBuildBo.class);
    }


    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@ApiOperation(value = "List all LearningBuilds", notes = "List all the LearningBuilds", response = LearningBuildDTO.class, responseContainer = "List",position=10)	
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})	    
    public Response list(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		List<LearningBuildDTO> list = listObjects();

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{learningBuildUID}")
	@ApiOperation(value = "Read LearningBuild", notes = "Lookup an LearningBuild", response = LearningBuildDTO.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 404, message = "LearningBuild does not exist", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response getLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name="learningBuildUID", value="unique id for the LearningBuild", required=true)
			@PathParam("learningBuildUID") String id
			) {
		LearningBuildDTO acct = readObject(id);

		String json = toJSONString(acct);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
/*
    @POST
    @UnitOfWork
    @Timed(name = "post-requests")
	@ApiOperation(value = "Create LearningBuild", notes = "Create a new LearningBuild", response = LearningBuildDTO.class,position=20)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response saveLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
		@ApiParam(name="newObj", value="new LearningBuild to create", required=true)
		@Valid LearningBuildDTO newObj) {

		LearningBuildDTO resp = saveObject(newObj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @UnitOfWork
    @Timed(name = "put-requests")
    @Path("/{learningBuildUID}")
	@ApiOperation(value = "Update LearningBuild", notes = "Update an LearningBuild", response = LearningBuildDTO.class,position=40)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
	public Response updateLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true) 
			@PathParam("learningBuildUID") String id,
			@ApiParam(name = "obj", value = "LearningBuild to update", required = true)
			@Valid LearningBuildDTO obj) {

		validateObjectUID(id, obj);
    	
		LearningBuildDTO resp = updateObject(id, obj);

		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();

    }
    */
 
    
    
    @DELETE
    @UnitOfWork
    @Timed(name = "delete-requests")
    @Path("/{learningBuildUID}")
	@ApiOperation(value = "Delete LearningBuild", notes = "Delete an LearningBuild", response = LearningBuildDTO.class,position=50)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public Response removeLearningBuild(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true) 
			@PathParam("learningBuildUID") String id) {

    	LearningBuildBo bo = (LearningBuildBo) this.createBoInstance();
    	
    	LearningBuildDTO resp = bo.deleteLearningBuildWithUID(id);
    	
		String json = toJSONString(resp);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
    
	@GET
	@UnitOfWork
	@Timed(name = "get-requests")
	@Path("/{learningBuildUID}/file")
	@ApiOperation(value = "Get list of files of the build", notes = "Get LearningFiles from LearningBuild", response = LearningFileInfoDTO.class, responseContainer = "List", position = 40)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully processed"),
			@ApiResponse(code = 400, message = "Invalid input", response = AktanaErrorResponseDTO.class),
			@ApiResponse(code = 500, message = "Server error", response = AktanaErrorResponseDTO.class) })
	public Response getBuildFiles(@Auth @ApiParam(access = "internal", required = false) UserPrincipalDTO user,
			@ApiParam(name = "learningBuildUID", value = "unique id for the LearningBuild", required = true) @PathParam("learningBuildUID") String learningBuildUID) {

		LearningBuildBo bo = (LearningBuildBo) this.createBoInstance();

		List<LearningFileInfoDTO> respList = bo.getFiles(learningBuildUID);

		String json = toJSONString(respList);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}
    
   
}
