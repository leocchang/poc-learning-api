package com.aktana.learning.api.resource.soap;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.dto.TokenRequestDTO;
import com.aktana.learning.api.dto.TokenResponseDTO;
import com.aktana.learning.api.resource.HibernateSessionHelper;
import com.aktana.learning.api.security.TokenGenerationHelper;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.cxf.annotations.WSDLDocumentation;
import org.apache.cxf.annotations.WSDLDocumentationCollection;
import org.apache.cxf.interceptor.OutFaultInterceptors;
import org.hibernate.Session;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@WebService(name="token", serviceName="TokenService", targetNamespace="http://api.learning.aktana.com/soap")
@OutFaultInterceptors(interceptors= {"com.aktana.learning.api.resource.soap.DefaultSoapFaultInterceptor"})
@WSDLDocumentationCollection(
	    {
	        @WSDLDocumentation("Service to retrieve token"),
	        @WSDLDocumentation(value = "Token Operations",
	                           placement = WSDLDocumentation.Placement.TOP),
	        @WSDLDocumentation(value = "Token Operations",
	                           placement = WSDLDocumentation.Placement.BINDING)
	    }
)
public class TokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenService.class);
    private TokenGenerationHelper requestHelper;

    public TokenService() {    	
    	requestHelper = new TokenGenerationHelper();    	
    }
    
    
    @Timed(name = "get-requests")
    @WebMethod
    @Metered    
    @WSDLDocumentation("Lookup token")        
    public TokenResponseDTO token(TokenRequestDTO tokenRequest) {    	    	
    	HibernateSessionHelper	helper = new HibernateSessionHelper(AktanaServiceDaos.getInstance().getSessionFactory());
		final Session session = helper.getSessionFactory().openSession();
        try {
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);
            String compact = "";
            TokenResponseDTO response = new TokenResponseDTO();
            try {
                LOGGER.info("Verifying user access...");    
                compact = requestHelper.validateTraditionalRequest(tokenRequest);
                response.setToken(compact);
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }

            return response;            
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        
    }
}
