/*****************************************************************
*
* @author $Author: adiel.cohen $
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.resource;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.resource.AbstractResourceSupport;
import com.aktana.learning.persistence.models.impl.OptimalLearningParams; 
import com.aktana.learning.api.bo.OptimalLearningParamsBo;
import com.aktana.learning.api.dto.OptimalLearningParamsDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.annotations.ApiResponse;

//import com.aktana.learning.persistence.models.impl.LearningRun; 


/** LearningSimulation Resource */
@Path("/v1.0/OptimalLearningParams")
@Api(value = "/v1.0/OptimalLearningParams", description = "Operations on OptimalLearningParams and OptimalPredictors",position=200) 
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class OptimalLearningParamsResource extends AbstractResourceSupport<OptimalLearningParamsDTO, OptimalLearningParams, String> {

   private static final Logger LOGGER = LoggerFactory.getLogger(OptimalLearningParamsResource.class);

   /**
    * @param type
    */
   public OptimalLearningParamsResource() {
       super(OptimalLearningParamsResource.class, OptimalLearningParamsDTO.class, (Class) OptimalLearningParamsBo.class);
   }
  
   
   @GET
   @UnitOfWork
   @Timed(name = "get-requests")
   @Path("/product/{productUID}/channel/{channelUID}/goal/{goal}/optimalParams")
	@ApiOperation(value = "Get list of optimal model params", notes = "List all the optimal params per product/ channel/ goal set", response = OptimalLearningParamsDTO.class, responseContainer = "List",position=10)
	@ApiResponses(value = {
			  @ApiResponse(code = 200, message = "Successfully processed"),
			  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
			  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
			})
   public Response listOptimalLearningParams(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user, 
		   @ApiParam(name = "productUID", value = "productUID for desired optimal params", required = true) @PathParam("productUID") String productUID,
		   @ApiParam(name = "channelUID", value = "channelUID for desired optimal params", required = true) @PathParam("channelUID") String channelUID,
		   @ApiParam(name = "goal", value = "goal for desired optimal params", required = true) @PathParam("goal") String goal) {
   	    	
	   OptimalLearningParamsBo bo = (OptimalLearningParamsBo) this.createBoInstance();
	   List<OptimalLearningParamsDTO> list = bo.getOptimalLearningParams(productUID, channelUID, goal);

		String json = toJSONString(list);

		return Response.ok(json, MediaType.APPLICATION_JSON).build();
   }  
    

   
}
