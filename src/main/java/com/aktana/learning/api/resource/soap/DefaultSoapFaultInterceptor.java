package com.aktana.learning.api.resource.soap;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.resource.exceptionmapper.AktanaExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.GenericExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.HibernateConstraintViolationExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.InvalidEntityExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.JsonMappingExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.JsonParseExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.UnmarshalExceptionMapper;
import com.aktana.learning.api.resource.exceptionmapper.ValidationExceptionMapper;
import com.aktana.learning.common.exception.AktanaException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class DefaultSoapFaultInterceptor extends AbstractSoapInterceptor {   

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSoapFaultInterceptor.class);
    private static final String APPLICATION_EXCEPTION =  "APP";
    private static final String SYSTEM_EXCEPTION =  "SYS";
	
	public DefaultSoapFaultInterceptor() 
	{ 
		super(Phase.MARSHAL); 
	} 
	
	@Override 
	public void handleMessage(SoapMessage message) throws Fault 
	{ 
		Fault f = (Fault) message.getContent(Exception.class);   
		Throwable cause = f.getCause(); 
		
		if (cause instanceof AktanaException) { 
			//LOGGER.info("Exception Thrown", cause); 
			AktanaException e = (AktanaException) cause; 			
			AktanaExceptionMapper mapper = new AktanaExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			
			if (response.getStatus() == null) {
				response.setStatus(400);				
			}
			
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}
			
			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   			
		} else if (cause instanceof JsonParseException)
		{ 
			JsonParseException e = (JsonParseException) cause; 			
			JsonParseExceptionMapper mapper = new JsonParseExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}
			
			//LOGGER.info("Exception Thrown", cause); 
			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   			
		} else if (cause instanceof JsonMappingException) 
		{
			//LOGGER.info("Exception Thrown", cause); 			
			JsonMappingException e = (JsonMappingException) cause; 			
			JsonMappingExceptionMapper mapper = new JsonMappingExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}
						
			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   									
		} else if (cause instanceof ConstraintViolationException)
		{
			//LOGGER.warn("Exception thrown ", cause); 
			ConstraintViolationException e = (ConstraintViolationException) cause; 			
			HibernateConstraintViolationExceptionMapper mapper = new HibernateConstraintViolationExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}
			
			
			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   											
		} else if (cause instanceof javax.validation.ConstraintViolationException)
		{
			//LOGGER.warn("Exception thrown ", cause); 
			javax.validation.ConstraintViolationException e = (javax.validation.ConstraintViolationException) cause; 			
			InvalidEntityExceptionMapper mapper = new InvalidEntityExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}

			
			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   														
		} else if (cause instanceof javax.validation.ValidationException)
		{
			//LOGGER.warn("Exception thrown ", cause); 
			javax.validation.ValidationException e = (javax.validation.ValidationException) cause; 			
			ValidationExceptionMapper mapper = new ValidationExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}

			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   														 
			
		} else if (cause instanceof javax.xml.bind.UnmarshalException)
		{
			//LOGGER.warn("Exception thrown ", cause); 
			javax.xml.bind.UnmarshalException e = (javax.xml.bind.UnmarshalException) cause; 			
			UnmarshalExceptionMapper mapper = new UnmarshalExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}

			f.setFaultCode(new QName("http://api.aktana.com/soap", APPLICATION_EXCEPTION));   														 			
		} else {
			LOGGER.warn("Exception thrown ", cause); 

			//LOGGER.warn("Exception thrown ", cause); 
			Exception e = (Exception) cause; 			
			GenericExceptionMapper mapper = new GenericExceptionMapper();
			ObjectMapper objmapper = new ObjectMapper();			
			AktanaErrorResponseDTO response = mapper.getErrorResponse(e);
			try {
				f.setMessage(objmapper.writeValueAsString(response));
			} catch (JsonProcessingException e1) {
				LOGGER.error("Error translating error message", e1);
			}
			
			f.setFaultCode(new QName("http://api.aktana.com/soap", SYSTEM_EXCEPTION));   														
		}
	} 	
}
	
