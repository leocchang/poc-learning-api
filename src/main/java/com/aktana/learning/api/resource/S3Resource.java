 /*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.bo.LearningConfigBo;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.security.TokenGenerationHelper;
import com.aktana.learning.common.exception.GenericValidationException;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import com.aktana.learning.common.learningS3.LearningS3ConnectionException;
import com.aktana.learning.common.learningS3.LearningS3Connector;

/** Build Status Information based on v2 implementation */
@Path("/v1.0/S3")
@Api(value = "/v1.0/S3", description = "basic s3 operations", position=20)
@Produces(MediaType.TEXT_PLAIN)
public class S3Resource {	
    
	
	@GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@Path("/pathPrefix")
	@ApiOperation(value = "make s3 test bucket", notes = "Create a new LearningConfig draft", response = String.class, position = 10)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String s3GetObjectPathHeader(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user) {
		try{ return LearningS3Connector.getInstance().getConnection().getFullS3path(); }
		catch (LearningS3ConnectionException e) { throw new  GenericValidationException(e.getMessage()); }
    }
	
    
	@GET
    @UnitOfWork
    @Timed(name = "get-requests")
	@Path("/{targetPath : .+}/objectKeys")
	@ApiOperation(value = "make s3 test bucket", notes = "Create a new LearningConfig draft", response = String.class, position = 10)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String s3GetObjectPaths(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="targetPath", value="a path", required=true) @PathParam("targetPath") String targetPath) {
		
		try { 
			if (targetPath.equals("")) {
				return LearningS3Connector.getInstance().callListKeys().replace(";", "\n");
			} else {
				return LearningS3Connector.getInstance().callListKeys(targetPath).replace(";", "\n");
			}
		} catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }		
    }

	@GET
    @UnitOfWork
    @Timed(name = "get-requests")
    @Path("/{targetPath : .+}/objectContent")
	@ApiOperation(value = "Get s3 object contnet", notes = "Get s3 object contnet with s3 key", response = String.class,position=30)
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String s3Download(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="targetPath", value="a path", required=true) @PathParam("targetPath") String targetPath) {

		try { return LearningS3Connector.getInstance().callDownload(targetPath); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }
    }
	
	@POST
    @UnitOfWork
    @Timed(name = "post-requests")
    @Path("/{targetPath : .+}/uploadObject")
	@ApiOperation(value = "Upload s3 object", notes = "Upload s3 object with s3 key", response = String.class,position=30)
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String s3Upload(@Auth @ApiParam(access="internal", required=false)  UserPrincipalDTO user,
    		@ApiParam(name="targetPath", value="a path", required=true) @PathParam("targetPath") String targetPath,
    		@ApiParam(name="targetContent", value="a path", required=true) String targetContent) {
		
		try { return LearningS3Connector.getInstance().callUpload(targetPath, targetContent); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }
	}
	
	
	@DELETE
    @UnitOfWork
	@Timed(name = "delete-requests")
    @Path("/{targetPath : .+}/deleteObject")
	@ApiOperation(value = "Delete s3 object", notes = "Delete s3 object with s3 key", response = String.class,position=30)
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Successfully processed"),
	  @ApiResponse(code = 400, message = "Invalid input", response=AktanaErrorResponseDTO.class),
	  @ApiResponse(code = 500, message = "Server error", response=AktanaErrorResponseDTO.class)
	})
    public String s3Delete(@Auth @ApiParam(access="internal", required=false) UserPrincipalDTO user,
    		@ApiParam(name = "targetPath", value = "a path", required = true) @PathParam("targetPath") String targetPath) {
		
		try { LearningS3Connector.getInstance().callDownload(targetPath); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }
		
		try { return LearningS3Connector.getInstance().callDelete(targetPath); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }
    }

}