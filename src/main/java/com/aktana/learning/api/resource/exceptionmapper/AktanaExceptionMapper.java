/*****************************************************************
 * 
 * AktanaExceptionMapper is responsible to translate AktanaException (or exceptions derived from AktanaException) into error that is returned by the EndPoint.
 * 
 * 
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20346 $ on $Date: 2014-06-28 09:51:34 -0700 (Sat, 28 Jun 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.resource.exceptionmapper;

import java.util.ArrayList;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.AktanaErrorDTO;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.common.exception.AktanaException;

@Provider
public class AktanaExceptionMapper implements ExceptionMapper<AktanaException> {
    private ObjectMapper _mapper;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    public AktanaExceptionMapper() {
        LOGGER = LoggerFactory.getLogger(getClass());

        _mapper = new ObjectMapper();
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

//        notificator = new AktanaAirbrakeNotifier();
        // _airbrakeNotifier = null;
    }


    public AktanaErrorResponseDTO getErrorResponse(AktanaException ex) {
    	AktanaErrorResponseDTO errorResponse = new AktanaErrorResponseDTO();

    	errorResponse.setStatus(ex.getStatus());
    	errorResponse.setMessage(ex.getMessage());
    	errorResponse.setErrors(new ArrayList<AktanaErrorDTO>());
    	
    	if (ex.getErrors() != null) {
    		for(int i=0;i<ex.getErrors().size();++i) {
    			AktanaErrorDTO err = new AktanaErrorDTO();
    			
    			err.setCode(ex.getErrors().get(i).getCode());
    			err.setField(ex.getErrors().get(i).getField());
    			err.setMessage(ex.getErrors().get(i).getMessage());        			
    			
    			errorResponse.getErrors().add(err);
    		}
    	}
    	
    	return errorResponse;
    }
    
    
    
    @Override
    /**
     * interface method that is called by JAX-RS to convert exception into JSON
     */
    public Response toResponse(AktanaException ex) {
        try {        	
        	AktanaErrorResponseDTO errorResponse = getErrorResponse(ex);
        	       	
//            _errorInfo.populate(ex);
//            String requestData = getRequestData(filter.getCachedRequest());
//            logException(ex, requestData);
//            notifyException(ex);

            // AktanaExceptions will have the status returned in the Exception. It should be mostly 400
            return Response.status(ex.getStatus())
            		.entity(_mapper.writeValueAsString(errorResponse))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (Exception ex2) {
        	ex2.printStackTrace();
            throw new RuntimeException(ex2.toString());
        }
    }

    /* COMMENTED out until the productionization effort 
    private void notifyException(Exception ex) throws IOException {
        boolean notificable = isNotificable(ex);
        if (!notificable)
            return;
        
        notificator.sendNotification(filter.getCachedRequest(), ex);

    }

    private boolean isNotificable(Exception ex) {
        if (ex.getClass() == com.sun.jersey.api.NotFoundException.class)
            return false;

        // If is an AktanaException, it should not be notificable
        // because it is a Aktana-functionality related exception
        if (ex instanceof AktanaException)
            return false;

        // This is the duplicate key case...I guess we don't want to send an airbrake here
        if (ex instanceof ConstraintViolationException)
            return false;

        // For every no Aktana exception, notify
        // maybe we will need a blacklist containing the
        // exception classes that should not notify
        return true;
    }

    private void logException(Exception ex, String requestData) {
        String className = ex.getStackTrace()[0].getClassName();
        String methodName = ex.getStackTrace()[0].getMethodName();
        String exceptionMsg = ex.getMessage();
        LOGGER.error("Headers {} ", requestData);
        LOGGER.error("Exception in '{}.{}()': {}. Exception information: {}", className, methodName, exceptionMsg, ex);
    }

    private String getRequestData(HttpServletRequest request) throws IOException {
        String sessionId = request.getRequestedSessionId() == null ? " " : request
                .getRequestedSessionId();
        StringBuilder requestData = new StringBuilder(String.format("HOST %s URL:%s", request.getRemoteHost(),
                request.getRequestURI()));
        requestData.append(String.format("\t(Body request {%s} ", notificator.getBodyRequest(request)));
        requestData.append(String.format(":METHOD {%s}", request.getMethod()));
        requestData.append(String.format(" :SessionID {%s}", sessionId));

        return requestData.toString();
    }
    */

}
