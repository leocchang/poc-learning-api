package com.aktana.learning.api.health;

import java.net.URI; 

import javax.ws.rs.core.MediaType;

import  javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

import com.aktana.learning.common.dse.DSEAPIEnum;
import com.codahale.metrics.health.HealthCheck;

public class PullServicesHealthChecker extends HealthCheck {

	private String serviceName;
	private Client client;
	private URI pullServiceURI;
	
	public PullServicesHealthChecker(String name, Client client, URI pullServiceURI) {
//		super(String.format("Checking Health for service '%s'", name));
		this.serviceName = name;
		this.client = client;
		this.pullServiceURI = pullServiceURI;
	}

	
	@Override
	protected Result check() throws Exception {
		String response = invokepullServiceCheck();

		if (response == null) {
			Result.unhealthy("Pull Services URI '%s'", this.serviceName);			
		}
		
		return Result.healthy();
	}

    private String invokepullServiceCheck() {  
    	client = ClientBuilder.newClient();
	WebTarget target = client.target(pullServiceURI).path("api").path("info");
	Invocation.Builder invocation = target.request();  	
    	String response = invocation.accept(MediaType.TEXT_HTML).get(String.class);
    	
    	return response;
    }
	
}
