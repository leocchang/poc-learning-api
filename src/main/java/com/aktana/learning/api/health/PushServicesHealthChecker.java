package com.aktana.learning.api.health;

import com.aktana.learning.api.AktanaServiceDaos;
import com.codahale.metrics.health.HealthCheck;

public class PushServicesHealthChecker extends HealthCheck {

	private String serviceName;
	
	public PushServicesHealthChecker(String name) {
//		super(String.format("Checking Health for service '%s'", name));
		this.serviceName = name;		
	}

	@Override
	protected Result check() throws Exception {
		
		// verify if database is up		
		if (AktanaServiceDaos.getInstance().getSessionFactory() == null) {
			Result.unhealthy("Database is unavailable for service '%s'", this.serviceName);
		} 
						
		return Result.healthy();
	}

}
