package com.aktana.learning.api;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DSEServiceConfiguration {
	
	@JsonProperty
    @NotNull
    private String username;

    @JsonProperty
    @NotNull
    private String password;

    @JsonProperty
    private String secret;

    @JsonProperty
    @NotNull
    private String url;

  

	/**
     * Returns the username used to connect to DSE API
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username to connect to DSE API
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the password used to connect to DSE API
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password to be used in the connection
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the security token. If not defined sets to an empty string.
     * @return the securityToken
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Sets the security token if exists
     * @param securityToken
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * Returns the authentication endpoint to connect to DSE API
     * @return the authEndpoint
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the auth endpoint to connect to DSE API
     * @param authEndpoint
     */
    public void setUrl(String url) {
        this.url = url;
    }

}
