package com.aktana.learning.api;
import java.util.Properties;

import io.dropwizard.db.DataSourceFactory; 
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.SessionFactoryFactory;
import io.dropwizard.setup.Environment;

import javax.inject.Named;

import org.hibernate.SessionFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.annotation.Transactional;
import com.aktana.learning.api.interceptor.TransactionInterceptor;

import com.fiestacabin.dropwizard.quartz.SchedulerConfiguration;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import static com.google.inject.matcher.Matchers.*;


public class AktanaServiceModule extends AbstractModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaServiceModule.class);

    private final HibernateBundle<AktanaServiceConfiguration> hibernateBundle =
            new HibernateBundle<AktanaServiceConfiguration>(AktanaService.getHibernateMappings(), new SessionFactoryFactory())
            {
				@Override
				public DataSourceFactory getDataSourceFactory(
						AktanaServiceConfiguration configuration) {
					return configuration.getDatabaseConfiguration();
				}
    };
		
	@Override
	protected void configure() {
		
		bindInterceptor(				
	            annotatedWith(Transactional.class),
	            any(),
	            new TransactionInterceptor());				
		
		bind(AktanaServiceDaos.class);		
	}

	
	@Provides
	@Named("customer")
	public String provideCustomer(AktanaServiceConfiguration configuration) {
		return configuration.getTheCustomerEnvironmentName();
	}	

	@Provides
	public SessionFactory provideSessionFactory(AktanaServiceConfiguration configuration, Environment environment) {
		SessionFactory sf = hibernateBundle.getSessionFactory();
        AktanaServiceDaos daos = AktanaServiceDaos.getInstance();
		
	    if (sf == null) {
	        try {
	            hibernateBundle.run(configuration, environment);
	            daos.setSessionFactory(hibernateBundle.getSessionFactory());	            
	            return hibernateBundle.getSessionFactory();
	        } catch (Exception e) {
	            LOGGER.error("Unable to run hibernatebundle", e);
	        }
	    } else {
	        return sf;
	    }		
	    
		return hibernateBundle.getSessionFactory();
	}	

		
}
