/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api;

import com.fasterxml.jackson.annotation.JsonProperty; 

import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/** Service Configuration based on v2 */
public class AktanaServiceConfiguration extends Configuration {

	// subdirectory name from which CustomMetadata.json is loaded. It is usually in resources/custom/<name>/ directory
	@NotEmpty
    private String theCustomerEnvironmentName;
	
	@NotEmpty
    @JsonProperty("deploymentEnvironment")
    private String deploymentEnvironment;

    private String copystormSchemaName;

	@NotEmpty
    private String pullservicesUrl;

	
	@Valid
    @NotNull
    @JsonProperty
	private AktanaSubscriptionServiceConfiguration subscriptionservices = new AktanaSubscriptionServiceConfiguration();
	
	
	@Valid
    @NotNull
    @JsonProperty
	private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

	
    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    //---- V2 sub-configurations added to V3 as part of MAIN-11812 ---
    
    @Valid
    @NotNull
    @JsonProperty
    private PasswordResetConfiguration passwordReset = new PasswordResetConfiguration();
    
    //---- 
    
    @Valid
    @NotNull
    @JsonProperty("dseService")
    private DSEServiceConfiguration dseService = new DSEServiceConfiguration();
    
    
    @Valid
    @NotNull
    @JsonProperty("rundeckService")
    private RundeckServiceConfiguration rundeckService = new RundeckServiceConfiguration();
    
    @Valid
    @NotNull
    @JsonProperty
    private AimConfiguration aim = new AimConfiguration();

    @Valid
    @NotNull
    @JsonProperty("learningS3Service")
    private LearningS3ServiceConfiguration learningS3Service = new LearningS3ServiceConfiguration();
    
    //------------------------- public accessor methods ---------------------
    
    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return httpClient;
    }	
    


    public DataSourceFactory getDatabaseConfiguration() {
        return database;
    }    

    public void setDatabaseConfiguration(DataSourceFactory databaseConfiguration) {
        this.database = databaseConfiguration;
    }

    
    
    

	public String getPullservicesUrl() {
		return pullservicesUrl;
	}


	public void setPullservicesUrl(String pullservicesUrl) {
		this.pullservicesUrl = pullservicesUrl;
	}



	public AktanaSubscriptionServiceConfiguration getSubscriptionservices() {
		return subscriptionservices;
	}



	public void setSubscriptionservices(
			AktanaSubscriptionServiceConfiguration subscriptionservices) {
		this.subscriptionservices = subscriptionservices;
	}



	public String getTheCustomerEnvironmentName() {
		return theCustomerEnvironmentName;
	}

	public void setTheCustomerEnvironmentName(String theCustomerEnvironmentName) {
		this.theCustomerEnvironmentName = theCustomerEnvironmentName;
		TheCustomerEnvironmentName.setName(theCustomerEnvironmentName);
	}
	
	public String getDeploymentEnvironment() {
		return deploymentEnvironment;
	}

	public void setDeploymentEnvironment(String deploymentEnvironment) {
		this.deploymentEnvironment = deploymentEnvironment;
		DeploymentEnvironment.setEnvironment(deploymentEnvironment);
	}
	
    public String getCopystormSchemaName() {
        return copystormSchemaName;
    }

    public void setCopystormSchemaName(String copystormSchemaName) {
        this.copystormSchemaName = copystormSchemaName;
    }

    public final PasswordResetConfiguration getPasswordResetConfiguration()
    {
        return passwordReset;
    }

    
	public DSEServiceConfiguration getDSEServiceConfiguration() {
		return dseService;
	}



	public void setDSEServiceConfiguration(
			DSEServiceConfiguration dseService) {
		this.dseService = dseService;
	}
	
	public RundeckServiceConfiguration getRundeckServiceConfiguration() {
		return rundeckService;
	}

	public void setRundeckServiceConfiguration(
			RundeckServiceConfiguration rundeckService) {
		this.rundeckService = rundeckService;
	}
	
	public final AimConfiguration getAimConfiguration() { return aim; }

	public void setLearningS3ServiceConfiguration(
			LearningS3ServiceConfiguration learningS3Service) {
		this.learningS3Service = learningS3Service;
	}
        
	public LearningS3ServiceConfiguration getLearningS3ServiceConfiguration() {
		return learningS3Service;
	}

}
