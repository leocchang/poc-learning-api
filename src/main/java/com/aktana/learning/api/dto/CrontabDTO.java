/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.dto;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A crontab string")
@Data
public class CrontabDTO {
	@ApiModelProperty(required=true, value="A rundeck job id")
	@Length(min=1, max=80)	
	private String jobId;
	
	@ApiModelProperty(required=true, value="A rundeck job name")
	private String jobName;	
	
	@ApiModelProperty(required=true, value="A crontab string")
	@Length(min=1, max=80)	
	private String crontab;	
	
}
