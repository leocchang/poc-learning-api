package com.aktana.learning.api.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** TokenRequest Representation */
@ApiModel(value = "A request for token")
@Data
public class TokenRequestDTO {
	@ApiModelProperty(required=true, value="user name for the request")
	@NotBlank			
	private String username;

	@ApiModelProperty(required=true, value="password for the user")
	@NotBlank				
	private String password;

	@ApiModelProperty(required=true, value="secret for the partner")
	@NotBlank					
	private String secret;
}
