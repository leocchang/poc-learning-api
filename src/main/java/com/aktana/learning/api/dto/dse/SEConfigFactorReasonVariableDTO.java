package com.aktana.learning.api.dto.dse;

import java.io.Serializable;

public class SEConfigFactorReasonVariableDTO implements Serializable {

	private String name;
	private String displayName;
	private String description;
	private String exampleValue;
	private String supportedDestination;
	private String ruleType;
	private String categoryId;
	private String defaultValue;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExampleValue() {
		return exampleValue;
	}
	public void setExampleValue(String exampleValue) {
		this.exampleValue = exampleValue;
	}
	
	public String getSupportedDestination() {
		return supportedDestination;
	}
	public void setSupportedDestination(String supportedDestination) {
		this.supportedDestination = supportedDestination;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
}
