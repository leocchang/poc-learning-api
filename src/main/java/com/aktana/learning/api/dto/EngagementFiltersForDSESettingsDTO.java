/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config param")
@Data
public class EngagementFiltersForDSESettingsDTO implements IExternallyIdentifiableDTO {

	@ApiModelProperty(required=true, value="Parameter index to specify order is case of multiple items")
	@NotNull
	private Integer seConfigId;
	
	@ApiModelProperty(required=true, value="Suggestion Type should be one of: {trigger, target}")
	@NotBlank
	@Length(min=1, max=8, message="suggestionType must be between 1 and 7 characters in length")	
	private String suggestionType;
	
	@ApiModelProperty(required=false, value="Comma separated rep team UIDs")
	private String repTeamUIDs;
	
	@ApiModelProperty(required=false, value="Comma separated territory Ids")
	private String territoryIds;

	@ApiModelProperty(required=false, value="bottom percentile")
	@Range(min=1, message="startYear must be greater than or equal to 1")
	private Integer startYear;
	
	@ApiModelProperty(required=false, value="bottom percentile")
	@Range(min=1, max=12, message="startMonth must be between 1 and 12 inclusive")
	private Integer startMonth;
	
	@ApiModelProperty(required=false, value="Rep Threshold Type should be one of: {mean, median, mode, bottom}")
	@NotBlank
	@Length(min=1, max=8, message="repThresholdType must be between 1 and 7 characters in length")	
	private String repThresholdType;
	
	@ApiModelProperty(required=false, value="bottom percentile")
	@Range(min=1, max=100, message="bottomPercentile must be between 1 and 100 inclusive")
	private Integer bottomPercentile;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return ((seConfigId != null) ? seConfigId : "") + "~" + ((suggestionType != null) ? suggestionType : "");
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}

