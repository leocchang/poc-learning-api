package com.aktana.learning.api.dto.dse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SEConfigDimensionValueDTO {
	// non-JSON attribute, to hold the integer id value
	private int integerId;
	// JSON attributes
	private String id; // the integer id, as a String
	private String name;
	private String description;
	private String parent;

	// Note: this thing with hold the id as both an int and a String is because
	// the Front End team is insisting that the data type should be consistent here,
	// and how user-specified dimension id values are passed in the SEConfigSpecDTO JSON.
	// In the SEConfigSpecDTO JSON, for any field that corresponds to an SE "parameter",
	// the value is always passed as a String, regardless of the parameter's data type.
	// This is necessary because parameter data types such as "int[]" and "double[]" are
	// handled in the SE as a comma-delimited String.
	
	@JsonIgnore
	public int getIntegerId() {
		return integerId;
	}

	@JsonIgnore
	public void setIntegerId(int integerId) {
		this.integerId = integerId;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	@JsonProperty
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty
	public String getDescription() {
		return description;
	}

	@JsonProperty
	public void setDescription(String description) {
		this.description = description;
	}
		
	@JsonProperty
	public String getParent() {
		return parent;
	}

	@JsonProperty
	public void setParent(String parent) {
		this.parent = parent;
	}
}
