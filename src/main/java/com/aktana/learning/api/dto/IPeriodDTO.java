/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import com.aktana.learning.api.constraint.ValidDateString;
import com.aktana.learning.api.constraint.ValidPeriodDTO;


/** 
 * Interface for any Aktana DTO that has String attributes "startDate" and "endDate".
 * 
 * Any DTO that implements this interface will have the full set of annotation-driven
 * validations performed on its (startDate,endDate) attributes, including startDate<=endDate.
 * 
 * Notes: 
 * (1) Aktana convention is that all dates that are expressed as String in
 * 		any DTO should use PureDate.AKTANA_PURE_DATE_FORMAT java format.
 * (2) @ValidDateString triggers Aktana custom validation on each field, separately
 * (3) @ValidPeriodDTO triggers Aktana custom cross-field validation of startDate <= endDate
 */
@ValidPeriodDTO
public interface IPeriodDTO {
	
	@ValidDateString(fieldName="startDate")
	public String getStartDate();
	
	public void setStartDate(String aDate);
	
	@ValidDateString(fieldName="endDate")
	public String getEndDate();

	public void setEndDate(String aDate);
}
