package com.aktana.learning.api.dto.dse;

import java.util.ArrayList;
import java.util.List;

public class SEConfigStaticReferenceDataDTO {

	// SE metadata
	protected List<SEConfigDimensionTypeDTO> dimensions;
	protected List<SEConfigCategoryTypeDTO> reasonCategories;
	protected List<SEConfigFactorTypeDTO> factorTypes;
	protected List<SEConfigMessageStrategyTypeDTO> messageStrategyTypes;
	protected List<SEConfigGlobalParameterTypeDTO> globalParameterTypes;
	
	// related application data
	protected List<SEConfigDimensionValueDTO> eventTypes;
	protected List<SEConfigDimensionValueDTO> fixedDateTypes;
	protected List<SEConfigCustomAttributeDTO> metrics;	//NOTE: "metrics" = numeric AccountProduct attributes
	protected List<SEConfigDimensionValueDTO> labelTypes;
	protected List<SEConfigDimensionValueDTO> products;
	protected List<SEConfigDimensionValueDTO> courses;
	protected List<SEConfigDimensionValueDTO> messageChannels;
	protected List<SEConfigDimensionValueDTO> repActionChannels;
	protected List<SEConfigDimensionValueDTO> repActionTypes;
	protected List<SEConfigDimensionValueDTO> crmFieldNames;
	protected List<SEConfigDimensionValueDTO> reasonDestinations;
	protected List<SEConfigDimensionValueDTO> ruleTypes;
	
	// Account Attribute metadata
	protected List<SEConfigCustomAttributeDTO> accountAttributes; // attribute metadata to be sent down to Front End

	// Event Selector Attribute metadata
	protected List<SEConfigCustomAttributeDTO> eventSelectorAttributes; // event selector attribute metadata to be sent down to Front End
	
	// Rep Account Attribute metadata
	protected List<SEConfigCustomAttributeDTO> repAccountAttributes; // repAccount attribute metadata to be sent down to Front End
	
	public SEConfigStaticReferenceDataDTO() {
	}

	public List<SEConfigGlobalParameterTypeDTO> getGlobalParameterTypes() {
		// lazy initialization
		if (null == globalParameterTypes) {
			globalParameterTypes = new ArrayList<SEConfigGlobalParameterTypeDTO>();
		}
		return globalParameterTypes;
	}

	public void setGlobalParameterTypes(List<SEConfigGlobalParameterTypeDTO> globalParameterTypes) {
		this.globalParameterTypes = globalParameterTypes;
	}
	
	public List<SEConfigDimensionTypeDTO> getDimensions() {
		// lazy initialization
		if (null == dimensions) {
			dimensions = new ArrayList<SEConfigDimensionTypeDTO>();
		}
		return dimensions;
	}

	public void setDimensions(List<SEConfigDimensionTypeDTO> dimensions) {
		this.dimensions = dimensions;
	}
	
	public List<SEConfigCategoryTypeDTO> getReasonCategories() {
		// lazy initialization
		if (null == reasonCategories) {
			reasonCategories = new ArrayList<SEConfigCategoryTypeDTO>();
		}
		return reasonCategories;
	}

	public void setReasonCategories(List<SEConfigCategoryTypeDTO> reasonCategories) {
		this.reasonCategories = reasonCategories;
	}

	
	public List<SEConfigMessageStrategyTypeDTO> getMessageStrategyTypes() {
		// lazy initialization
		if (null == messageStrategyTypes) {
			messageStrategyTypes = new ArrayList<SEConfigMessageStrategyTypeDTO>();
		}
		return messageStrategyTypes;
	}

	public void setMessageStrategyTypes(List<SEConfigMessageStrategyTypeDTO> messageStrategyTypes) {
		this.messageStrategyTypes = messageStrategyTypes;
	}
	
	
	
	public List<SEConfigFactorTypeDTO> getFactorTypes() {
		// lazy initialization
		if (null == factorTypes) {
			factorTypes = new ArrayList<SEConfigFactorTypeDTO>();
		}
		return factorTypes;
	}

	public void setFactorTypes(List<SEConfigFactorTypeDTO> factorTypes) {
		this.factorTypes = factorTypes;
	}

	public List<SEConfigDimensionValueDTO> getEventTypes() {
		// lazy initialization
		if (null == eventTypes) {
			eventTypes = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return eventTypes;
	}

	public void setEventTypes(List<SEConfigDimensionValueDTO> eventTypes) {
		this.eventTypes = eventTypes;
	}
	
	public List<SEConfigDimensionValueDTO> getFixedDateTypes() {
		// lazy initialization
		if (null == fixedDateTypes) {
			fixedDateTypes = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return fixedDateTypes;
	}

	public void setFixedDateTypes(List<SEConfigDimensionValueDTO> fixedDateTypes) {
		this.fixedDateTypes = fixedDateTypes;
	}

	//NOTE: "metrics" = numeric AccountProduct attributes
	public List<SEConfigCustomAttributeDTO> getMetrics() {
		// lazy initialization
		if (null == metrics) {
			metrics = new ArrayList<SEConfigCustomAttributeDTO>();
		}
		return metrics;
	}

	//NOTE: "metrics" = numeric AccountProduct attributes
	public void setMetrics(List<SEConfigCustomAttributeDTO> metrics) { 
		this.metrics = metrics;
	}
	
	public List<SEConfigDimensionValueDTO> getLabelTypes() {
		// lazy initialization
		if (null == labelTypes) {
			labelTypes = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return labelTypes;
	}

	public void setLabelTypes(List<SEConfigDimensionValueDTO> labelTypes) {
		this.labelTypes = labelTypes;
	}

	public List<SEConfigDimensionValueDTO> getProducts() {
		// lazy initialization
		if (null == products) {
			products = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return products;
	}

	public void setProducts(List<SEConfigDimensionValueDTO> products) {
		this.products = products;
	}

	public List<SEConfigDimensionValueDTO> getCourses() {
		// lazy initialization
		if (null == courses) {
			courses = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return courses;
	}

	public void setCourses(List<SEConfigDimensionValueDTO> courses) {
		this.courses = courses;
	}

	public List<SEConfigDimensionValueDTO> getMessageChannels() {
		// lazy initialization
		if (null == messageChannels) {
			messageChannels = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return messageChannels;
	}

	public void setMessageChannels(List<SEConfigDimensionValueDTO> messageChannels) {
		this.messageChannels = messageChannels;
	}

	public List<SEConfigDimensionValueDTO> getRepActionChannels() {
		// lazy initialization
		if (null == repActionChannels) {
			repActionChannels = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return repActionChannels;
	}

	public void setRepActionChannels(List<SEConfigDimensionValueDTO> repActionChannels) {
		this.repActionChannels = repActionChannels;
	}
	
	public List<SEConfigDimensionValueDTO> getRepActionTypes() {
		// lazy initialization
		if (null == repActionTypes) {
			repActionTypes = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return repActionTypes;
	}
	
	public void setRepActionTypes(List<SEConfigDimensionValueDTO> repActionTypes) {
		this.repActionTypes = repActionTypes;
	}
	
	public List<SEConfigCustomAttributeDTO> getAccountAttributes() {
		return accountAttributes;
	}

	public void setAccountAttributes(
			List<SEConfigCustomAttributeDTO> accountAttributes) {
		this.accountAttributes = accountAttributes;
	}
	
	public List<SEConfigCustomAttributeDTO> getEventSelectorAttributes() {
		return eventSelectorAttributes;
	}

	public void setEventSelectorAttributes(List<SEConfigCustomAttributeDTO> eventSelectorAttributes) {
		this.eventSelectorAttributes = eventSelectorAttributes;
	}

	public List<SEConfigDimensionValueDTO> getCrmFieldNames() {
		return crmFieldNames;
	}

	public void setCrmFieldNames(List<SEConfigDimensionValueDTO> crmFieldNames) {
		this.crmFieldNames = crmFieldNames;
	}

	public List<SEConfigCustomAttributeDTO> getRepAccountAttributes() {
		return repAccountAttributes;
	}

	public void setRepAccountAttributes(
			List<SEConfigCustomAttributeDTO> repAccountAttributes) {
		this.repAccountAttributes = repAccountAttributes;
	}
	
	public List<SEConfigDimensionValueDTO> getReasonDestinations() {
		// lazy initialization
		if (null == reasonDestinations) {
			reasonDestinations = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return reasonDestinations;
	}
	
	public void setReasonDestinations(List<SEConfigDimensionValueDTO> reasonDestinations) {
		this.reasonDestinations = reasonDestinations;
	}
	
	public List<SEConfigDimensionValueDTO> getRuleTypes() {
		// lazy initialization
		if (null == ruleTypes) {
			ruleTypes = new ArrayList<SEConfigDimensionValueDTO>();
		}
		return ruleTypes;
	}
	
	public void setRuleTypes(List<SEConfigDimensionValueDTO> ruleTypes) {
		this.ruleTypes = ruleTypes;
	}
}
