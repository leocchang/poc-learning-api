/*****************************************************************
 * <p>Password Reset Business Object</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.useradmin;

import java.io.Serializable;

public class PasswordResetDTO implements Serializable {


    private String userName;
    private String password;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
