/*****************************************************************
 *
 * @author $Author: robert.flint $
 * @version $Revision: 20139 $ on $Date: 2014-05-30 08:05:05 -0700 (Fri, 30 May 2014) $ by $Author: robert.flint $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** 
 * Aktana Error Representation 
 * 
 */
@ApiModel(value = "Represents an Error")
@Data()
public class AktanaErrorDTO {	
	public AktanaErrorDTO() {		
	}
	
	@ApiModelProperty(value = "AKTANA internal error code", required=true)
	private Integer code;

	@ApiModelProperty(value = "field name", required=false)
	private String field;
	
	@ApiModelProperty(value = "message", required=true)
	private String message;
}
