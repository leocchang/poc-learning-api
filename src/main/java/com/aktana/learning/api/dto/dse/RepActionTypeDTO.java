/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2015 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** RepActionType Representation */
@ApiModel(value = "RepActionType")
@Data
public class RepActionTypeDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank		
	@Length(min=1, max=20, message="repActionTypeUID must be between 1 and 20 characters in length")	
	private String repActionTypeUID;
	
	@ApiModelProperty(required=false, value="Internal DSE unique identifier - read only")
	@Range(min=0, max=0, message="read only value")	
	private Integer repActionTypeId;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return repActionTypeUID;
	}	
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}

