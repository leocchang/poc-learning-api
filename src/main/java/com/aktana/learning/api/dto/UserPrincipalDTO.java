package com.aktana.learning.api.dto;

import java.security.Principal;
import java.util.Set;

import com.aktana.learning.api.security.AktanaAimPermissionEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;

/** UserPrincipal Representation */
@ApiModel(value = "Identifies a user who is using the API")
@Data
public class UserPrincipalDTO implements Principal {
	@JsonIgnore
	private String name;
	private Set<String> aimPermissions;

	@JsonIgnore
	public boolean isAdminUser() {
		return aimPermissions != null && aimPermissions.contains(AktanaAimPermissionEnum.USER.getCode());
	}
}
