/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.util.List;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** CustomerConfigurations Representation */
@ApiModel(value = "DSE engagement-related parameters")
@Data
public class DSEParametersForREMDTO {
	
	@ApiModelProperty(required=true, value="enabledRepActionChannels value aggrigated over all DSE configs")
	@NotBlank
	private String enabledRepActionChannels;
	
	@ApiModelProperty(required=true, value="numPastDaysToAssumeActionIsCompleted value aggrigated over all DSE configs")
	@NotBlank
	private Integer numPastDaysToAssumeActionIsCompleted;
	
	@ApiModelProperty(required=true, value="maxFutureCalendarDatesForSuggestingInCRM value aggrigated over all DSE configs")
	@NotBlank
	private Integer maxFutureCalendarDatesForSuggestingInCRM;
	
	@ApiModelProperty(required=true, value="enabled repActionTypes, mapped from enabledRepActionChannels")
	@NotBlank
	private List<ChannelActionMapDTO> enabledRepActionTypes;
				
}
