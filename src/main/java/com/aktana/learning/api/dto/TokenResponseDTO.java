package com.aktana.learning.api.dto;

import lombok.Data;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** TokenRequest Response Representation for SOAP */
@ApiModel(value = "Response for token request")
@Data
public class TokenResponseDTO {
	@ApiModelProperty(required=true, value="token for the request")
	private String token;
}
