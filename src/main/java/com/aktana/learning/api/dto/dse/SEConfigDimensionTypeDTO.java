package com.aktana.learning.api.dto.dse;

public class SEConfigDimensionTypeDTO {
	private String id;
	private String shortName;	// unique name
	private String name;		// optional description
	private String parentDimensionId; // optional parent dimension id
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortUniqueName) {
		this.shortName = shortUniqueName;
	}

	public String getName() {
		return name;
	}

	public void setName(String descriptiveName) {
		this.name = descriptiveName;
	}

	public String getParentDimensionId() {
		return parentDimensionId;
	}

	public void setParentDimensionId(String parentDimensionId) {
		this.parentDimensionId = parentDimensionId;
	}
}
