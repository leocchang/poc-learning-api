package com.aktana.learning.api.dto.dse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;



/** TimeToEngageAlgorithm Representation */
@ApiModel(value = "A time to engage algorithm is an object having a unique name, id that describes a selection algorithm for the new time to engage(s) in a set to be delivered ")
@Data
public class TimeToEngageAlgorithmDTO implements IExternallyIdentifiableDTO {

	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for the time to engage algorithm")
	@NotBlank		
	@Length(min=1, max=40, message="tteAlgorithmUID must be between 1 and 40 characters in length")
	private String tteAlgorithmUID;
	@ApiModelProperty(required=true, value="A the time to engage algorithm name that (also) uniquely identifies the the time to engage algorithm.")
	@NotBlank		
	@Length(min=1, max=255, message="tteAlgorithmName must be between 1 and 255 characters in length")
	private String tteAlgorithmName;
	@ApiModelProperty(required=false, value="Description of the time to engage algorithm")
	@Length(min=0, max=255, message="tteAlgorithmDescription must be between 0 and 255 characters in length")
	private String tteAlgorithmDescription;
	@ApiModelProperty(required=true, value="true=the tteAlgorithm is active, false=the tteAlgorithm is not active.")
	private Boolean isActive;
	@ApiModelProperty(required=true, value="true=the tteAlgorithm has been deleted, false=the tteAlgorithm is no deleted.")
	private Boolean isDeleted;
			
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return tteAlgorithmUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}

