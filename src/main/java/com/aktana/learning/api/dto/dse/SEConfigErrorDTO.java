package com.aktana.learning.api.dto.dse;

public class SEConfigErrorDTO {
	
	public static final String CONFIG_ERROR_SYNTAX = "syntax";
	public static final String CONFIG_ERROR_REFERENCE = "reference";

	protected String type; // syntax, reference
	protected String message;
	protected String description;
	

	
	public SEConfigErrorDTO(String type, String message) {
		this(type, message, message);
	}

	public SEConfigErrorDTO(String type, String message, String description) {
		this.type = type;
		this.message = message;
		this.description = description;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
}
