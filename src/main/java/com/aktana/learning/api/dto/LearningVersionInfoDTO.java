/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config version")
@Data
public class LearningVersionInfoDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {

	@ApiModelProperty(required=true, value="Externally-assigned unique version identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unique and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique config identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigUID must be unique and between 1 and 80 characters in length")	
	private String learningConfigUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier current build")
	@Length(min=0, max=40, message="optional currentLearningBuildUID, if present must between 1 and 40 characters in length")	
	private String currentLearningBuildUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned current build status")
	@Length(min=0, max=40, message="optional currentLearningBuildUIDStatus, if present must between 1 and 40 characters in length")	
	private String currentLearningBuildUIDStatus;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String currentLearningBuildUIDstartDateTime;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier run")
	@Length(min=0, max=40, message="optional latestRunUID, if present must between 1 and 40 characters in length")	
	private String latestRunUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier run status")
	@Length(min=0, max=40, message="optional latestRunUIDStatus, if present must between 1 and 40 characters in length")	
	private String latestRunUIDStatus;
	
	@ApiModelProperty(required=false, value="Boolean identifier: is run publihed")
	private Boolean latestRunUIDIsPublished;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDCreatedAt;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDupdatedAt;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDStartDateTime;
	
	@ApiModelProperty(required=true, value="true=the learningVersion has been finalized, false=the learningVersion has not been finalized")
	@NotNull	
	private Boolean isFinalized;
	@ApiModelProperty(required=true, value="true=the learningVersion is the deployed version, false=the learningVersion is not deployed")
	@NotNull	
	private Boolean isDeployed;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String currentStartDateTime;

	@ApiModelProperty(required=true, value="true=the learningVersion has been deleted, false=the learningVersion is active")
	@NotNull
	private Boolean isDeleted;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningVersionUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
