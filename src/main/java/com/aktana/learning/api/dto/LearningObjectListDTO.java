/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningObjectList Representation */
@ApiModel(value = "A learning objecct")
@Data
public class LearningObjectListDTO {
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned run unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unqiue and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned object unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="objectUID must be unqiue and between 1 and 80 characters in length")	
	private String objectUID;
	
	@ApiModelProperty(required=true, value="list type identifier")
	@NotBlank
	@Length(min=1, max=80, message="listType must be between 1 and 80 characters in length")	
	private String listType;
	
	@ApiModelProperty(required=true, value="Object type identifier")
	@NotBlank
	@Length(min=1, max=80, message="objectType must be between 1 and 80 characters in length")	
	private String objectType;
				
}