package com.aktana.learning.api.dto.dse;

import java.io.Serializable;

public class SEConfigElementTypeDTO implements Serializable {

	protected String id;
	protected String displayName;
	protected String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
