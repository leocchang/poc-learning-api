package com.aktana.learning.api.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** TokenRequest Representation */
@ApiModel(value = "A request for token")
@Data
public class SimpleTokenRequestDTO {
	@ApiModelProperty(required=true, value="user name for the request")
	private String username;

	@ApiModelProperty(required=true, value="password for the user")
	private String password;
}
