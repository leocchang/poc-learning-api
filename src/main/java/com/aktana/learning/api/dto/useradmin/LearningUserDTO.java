/*****************************************************************
 * <p>AktanaUserDTO</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.useradmin;

public class LearningUserDTO
{
    private String userId;
    private String userName;
    private String userDisplayName;
    private String password;
    private String roleId;
	private String ssoId;
	private Boolean isSsoEnabled;
    private String email;
    private Boolean isDeleted;
    private String createdAt;
    private String updatedAt;
	
    public LearningUserDTO()
    {

    }

    public String getUserName()
    {
        return userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    

    public String getUserDisplayName()
    {
        return userDisplayName;
    }
    
    public void setUserDisplayName(String userDisplayName)
    {
        this.userDisplayName = userDisplayName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public Boolean getIsSsoEnabled() {
		return isSsoEnabled;
	}

	public void setIsSsoEnabled(Boolean isSsoEnabled) {
		this.isSsoEnabled = isSsoEnabled;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
    
}
