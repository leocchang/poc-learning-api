/*****************************************************************
 *
 * @author $Author: robert.flint $
 * @version $Revision: 20139 $ on $Date: 2014-05-30 08:05:05 -0700 (Fri, 30 May 2014) $ by $Author: robert.flint $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.util.List;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** 
 * Aktana Error Response Representation 
 * 
 */
@ApiModel(value = "Response object for errors")
@Data()
public class AktanaErrorResponseDTO {
	
	public AktanaErrorResponseDTO() {		
	}
	
	@ApiModelProperty(value = "AKTANA internal error code for the entire response", required=true)
	private Integer status;

	@ApiModelProperty(value = "message", required=false)
	private String message;
	
	@ApiModelProperty(value = "list of detailed errors", required=false)
	private List<AktanaErrorDTO> errors;	
}
