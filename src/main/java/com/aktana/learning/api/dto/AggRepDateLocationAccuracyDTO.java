/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationSegment Representation */
@ApiModel(value = "Anchor Aggregate scores")
@Data
public class AggRepDateLocationAccuracyDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="learningBuildUID must be between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned run unique identifier")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="learningRunUID must be between 1 and 80 characters in length")	
	private String latestLearningRunUID;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotNull		
	private Double pctOverlapCoverage;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotNull		
	private Double pctPredictedVisited;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotNull		
	private Double pctSuggestionPredicted;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotNull		
	private Double pctOverallPrediction;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotNull		
	private Integer latestRunPredictionCount;
	
	@ApiModelProperty(required=true, value="date created")
	@NotBlank
	private String createdAt;
	
	@ApiModelProperty(required=true, value="date last updated")
	@NotBlank
	private String updatedAt;
			
}