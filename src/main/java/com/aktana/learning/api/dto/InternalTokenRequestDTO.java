package com.aktana.learning.api.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** TokenRequest Representation */
@ApiModel(value = "A request for token")
@Data
public class InternalTokenRequestDTO {

	@ApiModelProperty(required=true, value="secret for the partner")
	private String secret;
}
