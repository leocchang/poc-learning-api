/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config")
@Data
public class LearningConfigInfoDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigUID must be unqiue and between 1 and 80 characters in length")	
	private String learningConfigUID;
	@ApiModelProperty(required=true, value="Unique Name of the LearningConfig")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigName must be between 1 and 80 characters in length")
	private String learningConfigName;
	@ApiModelProperty(required=true, value="Description of the LearningConfig")
	@NotBlank
	@Length(min=1, max=255, message="learningConfigDescription must be between 1 and 255 characters in length")
	private String learningConfigDescription;
	
	@ApiModelProperty(required=true, value="Type of model (e.g. MSO or REM")
	@NotBlank
	@Length(min=1, max=20, message="Model type must be between 1 and 20 characters in length")	
	private String modelType;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier of product")
	@NotBlank
	@Length(min=0, max=80, message="If specified, productUID must be between 1 and 80 characters in length")	
	private String productUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier of channel (VISIT_CHANNEL, SEND_CHANNEL or WEB_INTERACTIVE_CHANNEL)")
	@NotBlank
	@Length(min=0, max=80, message="If specified, channelUID must be between 1 and 80 characters in length")
	private String channelUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier of current version")
	@Length(min=0, max=80, message="optional currentLearningVersionUID, if present must be between 1 and 80 characters in length")	
	private String currentLearningVersionUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier current build")
	@Length(min=0, max=40, message="optional currentLearningBuildUID, if present must between 1 and 40 characters in length")	
	private String currentLearningBuildUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned current build status")
	@Length(min=0, max=40, message="optional currentLearningBuildUIDStatus, if present must between 1 and 40 characters in length")	
	private String currentLearningBuildUIDStatus;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier run")
	@Length(min=0, max=40, message="optional latestRunUID, if present must between 1 and 40 characters in length")	
	private String latestRunUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned unique identifier run status")
	@Length(min=0, max=40, message="optional latestRunUIDStatus, if present must between 1 and 40 characters in length")	
	private String latestRunUIDStatus;
	
	@ApiModelProperty(required=false, value="Boolean identifier: is run published")
	private Boolean latestRunUIDIsPublished;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDCreatedAt;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDupdatedAt;
	
	@ApiModelProperty(required=false, value="The date and time that the current assignment began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String latestRunUIDStartDateTime;
	
	@ApiModelProperty(required=true, value="true=the learningConfig has been published, false=the learningConfig has not been published")
	@NotNull	
	private Boolean isPublished;
	
	@ApiModelProperty(required=true, value="true=the learningConfig has been deployed, false=the learningConfig has not been deployed")
	@NotNull	
	private Boolean isDeployed;

	@ApiModelProperty(required=true, value="true=the learningConfig has been deleted, false=the learningConfig is active")
	@NotNull
	private Boolean isDeleted;
	
	
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningConfigUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
