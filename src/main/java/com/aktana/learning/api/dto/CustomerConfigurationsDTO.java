/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** CustomerConfigurations Representation */
@ApiModel(value = "A learning config param")
@Data
public class CustomerConfigurationsDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="configurationType must be between 1 and 80 characters in length")	
	private String configurationType;
	
	@ApiModelProperty(required=true, value="Paramter name")
	@NotBlank
	@Length(min=1, max=80, message="configurationValue must be between 1 and 80 characters in length")	
	private String configurationValue;
	
	@ApiModelProperty(required=true, value="date created")
	@NotBlank
	private String createdAt;
	
	@ApiModelProperty(required=true, value="date last updated")
	@NotBlank
	private String updatedAt;
	
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return ((configurationType != null) ? configurationType : "") + "~" + ((configurationValue != null) ? configurationValue : "");
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}
