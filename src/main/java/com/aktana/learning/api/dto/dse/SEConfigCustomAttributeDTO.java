package com.aktana.learning.api.dto.dse;

import java.util.ArrayList;
import java.util.List;

public class SEConfigCustomAttributeDTO {
	private String id;			// field name
	private String name;		// field label used in front-end
	private String description; // field description used in front-end
	private String type;        // String, Integer, Double, Enum, Boolean
	private List<String> values;		// list of values for Enum type only
	private Boolean nullAllowed;
	private String minValue;	// minValue for Integer, Double
	private String maxValue;	// maxValue for Integer, Double
	private Integer minStringLength; // minStringLength for String
	private Integer maxStringLength; // maxStringLength for String
	private boolean multiValued;   // whether value can be multivalued
	private String multiValueDelimiter; // delimiter for multiValueDelimiter
	private List<String> restrictToProducts; // list of uids for associated products
	
	public SEConfigCustomAttributeDTO() {
		
	}
	
	public SEConfigCustomAttributeDTO clone() {
		SEConfigCustomAttributeDTO newDTO = new SEConfigCustomAttributeDTO();
		newDTO.id = id;
		newDTO.name = name;
		newDTO.description = description;
		newDTO.type = type;
		newDTO.values = new ArrayList<String>(values);
		newDTO.nullAllowed = nullAllowed;
		newDTO.minValue = minValue;
		newDTO.maxValue = maxValue;
		newDTO.minStringLength = minStringLength;
		newDTO.maxStringLength = maxStringLength;
		newDTO.multiValued = multiValued;
		newDTO.multiValueDelimiter = multiValueDelimiter;
		newDTO.restrictToProducts =  new ArrayList<String>(restrictToProducts);
		
		return newDTO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public Boolean getNullAllowed() {
		return nullAllowed;
	}

	public void setNullAllowed(Boolean nullAllowed) {
		this.nullAllowed = nullAllowed;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public Integer getMinStringLength() {
		return minStringLength;
	}

	public void setMinStringLength(Integer minStringLength) {
		this.minStringLength = minStringLength;
	}

	public Integer getMaxStringLength() {
		return maxStringLength;
	}

	public void setMaxStringLength(Integer maxStringLength) {
		this.maxStringLength = maxStringLength;
	}

	public boolean getMultiValued() {
		return multiValued;
	}

	public void setMultiValued(boolean multiValued) {
		this.multiValued = multiValued;
	}

	public String getMultiValueDelimiter() {
		return multiValueDelimiter;
	}

	public void setMultiValueDelimiter(String multiValueDelimiter) {
		this.multiValueDelimiter = multiValueDelimiter;
	}

	public List<String> getRestrictToProducts() {
		return restrictToProducts;
	}

	public void setRestrictToProducts(List<String> restrictToProducts) {
		this.restrictToProducts = restrictToProducts;
	}
	
}
