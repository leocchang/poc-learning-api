package com.aktana.learning.api.dto;

import lombok.Data;
/** 
 * Aktana DTO that represents response of a sync operation 
 * 
 */
@Data
public class AktanaSyncResponseDTO {
	private int status;  // 1 = ok, 0 = failed
	private String message; // optional error message
	
	private int updates; // number of objects updated
	private int deletes; // number of objects deleted
}
