/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.aktana.learning.common.util.datetime.PureDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** RepAccount Representation */
@ApiModel(value = "RepAccountEngagementDTO")
@Data
@EqualsAndHashCode(callSuper=false)

public class RepAccountEngagementDTO extends LearningScoreDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique run identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unique and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for account")
	@NotBlank		
	private String accountUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for rep")
	@NotBlank		
	private String repUID;
	
	@ApiModelProperty(required=false, value="Rep action type Id")	
	private Integer repActionTypeId;
	
	@ApiModelProperty(required=true, value="Suggestion type")
	@NotBlank		
	private String suggestionType;
	
	@ApiModelProperty(required=true, value="Probability of engagement")
	@NotBlank		
	private double probability;
	
	@ApiModelProperty(required=false, value="Required date, format="+PureDate.AKTANA_PURE_DATE_FORMAT)
	private String date;
	
	@ApiModelProperty(required=false, value="Required date, format="+PureDate.AKTANA_PURE_DATE_FORMAT)
	private String runDate;
	

	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return ((learningRunUID != null) ? learningRunUID : "") + "~" + ((repUID != null) ? repUID : "") + "~" +  ((date != null) ? "~"+date : "");
	}		
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}
