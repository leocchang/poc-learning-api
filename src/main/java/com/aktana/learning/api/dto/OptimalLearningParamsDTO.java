/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config param")
@Data
public class OptimalLearningParamsDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="productUID must be unique and between 1 and 80 characters in length")	
	private String productUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="channelUID must be unique and between 1 and 80 characters in length")	
	private String channelUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="goal must be unique and between 1 and 40 characters in length")	
	private String goal;	

	@ApiModelProperty(required=true, value="Paramter name")
	@NotBlank
	@NotNull
	@Length(min=1, max=80, message="paramName must be between 1 and 80 characters in length")	
	private String paramName;
	
	@ApiModelProperty(required=false, value="Parameter value")
	@NotBlank
	@Length(min=0, message="paramValue is optional")	
	private String paramValue;
	
	@ApiModelProperty(required=true, value="date created")
	@NotBlank
	private String createdAt;
	
	@ApiModelProperty(required=true, value="date last updated")
	@NotBlank
	private String updatedAt;
	
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return ((productUID != null) ? productUID : "") + ((channelUID != null) ? "~"+channelUID : "") + ((goal != null) ? "~"+goal : "") + ((paramName != null) ? "~"+paramName : "");
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}
