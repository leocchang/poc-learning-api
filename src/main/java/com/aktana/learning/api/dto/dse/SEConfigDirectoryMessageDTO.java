package com.aktana.learning.api.dto.dse;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SEConfigDirectoryMessageDTO extends GenericResponseDTO {

	private List<SEConfigDirectoryEntryDTO> directory;

	public SEConfigDirectoryMessageDTO() {
	}

	@JsonProperty
	public List<SEConfigDirectoryEntryDTO> getDirectory() {
		// lazy initialization
		if (null == directory) {
			directory = new ArrayList<SEConfigDirectoryEntryDTO>();
		}
		return directory;
	}

	@JsonProperty
	public void setDirectory(List<SEConfigDirectoryEntryDTO> directory) {
		this.directory = directory;
	}

	public void addToDirectory(SEConfigDirectoryEntryDTO entry) {
		directory.add(entry);
	}
}
