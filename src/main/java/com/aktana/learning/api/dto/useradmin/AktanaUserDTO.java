/*****************************************************************
 * <p>AktanaUserDTO</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.useradmin;

import com.aktana.learning.api.dto.UserPrincipalDTO;

public class AktanaUserDTO extends UserPrincipalDTO
{
    private String userId;
    private String userName;
    private String userDisplayName;
    private String email;
	
    public AktanaUserDTO()
    {

    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    

    public String getUserDisplayName()
    {
        return userDisplayName;
    }
    
    public void setUserDisplayName(String userDisplayName)
    {
        this.userDisplayName = userDisplayName;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
