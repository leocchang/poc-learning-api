/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "Message info")
@Data
public class TerritoriesDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned territories unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="territory UID")	
	private String territoryUID;
	
	@ApiModelProperty(required=true, value="name of the territory")
	private String territoryName;

}