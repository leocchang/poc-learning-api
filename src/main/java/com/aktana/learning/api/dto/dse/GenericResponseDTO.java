package com.aktana.learning.api.dto.dse;

import org.eclipse.jetty.http.HttpStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GenericResponseDTO {

    private int status;
	private String message;
	private List<String> errors;

    public GenericResponseDTO() {
        this.status = HttpStatus.OK_200;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		// lazy initialization
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public void addError(String error) {
		this.getErrors().add(error);
		// if no message is set, copy the error message to the message
		if (message == null) {
			message = error;
		}
	}

	public void addErrors(Collection<String> errors) {
		this.getErrors().addAll(errors);
		// if no message is set, but there are errors, set a default message
		if (message == null && this.getErrors().size() > 0) {
			message = ""+ this.getErrors().size() + "errors have been detected";
		}
	}

	public boolean hasErrors() {
		return this.getErrors().size() > 0;
	}

    public int getStatus() {
        return status;
    }

    public void setStatus(int resultCode) {
        this.status = resultCode;
    }
}
