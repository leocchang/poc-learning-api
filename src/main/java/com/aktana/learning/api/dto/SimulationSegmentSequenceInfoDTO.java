/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.aktana.learning.api.dto.SimulationSegmentMessageSequenceDTO;
import java.util.List;
import java.util.ArrayList;

/** LearningConfig Representation */
@ApiModel(value = "Simulation Sequence info")
@Data
public class SimulationSegmentSequenceInfoDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned run unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be between 1 and 80 characters in length")	
	private String learningRunUID;

	@ApiModelProperty(required=true, value="Externally-assigned unique version identifier")
	@NotBlank
	private Double probability;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique config identifier")
	@NotBlank
	private List<SimulationSegmentMessageSequenceDTO> sequence;

}