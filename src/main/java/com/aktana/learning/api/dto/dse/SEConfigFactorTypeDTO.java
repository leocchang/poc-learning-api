package com.aktana.learning.api.dto.dse;

import java.util.ArrayList;
import java.util.List;

public class SEConfigFactorTypeDTO extends SEConfigElementTypeDTO{

	protected boolean allowsMultipleInstances;
	protected boolean requiresRules;
	protected List<SEConfigFactorReasonVariableDTO> reasonVariables;
	protected List<SEConfigParameterTypeDTO> parameters;

	public SEConfigFactorTypeDTO() {
	}

	public boolean isAllowsMultipleInstances() {
		return allowsMultipleInstances;
	}

	public void setAllowsMultipleInstances(boolean allowsMultipleInstances) {
		this.allowsMultipleInstances = allowsMultipleInstances;
	}

	public List<SEConfigFactorReasonVariableDTO> getReasonVariables() {
		// lazy initialization
		if (null == reasonVariables) {
			reasonVariables = new ArrayList<SEConfigFactorReasonVariableDTO>();
		}
		return reasonVariables;
	}

	public void setReasonVariables(List<SEConfigFactorReasonVariableDTO> reasonVariables) {
		this.reasonVariables = reasonVariables;
	}

	public List<SEConfigParameterTypeDTO> getParameters() {
		// lazy initialization
		if (null == parameters) {
			parameters = new ArrayList<SEConfigParameterTypeDTO>();
		}
		return parameters;
	}

	public void setParameters(List<SEConfigParameterTypeDTO> parameters) {
		this.parameters = parameters;
	}

	public boolean isRequiresRules() {
		return requiresRules;
	}

	public void setRequiresRules(boolean requiresRules) {
		this.requiresRules = requiresRules;
	}
	
	
}
