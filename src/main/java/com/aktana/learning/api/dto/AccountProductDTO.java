/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** RepAccount Representation */
@ApiModel(value = "AccountProductDTO")
@Data
@EqualsAndHashCode(callSuper=false)

public class AccountProductDTO extends LearningScoreDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique run identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unique and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for account")
	@NotBlank
	private String accountUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for message")
	@NotBlank		
	private String productUID;
	
	@ApiModelProperty(required=true, value="Visit Probability of engagement")
	@NotNull		
	private Double visitProbability_akt;
	
	@ApiModelProperty(required=true, value="Send Probability of engagement")
	@NotNull		
	private Double sendProbability_akt;
	
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return ((learningRunUID != null) ? learningRunUID : "")  + "~" +  ((accountUID != null) ? accountUID : "") +  "~" + ((productUID != null) ? productUID : "");
	}		
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}
