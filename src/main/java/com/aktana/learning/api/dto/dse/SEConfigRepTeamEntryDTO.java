package com.aktana.learning.api.dto.dse;

import java.io.Serializable;

public class SEConfigRepTeamEntryDTO implements Serializable {

	protected String uid;
	protected String name;
	protected Integer numReps;
	

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumReps() {
		return numReps;
	}

	public void setNumReps(Integer numReps) {
		this.numReps = numReps;
	}

	
		
}
