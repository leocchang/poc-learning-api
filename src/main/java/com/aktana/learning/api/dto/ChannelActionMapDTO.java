/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationSegment Representation */
@ApiModel(value = "Mapping from repActionChannel to repActionType")
@Data
public class ChannelActionMapDTO {
	
	@ApiModelProperty(required=true, value="Rep action type identifier")
	@NotBlank
	@NotNull
	private Integer repActionTypeId;
	
	@ApiModelProperty(required=true, value="Rep action type name")
	@NotBlank
	@NotNull		
	private String repActionTypeName;
	
	@ApiModelProperty(required=true, value="Mapped rep action channel")
	@NotNull		
	private String repActionChannel;
	
	@ApiModelProperty(required=true, value="Identifier: action type appears in interaction data")
	@NotNull		
	private Boolean appearsInInteractionData;
	
//	@ApiModelProperty(required=true, value="date created")
//	@NotBlank
//	private String createdAt;
//	
//	@ApiModelProperty(required=true, value="date last updated")
//	@NotBlank
//	private String updatedAt;
			
}