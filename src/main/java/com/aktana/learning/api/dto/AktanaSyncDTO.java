package com.aktana.learning.api.dto;

import java.util.List;



import lombok.Data;
/** 
 * Aktana DTO to support Sync API with the client 
 * 
 */
@Data
public class AktanaSyncDTO<T> {
	private List<T> list;	
	private List<String> delete;	
}
