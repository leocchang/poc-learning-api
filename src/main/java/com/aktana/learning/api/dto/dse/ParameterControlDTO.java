/*****************************************************************
 * <p>ParameterControlDTO</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.dse;

public class ParameterControlDTO
{
    private String parameterControlId;
    private String parentElementId;
    private String parameterId;
    private String deployedDefaultValue;
    private Boolean hideFromConfigurator;
    private String comment;
	
    public ParameterControlDTO()
    {

    }

	public String getParameterControlId() {
		return parameterControlId;
	}

	public void setParameterControlId(String parameterControlId) {
		this.parameterControlId = parameterControlId;
	}

	public String getParentElementId() {
		return parentElementId;
	}

	public void setParentElementId(String parentElementId) {
		this.parentElementId = parentElementId;
	}

	public String getParameterId() {
		return parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

	public String getDeployedDefaultValue() {
		return deployedDefaultValue;
	}

	public void setDeployedDefaultValue(String deployedDefaultValue) {
		this.deployedDefaultValue = deployedDefaultValue;
	}

	public Boolean getHideFromConfigurator() {
		return hideFromConfigurator;
	}

	public void setHideFromConfigurator(Boolean hideFromConfigurator) {
		this.hideFromConfigurator = hideFromConfigurator;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

   
    
}
