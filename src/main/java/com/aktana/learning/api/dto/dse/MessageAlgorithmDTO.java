/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** Message Representation */
@ApiModel(value = "A message algorithm is an object having a unique name, id that describes a selection algorithm for the new message(s) in a set to be delivered ")
@Data
public class MessageAlgorithmDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for the message algorithm")
	@NotBlank		
	@Length(min=1, max=40, message="messageAlgorithmUID must be between 1 and 40 characters in length")	
	private String messageAlgorithmUID;
	@ApiModelProperty(required=true, value="A message algorithm name that (also) uniquely identifies the message algorithm.")
	@NotBlank		
	@Length(min=1, max=80, message="messageAlgorithmName must be between 1 and 255 characters in length")	
	private String messageAlgorithmName;
	@ApiModelProperty(required=false, value="Description of the message algorithm")
	@Length(min=0, max=255, message="messageAlgorithmDescription must be between 0 and 255 characters in length")	
	private String messageAlgorithmDescription;	
	@ApiModelProperty(required=true, value="Product to which this messageSet is related to")
	@NotBlank		
	@Length(min=1, max=20, message="productUID must be between 1 and 20 characters in length")	
	private String productUID;
	@ApiModelProperty(required=true, value="VISIT_CHANNEL (0) or SEND_CHANNEL (1) or WEB_INTERACTIVE_CHANNEL (2) ", allowableValues="VISIT_CHANNEL,SEND_CHANNEL")
	@NotNull	
	private String repActionChannel;
	@ApiModelProperty(required=true, value="ROUND_ROBIN (0) or MESSAGE_SEQUENCE_LEARNING_MODEL (1)", allowableValues="ROUND_ROBIN,MESSAGE_SEQUENCE_LEARNING_MODEL")
	@NotNull	
	private String messageAlgorithmType;
	@ApiModelProperty(required=true, value="true=the messageAlgorithm is active, false=the messageAlgorithm is not active.")
	private Boolean isActive;
	@ApiModelProperty(required=true, value="true=the messageAlgorithm has been deleted, false=the messageAlgorithm is no deleted.")
	private Boolean isDeleted;
			
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return messageAlgorithmUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
