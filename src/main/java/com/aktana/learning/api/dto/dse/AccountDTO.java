/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IMarkableForDeleteDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** Account Representation */
@ApiModel(value = "The business entity for which Targets are set, and to which Reps make visits.  Normally, this is an individual doctor")
@Data
public class AccountDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for the Account")
	@NotBlank
	@Length(min=1, max=40, message="accountUID must be between 1 and 40 characters in length")	
	private String accountUID;
	@ApiModelProperty(required=true, value="Name of the Account")
	@Length(min=0, max=255, message="accountName must be between 1 and 255 characters in length. Optional if updating only account attributes ")	
	private String accountName;
	@ApiModelProperty(required=false, value="Identifies the default location associated with the Account. Although optional, this is a very important attribute.")
	private String facilityUID;
	@ApiModelProperty(required=true, value="true=the Account has been deleted, false=the Account is active. Optional if updating only account attributes")
	private Boolean isDeleted;
	
	@Override // for IExternallyIdentifiableDTO
	@JsonIgnore
	public String getObjectUID() {
		return accountUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
