/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.aktana.learning.common.util.datetime.PureDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** AccountTimeToEngage Representation */
@ApiModel(value = "AccountTimeToEngageDTO")
@Data
@EqualsAndHashCode(callSuper=false)

public class AccountTimeToEngageDTO extends LearningScoreDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique run identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unique and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for account")
	@NotBlank
	@Length(min=1, max=80, message="accountUID must be between 1 and 80 characters in length")	
	private String accountUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for channel")
	@NotBlank
	@Length(min=1, max=80, message="channelUID must be between 1 and 80 characters in length")	
	private String channelUID;
	
	@ApiModelProperty(required=true, value="identifier for method")
	@NotBlank
	@Length(min=1, max=40, message="method must be between 1 and 80 characters in length")	
	private String method;
		
	@ApiModelProperty(required=true, value="Probability of engagement")		
	private Double probability;
	
	
	@ApiModelProperty(required=false, value="Required date, format="+PureDate.AKTANA_PURE_DATE_FORMAT)
	@NotBlank
	private String predictionDate;
	

	
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return ((learningRunUID != null) ? learningRunUID : "")  + "~" +  ((accountUID != null) ? accountUID : "") 
				+  "~" + ((channelUID != null) ? channelUID : "") + "~" + ((method != null) ? method : "") + "~" + ((predictionDate != null) ? predictionDate : "");
	}		
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}