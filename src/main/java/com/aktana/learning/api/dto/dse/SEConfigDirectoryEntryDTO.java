package com.aktana.learning.api.dto.dse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SEConfigDirectoryEntryDTO implements Serializable {

	protected Integer id;
	protected String name;
	protected String description;
	protected String strategyName;
	protected Integer numFactors;
	protected Integer numCourseFactors;
	protected Integer numActiveUsers;
	protected Integer numUsers;
	protected Integer numAccounts;
	protected Boolean isReadOnly;
	protected String createdAt;
	protected Integer createdByUserId;
	protected String updatedAt;
	protected Integer updatedByUserId;	
    protected Integer statusId; // 0 = none, 1=In Production, 2=To Be Promoted
    
    // when statusId = 0, replacesConfigId and replacementConfigId will be null    
    // the configId which this config is going to replace           
    // Will be set only when statusId = 2. Will go back to null after the run or after reset is called
    protected Integer replacesConfigId;
    protected String replacesConfigName;

    // the configId which is going to replace the current configId during the next nightly run                
    // Will be set only when statusId = 1. Will go back to null after the run or after reset is called on the promoted config
    protected Integer replacementConfigId;
    protected String replacementConfigName;

    protected Integer promotedByUserId;
    
    //private List<SEConfigRepTeamEntryDTO> repTeams;
    private List<SEConfigErrorDTO> errors;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStrategyName() {
		return strategyName;
	}

	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}

	public Integer getNumFactors() {
		return numFactors;
	}

	public void setNumFactors(Integer numFactors) {
		this.numFactors = numFactors;
	}
	
	public Integer getNumCourseFactors() {
		return numCourseFactors;
	}

	public void setNumCourseFactors(Integer numCourseFactors) {
		this.numCourseFactors = numCourseFactors;
	}

	public Integer getNumActiveUsers() {
		return numActiveUsers;
	}

	public void setNumActiveUsers(Integer numActiveUsers) {
		this.numActiveUsers = numActiveUsers;
	}

	public Integer getNumUsers() {
		return numUsers;
	}

	public void setNumUsers(Integer numUsers) {
		this.numUsers = numUsers;
	}
	
	public Integer getNumAccounts() {
		return numAccounts;
	}

	public void setNumAccounts(Integer numAccounts) {
		this.numAccounts = numAccounts;
	}

	public Boolean getIsReadOnly() {
		return isReadOnly;
	}

	public void setIsReadOnly(Boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(Integer createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(Integer updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getReplacesConfigId() {
		return replacesConfigId;
	}

	public void setReplacesConfigId(Integer replacesConfigId) {
		this.replacesConfigId = replacesConfigId;
	}

	public String getReplacesConfigName() {
		return replacesConfigName;
	}

	public void setReplacesConfigName(String replacesConfigName) {
		this.replacesConfigName = replacesConfigName;
	}

	public Integer getReplacementConfigId() {
		return replacementConfigId;
	}

	public void setReplacementConfigId(Integer replacementConfigId) {
		this.replacementConfigId = replacementConfigId;
	}

	public String getReplacementConfigName() {
		return replacementConfigName;
	}

	public void setReplacementConfigName(String replacementConfigName) {
		this.replacementConfigName = replacementConfigName;
	}

	public Integer getPromotedByUserId() {
		return promotedByUserId;
	}

	public void setPromotedByUserId(Integer promotedByUserId) {
		this.promotedByUserId = promotedByUserId;
	}
	/*
	@JsonProperty
	public List<SEConfigRepTeamEntryDTO> getRepTeams() {
		// lazy initialization
		if (null == repTeams) {
			repTeams = new ArrayList<SEConfigRepTeamEntryDTO>();
		}
		return repTeams;
	}

	@JsonProperty
	public void setRepTeams(List<SEConfigRepTeamEntryDTO> repTeams) {
		this.repTeams = repTeams;
	}

	public void addRepTeam(SEConfigRepTeamEntryDTO repTeam) {
		getRepTeams().add(repTeam);
	}
	*/
	@JsonProperty
	public List<SEConfigErrorDTO> getErrors() {
		// lazy initialization
		if (null == errors) {
			errors = new ArrayList<SEConfigErrorDTO>();
		}
		return errors;
	}

	@JsonProperty
	public void setErrors(List<SEConfigErrorDTO> errors) {
		this.errors = errors;
	}

	public void addError(SEConfigErrorDTO error) {
		getErrors().add(error);
	}
	
		
}
