/*****************************************************************
 * <p>loginDTO</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.useradmin;

import com.aktana.learning.api.dto.useradmin.AktanaUserDTO;

public class LoginDTO extends AktanaUserDTO
{
    private String result;
    private String sessionId;
    private String token;
    private String roleId;
    private String refreshToken;

    public LoginDTO()
    {

    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
