/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationMessageSequence Representation */
@ApiModel(value = "A Simulation Message Sequence")
@Data
public class SimulationSegmentMessageSequenceDTO { 
	
	@ApiModelProperty(required=true, value="Externally-assigned segment unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="messageSeqUID must be between 1 and 80 characters in length")	
	private String messageSeqUID;
	
	@ApiModelProperty(required=true, value="message seqence hash ")
	@NotBlank
	@Length(min=1, max=80, message="messageSeqHash must be between 1 and 80 characters in length")	
	private String messageSeqHash;
	
	@ApiModelProperty(required=true, value="message identifier")
	@NotBlank
	@Length(min=1, max=80, message="messageID must be between 1 and 80 characters in length")	
	private String messageID;
	
	@ApiModelProperty(required=true, value="sequence order")	
	@NotBlank
	private Integer sequenceOrder;
	
	@ApiModelProperty(required=false, value="name of the message")
	private String name;
	
	@ApiModelProperty(required=false, value="subject of sent email")
	private String emailSubject;
	
	@ApiModelProperty(required=false, value="body of sent email")
	private String emailBody;
	
	@ApiModelProperty(required=true, value="date created")
	@NotBlank
	private String createdAt;
	
	@ApiModelProperty(required=true, value="date last updated")
	@NotBlank
	private String updatedAt;
				
}