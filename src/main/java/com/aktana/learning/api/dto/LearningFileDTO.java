/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.sql.Blob;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning file")
@Data
public class LearningFileDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique file identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unique and between 1 and 80 characters in length")	
	private String learningFileUID;
	
	@ApiModelProperty(required=false, value="Externally-assigned run unique identifier")
	@NotBlank
	@Length(min=0, max=80, message="If provided, learningRunUID must be unqiue and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;

	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unqiue and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigUID must be unqiue and between 1 and 80 characters in length")	
	private String learningConfigUID;
	
	@ApiModelProperty(required=true, value="File name")
	@NotBlank
	@Length(min=1, max=80, message="fileName must be unique and between 1 and 80 characters in length")	
	private String fileName;
	
	@ApiModelProperty(required=true, value="File type")
	@NotBlank
	@Length(min=1, max=80, message="fileType must be unique and between 1 and 20 characters in length")	
	private String fileType;
	
	@ApiModelProperty(required=true, value="File size in bytes.")
	private int fileSize;
	
	@ApiModelProperty(required=true, value="File binary content.")
	private String fileData;

	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningFileUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}

}
