/*****************************************************************
*
* @author $Author: robert.flint $
* @version $Revision: 20214 $ on $Date: 2014-06-10 09:05:02 -0700 (Tue, 10 Jun 2014) $ by $Author: robert.flint $
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.dto;


/** 
* "Marker" interface for any Aktana DTO that contains an isDeleted flag
* 
*/
public interface IMarkableForDeleteDTO {
	
	public Boolean getIsDeleted();
}
