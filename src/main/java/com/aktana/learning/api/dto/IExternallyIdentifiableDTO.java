/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.dto;

import com.aktana.learning.api.constraint.ValidExternallyIdentifiableDTO;


/** 
* "Marker" interface for any Aktana DTO that contains an externally-specified
* String objectUID.
* 
*/
@ValidExternallyIdentifiableDTO
public interface IExternallyIdentifiableDTO {
	
	public String getObjectUID();
	
	public boolean isCompoundObjectUID();
	
}
