/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning build")
@Data
public class LearningRunDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique run identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be unique and between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned version unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unqiue and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique config identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigUID must be unique and between 1 and 80 characters in length")	
	private String learningConfigUID;
	
	@ApiModelProperty(required=true, value="true=the scores for the run have been published to the DSE, false=the scores have been save in learning DB only.")
	@NotNull	
	private Boolean isPublished;
	
	@ApiModelProperty(required=false, value="type of run- MSO,REM,TTE,ANCHOR,SIMULATION.")
	private String runType;
	
	@ApiModelProperty(required=false, value="Execution status: running, timeout, success, failure.")
	private String executionStatus;
	
	@ApiModelProperty(required=false, value="The date and time that execution began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String executionDatetime;

	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningBuildUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
