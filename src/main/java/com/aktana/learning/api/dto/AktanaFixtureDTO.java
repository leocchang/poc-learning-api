/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.util.List;



import lombok.Data;

/** 
 * Aktana Fixture Representation 
 * 
 * TODO Add explanation of this class's role and usage
 */
@Data(staticConstructor="of")
public class AktanaFixtureDTO<T> {
	private List<T> list;	
	private T read;
	private T save;	
	private T update;
	private T delete;
}

