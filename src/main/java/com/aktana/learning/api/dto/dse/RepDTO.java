/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;


import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IMarkableForDeleteDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** Rep Representation */
@ApiModel(value = "A representative of the PharmaCo that makes visits to Accounts")
@Data
public class RepDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank		
	@Length(min=1, max=20, message="repUID must be between 1 and 20 characters in length")	
	private String repUID;
	@ApiModelProperty(required=false, value="Optional name of the Rep")
	@Length(min=0, max=150, message="repName must be between 0 and 150 characters in length")
	private String repName;	
	@ApiModelProperty(required=false, value="V3 Only. Optional identifier for RepType. Defaults to DEFAULT_REPTYPE if not provided.")
	@Length(min=1, max=20, message="repTypeUID must be between 1 and 20 characters in length")		
	private String repTypeUID;
	@ApiModelProperty(required=false, value="V3 Only. Optional identifier for WorkWeek. Defaults to DEFAULT_WORKWEEK if not provided.")
	@Length(min=1, max=20, message="workWeekUID must be between 1 and 20 characters in length")	
	private String workWeekUID;
	@ApiModelProperty(required=false, value="V3 Only. Optional. Reserved for future use - The latitude, expressed in degrees, as a floating point number, with at least 4 fractional digits for sufficient accuracy. -90.0000 to 0.0000 for the southern hemisphere, 0.0000 to +90.0000 for the northern hemisphere.")	
	private Double latitude;
	@ApiModelProperty(required=false, value="V3 Only. Optional. Reserved for future use - The longitude, expressed in degrees, as a floating point number, with at least 4 fractional digits for sufficient accuracy. -180.0000 to 0.0000 for the west of Greenwich, 0.0000 to +180.0000 for the east of Greenwich.")	
	private Double longitude;	
	@ApiModelProperty(required=false, value="Identifies the timezone that the Rep is in, using timezoneID values as per the IANA/Olson Time Zone Database (also known as ZoneInfo, TZDB or the TZ database). Example: America/New_York,Europe/London,Asia/Tokyo. If null/omitted, Aktana will use the configured default timezoneID")
	private String timezoneID;
	@ApiModelProperty(required=true, value="true=the Rep is enabled for Suggestions/Insights, false=the Rep is not enabled")
	@NotNull		
	private Boolean isActivated;
	@ApiModelProperty(required=true, value="true=the Rep has been deleted, false=the Rep is active")
	@NotNull		
	private Boolean isDeleted;
	@ApiModelProperty(required=false, value="V3 Only. DseConfig Id. Use NULL to unset")
	private Integer seConfigId;
	@ApiModelProperty(required=false, value="V3 Only. Readonly.")	
	private List<RepTeamDTO> repTeams;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return repUID;
	}	
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}

