/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "Message info")
@Data
public class RepEngagementDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique rep identifier")
	@NotBlank
	private String repUID;
	
	@ApiModelProperty(required=true, value="Performance")
	@NotBlank
	private Double performance;
	
}