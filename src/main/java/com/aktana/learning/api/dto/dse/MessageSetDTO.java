/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** MessageSet Representation */
@ApiModel(value = "A message set is an object having a unique name, id that describes an ordered sequence of messages ")
@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class MessageSetDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for the messageSet")
	@NotBlank		
	@Length(min=1, max=40, message="messageSetUID must be between 1 and 40 characters in length")	
	private String messageSetUID;
	@ApiModelProperty(required=true, value="A messageSet name that (also) uniquely identifies the messageSet.")
	@NotBlank		
	@Length(min=1, max=80, message="messageSetName must be between 1 and 80 characters in length")	
	private String messageSetName;
	@ApiModelProperty(required=false, value="Description of the messageSet")
	@Length(min=0, max=255, message="messageSetDescription must be between 0 and 255 characters in length")	
	private String messageSetDescription;
	@ApiModelProperty(required=true, value="Product to which this messageSet is related to")
	@NotBlank		
	@Length(min=1, max=20, message="productUID must be between 1 and 20 characters in length")	
	private String productUID;	
	@ApiModelProperty(required=true, value="ordered list of message UIDs")
	@NotNull
	@Valid
	private List<String> messages;
	@ApiModelProperty(required=true, value="VISIT_CHANNEL (0) or SEND_CHANNEL (1)", allowableValues="VISIT_CHANNEL,SEND_CHANNEL")
	@NotNull	
	private String repActionChannel;	
	@ApiModelProperty(required=false, value="Message Algorithm assigned to this messageSet (null for default round robin)")	
	@Length(min=1, max=40, message="If set, messageAlgorithmUID must be between 1 and 40 characters in length")	
	private String messageAlgorithmUID;	
	@ApiModelProperty(required=false, value="Accuracy or Message Algorithm for this messageSet")		
	private double messageAlgorithmAccuracy;
	@ApiModelProperty(required=false, value="true=default to rotating selector if algorithm did not find a message.")
	private Boolean defaultToRotatingSelector;
	
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return messageSetUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}	

}
