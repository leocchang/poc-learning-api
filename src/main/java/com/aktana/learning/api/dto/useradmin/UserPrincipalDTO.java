package com.aktana.learning.api.dto.useradmin;

import lombok.Data;

import com.aktana.learning.api.security.AktanaUserRoleEnum;
import com.wordnik.swagger.annotations.ApiModel;

/** UserPrincipal Representation */
@ApiModel(value = "Identifies a user who is using the API")
@Data
public class UserPrincipalDTO {
	private String name;
	private AktanaUserRoleEnum role;
}
