package com.aktana.learning.api.dto.dse;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** Message Representation */
@ApiModel(value = "A message is an object having a unique name, id that describes a product and can be delivered through channel ")
@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class MessageDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier for the message")
	@NotBlank		
	@Length(min=1, max=40, message="messageUID must be between 1 and 40 characters in length")	
	private String messageUID;
	@ApiModelProperty(required=true, value="A message name that (also) uniquely identifies the message.")
	@NotBlank		
	@Length(min=1, max=255, message="messageName must be between 1 and 255 characters in length")	
	private String messageName;
	@ApiModelProperty(required=true, value="Product UID to which this message is related to")
	@NotBlank		
	@Length(min=1, max=20, message="productUID must be between 1 and 20 characters in length")	
	private String productUID;
	@ApiModelProperty(required=true, value="Message Channel UID to which this message is related to")
	@NotBlank		
	@Length(min=1, max=40, message="messageChannelUID must be between 1 and 40 characters in length")	
	private String messageChannelUID;
	@ApiModelProperty(required=true, value="Message Topic UID to which this message is related to")
	@NotBlank		
	@Length(min=1, max=40, message="messageTopicUID must be between 1 and 40 characters in length")	
	private String messageTopicUID;
	@ApiModelProperty(required=false, value="Description of the message")
	@Length(min=0, max=255, message="messageDescription must be between 0 and 255 characters in length")	
	private String messageDescription;	
	@ApiModelProperty(required=true, value="VISIT_CHANNEL (0) or SEND_CHANNEL (1)", allowableValues="VISIT_CHANNEL,SEND_CHANNEL")
	@NotNull	
	private String repActionChannel;	
	@ApiModelProperty(required=false, value="Externally assigned unique identifier of Last physicalmessage associated with the message")	
	@Length(min=0, max=40, message="lastPhysicalMessageUID must be between 0 and 40 characters in length")	
	private String lastPhysicalMessageUID;
			
	@Override
	@JsonIgnore	
	public String getObjectUID() {
		return messageUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
