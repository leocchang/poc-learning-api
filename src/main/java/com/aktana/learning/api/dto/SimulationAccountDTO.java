/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationMessageSequence Representation */
@ApiModel(value = "A Simulation Message Sequence")
@Data
public class SimulationAccountDTO { 
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned run unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned account unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="account must be unqiue and between 1 and 80 characters in length")	
	private String accountUID;	
	
	@ApiModelProperty(required=true, value="Account name")
	@Length(min=1, max=255, message="accountName must be between 1 and 255 characters in length")	
	private String accountName;			
}