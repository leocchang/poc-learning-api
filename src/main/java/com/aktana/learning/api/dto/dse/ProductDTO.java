/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IMarkableForDeleteDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** Product Representation */
@ApiModel(value = "A particular drug")
@Data
public class ProductDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@Length(min=1, max=20, message="productUID must be between 1 and 20 characters in length")	
	private String productUID;
	@ApiModelProperty(required=true, value="Unique Name of the Product")
	@NotBlank
	@Length(min=1, max=30, message="productName must be between 1 and 30 characters in length")
	private String productName;
	@ApiModelProperty(required=true, value="true=the product has been activated for this client, false=the product has not been activated")
	@NotNull	
	private Boolean isActive;
	@ApiModelProperty(required=true, value="true=the product has been deleted, false=the product is active")
	@NotNull
	private Boolean isDeleted;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return productUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
