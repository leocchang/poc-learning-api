/*****************************************************************
 * <p>DTO to represent operation results</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.dto.useradmin;

import java.io.Serializable;

public class PasswordResetResultDTO implements Serializable {


    private int resultCode;
    private String message;
    private String userName;
    private transient String token;


    public PasswordResetResultDTO() {

    }

    public PasswordResetResultDTO(int resultCode, String message) {
        this(resultCode, message, "", null);
    }

    public PasswordResetResultDTO(int resultCode, String message, String userName) {
        this(resultCode, message, userName, null);
    }

    public PasswordResetResultDTO(int resultCode, String message, String userName, String token) {
        this.resultCode = resultCode;
        this.message = message;
        this.userName = userName;
        this.token = token;
    }


    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String tokenValue() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
