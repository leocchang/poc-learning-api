/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning build")
@Data
public class LearningBuildDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be unqiue and between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned version unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unqiue and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="true=the learningConfig has been deployed, false=the learningConfig has not been deployed")
	@NotNull	
	private Boolean isDeployed;
	
	@ApiModelProperty(required=false, value="Execution status: running, timeout, success, failure.")
	private String executionStatus;
	
	@ApiModelProperty(required=false, value="The date and time that execution began.  Format: per ISO 8601.  Can be expressed in terms of any timezone.")
	private String executionDatetime;
	
	@ApiModelProperty(required=true, value="true=the learningBuild has been deleted, false=the learningBuild is active")
	@NotNull
	private Boolean isDeleted;

	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningBuildUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}
