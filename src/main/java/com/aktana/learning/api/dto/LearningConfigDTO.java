/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import org.hibernate.validator.constraints.Length;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config")
@Data
public class LearningConfigDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@Length(min=1, max=80, message="learningConfigUID must be unqiue and between 1 and 80 characters in length")	
	private String learningConfigUID;
	
	@ApiModelProperty(required=false, value="Unique Name of the LearningConfig")
	@Length(min=1, max=80, message="learningConfigName must be between 1 and 80 characters in length")
	private String learningConfigName;
	
	@ApiModelProperty(required=false, value="Description of the LearningConfig")
	@Length(min=0, max=255, message="learningConfigDescription must be between 1 and 255 characters in length")
	private String learningConfigDescription;
		
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningConfigUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}	
	
	
	@JsonIgnore	
	public LearningConfigInfoDTO createLearningConfigInfoDTO() {
		LearningConfigInfoDTO dto = new LearningConfigInfoDTO();
		dto.setChannelUID(null);		
		dto.setLearningConfigName(learningConfigName);
		dto.setLearningConfigDescription((learningConfigDescription == null?"":learningConfigDescription));
		dto.setLearningConfigUID(learningConfigUID);
		dto.setModelType(null);
		dto.setProductUID(null);
		dto.setIsDeployed(false);
		dto.setIsDeleted(false);
		dto.setIsPublished(false);
		return dto;
	}
}
