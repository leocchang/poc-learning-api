package com.aktana.learning.api.dto.dse;

import java.util.ArrayList;
import java.util.List;

public class SEConfigGlobalParameterTypeDTO extends SEConfigElementTypeDTO {

	protected List<SEConfigParameterTypeDTO> parameters;

	public SEConfigGlobalParameterTypeDTO() {
	}

	public List<SEConfigParameterTypeDTO> getParameters() {
		// lazy initialization
		if (null == parameters) {
			parameters = new ArrayList<SEConfigParameterTypeDTO>();
		}
		return parameters;
	}

	public void setParameters(List<SEConfigParameterTypeDTO> parameters) {
		this.parameters = parameters;
	}
}
