/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "Message info")
@Data
public class MessageInfoDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned message unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="messageUID")	
	private String messageUID;
	
	@ApiModelProperty(required=false, value="name of the message")
	private String messageName;
	
	@ApiModelProperty(required=false, value="subject of sent email")
	private String emailSubject;
	
	@ApiModelProperty(required=false, value="body of sent email")
	private String emailBody;

}