/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config version")
@Data
public class LearningVersionDTO implements IExternallyIdentifiableDTO {

	@ApiModelProperty(required=true, value="Externally-assigned unique version identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unique and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned unique config identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningConfigUID must be unique and between 1 and 80 characters in length")	
	private String learningConfigUID;

	@Override
	@JsonIgnore
	public String getObjectUID() {
		return learningVersionUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}	
	
	
	@JsonIgnore	
	public LearningVersionInfoDTO createLearningVersionInfoDTO() {
		LearningVersionInfoDTO dto = new LearningVersionInfoDTO();
		dto.setLearningConfigUID(learningConfigUID);
		dto.setLearningVersionUID(learningVersionUID);
		dto.setIsDeployed(false);
		dto.setIsFinalized(false);
		dto.setIsDeleted(false);
		return dto;
	}
}
