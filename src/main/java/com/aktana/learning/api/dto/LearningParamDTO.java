/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "A learning config param")
@Data
public class LearningParamDTO implements IExternallyIdentifiableDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningVersionUID must be unique and between 1 and 80 characters in length")	
	private String learningVersionUID;
	
	@ApiModelProperty(required=true, value="Paramter name")
	@NotBlank
	@Length(min=1, max=80, message="paramName must be between 1 and 80 characters in length")	
	private String paramName;
	
	@ApiModelProperty(required=true, value="Parameter index to specify order is case of multiple items")
	@NotNull
	@Range(min=0, message="paramIndex must be equal or larger than 0")
	private Integer paramIndex;
	
	@ApiModelProperty(required=false, value="Parameter value")
	@NotBlank
	@Length(min=0, message="paramValue is optional")	
	private String paramValue;
	
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return ((learningVersionUID != null) ? learningVersionUID : "") + "~" + ((paramName != null) ? paramName : "") + ((paramIndex != null) ? "~"+paramIndex : "");
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return true;
	}				
}
