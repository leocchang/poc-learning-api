/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationSegment Representation */
@ApiModel(value = "A Simulation Segment")
@Data
public class SimulationSegmentDTO implements IExternallyIdentifiableDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned segment unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="segmentUID must be unqiue and between 1 and 80 characters in length")	
	private String segmentUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned build unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningBuildUID must be between 1 and 80 characters in length")	
	private String learningBuildUID;
	
	@ApiModelProperty(required=true, value="Externally-assigned run unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="learningRunUID must be between 1 and 80 characters in length")	
	private String learningRunUID;
	
	@ApiModelProperty(required=false, value="Segment name")
	@NotBlank
	@Length(min=1, max=45, message="segmentName must be between 1 and 45 characters in length")	
	private String segmentName;
	
	@ApiModelProperty(required=false, value="Segment Value")
	@NotBlank
	@Length(min=1, max=45, message="segmentValue must be between 1 and 45 characters in length")	
	private String segmentValue;
	
	@ApiModelProperty(required=true, value="date created")
	@NotBlank
	private String createdAt;
	
	@ApiModelProperty(required=true, value="date last updated")
	@NotBlank
	private String updatedAt;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return segmentUID;
	}
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}