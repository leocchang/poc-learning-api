/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** SimulationMessageSequence Representation */
@ApiModel(value = "A Simulation Message Sequence")
@Data
public class SimulationAccountMessageSequenceDTO { 
	
	@ApiModelProperty(required=true, value="Externally-assigned segment unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="messageSeqUID must be between 1 and 80 characters in length")	
	private String messageSeqUID;
	
	@ApiModelProperty(required=true, value="message seqence hash ")
	@NotBlank
	@Length(min=1, max=80, message="messageSeqHash must be between 1 and 80 characters in length")	
	private String messageSeqHash;
	
	@ApiModelProperty(required=true, value="message identifier")
	@NotBlank
	@Length(min=1, max=80, message="messageID must be between 1 and 80 characters in length")	
	private String messageID;
	
	@ApiModelProperty(required=true, value="sequence order")	
	@NotBlank
	private Integer sequenceOrder;
	
	
	@ApiModelProperty(required=true, value="Externally-assigned interaction unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="interactionId must be between 1 and 80 characters in length")	
	private String interactionId;
	
	@ApiModelProperty(required=true, value="Externally-assigned account unique identifier")
	@NotBlank
	@Length(min=1, max=80, message="accountUID must be between 1 and 80 characters in length")	
	private String accountUID;
	
	@ApiModelProperty(required=false, value="isOpen must be true or false")
	private Boolean isOpen;
	
	@ApiModelProperty(required=false, value="isOpen must be true or false")
	private String status;
	
	@ApiModelProperty(required=false, value="number of email clicks")
	private Integer clickCount;
	
	@ApiModelProperty(required=false, value="name of the message")
	private String name;
	
	@ApiModelProperty(required=false, value="subject of sent email")
	private String emailSubject;
	
	@ApiModelProperty(required=false, value="body of sent email")
	private String emailBody;
	
	@ApiModelProperty(required=false, value="date email was sent")
	private String emailSentDate;
	
	@ApiModelProperty(required=false)
	private String senderEmailID;
	
	@ApiModelProperty(required=false)
	private String accountEmailID;
	
	@ApiModelProperty(required=false, value="date email was opened")
	private String emailLastOpenedDate;
	
	@ApiModelProperty(required=false, value="date email was last clicked")
	private String lastClickDate;
	
	@ApiModelProperty(required=false, value="date created")
	private String createdAt;
	
	@ApiModelProperty(required=false, value="date last updated")
	private String updatedAt;
				
}