/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** LearningConfig Representation */
@ApiModel(value = "Message info")
@Data
public class RepEngagementCalculationDTO {
	
	@ApiModelProperty(required=true, value="Externally-assigned unique repTeam identifier")
	@NotBlank
	private List<RepTeamsDTO> repTeams;

	@ApiModelProperty(required=true, value="Externally-assigned unique territories identifier")
	@NotBlank
	private List<TerritoriesDTO> territories;
	
	@ApiModelProperty(required=true, value="Years")
	@NotBlank
	private List<String> startAndEndDates;
	
}