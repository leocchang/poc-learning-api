/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2015 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.dto.dse;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IMarkableForDeleteDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/** RepTeam Representation */
@ApiModel(value = "Reps are typically divided into teams based on (a) which Product or Products the team covers, and geographic territory")
@Data
public class RepTeamDTO implements IExternallyIdentifiableDTO, IMarkableForDeleteDTO {
	@ApiModelProperty(required=true, value="Externally-assigned unique identifier")
	@NotBlank		
	@Length(min=1, max=20, message="repTeamUID must be between 1 and 20 characters in length")	
	private String repTeamUID;
	@ApiModelProperty(required=true, value="Unique short name of the RepTeam")
	@Length(min=0, max=30, message="repTeamName must be between 0 and 30 characters in length")	
	@NotBlank			
	private String repTeamName;
	@ApiModelProperty(required=true, value="true=the RepTeam has been deleted, false=the RepTeam is active")
	@NotNull	
	private Boolean isDeleted;
	@ApiModelProperty(required=false, value="Read Only. DseConfig Id.")
	private Integer seConfigId;
	@ApiModelProperty(required=false, value="Read Only. Number of reps assigned to team.")	
	private Integer numReps;
	@ApiModelProperty(required=false, value="Read Only. Number of active reps(active and not deleted) assigned to team.")
	private Integer activeNumReps;
	
	@Override
	@JsonIgnore
	public String getObjectUID() {
		return repTeamUID;
	}	
	
	@Override
	@JsonIgnore	
	public boolean isCompoundObjectUID() {
		return false;
	}				
}

