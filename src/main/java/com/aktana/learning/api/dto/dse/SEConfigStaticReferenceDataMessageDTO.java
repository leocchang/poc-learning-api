package com.aktana.learning.api.dto.dse;

public class SEConfigStaticReferenceDataMessageDTO extends GenericResponseDTO {

	private SEConfigStaticReferenceDataDTO referenceData;

	public SEConfigStaticReferenceDataMessageDTO() {
		referenceData = new SEConfigStaticReferenceDataDTO();
	}

	public SEConfigStaticReferenceDataDTO getReferenceData() {
		return referenceData;
	}

	public void setReferenceData(SEConfigStaticReferenceDataDTO referenceData) {
		this.referenceData = referenceData;
	}
}
