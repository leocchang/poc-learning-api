package com.aktana.learning.api.security;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AktanaAccessToken {

    private String token;
    private String refreshToken;
    private boolean isInternal;
}
