package com.aktana.learning.api.security;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.ServiceLocatorProvider;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

public class AktanaApiAuthFeature implements Feature {
    @Override
    public boolean configure(FeatureContext ctx) {
        ServiceLocator locator = ServiceLocatorProvider.getServiceLocator(ctx);
        AktanaApiAuthenticator authenticator = new AktanaApiAuthenticator();
        locator.inject(authenticator);
        ctx.register(new AuthDynamicFeature(new OAuthCredentialAuthFilter.Builder<UserPrincipalDTO>()
                .setAuthenticator(authenticator)
                .setPrefix("Bearer")
                .buildAuthFilter()));
        ctx.register(new AuthValueFactoryProvider.Binder<>(UserPrincipalDTO.class));
        return true;
    }
}