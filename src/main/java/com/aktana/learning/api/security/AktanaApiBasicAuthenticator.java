package com.aktana.learning.api.security;

import java.util.Optional;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.UserPrincipalDTO;

public class AktanaApiBasicAuthenticator implements Authenticator<BasicCredentials, UserPrincipalDTO>  {
    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaApiAuthenticator.class);
  
    @Override
    public Optional<UserPrincipalDTO> authenticate(BasicCredentials credentials) throws AuthenticationException {
    	
		TokenGenerationHelper helper = new TokenGenerationHelper();		
		UserPrincipalDTO dto = helper.validate(credentials.getPassword());
		return Optional.of(dto);
		
    }    
    	
}
