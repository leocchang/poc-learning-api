package com.aktana.learning.api.security;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AktanaAimPermissionEnum {

	USER("Learning.User");

	private String code;

	AktanaAimPermissionEnum(String code) {
		this.code = code;
	}
	
	@JsonValue
	public String getCode() {
		return code;
	}

}
