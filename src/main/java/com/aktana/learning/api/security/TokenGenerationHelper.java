package com.aktana.learning.api.security;

import com.aktana.learning.api.AimConfiguration;
import com.aktana.learning.api.AktanaService;
import com.aktana.learning.api.allconstsandenums.Constants;
import com.aktana.learning.api.bo.LearningUserBo;
import com.aktana.learning.api.bo.special.SysParameterBo;
import com.aktana.learning.api.dto.SimpleTokenRequestDTO;
import com.aktana.learning.api.dto.TokenRequestDTO;
import com.aktana.learning.api.dto.InternalTokenRequestDTO;
import com.aktana.rbac.lib.keycloak.KeycloakAccessTokenResponse;
import com.aktana.rbac.lib.token.UserTokenObject;
import com.aktana.rbac.lib.util.AccessEvaluator;
import com.aktana.rbac.lib.util.UserAccess;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.learning.api.dto.useradmin.AktanaUserDTO;
import com.aktana.learning.api.dto.useradmin.LearningUserDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.AktanaUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;

public class TokenGenerationHelper {
	
	/**
     * Token Cache
     * 
     * Cache holds all active tokens
     * Allows fast retrieval of user info and more importantly allows invalidating tokens which would otherwise be vulnerable for hijacking.
     */
    private static ConcurrentHashMap<String,LearningUserDTO> LEARNING_TOKEN_CACHE = new ConcurrentHashMap<String,LearningUserDTO>();
	
    /**
     * Token Lookup
     * 
     * Lookup table holds a list of tokens for each user to allow log out functionality.
     * We allow a user to have multiple session tokens. On logout, all user's tokens are invalidated.
     */
    private static ConcurrentHashMap<String, List<String>> TOKEN_LOOKUP = new ConcurrentHashMap<String, List<String>>();

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenGenerationHelper.class);
    private static Set<String> TOKEN_CACHE = ConcurrentHashMap.newKeySet();
	private static AimConfiguration aimConfiguration;
	private static AccessEvaluator accessEvaluator;
	private LearningUserBo LearningUserBo;

	public TokenGenerationHelper() {    
		LearningUserBo = new LearningUserBo();
    }
	
	static {
		try {
			aimConfiguration = AktanaService.getInstance().getConfiguration().getAimConfiguration();
			accessEvaluator = AccessEvaluator.getInstance(aimConfiguration.getAuthenticationURL(), aimConfiguration.getApplicationId(), aimConfiguration.getApplicationToken());
		} catch (Exception e) {
			LOGGER.error("Failed to connect to AIM service");
		}
	}

	/*public AktanaAccessToken validateRequest(SimpleTokenRequestDTO tokenRequest, boolean validateSecret) {
		AktanaAccessToken token;

		try {
			token = validateTraditionalRequest(tokenRequest, validateSecret);
		} catch (GenericValidationException e) {
			token = validateAimRequest(tokenRequest);
		}
		return token;
	} */


	public String validateTraditionalRequest(TokenRequestDTO tokenRequest) {
		// verify passed in secret is the same as configured value
        String secret = SysParameterBo.getStringValue("apitokenSecret");        
        if (!secret.equals(tokenRequest.getSecret())) {
        	LOGGER.debug("apitokenSecret does not match configuration: " + tokenRequest.getSecret());
    		throw new GenericValidationException("Invalid Username, Password or Secret");	    					        	            	
        }
        
        // verify password      
        String userName = tokenRequest.getUsername();
        AktanaUser user =  getUserByUsername(userName);
        
        if (user != null) {
        	if(user.getIsDeleted()) {
            	LOGGER.debug("Inactive user");        		
        		throw new GenericValidationException("Invalid Username, Password or Secret");	       		
        	}

			if (!PasswordHelper.validate(tokenRequest.getPassword(), user.getPassword())) {
				LOGGER.debug("Invalid password");
        		throw new GenericValidationException("Invalid Username, Password or Secret");	    					        	            	
        	}
        } else {
        	LOGGER.debug("Invalid username");        	
    		throw new GenericValidationException("Invalid Username, Password or Secret");	    					        	
        }
        
        LearningUserDTO dto = LearningUserBo.toDTO(user);
		
        userName = dto.getUserName();
        
        //cache cleanup
		cleanCache();
		
        secret = SysParameterBo.getStringValue("apitokenSecret");
		int expiry = SysParameterBo.getIntegerValue("apitokenExpiryMinutes");
		DateTime time = DateTime.now();        
		time = time.plusMinutes(expiry);
        String compact = Jwts.builder().setSubject(userName).setExpiration(time.toDate()).signWith(HS256, secret).compact();
        
        List<String> tokenList = TOKEN_LOOKUP.get(userName);
        if(tokenList == null) {
        	tokenList = new ArrayList<String>();
        	TOKEN_LOOKUP.put(userName, tokenList);
        }
        tokenList.add(compact);
        
        LEARNING_TOKEN_CACHE.put(compact, dto);        
        return compact;		
		
	}

    
    public AktanaAccessToken validateAimRequest(SimpleTokenRequestDTO tokenRequest) {
		AktanaAccessToken accessToken = new AktanaAccessToken();
		try {
			KeycloakAccessTokenResponse tokenResponse = accessEvaluator.login(aimConfiguration.getAuthenticationURL(), tokenRequest.getUsername(), tokenRequest.getPassword());
			accessToken.setInternal(false);
			accessToken.setToken(tokenResponse.getToken());
			accessToken.setRefreshToken(tokenResponse.getRefreshToken());
		} catch (Exception e) {
			throw new GenericValidationException(401, "Invalid credentials!");
		}
		return accessToken;
    }

	public AktanaAccessToken validateInternalRequest(InternalTokenRequestDTO tokenRequest) {
		String secret = SysParameterBo.getStringValue("apitokenSecret");
		if (secret!= null && secret.equals(tokenRequest.getSecret()) // secret always need to be matched
		) {
			LOGGER.info("Internal service logged in!!");
			String token = generateToken();
			TOKEN_CACHE.add(token);
			AktanaAccessToken accessToken = new AktanaAccessToken();
			accessToken.setInternal(true);
			accessToken.setToken(token);
			return accessToken;
		}

		throw new GenericValidationException(401, "Invalid credentials!");
	}
    
	public AktanaUserDTO getUserFromToken(AktanaAccessToken accessToken) {
		String username = SysParameterBo.getStringValue("apitokenUsername");
		AktanaUserDTO userDTO = new AktanaUserDTO();
		String token = accessToken.getToken();
		//Note: Hitting here implies that the token has been generated, either AIM token or Internal token.
		if (!accessToken.isInternal()) {
			// Case: AIM token
			try {
				UserAccess userAccess = accessEvaluator.getUserAccess(aimConfiguration.getRealmId(), aimConfiguration.getPublicKey(), token);
				UserTokenObject userTokenObject = userAccess.getUserTokenObject();
				userDTO.setEmail(userTokenObject.getUserEmail());
				userDTO.setUserDisplayName(userTokenObject.getUserFirstName() + " " + userTokenObject.getUserLastName());
				userDTO.setUserName(userTokenObject.getUserUsername());
				userDTO.setUserId(userTokenObject.getUserSubject());
				userDTO.setAimPermissions(userAccess.getPermissionSet());
			} catch (Exception e) {
				throw new GenericValidationException(403, "Invalid realm. Access denied");
			}
		} else {
			// Case: Internal token
			userDTO.setUserName(StringUtils.isNotBlank(username) ? username : Constants.INTERNAL_SYS_USER );
			userDTO.setUserDisplayName(userDTO.getUserName());
			userDTO.setUserId(new UUID(0,0).toString());
			grantAllPermissions(userDTO);
		}
		return userDTO;
	}

	public UserPrincipalDTO validate(String token) {
		UserPrincipalDTO dto;
		try {
			dto = validateInternalToken(token);
		} catch (Exception e) {
			dto = validateAimToken(token);
		}
		return dto;
	}

	private UserPrincipalDTO validateInternalToken(String token) {
		String secretKey = SysParameterBo.getStringValue("apitokenSecret");            	
		Boolean disableTokenSecurity = SysParameterBo.getBooleanValue("disableTokenSecurity"); 
		if(disableTokenSecurity == null) {
			disableTokenSecurity = false;
		}
		
		String userName = null;
		try {
			userName = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
		} catch (SignatureException e) {
			//don't trust the JWT!
			LOGGER.debug("Unable to parse token", e);
			LEARNING_TOKEN_CACHE.remove(token);
			throw new GenericValidationException("Invalid Token");	    					        	            				
		} catch (MalformedJwtException e) {
			LOGGER.debug("Malformed JWT Exception", e);
			LEARNING_TOKEN_CACHE.remove(token);
			throw new GenericValidationException("Malformed Token");	    					        	            							
		} catch (io.jsonwebtoken.ExpiredJwtException e) {
			LOGGER.debug("Expired JWT Exception", e);
			LEARNING_TOKEN_CACHE.remove(token);
			throw new GenericValidationException("Expired Token: " + e.getMessage());	    					        	            										
		}
		
		UserPrincipalDTO userPrincipalDTO = new UserPrincipalDTO();

		return userPrincipalDTO;
	}

	private UserPrincipalDTO validateAimToken(String token) {
		try {
			UserAccess userAccess = accessEvaluator.getUserAccess(aimConfiguration.getRealmId(), aimConfiguration.getPublicKey(), token);
			UserPrincipalDTO userPrincipalDTO = new UserPrincipalDTO();
			userPrincipalDTO.setName(userAccess.getUserTokenObject().getUserUsername());
			userPrincipalDTO.setAimPermissions(userAccess.getPermissionSet());
			return userPrincipalDTO;
		} catch (Exception e) {
			LOGGER.debug("Access denied - " + e.getMessage());
			throw new GenericValidationException(403, "Access denied!");
		}
	}

	private String generateToken() {
		String secret = SysParameterBo.getStringValue("apitokenSecret");
		int expiry = SysParameterBo.getIntegerValue("apitokenExpiryMinutes");
		DateTime time = DateTime.now();
		time = time.plusMinutes(expiry);
		String token = Jwts.builder().setSubject(secret).setExpiration(time.toDate()).signWith(HS256, secret).compact();
		return token;
	}

	private void grantAllPermissions(UserPrincipalDTO userDTO) {
		Set<String> permissionSet = new HashSet<>();
		permissionSet.addAll(Arrays.asList(AktanaAimPermissionEnum.values()).stream().map(p -> p.getCode()).collect(Collectors.toList()));
		userDTO.setAimPermissions(permissionSet);
	}
	
	private AktanaUser getUserByUsername(String userName) {
    	
    		AktanaUser user = new AktanaUser();
        user.setUserName(userName);
        List<AktanaUser> userList = LearningUserBo.findByExample(user);
        
        AktanaUser lookupUser = null;
        if (userList.size() >= 1) {        	
        	lookupUser = userList.get(0);        	
        }
 
        return lookupUser;
    }
	
	private void removeTokenFromLookup(String compact, String userName) {
		
		if(userName != null) {
			List<String> tokenList = TOKEN_LOOKUP.get(userName);
			if(tokenList != null) {
				tokenList.remove(compact);
				if(tokenList.size() == 0) {
					TOKEN_LOOKUP.remove(userName);
				}
			}
		}
		
		LOGGER.debug("Cache Cleanup: AFTER Token cache size="+TOKEN_CACHE.size()+ " lookup size="+TOKEN_LOOKUP.size());
	}
	
	private void cleanCache() {
		String secretKey = SysParameterBo.getStringValue("apitokenSecret");
		
		LOGGER.debug("Cache Cleanup: BEFORE Token cache size="+TOKEN_CACHE.size()+ " lookup size="+TOKEN_LOOKUP.size());
		
		// iterate over cache and cleanup expired tokens
		for (Entry<String, LearningUserDTO> entry : LEARNING_TOKEN_CACHE.entrySet()) {
			String compact = entry.getKey();
			LearningUserDTO user = entry.getValue();
			
			try {
				Jwts.parser().setSigningKey(secretKey).parseClaimsJws(compact).getBody().getSubject();
			} catch (Exception e) {
				LOGGER.debug("Cleaning token "+compact);
				TOKEN_CACHE.remove(compact);
				removeTokenFromLookup(compact, user.getUserName());
			}
		}
		
		LOGGER.debug("Cache Cleanup: AFTER Token cache size="+TOKEN_CACHE.size()+ " lookup size="+TOKEN_LOOKUP.size());
	}
}

