package com.aktana.learning.api.security;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AktanaUserRoleEnum {
	
	USER("0"),
	EDITOR("1"),
	MANAGER("2"),
	ADMIN("3");

	private String id;


	private AktanaUserRoleEnum(String id) {
		this.id = id;
	}


	public static AktanaUserRoleEnum forOrdinal(int ordinal) {  
		for (AktanaUserRoleEnum anEnum : AktanaUserRoleEnum.values()) {
			if (ordinal == anEnum.ordinal()) {
				return anEnum;
			}
		}
		return null;
	}
	
	public static AktanaUserRoleEnum fromId(String id) {  
		for (AktanaUserRoleEnum anEnum : AktanaUserRoleEnum.values()) {
			if (anEnum.getId().equals(id)) {
				return anEnum;
			}
		}
				
		return null;
	}
	
	
	@JsonValue
	public String getUid() {
		return this.toString();
	}
	
	@JsonValue
	public String getId() {
		return id;
	}

}
