package com.aktana.learning.api.security;
import java.util.Optional;
import com.aktana.learning.api.dto.UserPrincipalDTO;
import com.aktana.rbac.lib.util.AccessLogUtil;
import com.google.inject.Provider;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

public class AktanaApiAuthenticator implements Authenticator<String, UserPrincipalDTO>  {
    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaApiAuthenticator.class);
        
    @Context
    Provider<HttpServletRequest> requestProvider;
    @Override
    public Optional<UserPrincipalDTO> authenticate(String compactJwt) throws AuthenticationException {
        
        TokenGenerationHelper helper = new TokenGenerationHelper();     
        UserPrincipalDTO dto = helper.validate(compactJwt);
        HttpServletRequest request = requestProvider.get();
        if (request != null) {
            AccessLogUtil.info(LOGGER, request.getMethod(), getRequestUri(request), request.getQueryString(),
                    request.getParameterMap(), dto.getName());
        } else {
            LOGGER.warn("Injection of HttpServletRequest failed!! Logging into datalog will be skipped.");
        }
        return Optional.of(dto);
        
    }
    private String getRequestUri(HttpServletRequest request) {
        String uri = (String) request.getAttribute("javax.servlet.include.request_uri");
        if (uri == null) {
            uri = request.getRequestURI();
        }
        return uri;
    }
}