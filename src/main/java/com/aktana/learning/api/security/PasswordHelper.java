package com.aktana.learning.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordHelper.class);
	private static String secret = "sugar!";

	public static final int PASSWORD_MIN_LENGTH = 8;
	public static final int PASSWORD_MIN_DIGITS = 1;
	public static final int PASSWORD_MIN_UPPER = 1;
	public static final int PASSWORD_MIN_LOWER = 1;
	public static final int PASSWORD_MIN_SPECIAL = 1;
	
	public static final String SPECIAL_CHARS = "!?&*%$^~#@";
	public static final String PASSWORD_MESSAGE = "password must be at least 8 characters long and have at least 1 digit, 1 uppercase letter, and 1 of the following special characters: !?&*%%$^~#@ .";
	
	public static String encrypt(String password) {

		try {
			return crypt(password);
		} catch (Exception e) { }
		
		return null;
	}
	
	public static boolean validate(String password, String encryptedPassword) {

		try {
			String encrypted = crypt(password);
			return encrypted.equals(encryptedPassword);
		} catch (Exception e) {
			LOGGER.error("Couldn't able to verify password, returning false.", e);
		}

		return false;
	}
	
	public static boolean check(String password) {
		if (password == null || password.length() < PASSWORD_MIN_LENGTH) {
			return false;
		}
		
		int digitCounter = 0;
		int upperCounter = 0;
		int lowerCounter = 0;
		int specialCounter = 0;
		
		for(int i = 0; i< password.length(); i++) {
			char c = password.charAt(i);
            if(Character.isUpperCase(c)){
            	upperCounter++;
            }
            if(Character.isLowerCase(c)){
            	lowerCounter++;
            }
            if(Character.isDigit(c)){
            	digitCounter++;
            }
            if(SPECIAL_CHARS.indexOf(c) != -1) {
            	specialCounter++;
            }
		}
		
		return (upperCounter >= PASSWORD_MIN_UPPER && lowerCounter >= PASSWORD_MIN_LOWER && digitCounter >= PASSWORD_MIN_DIGITS && specialCounter >= PASSWORD_MIN_SPECIAL);
	}

    private static String crypt(String str) throws NoSuchAlgorithmException {
    	if (str == null || str.length() == 0) {
    		throw new IllegalArgumentException("String to encrypt cannot be null or zero length");
    	}

    	str += secret;
    	
    	MessageDigest digester = MessageDigest.getInstance("MD5");
    	digester.update(str.getBytes());
    	byte[] hash = digester.digest();
    	StringBuffer hexString = new StringBuffer();
    	for (int i = 0; i < hash.length; i++) {
    		if ((0xff & hash[i]) < 0x10) {
    			hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
    		}
    		else {
    			hexString.append(Integer.toHexString(0xFF & hash[i]));
    		}
    	}
    	return hexString.toString();
    }
}
