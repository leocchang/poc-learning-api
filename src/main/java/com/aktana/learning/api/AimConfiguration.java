/*****************************************************************
 * <p>Log Configuration class wraps all the properties required to configure a logger and appender.</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class AimConfiguration {

    @NotNull
    @JsonProperty
    // this is the AIM service url
    private String authenticationURL;

    @NotNull
    @JsonProperty
    // the client_id to the OAuth standard.
    private String applicationId;

    @NotNull
    @JsonProperty
    // the client_secret to the OAuth standard.
    private String applicationToken;

    @NotNull
    @JsonProperty
    // this is the id implying the data scope
    private String realmId;

    @NotNull
    @JsonProperty
    // the public key for jwt token
    private String publicKey;

    public String getAuthenticationURL() {
        return authenticationURL;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getApplicationToken() {
        return applicationToken;
    }

    public String getRealmId() {
        return realmId;
    }

    public String getPublicKey() {
        return publicKey;
    }
}
