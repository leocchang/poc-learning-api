package com.aktana.learning.api.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;
import com.aktana.learning.api.resource.HibernateSessionHelper;
import javax.inject.*;

/*
 * Any method decorated with the @Trasactional annotation will go through this
 * guice invoked interceptor to open session and cleanup session
 * 
 * Used for Quartz jobs and SOAP service invocations (possibly)
 */
public class TransactionInterceptor implements MethodInterceptor {
	
	private SessionFactory sessionFactory;
	
	@Inject
    public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
		HibernateSessionHelper helper = new HibernateSessionHelper(getSessionFactory());		
		final Session session = helper.getSessionFactory().openSession();		
		Object retvalue = null;		
        try {        	
            helper.configureSession(session);
            ManagedSessionContext.bind(session);
            helper.beginTransaction(session);            
            try {
            	retvalue = invocation.proceed();            	
        		helper.commitTransaction(session);        		
            } catch (Exception e) {
                helper.rollbackTransaction(session);
                helper.<RuntimeException>rethrow(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(helper.getSessionFactory());
            helper=null;
        }        

        return retvalue;
    }
}