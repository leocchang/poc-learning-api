package com.aktana.learning.api.bo.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.glasnost.orika.MapperFactory;

import com.aktana.learning.api.dto.AccountMessageSequenceDTO;
import com.aktana.learning.persistence.models.impl.AccountMessageSequence;



public class AccountMessageSequenceMapper extends AbstractMapperSupport<AccountMessageSequence, AccountMessageSequenceDTO>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountMessageSequenceMapper.class);

    private static AccountMessageSequenceMapper instance;
    
    public static AccountMessageSequenceMapper getInstance()  {

		synchronized(AccountMessageSequenceMapper.class) {
			if (instance == null) {
				instance = new AccountMessageSequenceMapper();
			}
		}
    	
    	return instance;    	
    }
	
	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(AccountMessageSequenceDTO.class, AccountMessageSequence.class)
				.fieldMap("probability", "probability").add()
				.fieldMap("isPredict", "isPredict").add()
				.fieldMap("modelId", "modelId").add()
				//.fieldMap("accountUID", "id.account.externalId").add()
				//.fieldMap("messageAlgorithmUID", "id.messageAlgorithm.externalId").add()	
				//.fieldMap("messageUID", "id.message.externalId").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("endDate", "endDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());

	}

	@Override
	protected void initializeMappingsToDTO(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(AccountMessageSequence.class, AccountMessageSequenceDTO.class)
				.fieldMap("probability", "probability").add()
				.fieldMap("isPredict", "isPredict").add()
				.fieldMap("modelId", "modelId").add()
				//.fieldMap("id.account.externalId", "accountUID").add()
				//.fieldMap("id.messageAlgorithm.externalId", "messageAlgorithmUID").add()	
				//.fieldMap("id.message.externalId", "messageUID").add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());	
	}


	@Override
	public Class<?> getPojoClass() {
		return AccountMessageSequence.class;
	}

	@Override
	public Class<?> getDTOClass() {
		return AccountMessageSequenceDTO.class;
	}

	
	@Override
	public AccountMessageSequence toPojo(AccountMessageSequenceDTO dto)
	{
		AccountMessageSequence pojo = super.toPojo(dto);
		this.applyCustomMappingsToPojo(dto, pojo);
		return pojo;
	}


	@Override
	public AccountMessageSequenceDTO toDTO(AccountMessageSequence pojo)
	{
		AccountMessageSequenceDTO dto = super.toDTO(pojo);
		this.applyCustomMappingsToDTO(pojo, dto);
		return dto;
	}
	
}