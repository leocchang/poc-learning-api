/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.persistence.models.impl.AktanaUser;
import com.aktana.learning.api.bo.mapper.LearningUserMapper;
import com.aktana.learning.api.dto.useradmin.AktanaUserDTO;
import com.aktana.learning.api.dto.useradmin.LearningUserDTO;
import com.aktana.learning.api.security.AktanaUserRoleEnum;
import com.aktana.learning.api.security.PasswordHelper;
import com.aktana.learning.common.exception.GenericValidationException;


/** BO class for AktanaUser 
 * 
 */
public class LearningUserBo extends AbstractBo<LearningUserDTO, AktanaUser, Integer> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningUserBo() {
		super(LearningUserBo.class, LearningUserDTO.class, AktanaUser.class, LearningUserMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());
		
	}


	/** use id as externalId **/
	@Override
	public AktanaUser readModelWithExternalId(String id) {
		return getDao().get(Integer.parseInt(id));
	}
	
	
	@Override
	protected AktanaUser beforeAction(String action, LearningUserDTO dto, AktanaUser model) {				
		
		if (action.equalsIgnoreCase(LearningUserBo.ACTION_SAVE) || action.equalsIgnoreCase(LearningUserBo.ACTION_UPDATE)) {
        	   
			String encryptedPassword = null;
			
			String password = model.getPassword();
			if(password != null && !password.trim().isEmpty()) {  // new password provided
				encryptedPassword = PasswordHelper.encrypt(password.trim());
				if(encryptedPassword == null) {
					throw new GenericValidationException(500, 0, "Encryption Failed");
				}
			} else if (action.equalsIgnoreCase(LearningUserBo.ACTION_UPDATE)) {  // password already exists
				AktanaUser existingUser = getDao().get(model.getId());
				encryptedPassword = existingUser.getPassword();
			} else {  // no password provided - generate random
				encryptedPassword = PasswordHelper.encrypt(Double.toString(Math.round(Math.random() * 1000000)));
				if(encryptedPassword == null) {
					throw new GenericValidationException(500, 0, "Encryption Failed");
				}
			}
			
			model.setPassword(encryptedPassword);
		}
				
		return model;
	}
	
    private boolean isUserNameUnique(String userName, String userId) {
    	
    	AktanaUser user = new AktanaUser();
        user.setUserName(userName);
        List<AktanaUser> userList = this.findByExample(user);
        if(userList.size() < 1) {
        	return true;
        }
        
        // sanity check
        if(userList.size() > 1) {
        	throw new GenericValidationException(400, 0, "User "+userName+" has multiple entries");
        }
        
        // existing user
        if ((userId != null) && !userId.isEmpty()) {
        	AktanaUser lookupUser = userList.get(0);
        	if(userId.equals(""+lookupUser.getId())) {
        		return true;
        	}
        }
        
        return false;
    }
	
	@Override	
	protected void validateDTO(LearningUserDTO dto) {
		super.validateDTO(dto);
		
		String userName = dto.getUserName();
		if(userName == null || userName.trim().isEmpty()) {
			throw new GenericValidationException(400, 0, "Username required");
		}
		
		if(!this.isUserNameUnique(userName, dto.getUserId())) {
		   throw new GenericValidationException(400, 0, "User "+userName+" already in use");
		}
		
		String password = dto.getPassword();
		if(password != null && !password.trim().isEmpty() && !PasswordHelper.check(password.trim())) {
			throw new GenericValidationException(400, 0, PasswordHelper.PASSWORD_MESSAGE);
		}
		
		String roleId = dto.getRoleId();
		if(roleId == null || roleId.isEmpty() || AktanaUserRoleEnum.fromId(roleId) == null) {
			throw new GenericValidationException(400, 0, "Role Id required");
		}
		
		Boolean isSsoEnabled = dto.getIsSsoEnabled();
		if(isSsoEnabled != null && isSsoEnabled) {
			String ssoId = dto.getSsoId();
			if(ssoId == null || ssoId.isEmpty()) {
				throw new GenericValidationException(400, 0, "SSO Id required when SSO is enabled");
			}
		}

	}

	@Override
	protected void transferId(AktanaUser newModel, AktanaUser oldModel) {
		newModel.setId(oldModel.getId());
		
	}

	@Override
	protected void setId(AktanaUser newModel, Integer pk) {	
	}
}
