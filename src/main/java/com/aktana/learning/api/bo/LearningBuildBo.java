/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningBuildMapper;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;

import com.aktana.learning.common.learningS3.LearningS3ConnectionException;
import com.aktana.learning.common.learningS3.LearningS3Connector;

import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningFile;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.LearningVersion;
import com.aktana.learning.persistence.models.impl.S3PartialPaths;

import com.aktana.learning.api.bo.LearningRunBo;
import com.aktana.learning.api.bo.LearningConfigBo;
import com.aktana.learning.persistence.models.ILearningConfig;

/** Base class for LearningBuild */
public class LearningBuildBo extends AbstractBo<LearningBuildDTO, LearningBuild, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningVersion, String> learningVersionDao = null;
    private GenericDao<LearningFile, String> learningFileDao = null;
    private GenericDao<LearningRun, String> learningRunDao = null;

	
    
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningBuildBo() {
		super(LearningBuildBo.class, LearningBuildDTO.class, LearningBuild.class, LearningBuildMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
		learningVersionDao = new GenericDaoImpl<LearningVersion, String>(LearningVersion.class);
		learningFileDao = new GenericDaoImpl<LearningFile, String>(LearningFile.class);
		learningRunDao = new GenericDaoImpl<LearningRun, String>(LearningRun.class);

	}
	private GenericDao<LearningVersion, String> getLearningVersionDao() {
		learningVersionDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningVersionDao;
	}

	private GenericDao<LearningFile, String> getLearningFileDao() {
		learningFileDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningFileDao;
	}
	
	private GenericDao<LearningRun, String> getLearningRunDao() {
		learningRunDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningRunDao;
	}
	
	/** common method for listing a resource */
	@Override	
	public List<LearningBuildDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** Returns a List of all LearningBuild objects. */
	public List<LearningBuild> findAll() {
		return getDao().findAll();
	}
	
	@Override
	protected LearningBuild beforeAction(String action, LearningBuildDTO dto, LearningBuild model) {	
		
		if (action.equalsIgnoreCase(LearningConfigBo.ACTION_REMOVE)) {
						
			if(model.getIsDeleted()) {
				throw new GenericValidationException("LearningBuild %s is already deleted", model.getId());
			}
			
			if(model.getIsDeployed()) {
				throw new GenericValidationException("LearningBuild %s is deployed and cannot be deleted", model.getId());
			}
			
			this.removeBuild(model);
			
		}
		
		return model;
	}
	
	
	private void removeBuild(LearningBuild learningBuild) {
		S3PartialPaths s3PartialPath = getPartialPaths("learningBuilds");
		String targetPath = s3PartialPath.getPathPrefix() + learningBuild.getId() + s3PartialPath.getPathSuffix();
		LOGGER.info("Deleting individual learningBuild folder with s3 key: " + targetPath );
		try { LearningS3Connector.getInstance().callDelete(targetPath); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(500, "Learning s3 delete call failed with: "+e.getMessage()); }	
	}
	
	
	public List<LearningFileInfoDTO> getFiles(String learningBuildUID) {
		
		if(learningBuildUID == null || learningBuildUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningBuildUID %s", learningBuildUID);	    								    								
		}

		if(this.readModelWithExternalId(learningBuildUID) == null) {
			throw new GenericValidationException("Existing learningBuild %s not found", learningBuildUID);
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put("learningBuildUID", learningBuildUID);
		
		List<LearningFile> learningFiles = this.getLearningFileDao().findWithNamedQuery("LearningFile.custom.findByLearningBuild", map);
		List<LearningFileInfoDTO> learningFileDTOs = new ArrayList<LearningFileInfoDTO>();
		for(LearningFile learningFile : learningFiles) {
			LearningFileInfoDTO dto = new LearningFileInfoDTO();
			
			LearningBuild learningBuild = learningFile.getLearningBuild();
			String learningRunUID = learningFile.getLearningRun() == null?null:learningFile.getLearningRun().getId();
			
			dto.setLearningConfigUID(learningBuild.getLearningConfig().getId());
			dto.setLearningVersionUID(learningBuild.getLearningVersion().getId());
			dto.setLearningBuildUID(learningBuildUID);
			dto.setLearningRunUID(learningRunUID);
			dto.setLearningFileUID(learningFile.getId());
			dto.setFileName(learningFile.getFileName());
			dto.setFileType(learningFile.getFileType());
			dto.setFileSize(learningFile.getFileSize());
			
			learningFileDTOs.add(dto);
		}
		
		return learningFileDTOs;
	}

	/** common method for reading a resource */
	@Override	
	public LearningBuildDTO readObject(String id) {
		return super.readObject(id);		
	}

	
	/** common method to save a resource */
	public LearningBuildDTO saveObject(LearningBuildDTO obj) {				
		return super.saveObject(obj);
	}
		
	/** common method to update a resource */
	@Override	
	public LearningBuildDTO updateObject(String id, LearningBuildDTO obj) {		
		return super.updateObject(id, obj);		
	}

		
	/** common method to remove a resource */
	@Override
	public LearningBuildDTO removeObject(String id) {		
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningBuild newModel, LearningBuild oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningBuild model, String pk) {
		model.setId(pk);
	}
	
	@Override
	protected LearningBuild toModelAdditionalMappings(LearningBuildDTO dto, LearningBuild model) {
    	
    	if (model == null) {
    		model = new LearningBuild();
    	}
    	

    	String learningVersionUID =  dto.getLearningVersionUID();

    	LearningVersion learningVersion = getLearningVersionDao().get(learningVersionUID);
    	if (learningVersion == null) {
        	throw new GenericValidationException("Invalid learningVersionUID %s in LearningBuild", learningVersionUID);	    								    								
    	}
    	
    	
    	model.setLearningVersion(learningVersion);
    	model.setLearningConfig(learningVersion.getLearningConfig());
    	    	
		return model;
	}

	
	@Override
	protected LearningBuildDTO toDTOAdditionalMappings(LearningBuild model, LearningBuildDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningBuildDTO();
		}
		
		dto.setLearningBuildUID(model.getId());
		dto.setLearningVersionUID(model.getLearningVersion().getId());
		
		dto.setExecutionStatus(model.getExecutionStatus());
		Date executionStartDate = model.getExecutionDatetime();
		if(executionStartDate != null) {
			// time should be in ISO format with the offsets for the particular TimezoneId associated with the visit		
			DateTimeFormatter formatter    = ISODateTimeFormat.dateTimeNoMillis();			
			DateTime utcTime = new DateTime(executionStartDate);

			// format it in ISO
			String datestr  = formatter.print(utcTime);		
			dto.setExecutionDatetime(datestr);	
		}
		dto.setIsDeployed(model.getIsDeployed());
		dto.setIsDeleted(model.getIsDeleted());
	
		return dto;
	}
	
	
	@Override
	protected void validateDTO(LearningBuildDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();
		
		if ((dto.getLearningBuildUID() == null) || dto.getLearningBuildUID().length() < 1 || dto.getLearningVersionUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningBuildUID");
			err.setMessage(String.format("LearningBuild UID is required and must be between 1 and 80 characters long: %s", dto.getLearningBuildUID()));
			errors.add(err);
		}
		
		
		
		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}
				
	}

	/**
	 * Returns all deployed builds by learning config
	 */	
	public List<LearningBuild> getAllDeployedLearningBuildsByLearningConfig(String learningConfigUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningConfigUID", learningConfigUID);
		
		List<LearningBuild> params = this.getDao().findWithNamedQuery("LearningBuild.custom.findDeployedByConfig", map);
		return params;
	}

	

	/**
	 * Returns all learningBuilds by learning version
	 */	
	public List<LearningBuild> getAllLearningBuildsByLearningVersion(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		List<LearningBuild> params = this.getDao().findWithNamedQuery("LearningBuild.custom.findByLearningVersion", map);
		return params;
	}
	
	/**
	 * Returns all learningBuilds by learning version ordered by created date
	 */	
	public List<LearningBuild> getAllLearningBuildsByLearningVersionOrderedByCreated(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		List<LearningBuild> params = this.getDao().findWithNamedQuery("LearningBuild.custom.findBuildsByLearningVersionOrderByCreated", map);
		return params;
	}
	
	/**
	 * Returns latest successful learningBuild by learning version ordered by execution date
	 */	
	public LearningBuild getLatestSuccessfullBuildByLearningVersionOrderedByExecutionDate(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		List<LearningBuild> params = this.getDao().findWithNamedQuery("LearningBuild.custom.findLatestSuccessfulBuilds", map);
		
		if(params.isEmpty()) { 
			return null;
		}
		
		return params.get(0);
	}
	
	
	private List<LearningRun> getLearningRunsWithBuildUID(String learningBuildId) {
		
		String namedQueryName = "LearningRun.custom.findByLearningBuild";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningBuildUID", learningBuildId);
		List<LearningRun> learningRuns = getLearningRunDao().findWithNamedQuery(namedQueryName, map);
		return learningRuns;
	
	}
	

	/** 
	 * set currentLearningBuildUID to null in LearningConfig table if necessary 
	 * @param the learningBuildUID that may need to be cleaned from LearningConfig table
	 */
	public void updateLearningConfigTable(String learningBuildId) {		
		LearningConfigBo learningConfigBo = new LearningConfigBo();	
		GenericDao<LearningConfig, String> modelDao = learningConfigBo.getDao();
			
		HashMap<String, Object> buildMap = new HashMap<String, Object>();
		String namedQueryName = "LearningConfig.custom.findByCurrentLearningBuild";
		buildMap.put("currentLearningBuildUID", learningBuildId);		
		List<LearningConfig> learningConfigList = modelDao.findWithNamedQuery(namedQueryName, buildMap);
		
		if(learningConfigList.isEmpty()) return;
		LearningConfig learningConfig = learningConfigList.get(0);
		
		HashMap<String, Object> versionMap = new HashMap<String, Object>();
		versionMap.put("learningVersionUID", learningConfig.getCurrentLearningVersion().getId());
			
		List<LearningBuild> params = this.getDao().findWithNamedQuery("LearningBuild.custom.findBuildsByLearningVersionOrderByCreated", versionMap);
		LearningBuild nextSuccessfulLerningBuild = new LearningBuild();
		
		if(params.size() < 1) { 
			nextSuccessfulLerningBuild = null;
		} else {
			nextSuccessfulLerningBuild = params.get(0);
		} 

		
		((ILearningConfig)learningConfig).setCurrentLearningBuild(nextSuccessfulLerningBuild);
		
		modelDao.merge(learningConfig);
		
		modelDao.flush();

	}
	
	/** 
	 * method to soft delete a learningBuild and clean the build out of all other learningDB tables 
	 * must validate build in a wrapper function
	 * @param the LearningBUild to delete 
	 * @return the deleted LearningBuildDTO
	 */
	private LearningBuildDTO cleanLearningBuild(LearningBuild learningBuild) {
		
		String learningBuildId = learningBuild.getId();
		
		// should only attempt delete from S3 if build hasnt already been deleted
		Boolean tryDeleteInLearningS3 = !learningBuild.getIsDeleted();
		
		if(!learningBuild.getIsDeleted()) {
			learningBuild.setIsDeleted(true);
	
			getDao().merge(learningBuild); // re-add model to schema 
			getDao().flush();
		}
		
		updateLearningConfigTable(learningBuildId);
		
		getDao().delete(learningBuild); // make use of cascade delete
		getDao().flush();		
		
		getDao().save(learningBuild); // re-ad
		getDao().flush();	
		
		if(tryDeleteInLearningS3) removeBuild(learningBuild);
		
		return toDTO( this.readModelWithExternalId(learningBuildId) );
		
	}
	
	/** delete a learningBuild if allowable and clean learningBuild and associated learningRuns from all other tables 
	 * @param the UID of the build to delete
	 * @return the DTO of the deleted build
	 */
	public LearningBuildDTO deleteLearningBuildWithUID(String learningBuildId) {
		
		LearningBuildDTO learningBuildDTO = new LearningBuildDTO();
		LearningBuild learningBuild = readModelWithExternalId(learningBuildId);		
		
		if(learningBuild == null) throw new GenericValidationException("No %s found with id %s", "LearningBuild", learningBuildId);
		
		if(learningBuild.getExecutionStatus() != null) {
			if(learningBuild.getExecutionStatus().equals("running")) throw new GenericValidationException("Cannot delete learningBuild: %s. learningBuild is running", learningBuildId);	 	
		}
		
		if(learningBuild.getIsDeployed()) throw new GenericValidationException("Cannot delete learningBuild: %s. learningBuild is deployed", learningBuildId);
		
		
		// now Ok to delete learningBuild (if no running learningRuns)
		learningBuildDTO = toDTO(learningBuild);
		
		List<LearningRun> learningRuns = new ArrayList<LearningRun>();
		learningRuns = getLearningRunsWithBuildUID(learningBuildId);
		
		Boolean deleteLearningBuild = true;
		LearningRunBo learningRunBo = new LearningRunBo();
		
		for(LearningRun learningRun : learningRuns) {
			if(learningRun.getExecutionStatus() != null) {
				if(learningRun.getExecutionStatus().equals("running")) {
					deleteLearningBuild = false;
					continue;
				}
			}
			if(learningRun.getIsPublished()) {
				deleteLearningBuild = false;
				continue;
			}
			// now ok delete learningRun and clean from other tables
			learningRunBo.deleteLearningRunWithUID(learningRun, false);	
		}
		
		// ok to soft delete learning build and clean from other tables
		if(deleteLearningBuild) return cleanLearningBuild(learningBuild);
		
		// return undeleted build
		return learningBuildDTO;
	}
	
}
