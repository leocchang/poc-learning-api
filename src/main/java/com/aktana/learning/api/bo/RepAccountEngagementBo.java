/*****************************************************************
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.HashMap;
import java.util.List;  
import java.util.Set;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.persistence.models.impl.RepAccountEngagement;
import com.aktana.learning.persistence.models.impl.RepAccountEngagementPK;
import com.aktana.learning.api.bo.mapper.RepAccountEngagementMapper;
import com.aktana.learning.api.dto.RepAccountEngagementDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.util.datetime.PureDate;


/** BO class for RepAccount */
public class RepAccountEngagementBo extends AbstractBo<RepAccountEngagementDTO, RepAccountEngagement, RepAccountEngagementPK> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public RepAccountEngagementBo() {
		super(RepAccountEngagementBo.class, RepAccountEngagementDTO.class, RepAccountEngagement.class, RepAccountEngagementMapper.getInstance());
		
		
		LOGGER = LoggerFactory.getLogger(this.getClass());		
	}
	
	/** common method for listing a resource */
	public List<RepAccountEngagementDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}

	/** common method for reading a resource */
	public RepAccountEngagementDTO readObject(String id) {
		return super.readObject(id);
	}

	
	/** common method to save a resource */
	public RepAccountEngagementDTO saveObject(RepAccountEngagementDTO obj) {
		return super.saveObject(obj);
	}

	
	/** common method to update a resource */
	public RepAccountEngagementDTO updateObject(String id, RepAccountEngagementDTO obj) {
		return super.updateObject(id,  obj);
	}

		
	/** common method to remove a resource */
	public RepAccountEngagementDTO removeObject(String id) {
		return super.removeObject(id);
	}
	
	
	@Override
	protected void transferId(RepAccountEngagement newModel, RepAccountEngagement oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}

	// setSoftDeletedFlag(User): no need to override

	@Override
	protected void setId(RepAccountEngagement model, RepAccountEngagementPK pk) {
		model.setId(pk);
	}
	



	@Override
	protected RepAccountEngagement toModelAdditionalMappings(RepAccountEngagementDTO dto, RepAccountEngagement model) {
    	
    	
    	RepAccountEngagementPK pk = new RepAccountEngagementPK();    
    

    	
    	PureDate pureDate = new PureDate(dto.getDate());
    	pk.setDate(pureDate.toJavaDate());


    	model.setId(pk);
    	    	
		return model;
	}

	

	@Override
	protected RepAccountEngagementDTO toDTOAdditionalMappings(RepAccountEngagement model, RepAccountEngagementDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new RepAccountEngagementDTO();
		}
		
				
		return dto;
	}
	
	
	
	/* helper method to read existing model object based on the composite ids */
	@Override
	public RepAccountEngagement readModelWithExternalId(String id) {
		HashMap<String, Object> map = parseAndValidateObjectUID(id);
				
		return super.readModelWithExternalId(map);		
	}

	


	private HashMap<String, Object> parseAndValidateObjectUID(String id) {
		String[] subkeys = id.split("\\~");
		HashMap<String, Object> map = new HashMap<String, Object>();
		

		if (subkeys.length != 2) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		map.put("repUID", subkeys[0]);


		// startDate
		try {
			PureDate date = new PureDate(subkeys[1]);
			map.put("date", date.toString());
		} catch (Exception e) {
			throw new GenericValidationException(
					"Invalid date in composite key: %s", subkeys[1]);
		}

		
		return map;
	}

	
	private HashMap<String, Object> parseAndValidateObjectUIDPattern(String id) {
		String[] subkeys = id.split("\\~", -1);
		HashMap<String, Object> map = new HashMap<String, Object>();

		if (subkeys.length != 2) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		if (!subkeys[0].isEmpty()) {
			map.put("repUID", subkeys[0]);
		}

		// date
		try {
			PureDate date = new PureDate(subkeys[1]);
			map.put("date", date.toJavaDate());
		} catch (Exception e) {
			throw new GenericValidationException(
					"Invalid date in composite key: %s", subkeys[1]);
		}
			
		return map;		
	}

	

}
