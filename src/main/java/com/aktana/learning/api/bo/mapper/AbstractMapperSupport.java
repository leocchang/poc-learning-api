/*****************************************************************
 * 
 * AbstractMapperSupport<POJO, DTO> is a base implementation for all mappers from POJO -> DTO and DTO -> POJO. 
 * 
 * The AbstractMapperSupport is Generic implementation based on
 * <ul>
 * <li><POJO> the Class of the corresponding Hibernate "model" (aka POJO) object</li>
 * <li><DTO> (Data Transfer Object) the DTO Class of the corresponding resource representation </li>
 * </ul>
 * 
 * The base class provides the following capabilities that can be used or derived by the children
 * <ul>
 * 		<li>
 * 			CORE Methods toPOJO, toDTO, translateDtoFilterToModelFilter, translateDtoOrderbyToModelOrderby
 * 		</li>
 *		<li>
 * 			Support Methods to initializeMappingsToPojo, initializeMappingsToDTO, setFilterFieldTranslations, getFilterFieldTranslations(), getPojoClass, getDTOClass
 * 		</li>
 *		<li>
 * 			Optional Base implementations of applyCustomMappingsToPojo, applyCustomMappingsToDTO that can perform additional model/DTO translations
 * 		</li>
 * </ul>
 * 
 * @author $Author: felipe.canaviri $
 * @version $Revision: 18428 $ on $Date: 2013-10-24 08:56:25 -0700 (Thu, 24 Oct 2013) $ by $Author: felipe.canaviri $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public abstract class AbstractMapperSupport<POJO, DTO>
{
	// ObjectMapperFactory to from DTO to POJO		
	private MapperFactory toPojoFactory;

	// ObjectMapperFactory to from POJO to DTO				
	private MapperFactory toDTOFactory;
	
	// place holder to translate DTO field Names to Model field Names for AbstractBo to help build criteria queries	
	private HashMap<String, String> filterFieldTranslations;

	public AbstractMapperSupport() {		
		// place holder to translate DTO field Names to Model field Names for building criteria queries
		filterFieldTranslations = new HashMap<String, String>();

		// ObjectMapperFactory to from DTO to POJO		
		toPojoFactory = new DefaultMapperFactory.Builder().build();
		
		// derived classes should provide implementation
		initializeMappingsToPojo(toPojoFactory);

		// ObjectMapperFactory to from POJO to DTO				
		toDTOFactory = new DefaultMapperFactory.Builder().build();
		
		// derived classes should provide implementation		
		initializeMappingsToDTO(toDTOFactory);		
	}



	/**
	 * Core method to translate DTO to POJO
	 * 
	 * @param obj DTO
	 * @return POJO
	 */
	@SuppressWarnings("unchecked")
	public POJO toPojo(DTO obj)
	{
		POJO pojo =  (POJO) toPojoFactory.getMapperFacade().map(obj, getPojoClass());
		
		applyCustomMappingsToPojo(obj, pojo);
		
		return pojo;
	}


	/**
	 * Core method to translate POJO to DTO
	 * 
	 * @param obj POJO
	 * @return DTO
	 */
	@SuppressWarnings("unchecked")
	public DTO toDTO(POJO obj)
	{
		DTO dto = (DTO) toDTOFactory.getMapperFacade().map(obj, getDTOClass());
		
		applyCustomMappingsToDTO(obj, dto);
		
		return dto;
	}


	/**
	 * Core method to translateDtoFilterToModelFilter using the filterNamesMap
	 * 
	 * @param filter based on DTO fieldNames
	 * @return filter based on POJO fieldNames
	 */
	public Map<String, Object> translateDtoFilterToModelFilter(Map<String, Object> filter) {

		//Adapter method to translate DTO fieldNames to Model fieldNames to enable Criteria Query Building		
		HashMap<String, Object> modelfilter = new HashMap<String, Object>();
		
		for (String key : filter.keySet()) {
			String newkey = translateDtoField(key);			
			if (newkey != null)
			{
				modelfilter.put(newkey, filter.get(key));
			} 
		}
						
		return modelfilter;
	}
	

	/**
	 * Core method to translateDtoOrderbyToModelOrderby using the filterNamesMap
	 * 
	 * @param orderby list based on DTO fieldNames
	 * @return orderby list based on POJO fieldNames
	 */
	public List<String> translateDtoOrderbyToModelOrderby(List<String> orderby) {
		
		List<String> model_orderby = new ArrayList<String>();
		
		for (String dtofield : orderby) {
			String modelfield = translateDtoField(dtofield);			
			if (modelfield != null)
			{
				model_orderby.add(modelfield);
			} 			
		}
		
		return model_orderby;
	}

	
	/**
	 * Support method to Initialize the ObjectMapperFactory for translation to Pojo
	 * Must be implemented by derived class
	 * 
	 * @param factory
	 */
	protected abstract void initializeMappingsToPojo(MapperFactory factory);


	/**
	 * Support method to Initialize the ObjectMapperFactory for translation to DTO
	 * Must be implemented by derived class
	 * 
	 * @param factory
	 */
	protected abstract void initializeMappingsToDTO(MapperFactory factory);


	/**
	 * Support method to get the POJO Class
	 * 
	 * @return Class
	 */
	public abstract Class<?> getPojoClass();

	/**
	 * Support method to get the DTO Class
	 * 
	 * @return
	 */
	public abstract Class<?> getDTOClass();

	
	/**
	 * Support method to get the translation map for DTO fieldNames to POJO fieldNames
	 * 
	 * @return Map of DTO fieldNames to POJO fieldNames
	 */
	public HashMap<String, String> getFilterFieldTranslations() {
		return filterFieldTranslations;
	}

	/**
	 * Support method to set the translation map for the AbstractBo to use for building criteria queries
	 * 
	 * @param filterFieldTranslations Map of DTO fieldNames to POJO fieldNames
	 */
	public void setFilterFieldTranslations(
			HashMap<String, String> filterFieldTranslations) {
		this.filterFieldTranslations = filterFieldTranslations;
	}
	

	/**
	 * Support method to translate one DTO fieldName to a POJO fieldName using the map
	 * 
	 * May be overridden by derived classes for customization
	 * 
	 * @param field
	 * @return
	 */
	protected String translateDtoField(String field) {		
		if (filterFieldTranslations.containsKey(field))
			return filterFieldTranslations.get(field);
		else			
			return null;
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(DTO src, POJO tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(POJO src, DTO tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
	}

}
