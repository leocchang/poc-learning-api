/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.persistence.models.impl.LearningBuild;

import ma.glasnost.orika.MapperFactory;


public class LearningBuildMapper extends AbstractMapperSupport<LearningBuild, LearningBuildDTO>
{
    public LearningBuildMapper()
    {
        super();
    }
    
    private static LearningBuildMapper instance;
    
    public static LearningBuildMapper getInstance()  {

		synchronized(LearningBuildMapper.class) {
			if (instance == null) {
				instance = new LearningBuildMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningBuild.class, LearningBuildDTO.class)
                .field("id", "learningBuildUID")
                .field("learningVersion.id", "learningVersionUID")
                .field("isDeployed", "isDeployed")
                .field("isDeleted", "isDeleted")
                .field("executionStatus", "executionStatus")
                .field("executionDatetime", "executionDatetime")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningBuildDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningBuild.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningBuildDTO.class, LearningBuild.class)
                .field("learningBuildUID", "id")
                .field("learningVersionUID", "learningVersion.id")
                .field("isDeployed", "isDeployed")
                .field("isDeleted", "isDeleted")
                .field("executionStatus", "executionStatus")
                .field("executionDatetime", "executionDatetime")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningBuildDTO src, LearningBuild tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningBuild src, LearningBuildDTO tgt)
	{
	
	}

}