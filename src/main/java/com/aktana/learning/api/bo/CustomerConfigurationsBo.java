/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.bo.mapper.CustomerConfigurationsMapper;
import com.aktana.learning.api.dto.CustomerConfigurationsDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.CustomerConfigurations;
import com.aktana.learning.persistence.models.impl.CustomerConfigurationsPK;



/** Base class for LearningParam */
public class CustomerConfigurationsBo extends AbstractBo<CustomerConfigurationsDTO, CustomerConfigurations, CustomerConfigurationsPK> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public CustomerConfigurationsBo() {
		super(CustomerConfigurationsBo.class, CustomerConfigurationsDTO.class, CustomerConfigurations.class, CustomerConfigurationsMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
	}
	
	
	/** Returns a List of all CustomerConfigurations objects. */
	public List<CustomerConfigurations> findAll() {
		return getDao().findAll();
	}

	@Override
	protected void transferId(CustomerConfigurations newModel, CustomerConfigurations oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
		newModel.setUpdatedAt(oldModel.getUpdatedAt());
	}


	@Override
	protected void setId(CustomerConfigurations model, CustomerConfigurationsPK pk) {
		model.setId(pk);
	}
	
	@Override
	protected CustomerConfigurations toModelAdditionalMappings(CustomerConfigurationsDTO dto, CustomerConfigurations model) {
		    	
    	if (model == null) {
    		model = new CustomerConfigurations();
    	}
    	
    	CustomerConfigurationsPK customerConfigurationsPK = new CustomerConfigurationsPK(dto.getConfigurationType(), dto.getConfigurationValue());	
    	model.setId(customerConfigurationsPK);
    	// model.setCreatedAt(new Date(dto.getCreatedAt()));
    	// model.setUpdatedAt(new Date(dto.getUpdatedAt()));   	
		return model;
	}

	

	@Override
	protected CustomerConfigurationsDTO toDTOAdditionalMappings(CustomerConfigurations model, CustomerConfigurationsDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new CustomerConfigurationsDTO();
		}
		
		dto.setConfigurationType(model.getId().getConfigurationType());
		dto.setConfigurationValue(model.getId().getConfigurationValue());
		if(model.getCreatedAt() != null) dto.setCreatedAt(dateTimeFormat.format(model.getCreatedAt()));
		if(model.getUpdatedAt() != null) dto.setUpdatedAt(dateTimeFormat.format(model.getUpdatedAt()));
				
		return dto;
	}
	
	@Override
	protected void validateDTO(CustomerConfigurationsDTO dto) {
		super.validateDTO(dto);			
	}
	
	private List<CustomerConfigurations> getConfigurations(String configurationType, String configurationValue) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("configurationType", configurationType);
		if(configurationValue != null) map.put("configurationValue", configurationValue);
		String namedQueryName = configurationValue == null ? "CustomerConfigurations.custom.findByConfigurationType" : "CustomerConfigurations.custom.findByConfigurationTypeAndValue";
		return getDao().findWithNamedQuery(namedQueryName, map);
	}
	
	public List<CustomerConfigurationsDTO> getConfigurationsByType(String configurationType) { 
		return toDTO(getConfigurations(configurationType, null));
	}
	
	public CustomerConfigurationsDTO deleteCustomerConfiguration(String configurationType, String configurationValue) {
		List<CustomerConfigurations> customerConfigurations = getConfigurations(configurationType, configurationValue);
		if(customerConfigurations.size() < 1) throw new GenericValidationException(404, "Configuration does not exist in table");
		CustomerConfigurations customerConfiguration = customerConfigurations.get(0);
		getDao().delete(customerConfiguration);
		getDao().flush();		
		return toDTO(customerConfiguration);
	}
	
	public CustomerConfigurationsDTO addCustomerConfiguration(String configurationType, String configurationValue) {
		List<CustomerConfigurations> customerConfigurations = getConfigurations(configurationType, configurationValue);
		if(customerConfigurations.size() > 0) throw new GenericValidationException(400, "Configuration already exists in table");
		CustomerConfigurations customerConfiguration = new CustomerConfigurations(new CustomerConfigurationsPK(configurationType, configurationValue));
		getDao().save(customerConfiguration); // re-ad
		getDao().flush();	
		return toDTO(customerConfiguration);	
	}
 
}