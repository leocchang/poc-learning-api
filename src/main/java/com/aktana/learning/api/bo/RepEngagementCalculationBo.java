/*****************************************************************
 *
 * @author $Author: jacob.adicoff $
 * @version $Revision: xxx $ on $Date: 2019-06-20 15:43:17 -0700 (Thu, 20 June 2019) $ by $Author: jacob.adicoff $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aktana.learning.api.dto.RepEngagementCalculationDTO;
import com.aktana.learning.api.dto.RepEngagementDTO;
import com.aktana.learning.api.dto.RepTeamsDTO;
import com.aktana.learning.api.dto.TerritoriesDTO;
import com.aktana.learning.api.dto.EngagementFiltersForDSESettingsDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.RepEngagementCalculationMapper;
import com.aktana.learning.persistence.models.impl.RepEngagementCalculation;
import com.aktana.learning.persistence.models.impl.EngagementFiltersForDSESettings;
import com.aktana.learning.persistence.models.impl.EngagementFiltersForDSESettingsPK;




/** Base class for LearningRun */
public class RepEngagementCalculationBo extends AbstractBo<RepEngagementCalculationDTO, RepEngagementCalculation, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;
	
    private GenericDao<EngagementFiltersForDSESettings, String> engagementFiltersForDSESettingsDao = null;

    
    /** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public RepEngagementCalculationBo() {
		super(RepEngagementCalculationBo.class, RepEngagementCalculationDTO.class, RepEngagementCalculation.class, RepEngagementCalculationMapper.getInstance());
		LOGGER = LoggerFactory.getLogger(this.getClass());		
		engagementFiltersForDSESettingsDao = new GenericDaoImpl<EngagementFiltersForDSESettings, String>(EngagementFiltersForDSESettings.class);

	}
	
	private GenericDao<EngagementFiltersForDSESettings, String> getEngagementFiltersForDSESettingsDao() {
		engagementFiltersForDSESettingsDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return engagementFiltersForDSESettingsDao;
	}
		
	
	@Override
	protected void transferId(RepEngagementCalculation newModel, RepEngagementCalculation oldModel) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void setId(RepEngagementCalculation newModel, String pk) {
		// TODO Auto-generated method stub
		
	}


	public RepEngagementCalculationDTO getRepEngagementCalculationParams(String configId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<String> YearsDTO = new ArrayList<String>();
		List<RepTeamsDTO> repTeamsDTO = new ArrayList<RepTeamsDTO>();
		List<TerritoriesDTO> territoriesDTO = new ArrayList<TerritoriesDTO>();
    		// String namedQueryName_Date_min = "RepEngagementCalculation.custom.getDate_min";
    		// String namedQueryName_Date_max = "RepEngagementCalculation.custom.getDate_max";
    		String namedQueryName_RepTeams = "RepEngagementCalculation.custom.getRepTeams";
    		String namedQueryName_Territories = "RepEngagementCalculation.custom.getTerritories";
    		map.put("configId", configId);
    		List<RepEngagementCalculation> minDate = getDao().findByNativeQuery("select cast(min(date1) as CHAR) as repUID, year, month, repName, repTeamUID, repTeamName, seConfigId, seConfigName, territoryId, territoryName, suggestionType, engagedUniqueSuggestionsCount,  totalSuggestionsDeliveredTimes from (select *, cast(concat(year,'-',month,'-','0') as DATE) as date1 from RepEngagementCalculation l where l.seConfigId = '" + configId + "' group by l.year, l.month) x;");
    		for(RepEngagementCalculation date : minDate) {
    			if(date != null) {
    				String x[] = date.getRepUID().split("-");
            		String year = x[0];
            		String month = x[1];
            		YearsDTO.add(year+"-"+ month);
    			}
        		else {
        			throw new GenericValidationException("Data on config %s not found", configId);	
        		}
    		} 
    		
    			
    		List<RepEngagementCalculation> maxDate = getDao().findByNativeQuery("select cast(max(date1) as CHAR) as repUID, year, month, repName, repTeamUID, repTeamName, seConfigId, seConfigName, territoryId, territoryName, suggestionType, engagedUniqueSuggestionsCount,  totalSuggestionsDeliveredTimes from (select *, cast(concat(year,'-',month,'-','0') as DATE) as date1 from RepEngagementCalculation l where l.seConfigId = '" + configId + "' group by l.year, l.month) x;");
    		for(RepEngagementCalculation date : maxDate) {
    			String x[] = date.getRepUID().split("-");
    			String year = x[0];
    			String month = x[1];
    			YearsDTO.add(year+"-"+ month);
    		}
    		
    		List<RepEngagementCalculation> repTeams = getDao().findWithNamedQuery(namedQueryName_RepTeams, map);
    		for(RepEngagementCalculation repTeam : repTeams) {
    			RepTeamsDTO rt = new RepTeamsDTO();
    			rt.setRepTeamUID(repTeam.getRepTeamUID());
    			rt.setRepTeamName(repTeam.getRepTeamName());
    			repTeamsDTO.add(rt);
    		}
    		
    		List<RepEngagementCalculation> territories = getDao().findWithNamedQuery(namedQueryName_Territories, map);
    		for(RepEngagementCalculation territory : territories) {
    			TerritoriesDTO t = new TerritoriesDTO();
    			t.setTerritoryUID(territory.getTerritoryId());
    			t.setTerritoryName(territory.getTerritoryName());
    			territoriesDTO.add(t);
    		}
    		RepEngagementCalculationDTO dto = new RepEngagementCalculationDTO();
    		
    		dto.setRepTeams(repTeamsDTO);
    		dto.setTerritories(territoriesDTO);
    		dto.setStartAndEndDates(YearsDTO);
    		
    		return dto;
	}
	
	public List<RepEngagementDTO> getRepPerformance(String configId, String repTeamUID, String startYear, String startMonth, String endYear, String endMonth, String territoryUID, String suggestionType) {
		List<RepEngagementDTO> repEngagementDTO = new ArrayList<RepEngagementDTO>();
		String query = "select sum(engagedUniqueSuggestionsCount) as engagedUniqueSuggestionsCount, sum(totalSuggestionsDeliveredTimes) as totalSuggestionsDeliveredTimes, year, month, repUID, repName, repTeamUID, repTeamName, seConfigId, seConfigName, territoryId, territoryName, suggestionType from RepEngagementCalculation";
		query = query + " where seConfigId = '" + configId + "'";
		if (startMonth == null && endMonth == null && startYear == null && endYear == null) {
			String newStartYear="";
			String newStartMonth="";
			String newEndYear="";
			String newEndMonth="";
			List<RepEngagementCalculation> minDate = getDao().findByNativeQuery("select cast(min(date1) as CHAR) as repUID, year, month, repName, repTeamUID, repTeamName, seConfigId, seConfigName, territoryId, territoryName, suggestionType, engagedUniqueSuggestionsCount,  totalSuggestionsDeliveredTimes from (select *, cast(concat(year,'-',month,'-','0') as DATE) as date1 from RepEngagementCalculation l where l.seConfigId = '" + configId + "' group by l.year, l.month) x;");
			for(RepEngagementCalculation date : minDate) {
    				if(date != null) {
    					String x[] = date.getRepUID().split("-");
    					newStartYear = x[0];
    					newStartMonth = x[1];
    				}
    				else {
    					throw new GenericValidationException("Data on config %s not found", configId);	
    				}
    			}
			
			List<RepEngagementCalculation> maxDate = getDao().findByNativeQuery("select cast(max(date1) as CHAR) as repUID, year, month, repName, repTeamUID, repTeamName, seConfigId, seConfigName, territoryId, territoryName, suggestionType, engagedUniqueSuggestionsCount,  totalSuggestionsDeliveredTimes from (select *, cast(concat(year,'-',month,'-','0') as DATE) as date1 from RepEngagementCalculation l where l.seConfigId = '" + configId + "' group by l.year, l.month) x;");
    			for(RepEngagementCalculation date : maxDate) {
    				if(date != null) {
    				String x[] = date.getRepUID().split("-");
    				newEndYear = x[0];
    				newEndMonth = x[1];
    				}
    				else {
    					throw new GenericValidationException("Data on config %s not found", configId);	
    				}
    			}
    			
    			query = query + " and DATE(CONCAT(Year, '-', Month, '-01')) BETWEEN DATE(CONCAT('" +newStartYear+ "', '-', '" +newStartMonth+ "', '-01')) AND DATE(CONCAT('" +newEndYear+ "', '-', '" +newEndMonth+ "', '-01'))";
		} else {
			if ((Integer.parseInt(startMonth) <= 0 || Integer.parseInt(startMonth) > 13)) {
				throw new GenericValidationException("Invalid start Month %s", startMonth);	 
			}
			if ((Integer.parseInt(endMonth) <= 0 || Integer.parseInt(endMonth) > 13)) {
				throw new GenericValidationException("Invalid end Month %s", endMonth);	
			}
			if ((Integer.parseInt(startYear) <= 0 || Integer.parseInt(startYear) > Calendar.getInstance().get(Calendar.YEAR))) {
				throw new GenericValidationException("Invalid start Year %s", startYear);	 
			}
			if ((Integer.parseInt(endYear) <= 0 || Integer.parseInt(endYear) > Calendar.getInstance().get(Calendar.YEAR))) {
				throw new GenericValidationException("Invalid end Year %s", endYear);	
			}
			query = query + " and DATE(CONCAT(Year, '-', Month, '-01')) BETWEEN DATE(CONCAT('" +startYear+ "', '-', '" +startMonth+ "', '-01')) AND DATE(CONCAT('" +endYear+ "', '-', '" +endMonth+ "', '-01'))";
		}
		
		if(repTeamUID!=null && repTeamUID.trim() != ""){
			String repTeams[] = repTeamUID.trim().split(",");
			String r[] = new String[repTeams.length];
			for(int i=0;i<repTeams.length;i++){  
				r[i] = "'"+repTeams[i]+"'";
			}
			String sb1 = String.join(",", r);
			query = query + " and repTeamUID in("+sb1+")";
		}
		
		if(territoryUID!=null && territoryUID.trim() != ""){
			String territoryUIDs[] = territoryUID.trim().split(",");
			String t[] = new String[territoryUIDs.length];
			for(int i=0;i<territoryUIDs.length;i++){  
				 t[i] = "'"+territoryUIDs[i]+"'";
			}
			String sb2 = String.join(",", t);
			query = query + " and territoryId in("+sb2+")";
		}
		if(suggestionType!=null && suggestionType.trim() != ""){
			query = query + " and suggestionType = '" + suggestionType + "'";
		}
		query = query + " group by repUID";

		List<RepEngagementCalculation> repEngagmentList = getDao().findByNativeQuery(query);
		for(RepEngagementCalculation repEngagment : repEngagmentList) {
			if(repEngagment != null) {
				RepEngagementDTO t = new RepEngagementDTO();
				t.setPerformance(Math.round((repEngagment.getEngagedUniqueSuggestionsCount() * 1.0)/ repEngagment.getTotalSuggestionsDeliveredTimes()* 1000000.0) / 1000000.0);
				t.setRepUID(repEngagment.getRepUID());
				repEngagementDTO.add(t);
			}
			else {
				throw new GenericValidationException("Data on config %s not found", configId);	
			}
		} 
		
		return repEngagementDTO;
	}
	
	private EngagementFiltersForDSESettingsDTO makeEngagementFiltersForDSESettingsDTO(EngagementFiltersForDSESettings model) {
		EngagementFiltersForDSESettingsDTO dto = new EngagementFiltersForDSESettingsDTO();
		dto.setSeConfigId(model.getId().getSeConfigId());
		dto.setSuggestionType(model.getId().getSuggestionType());	
		dto.setRepTeamUIDs(model.getRepTeamUIDs());
		dto.setTerritoryIds(model.getTerritoryIds());
		dto.setStartYear(model.getStartYear());
		dto.setStartMonth(model.getStartMonth());
		dto.setRepThresholdType(model.getRepThresholdType());
		dto.setBottomPercentile(model.getBottomPercentile());
		return dto;
	}
	
	private EngagementFiltersForDSESettings makeEngagementFiltersForDSESettingsModel(EngagementFiltersForDSESettingsDTO dto) {
		EngagementFiltersForDSESettings model = new EngagementFiltersForDSESettings(new EngagementFiltersForDSESettingsPK(dto.getSeConfigId(), dto.getSuggestionType()));	
		model.setRepTeamUIDs(dto.getRepTeamUIDs());
		model.setTerritoryIds(dto.getTerritoryIds());
		model.setStartYear(dto.getStartYear());
		model.setStartMonth(dto.getStartMonth());
		model.setRepThresholdType(dto.getRepThresholdType());
		model.setBottomPercentile(dto.getBottomPercentile());
		return model;
	}
	
	
	private EngagementFiltersForDSESettings getEngagementFiltersForDSESettings(Integer seConfigId, String suggestionType) {
		String namedQueryName = "EngagementFiltersForDSESettings.custom.findBySeConfigIdAndSuggestionType";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("seConfigId", seConfigId);
		map.put("suggestionType", suggestionType);
		List<EngagementFiltersForDSESettings> models = this.getEngagementFiltersForDSESettingsDao().findWithNamedQuery(namedQueryName, map);
		if(models.size() < 1) return null;		
		return models.get(0);
	}
	// for get all
	public List<EngagementFiltersForDSESettingsDTO> getAllEngagementFiltersForDSESettings() {
		List<EngagementFiltersForDSESettingsDTO> dtos = new ArrayList<EngagementFiltersForDSESettingsDTO>();
		List<EngagementFiltersForDSESettings> models = this.getEngagementFiltersForDSESettingsDao().findAll();
		models.forEach(model -> { dtos.add(makeEngagementFiltersForDSESettingsDTO(model)); });
		return dtos;
	}
	// for get
	public EngagementFiltersForDSESettingsDTO getEngagementFiltersForDSESettingsDTO(Integer seConfigId, String suggestionType) {
		EngagementFiltersForDSESettings model = getEngagementFiltersForDSESettings(seConfigId, suggestionType);
		if(model == null) return new EngagementFiltersForDSESettingsDTO();
		return makeEngagementFiltersForDSESettingsDTO(model);
	}
	// for post
	public EngagementFiltersForDSESettingsDTO addEngagementFiltersForDSESettings(EngagementFiltersForDSESettingsDTO dto) {
		EngagementFiltersForDSESettings model = null;
		if(dto.getSuggestionType().equalsIgnoreCase("both")) {
			model = getEngagementFiltersForDSESettings(dto.getSeConfigId(), "target");
			if(model != null) this.getEngagementFiltersForDSESettingsDao().delete(model);	
			model = getEngagementFiltersForDSESettings(dto.getSeConfigId(), "trigger");
			if(model != null) this.getEngagementFiltersForDSESettingsDao().delete(model);
			this.getEngagementFiltersForDSESettingsDao().saveOrUpdate(makeEngagementFiltersForDSESettingsModel(dto));
		} else {
			this.getEngagementFiltersForDSESettingsDao().saveOrUpdate(makeEngagementFiltersForDSESettingsModel(dto));
			model = getEngagementFiltersForDSESettings(dto.getSeConfigId(), "both");
			if(model != null) {
				this.getEngagementFiltersForDSESettingsDao().delete(model);
				EngagementFiltersForDSESettingsDTO newDto = makeEngagementFiltersForDSESettingsDTO(model);
				newDto.setSuggestionType(dto.getSuggestionType().equalsIgnoreCase("target") ? "trigger" : "target");
				this.getEngagementFiltersForDSESettingsDao().saveOrUpdate(makeEngagementFiltersForDSESettingsModel(newDto));			
			}
		}
		this.getEngagementFiltersForDSESettingsDao().flush();
		return(dto);
	}
	// for delete
	public EngagementFiltersForDSESettingsDTO deleteEngagementFiltersForDSESettingsDTO(Integer seConfigId, String suggestionType) {
		EngagementFiltersForDSESettings model = getEngagementFiltersForDSESettings(seConfigId, suggestionType);
		if(model == null) return new EngagementFiltersForDSESettingsDTO();
		this.getEngagementFiltersForDSESettingsDao().delete(model);
		this.getEngagementFiltersForDSESettingsDao().flush();
		return makeEngagementFiltersForDSESettingsDTO(model);
	}


	
	
}
