/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;  
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningFileMapper;
import com.aktana.learning.api.dto.LearningFileDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningFile;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.LearningVersion;



/** Base class for LearningFile */
public class LearningFileBo extends AbstractBo<LearningFileInfoDTO, LearningFile, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningBuild, String> learningBuildDao = null;
    private GenericDao<LearningRun, String> learningRunDao = null;
	
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningFileBo() {
		super(LearningFileBo.class, LearningFileInfoDTO.class, LearningFile.class, LearningFileMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
		learningBuildDao = new GenericDaoImpl<LearningBuild, String>(LearningBuild.class);
		learningRunDao = new GenericDaoImpl<LearningRun, String>(LearningRun.class);
	}
	
	private GenericDao<LearningBuild, String> getLearningBuildDao() {
		learningBuildDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningBuildDao;
	}
	
	private GenericDao<LearningRun, String> getLearningRunDao() {
		learningRunDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningRunDao;
	}

	
	/** common method for listing a resource */
	@Override	
	public List<LearningFileInfoDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** Returns a List of all LearningFile objects. */
	public List<LearningFile> findAll() {
		return getDao().findAll();
	}
	

	/** common method for reading a resource */
	@Override	
	public LearningFileInfoDTO readObject(String id) {
		return super.readObject(id);		
	}
	
	
	public LearningFileDTO readObjectWithData(String id) {
		
		LearningFile learningFile = this.readModelWithExternalId(id); 
		if (learningFile == null) {
    		throw new GenericValidationException("Unable to read: LearningFile with UID: %s", id);	    								
		}
		LearningFileInfoDTO infoDto = this.toDTO(learningFile);
		
		return infoDto.createLearningFileDTO(learningFile.getData());		
	}

	
	/** common method to save a resource */
	public LearningFileInfoDTO saveObject(LearningFileInfoDTO obj) {				
		return super.saveObject(obj);
	}
		
	/** common method to update a resource */
	@Override	
	public LearningFileInfoDTO updateObject(String id, LearningFileInfoDTO obj) {		
		return super.updateObject(id, obj);		
	}

		
	/** common method to remove a resource */
	@Override
	public LearningFileInfoDTO removeObject(String id) {		
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningFile newModel, LearningFile oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningFile model, String pk) {
		model.setId(pk);
	}
	
	@Override
	protected LearningFile toModelAdditionalMappings(LearningFileInfoDTO dto, LearningFile model) {
    	
    	if (model == null) {
    		model = new LearningFile();
    	}
    	

    	String learningBuildUID =  dto.getLearningBuildUID();

    	LearningBuild learningBuild = getLearningBuildDao().get(learningBuildUID);
    	if (learningBuild == null) {
        	throw new GenericValidationException("Invalid learningBuildUID %s in LearningFile", learningBuildUID);	    								    								
    	}
    	
    	String learningRunUID =  dto.getLearningRunUID();

    	if(learningRunUID != null && !learningRunUID.equals("")) {
    		LearningRun learningRun = getLearningRunDao().get(learningRunUID);
    		if (learningRun == null) {
    			throw new GenericValidationException("Invalid learningRunUID %s in LearningFile", learningRunUID);	    								    								
    		}
    	}
    	
    	model.setLearningBuild(learningBuild);
    	    	
		return model;
	}

	
	@Override
	protected LearningFileInfoDTO toDTOAdditionalMappings(LearningFile model, LearningFileInfoDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningFileInfoDTO();
		}
		
		dto.setLearningFileUID(model.getId());
		if(model.getLearningRun() != null) {
			dto.setLearningRunUID(model.getLearningRun().getId());
		}
		
		LearningBuild learningBuild = model.getLearningBuild();
		dto.setLearningBuildUID(learningBuild.getId());
		
		LearningVersion learningVersion = learningBuild.getLearningVersion();
		dto.setLearningVersionUID(learningVersion.getId());
		dto.setLearningConfigUID(learningVersion.getLearningConfig().getId());
		
		dto.setFileName(model.getFileName());
		dto.setFileType(model.getFileType());
		dto.setFileSize(model.getFileSize());
	
		return dto;
	}
	
	
	@Override
	protected void validateDTO(LearningFileInfoDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();
		
		if ((dto.getLearningFileUID() == null) || dto.getLearningFileUID().length() < 1 || dto.getLearningFileUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningFileUID");
			err.setMessage(String.format("LearningFile UID is required and must be between 1 and 80 characters long: %s", dto.getLearningFileUID()));
			errors.add(err);
		}
		
		if ((dto.getLearningBuildUID() == null) || dto.getLearningBuildUID().length() < 1 || dto.getLearningBuildUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningBuildUID");
			err.setMessage(String.format("LearningBuild UID is required and must be between 1 and 80 characters long: %s", dto.getLearningBuildUID()));
			errors.add(err);
		}
		

		if ((dto.getLearningFileUID() != null) && (dto.getLearningBuildUID().equals("") || dto.getLearningBuildUID().length() > 80)) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningFileUID");
			err.setMessage(String.format("If provided, LearningFile UID must be between 1 and 80 characters long: %s", dto.getLearningBuildUID()));
			errors.add(err);
		}
		
		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}
				
	}


	/**
	 * Returns all learningFiles by learning version
	 */	
	public List<LearningFile> getAllLearningFilesByLearningVersion(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		List<LearningFile> params = this.getDao().findWithNamedQuery("LearningFile.custom.findByLearningVersion", map);
		return params;
	}
	
	/**
	 * Returns all learningFiles by learning build
	 */	
	public List<LearningFile> getAllLearningFilesByLearningBuild(String learningBuildUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningBuildUID", learningBuildUID);
		
		List<LearningFile> params = this.getDao().findWithNamedQuery("LearningFile.custom.findByLearningBuild", map);
		return params;
	}

	/**
	 * Returns all learningFiles by learning run
	 */	
	public List<LearningFile> getAllLearningFilesByLearningRun(String learningRunUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningRunUID", learningRunUID);
		
		List<LearningFile> params = this.getDao().findWithNamedQuery("LearningFile.custom.findByLearningRun", map);
		return params;
	}
	

}
