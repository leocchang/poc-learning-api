/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningObjectListMapper;
import com.aktana.learning.api.dto.LearningObjectListDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.api.dto.SimulationSegmentDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.module.LearningModelHandler;
import com.aktana.learning.persistence.models.impl.LearningObjectList;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.SimulationSegment;




/** Base class for LearningRun */
public class LearningObjectListBo extends AbstractBo<LearningObjectListDTO, LearningObjectList, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningBuild, String> learningBuildDao = null;
    private GenericDao<LearningRun, String> learningRunDao = null;


    /** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningObjectListBo() {
		super(LearningObjectListBo.class, LearningObjectListDTO.class, LearningObjectList.class, LearningObjectListMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());

		learningBuildDao = new GenericDaoImpl<LearningBuild, String>(LearningBuild.class);
		learningRunDao = new GenericDaoImpl<LearningRun, String>(LearningRun.class);


	}

	private GenericDao<LearningBuild, String> getLearningBuildDao() {
		learningBuildDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningBuildDao;
	}
	
	private GenericDao<LearningRun, String> getLearningRunDao() {
		learningRunDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningRunDao;
	}
	
	
	/** method to check that a learningRunUID exists in learningRun table */
	private void validateRunID (String learningRunUID) {
	
		if(learningRunUID == null || learningRunUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningRunUID %s", learningRunUID);	    								    								
		}
		
    	LearningRun learningRun = getLearningRunDao().get(learningRunUID);
    	if (learningRun == null) {
        	throw new GenericValidationException("Invalid learningRunUID %s in LearningRun", learningRunUID);
    	}
		
	}

	/** method for api. returns LearningObjectList DTOs w/ objectType = 'Account' given learningRun. */
	public List<LearningObjectListDTO> listAccountsWithLearningRun(HashMap<String, Object> map, Integer page, Integer rows) {		
		
		// validate limit and offset
		if(page == null || page <= 0 || rows == null || rows <= 0) {
			page = 1;
			rows = 100;
		}

		Integer offset = rows*(page-1);
		Integer limit = offset + rows;
		
		validateRunID(map.get("learningRunUID").toString());
		
		String namedQueryName = "LearningObjectList.custom.findAccountByLearningRun";
		
		List<LearningObjectList> learningObjectLists = new ArrayList<LearningObjectList>();
		learningObjectLists = getDao().findWithNamedQuery(namedQueryName, map, offset, limit);
		
		List<LearningObjectListDTO> learningObjectListDTOs = new ArrayList<LearningObjectListDTO>();
		learningObjectListDTOs = toDTO(learningObjectLists);
		
		return learningObjectListDTOs;
	}
	

	/** common method for listing a resource */
	@Override
	public List<LearningObjectListDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
		
	/** common method for reading a resource */
	@Override
	public LearningObjectListDTO readObject(String id) {
		return super.readObject(id);
	}
	

	/** common method to save a resource */
	public LearningObjectListDTO saveObject(LearningObjectListDTO obj) {
		return super.saveObject(obj);
	}

	/** common method to update a resource */
	@Override
	public LearningObjectListDTO updateObject(String id, LearningObjectListDTO obj) {
		return super.updateObject(id, obj);
	}


	/** common method to remove a resource */
	@Override
	public LearningObjectListDTO removeObject(String id) {
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningObjectList newModel, LearningObjectList oldModel) {
		//super.transferId(newModel, oldModel);
	}


	@Override
	protected void setId(LearningObjectList model, String pk) {
		//super.setId(model, pk);
		//model.setId(pk);
	}

	@Override
	protected LearningObjectList toModelAdditionalMappings(LearningObjectListDTO dto, LearningObjectList model) {
		return super.toModelAdditionalMappings(dto, model);
	}


	@Override
	protected LearningObjectListDTO toDTOAdditionalMappings(LearningObjectList model, LearningObjectListDTO dto) {
		return super.toDTOAdditionalMappings(model, dto); 
	}


	@Override
	protected void validateDTO(LearningObjectListDTO dto) {
		super.validateDTO(dto); 
	}


}
