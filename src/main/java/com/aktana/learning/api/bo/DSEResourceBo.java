/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.lang.reflect.Type;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.ChannelActionMap;
import com.aktana.learning.api.dto.dse.SEConfigDirectoryEntryDTO;
import com.aktana.learning.api.dto.dse.ParameterControlDTO;
import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningBuildMapper;
import com.aktana.learning.api.dto.AktanaErrorResponseDTO;
import com.aktana.learning.api.dto.DSEParametersForREMDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.ChannelActionMapDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.internal.LinkedTreeMap;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DSEResourceBo {
	
	
    private GenericDao<ChannelActionMap, String> channelActiomMapDao = null;
	

    private static final Logger LOGGER = LoggerFactory.getLogger(DSEResourceBo.class);
	
    public DSEResourceBo() {
    	channelActiomMapDao = new GenericDaoImpl<ChannelActionMap, String>(ChannelActionMap.class);
    	
    }
    
	private GenericDao<ChannelActionMap, String> getChannelActiomMapDao() {
		channelActiomMapDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return channelActiomMapDao;
	}
	
    public DSEParametersForREMDTO getDSEConfigValuesForREM() {
	    // Set to default values (no endpoint for this :( )
    	String enabledRepActionChannels = "VISIT_CHANNEL;SEND_CHANNEL";
	    Integer numPastDaysToAssumeActionIsCompleted = 2;
		Integer maxFutureCalendarDatesForSuggestingInCRM = 5;
		
		// set values to parameterControl values if necessary
		String parameterControls;
		try { parameterControls = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_PARAMETER_CONTROL); }
		catch (DSEConnectionException e) { throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage()); }	
		Type parameterControlDTOCollectionType = new TypeToken<Collection<ParameterControlDTO>>(){}.getType();
		List<ParameterControlDTO> parameterControlDTOs = new ArrayList<ParameterControlDTO>(new Gson().fromJson(parameterControls, parameterControlDTOCollectionType));	
		for(ParameterControlDTO parameterControlDTO : parameterControlDTOs) {
			System.out.println(parameterControlDTO.getParameterId());
			if(parameterControlDTO.getParameterId().equalsIgnoreCase("enabledRepActionChannels")) { enabledRepActionChannels = parameterControlDTO.getDeployedDefaultValue(); }
			if(parameterControlDTO.getParameterId().equalsIgnoreCase("numPastDaysToAssumeActionIsCompleted")) { numPastDaysToAssumeActionIsCompleted = Integer.parseInt(parameterControlDTO.getDeployedDefaultValue()); }
			if(parameterControlDTO.getParameterId().equalsIgnoreCase("maxFutureCalendarDatesForSuggestingInCRM")) { maxFutureCalendarDatesForSuggestingInCRM = Integer.parseInt(parameterControlDTO.getDeployedDefaultValue()); }
		}
		
    	HashSet<String> enabledRepActionChannelsSet = new HashSet<String>(); // get base channel set, expand with config parameters if necessary
		for(String channel : enabledRepActionChannels.split(";")) { enabledRepActionChannelsSet.add(channel); }
		
		// get max values over all configs
		String configDirectory;
		try { configDirectory = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_DIRECTORY); }
		catch (DSEConnectionException e) { throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage()); }
		Map<String, Object> configMap = new Gson().fromJson(configDirectory, new TypeToken<HashMap<String, Object>>() {}.getType());
		List<Integer> configIds = new ArrayList<Integer>();
		
		for(LinkedTreeMap configEntry : (List<LinkedTreeMap>) configMap.get("directory")) {
			Integer configId = ((Double) configEntry.get("id")).intValue();
			
			try {
		    	String dseResponse = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS,"/"+configId+"/globalParameters/enabledRepActionChannels");
		    	Map<String, String> paramMap = new Gson().fromJson(dseResponse, new TypeToken<HashMap<String, Object>>() {}.getType());
			    if(paramMap.get("enabledRepActionChannels") != null) {
				    for(String channelName : paramMap.get("enabledRepActionChannels").split(";")) { enabledRepActionChannelsSet.add(channelName); }
			    }
		    } catch (DSEConnectionException e) {
		        throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		    }	
		    
		    try {
		    	String dseResponse = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS,"/"+configId+"/globalParameters/numPastDaysToAssumeActionIsCompleted");
		    	Map<String, String> paramMap = new Gson().fromJson(dseResponse, new TypeToken<HashMap<String, Object>>() {}.getType());
			    String configEngagedWindowString = paramMap.get("numPastDaysToAssumeActionIsCompleted");
			    Integer configEngagedWindowInt = configEngagedWindowString != null ? Integer.parseInt(configEngagedWindowString) : 0;
		    	if(configEngagedWindowInt > numPastDaysToAssumeActionIsCompleted) { numPastDaysToAssumeActionIsCompleted = configEngagedWindowInt; }
		    } catch (DSEConnectionException e) {
		        throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		    }
		    
		    try {
		    	String dseResponse = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS,"/"+configId+"/globalParameters/maxFutureCalendarDatesForSuggestingInCRM");
		    	Map<String, String> paramMap = new Gson().fromJson(dseResponse, new TypeToken<HashMap<String, Object>>() {}.getType());
			    String configLookForwardString = paramMap.get("maxFutureCalendarDatesForSuggestingInCRM");
			    Integer configLookForwardInt = configLookForwardString != null ? Integer.parseInt(configLookForwardString) : 0;
		    	if(configLookForwardInt > maxFutureCalendarDatesForSuggestingInCRM) { maxFutureCalendarDatesForSuggestingInCRM = configLookForwardInt; }
		    } catch (DSEConnectionException e) {
		        throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		    }		
		}
		
		List<ChannelActionMapDTO> channelActionMapDTOs = new ArrayList<ChannelActionMapDTO>();
		String namedQueryName = "ChannelActionMap.custom.findByRepActionChannel";
		HashMap<String, Object> map = new HashMap<String, Object>();
		for(String channel : enabledRepActionChannelsSet) {
			map.put("repActionChannel", channel);
			for(ChannelActionMap channelActionMap : getChannelActiomMapDao().findWithNamedQuery(namedQueryName, map)) {
				ChannelActionMapDTO channelActionMapDTO = new ChannelActionMapDTO();
				channelActionMapDTO.setRepActionTypeId(channelActionMap.getRepActionTypeId());
				channelActionMapDTO.setRepActionTypeName(channelActionMap.getRepActionTypeName());
				channelActionMapDTO.setRepActionChannel(channelActionMap.getRepActionChannel());
				channelActionMapDTO.setAppearsInInteractionData(channelActionMap.getAppearsInInteractionData());
				channelActionMapDTOs.add(channelActionMapDTO);
			}
		}
		
		DSEParametersForREMDTO dseParamsForREMDTO = new DSEParametersForREMDTO();
		dseParamsForREMDTO.setEnabledRepActionChannels(String.join(";", enabledRepActionChannelsSet));
		dseParamsForREMDTO.setNumPastDaysToAssumeActionIsCompleted(numPastDaysToAssumeActionIsCompleted);
		dseParamsForREMDTO.setMaxFutureCalendarDatesForSuggestingInCRM(maxFutureCalendarDatesForSuggestingInCRM);
		dseParamsForREMDTO.setEnabledRepActionTypes(channelActionMapDTOs);
		
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");

		System.out.println(dseParamsForREMDTO);
		
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");

		
		return dseParamsForREMDTO;
    }
	
	
    public HashMap<String, Object> getManyGlobalParameters(Integer configId, String parameterNames) {
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		for(String parameterName : parameterNames.split(";")) {
		    String dseResponse = null;
		    try {
		        dseResponse = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_DSECONFIG_GLOBALPARAMS,"/"+configId+"/globalParameters/"+parameterName);
		    } catch (DSEConnectionException e) {
		        throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		    }
		    Map<String, Object> paramMap = new Gson().fromJson(dseResponse, new TypeToken<HashMap<String, Object>>() {}.getType());
		    responseMap.put(parameterName, paramMap.get(parameterName)); 
		}
		return responseMap;
    }
	
}
	
