package com.aktana.learning.api.bo.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.glasnost.orika.MapperFactory;

import com.aktana.learning.api.dto.RepEngagementCalculationDTO;
import com.aktana.learning.persistence.models.impl.RepEngagementCalculation;



public class RepEngagementCalculationMapper extends AbstractMapperSupport<RepEngagementCalculation, RepEngagementCalculationDTO>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RepEngagementCalculationMapper.class);

    private static RepEngagementCalculationMapper instance;
    
    public static RepEngagementCalculationMapper getInstance()  {

		synchronized(RepEngagementCalculationMapper.class) {
			if (instance == null) {
				instance = new RepEngagementCalculationMapper();
			}
		}
    	
    	return instance;    	
    }
	
	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(RepEngagementCalculationDTO.class, RepEngagementCalculation.class)
				// .fieldMap("repUID", "rep.externalId").add()
				// .fieldMap("accountUID", "account.externalId").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("endDate", "endDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());

	}

	@Override
	protected void initializeMappingsToDTO(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(RepEngagementCalculation.class, RepEngagementCalculationDTO.class)
				// .fieldMap("rep.externalId", "repUID").add()
				// .fieldMap("account.externalId", "accountUID").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());	
	}


	@Override
	public void applyCustomMappingsToDTO(RepEngagementCalculation pojo, RepEngagementCalculationDTO dto)
	{		
		if (pojo == null) {
			LOGGER.error("toDTO: RepEngagementCalculation pojo is null");
		}	
		else if (dto == null) {
			LOGGER.error("toDTO: RepEngagementCalculation dto is null");
		}
		/* else {
			// set the dto's startDate and endDate
			if(pojo.getId().getDate() != null) {
				PureDate pureDate = new PureDate(pojo.getId().getDate());
				dto.setDate(pureDate.toString());
			}
		} */
	}


	@Override
	public Class<?> getPojoClass() {
		return RepEngagementCalculation.class;
	}

	@Override
	public Class<?> getDTOClass() {
		return RepEngagementCalculationDTO.class;
	}

	
	@Override
	public RepEngagementCalculation toPojo(RepEngagementCalculationDTO dto)
	{
		RepEngagementCalculation pojo = super.toPojo(dto);
		this.applyCustomMappingsToPojo(dto, pojo);
		return pojo;
	}


	@Override
	public RepEngagementCalculationDTO toDTO(RepEngagementCalculation pojo)
	{
		RepEngagementCalculationDTO dto = super.toDTO(pojo);
		this.applyCustomMappingsToDTO(pojo, dto);
		return dto;
	}
	
}