package com.aktana.learning.api.bo.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.glasnost.orika.MapperFactory;

import com.aktana.learning.api.dto.AccountProductDTO;
import com.aktana.learning.persistence.models.impl.AccountProduct;



public class AccountProductMapper extends AbstractMapperSupport<AccountProduct, AccountProductDTO>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountProductMapper.class);

    private static AccountProductMapper instance;
    
    public static AccountProductMapper getInstance()  {

		synchronized(AccountProductMapper.class) {
			if (instance == null) {
				instance = new AccountProductMapper();
			}
		}
    	
    	return instance;    	
    }
	
	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(AccountProductDTO.class, AccountProduct.class)
				//.fieldMap("accountUID", "id.account.externalId").add()
				//.fieldMap("messageAlgorithmUID", "id.messageAlgorithm.externalId").add()	
				//.fieldMap("messageUID", "id.message.externalId").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("endDate", "endDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());

	}

	@Override
	protected void initializeMappingsToDTO(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(AccountProduct.class, AccountProductDTO.class)
				//.fieldMap("id.account.externalId", "accountUID").add()
				//.fieldMap("id.messageAlgorithm.externalId", "messageAlgorithmUID").add()	
				//.fieldMap("id.message.externalId", "messageUID").add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());	
	}


	@Override
	public Class<?> getPojoClass() {
		return AccountProduct.class;
	}

	@Override
	public Class<?> getDTOClass() {
		return AccountProductDTO.class;
	}

	
	@Override
	public AccountProduct toPojo(AccountProductDTO dto)
	{
		AccountProduct pojo = super.toPojo(dto);
		this.applyCustomMappingsToPojo(dto, pojo);
		return pojo;
	}


	@Override
	public AccountProductDTO toDTO(AccountProduct pojo)
	{
		AccountProductDTO dto = super.toDTO(pojo);
		this.applyCustomMappingsToDTO(pojo, dto);
		return dto;
	}
	
}