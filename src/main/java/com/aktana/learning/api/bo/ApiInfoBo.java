package com.aktana.learning.api.bo;

import com.aktana.learning.api.AktanaServiceDaos;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import java.util.List;

/**
 * BO for {@link com.aktana.api.resource.ApiInfoResource}
 *
 * Created by sivasrinivas on 2/16/16.
 */
public class ApiInfoBo {

   
    
    /** 
     * Returns a list of Strings containing information about the most recent
     * deployments:  date/time UTC, revision, and branch
     * 
     * @param numRecentDeployments = the max # of Strings to return
     */
    public List<String> getMostRecentDeploymentsInfo(int numRecentDeployments) {

    	Session session = AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession();
        SQLQuery sqlQuery = session.createSQLQuery(
        		"SELECT CONCAT(deploymentChangedAt, ' UTC  ', srcVersion, '  ', srcBranch)"
        		+ "FROM AktanaDeploymentHist "
        		+ "ORDER BY deploymentChangeId DESC LIMIT "+numRecentDeployments);

		List<String> results = sqlQuery.list();
     	return results;
    }
    
    /**
     * Inserts a new row into AktanaDeploymentHist table, unless
     * the specified srcVersion and srcBranch are the same as the
     * most recently-inserted row.
	 *
	 * Returns TRUE if the specified version and branch are a new deployment.
     */
    public boolean recordDeploymentIfNew(String srcVersion, String srcBranch) {
 
    	if (srcVersion==null || srcVersion.trim().isEmpty()
    		|| srcBranch==null || srcBranch.trim().isEmpty()) {
    			return false;
    	}
    	
    	// If a new row gets inserted, then we've just had a new deployment
    	Session session = AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession();
        SQLQuery sqlInsert= session.createSQLQuery(
        		"INSERT INTO AktanaDeploymentHist(srcVersion,srcBranch) "
        	 	+"SELECT '"+srcVersion+"', '"+srcBranch+"' FROM DUAL "
        	 	+"WHERE NOT EXISTS ("
        	 		+"SELECT 1 FROM AktanaDeploymentHist "
        	 		+"WHERE deploymentChangeId=(SELECT MAX(deploymentChangeId) FROM AktanaDeploymentHist) "
        	 		+"AND srcVersion='"+srcVersion+"' AND srcBranch='"+srcBranch+"'"
        	 		+")");
        int n = sqlInsert.executeUpdate();
        return n > 0;
    }
    
    /**
     * Returns a String containing information about the most-recently-executed
     * Liquibase migration that was executed in the current database.

     */
    public String getMostRecentLiquibaseMigrationInfo() {
    	Session session = AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession();
        SQLQuery sqlQuery = session.createSQLQuery(
        		"SELECT CONCAT(DATEEXECUTED,'(UTC) ',ID,'  ',FILENAME,'   \"',COMMENTS,'\"') "
        		+"FROM DATABASECHANGELOG "
        		+"WHERE ORDEREXECUTED=(SELECT MAX(ORDEREXECUTED) FROM DATABASECHANGELOG)"
        		);
        List<String> results = sqlQuery.list();
        if (results==null || results.size()==0) return null;
        return results.get(0);
     }
}
