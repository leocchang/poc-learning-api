/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.LearningFileDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.persistence.models.impl.LearningFile;

import ma.glasnost.orika.MapperFactory;


public class LearningFileMapper extends AbstractMapperSupport<LearningFile, LearningFileInfoDTO>
{
    public LearningFileMapper()
    {
        super();
    }
    
    private static LearningFileMapper instance;
    
    public static LearningFileMapper getInstance()  {

		synchronized(LearningFileMapper.class) {
			if (instance == null) {
				instance = new LearningFileMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningFile.class, LearningFileInfoDTO.class)
                .field("id", "learningFileUID")
                .field("learningRun.id", "learningRunUID")
                .field("learningBuild.id", "learningBuildUID")
                .field("fileName", "fileName")
                .field("fileType", "fileType")
                .field("fileSize", "fileSize")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningFileInfoDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningFile.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningFileInfoDTO.class, LearningFile.class)
                .field("learningFileUID", "id")
                .field("learningRunUID", "learningRun.id")
                .field("learningBuildUID", "learningBuild.id")
                .field("fileName", "fileName")
                .field("fileType", "fileType")
                .field("fileSize", "fileSize")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningFileInfoDTO src, LearningFile tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningFile src, LearningFileInfoDTO tgt)
	{
	
	}

}