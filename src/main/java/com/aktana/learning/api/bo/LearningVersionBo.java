/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningVersionMapper;
import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import com.aktana.learning.api.dto.LearningVersionInfoDTO;
import com.aktana.learning.api.dto.AggRepDateLocationAccuracyDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.ILearningConfig;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningParam;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.LearningVersion;
import com.aktana.learning.persistence.models.impl.SimulationAccountSentEmail;
import com.aktana.learning.persistence.models.impl.AggRepDateLocationAccuracy;

import com.aktana.learning.api.bo.LearningBuildBo;



/** Base class for LearningVersion */
public class LearningVersionBo extends AbstractBo<LearningVersionInfoDTO, LearningVersion, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningConfig, String> learningConfigDao;
    
    private GenericDao<LearningVersion, String> learningVersionDao;
    
    private GenericDao<AggRepDateLocationAccuracy, String> aggRepDateLocationAccuracyDao;

    
    private LearningBuildBo learningBuildBo = null;
    
    private LearningRunBo learningRunBo = null;
	
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningVersionBo() {
		super(LearningVersionBo.class, LearningVersionInfoDTO.class, LearningVersion.class, LearningVersionMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
		learningBuildBo = new LearningBuildBo();
		learningRunBo = new LearningRunBo();
		
		learningConfigDao =  new GenericDaoImpl<LearningConfig, String>(LearningConfig.class);
		aggRepDateLocationAccuracyDao =  new GenericDaoImpl<AggRepDateLocationAccuracy, String>(AggRepDateLocationAccuracy.class);
		learningVersionDao = getDao(); // don't have to set session twice if not needed
	}
	
	private GenericDao<LearningConfig, String> getLearningConfigDao() {
		learningConfigDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningConfigDao;
	}
	
	private GenericDao<AggRepDateLocationAccuracy, String> getAggRepDateLocationAccuracyDao() {
		aggRepDateLocationAccuracyDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return aggRepDateLocationAccuracyDao;
	}

	
	/** common method for listing a resource */
	@Override	
	public List<LearningVersionInfoDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** Returns a List of all LearningVersion objects. */
	public List<LearningVersion> findAll() {
		return getDao().findAll();
	}
	

	/** common method for reading a resource */
	@Override	
	public LearningVersionInfoDTO readObject(String id) {
		return super.readObject(id);		
	}

	
	/** common method to save a resource */
	public LearningVersionInfoDTO saveObject(LearningVersionInfoDTO obj) {				
		return super.saveObject(obj);
	}
		
	/** common method to update a resource */
	@Override	
	public LearningVersionInfoDTO updateObject(String id, LearningVersionInfoDTO obj) {		
		return super.updateObject(id, obj);		
	}

	
	@Override
	protected LearningVersion beforeAction(String action, LearningVersionInfoDTO dto, LearningVersion model) {	
		
		if (action.equalsIgnoreCase(LearningConfigBo.ACTION_UPDATE)) {
						
			LearningVersion oldModel = this.readModelWithExternalId(dto.getLearningVersionUID());
			if(oldModel == null) {
				throw new GenericValidationException("Existing learningVersion %s not found", dto.getLearningVersionUID());
			}
			
			model.setIsDeployed(oldModel.getIsDeployed());
			model.setIsFinalized(oldModel.getIsFinalized());
			model.setIsDeleted(oldModel.getIsDeleted());	
		} else if (action.equalsIgnoreCase(LearningConfigBo.ACTION_REMOVE)) {
			
			if(model.getIsDeployed()) {
				throw new GenericValidationException("LearningVersion %s is the deployed version for learningConfig %s and therefore cannot be deleted", model.getId(), model.getLearningConfig().getId());
			} else {
				updateCurrentVersion(model);
			}
		}
		return model;
	}
		
	private void updateCurrentVersion(LearningVersion learningVersion) {
		// get config if it has CurrentLearningVersionUID = learningVersionUID
		HashMap<String, Object> versionMap = new HashMap<String, Object>();
		String namedQueryName = "LearningConfig.custom.findByCurrentLearningVersion";
		versionMap.put("currentLearningVersionUID", learningVersion.getId());		
		List<LearningConfig> learningConfigList = getLearningConfigDao().findWithNamedQuery(namedQueryName, versionMap);

		if(learningConfigList.size() == 0) {
			return;
		}
		LearningConfig learningConfig = learningConfigList.get(0); // can only be 1
		
		// get all learningVersions that belong to the confix
		HashMap<String, Object> configMap = new HashMap<String, Object>();
		configMap.put("learningConfigUID", learningConfig.getId());
		List<LearningVersion> learningVersions = this.getDao().findWithNamedQuery("LearningVersion.custom.findFinalizedVersionsByLearningConfigOrderByCreated", configMap);
		LearningVersion nextLearningVersion = new LearningVersion();
		
		// get next currentLearningVersion if possible
		if(learningVersions.size() < 1) { 
			nextLearningVersion = null;
		} else {
			nextLearningVersion = learningVersions.get(0);
			if(nextLearningVersion.equals(learningVersion)) {		
				if(learningVersions.size() < 2) {
					nextLearningVersion = null;
				} else {
					nextLearningVersion = learningVersions.get(1);
				}
			}
		}
		// dummy build to shorten logic
		LearningBuild nullBuild = null;
		
		// if null version, null build either
		if(nextLearningVersion == null) {
			((ILearningConfig)learningConfig).setCurrentLearningVersion(nextLearningVersion);
			((ILearningConfig)learningConfig).setCurrentLearningBuild(nullBuild);	
			learningConfigDao.merge(learningConfig);
			learningConfigDao.flush();
			return;	
		}

		
		// now update current learning build given new version
		HashMap<String, Object> nextVersionMap = new HashMap<String, Object>();
		nextVersionMap.put("learningVersionUID", nextLearningVersion.getId());
		namedQueryName = "LearningBuild.custom.findBuildsByLearningVersionOrderByCreated";
		List<LearningBuild> learningBuilds = learningBuildBo.getDao().findWithNamedQuery(namedQueryName, nextVersionMap);

		LearningBuild nextLearningBuild;
		if(learningBuilds.size() > 0) {
			nextLearningBuild = learningBuilds.get(0);
		} else {
			nextLearningBuild = nullBuild;
		}
		
		// set new current version and build and update model
		((ILearningConfig)learningConfig).setCurrentLearningVersion(nextLearningVersion);
		((ILearningConfig)learningConfig).setCurrentLearningBuild(nextLearningBuild);		
		learningConfigDao.merge(learningConfig);		
		learningConfigDao.flush();
	}
	
	
	public AggRepDateLocationAccuracyDTO getAnchorAggScoresByVersion(String learningVersionUID) {
		
		String namedQueryName = "AggRepDateLocationAccuracy.custom.findByLearningVersion";
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put("learningVersionUID", learningVersionUID);
		List <AggRepDateLocationAccuracy> aggScoresList = this.getAggRepDateLocationAccuracyDao().findWithNamedQuery(namedQueryName, map);

		if (aggScoresList.size() < 1) throw new GenericValidationException("No aggregate scores for LearningVersion: ", learningVersionUID);	    								    								

		// learning versionUID is a pk of the table, so only one possible response 
		AggRepDateLocationAccuracy aggScores = aggScoresList.get(0);

		AggRepDateLocationAccuracyDTO dto = new AggRepDateLocationAccuracyDTO();
		
		dto.setLearningVersionUID(aggScores.getLearningVersion().getId());
		dto.setLatestLearningRunUID(aggScores.getLatestLearningRun().getId());
		dto.setPctOverlapCoverage(aggScores.getPctOverlapCoverage());
		dto.setPctPredictedVisited(aggScores.getPctPredictedVisited());
		dto.setPctSuggestionPredicted(aggScores.getPctSuggestionPredicted());
		dto.setPctOverallPrediction(aggScores.getPctOverallPrediction());
		dto.setLatestRunPredictionCount(aggScores.getLatestRunPredictionCount());
		dto.setCreatedAt(dateTimeFormat.format(aggScores.getCreatedAt()));
		dto.setUpdatedAt(dateTimeFormat.format(aggScores.getUpdatedAt()));

		return dto;
	}

	
	
	
	
	/** common method to remove a resource */
	@Override
	public LearningVersionInfoDTO removeObject(String id) {		
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningVersion newModel, LearningVersion oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningVersion model, String pk) {
		model.setId(pk);
	}
	
	@Override
	protected LearningVersion toModelAdditionalMappings(LearningVersionInfoDTO dto, LearningVersion model) {
    	
    	if (model == null) {
    		model = new LearningVersion();
    	}
    	

    	String learningConfigUID =  dto.getLearningConfigUID();

    	LearningConfig learningConfig = getLearningConfigDao().get(learningConfigUID);
    	if (learningConfig == null) {
        	throw new GenericValidationException("Invalid learningConfigUID %s in LearningVersion", learningConfigUID);	    								    								
    	}
    	
    	model.setLearningConfig(learningConfig);
    	    	
		return model;
	}
	
	@Override
	protected LearningVersionInfoDTO toDTOAdditionalMappings(LearningVersion model, LearningVersionInfoDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningVersionInfoDTO();
		}
		
		Date dateTime = model.getCurrentStartDateTime();
		if(dateTime != null) dto.setCurrentStartDateTime(convertDateTimeToString(dateTime));
		
		dto.setIsDeployed(model.getIsDeployed());
		dto.setIsDeleted(model.getIsDeleted());
		dto.setIsFinalized(model.getIsFinalized());
		// dto.setLearningConfigUID(model.getId());
		dto.setLearningConfigUID(model.getLearningConfig().getId());

		List<LearningBuild> learningBuilds = learningBuildBo.getAllLearningBuildsByLearningVersionOrderedByCreated(model.getId());
		if (learningBuilds != null && !learningBuilds.isEmpty()) {
			LearningBuild latestBuild = Collections.max(learningBuilds, Comparator.comparing(c -> c.getUpdatedAt()));
			dto.setCurrentLearningBuildUID(latestBuild.getId());
			dto.setCurrentLearningBuildUIDStatus(latestBuild.getExecutionStatus());
			
			dateTime = latestBuild.getExecutionDatetime();
			if(dateTime != null) dto.setCurrentLearningBuildUIDstartDateTime(convertDateTimeToString(dateTime));
			List<LearningRun> learningRuns = learningRunBo.getAllLearningScoringRunsByLearningBuild(latestBuild.getId());
			if (learningRuns != null && !learningRuns.isEmpty()) {
				LearningRun latestRun = Collections.max(learningRuns, Comparator.comparing(c -> c.getUpdatedAt()));
				dto.setLatestRunUID(latestRun.getId());
				dto.setLatestRunUIDStatus(latestRun.getExecutionStatus());
				dto.setLatestRunUIDIsPublished(latestRun.getIsPublished());
				dateTime = latestRun.getCreatedAt();
				if(dateTime != null) dto.setLatestRunUIDCreatedAt(convertDateTimeToString(dateTime));
				
				dateTime = latestRun.getUpdatedAt();
				if(dateTime != null) dto.setLatestRunUIDupdatedAt(convertDateTimeToString(dateTime));
				
				dateTime = latestRun.getExecutionDatetime();
				if(dateTime != null) dto.setLatestRunUIDStartDateTime(convertDateTimeToString(dateTime));
			}
		}

		return dto;
	}
	
	@Override
	protected void validateDTO(LearningVersionInfoDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();
		
		if ((dto.getLearningVersionUID() == null) || dto.getLearningVersionUID().length() < 1 || dto.getLearningVersionUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningVersionUID");
			err.setMessage(String.format("LearningVersion UID is required and must be between 1 and 80 characters long: %s", dto.getLearningVersionUID()));
			errors.add(err);
		}
		
		if ((dto.getLearningConfigUID() == null) || dto.getLearningConfigUID().length() < 1 || dto.getLearningConfigUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningConfigUID");
			err.setMessage(String.format("learningConfig UID is required and must be between 1 and 80 characters long: %s", dto.getLearningConfigUID()));
			errors.add(err);
		}
		
		
		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}
				
	}
	
	// return list of deployed versions by config (hint: there should only be one)                                                                                                                                                              
	public List<LearningVersion> getAllDeployedLearningVersionsByLearningConfig(LearningConfig learningConfig) {                                                                                                                             
		HashMap<String, Object> configMap = new HashMap<String, Object>();    
		configMap.put("learningConfigUID", learningConfig.getId());
		String namedQueryName = "LearningVersion.custom.findDeployedVersionsByLearningConfigOrderByCreated";                                                                                                                                
		return this.getDao().findWithNamedQuery(namedQueryName, configMap);                                                                                                                                                                 
	}  

}
