/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningParamMapper;
import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.LearningParam;
import com.aktana.learning.persistence.models.impl.LearningParamPK;
import com.aktana.learning.persistence.models.impl.LearningVersion;



/** Base class for LearningParam */
public class LearningParamBo extends AbstractBo<LearningParamDTO, LearningParam, LearningParamPK> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningVersion, String> learningVersionDao = null;
	
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningParamBo() {
		super(LearningParamBo.class, LearningParamDTO.class, LearningParam.class, LearningParamMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
		learningVersionDao = new GenericDaoImpl<LearningVersion, String>(LearningVersion.class);
	}
	
	private GenericDao<LearningVersion, String> getLearningVersionDao() {
		learningVersionDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningVersionDao;
	}

	
	/** common method for listing a resource */
	@Override	
	public List<LearningParamDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** Returns a List of all LearningParam objects. */
	public List<LearningParam> findAll() {
		return getDao().findAll();
	}
	

	/** common method for reading a resource */
	@Override	
	public LearningParamDTO readObject(String id) {
		return super.readObject(id);		
	}

	
	/** common method to save a resource */
	public LearningParamDTO saveObject(LearningParamDTO obj) {				
		return super.saveObject(obj);
	}
		
	/** common method to update a resource */
	@Override	
	public LearningParamDTO updateObject(String id, LearningParamDTO obj) {		
		return super.updateObject(id, obj);		
	}

		
	/** common method to remove a resource */	
	@Override
	public LearningParamDTO removeObject(String id) {
		
		LearningParamDTO oldobj = null;
		
		try {
			oldobj = this.readObject(id);
		} catch (Exception e) {
			oldobj = null;
		}
		
		int ret = deleteModelsWithCompositeExternalIdPattern(id);
		
		return oldobj;
	}
	

	@Override
	protected void transferId(LearningParam newModel, LearningParam oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningParam model, LearningParamPK pk) {
		model.setId(pk);
	}
	
	@Override
	protected LearningParam toModelAdditionalMappings(LearningParamDTO dto, LearningParam model) {
		    	
    	if (model == null) {
    		model = new LearningParam();
    	}
    	
    	model.setParamValue(dto.getParamValue());
    	
    	String learningVersionUID = dto.getLearningVersionUID();
    	LearningVersion learningVersion = getLearningVersionDao().get(learningVersionUID);
    	if (learningVersion == null) {
        	throw new GenericValidationException("Invalid learningVersionUID %s in LearningBuild", learningVersionUID);	    								    								
    	}
    	
    	model.setLearningConfig(learningVersion.getLearningConfig());
    	
    	LearningParamPK learningParamPK = new LearningParamPK();
    	learningParamPK.setLearningVersion(learningVersion);
    	learningParamPK.setParamName(dto.getParamName());
    	learningParamPK.setParamIndex(dto.getParamIndex());
    	
    	model.setId(learningParamPK);
 
    	    	
		return model;
	}

	

	@Override
	protected LearningParamDTO toDTOAdditionalMappings(LearningParam model, LearningParamDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningParamDTO();
		}
		
		dto.setLearningVersionUID(model.getId().getLearningVersion().getId());
		dto.setParamName(model.getId().getParamName());
		dto.setParamIndex(model.getId().getParamIndex());
		dto.setParamValue(model.getParamValue());
				
		return dto;
	}
	
	@Override
	protected void validateDTO(LearningParamDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();
		
		
		if ((dto.getLearningVersionUID() == null) || dto.getLearningVersionUID().length() < 1 || dto.getLearningVersionUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningVersionUID");
			err.setMessage(String.format("LearningVersion UID is required and must be between 1 and 80 characters long: %s", dto.getLearningVersionUID()));
			errors.add(err);
		}
		
		if ((dto.getParamName() == null) || dto.getParamName().length() < 1 || dto.getParamName().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("paramName");
			err.setMessage(String.format("paramName is required and must be between 1 and 80 characters long: %s", dto.getParamName()));
			errors.add(err);
		}
		
		if ((dto.getParamIndex() == null) || dto.getParamIndex() < 0) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("paramIndex");
			err.setMessage(String.format("paramIndex is required and must be equal or greater than 0: %s", dto.getParamIndex()));
			errors.add(err);
		}
		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}
				
	}

	
	/* helper method to read existing model object based on the composite ids */
	@Override
	public LearningParam readModelWithExternalId(String id) {
		HashMap<String, Object> map = parseAndValidateObjectUID(id);
		
		LearningParam pojo = super.readModelWithExternalId(map);
				
		return 	pojo;
	}
	
		
	
	private HashMap<String, Object> parseAndValidateObjectUID(String id) {
		String[] subkeys = id.split("\\~", -1);
		
		if (subkeys.length != 3) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", subkeys[0]);
		map.put("paramName", subkeys[1]);
		
		Object key2 = subkeys[2];
		if(key2 != null && !key2.equals("")) {
			key2 = Integer.parseInt(subkeys[2]);
			map.put("paramIndex", key2);
		}		
			
		return map;		
	}
	
	private HashMap<String, Object> parseObjectUIDPattern(String id) {
		String[] subkeys = id.split("\\~");
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(subkeys.length > 0){
			map.put("learningVersionUID", subkeys[0]);
		}
		if(subkeys.length > 1){
			map.put("paramName", subkeys[1]);
		}
		if(subkeys.length > 2)
		{
			map.put("paramIndex", subkeys[2]);
		}
		
		return map;	
	
	}	

	/**
	 * Delete objects based on partially filled composite ids
	 * @param id
	 */
	public int deleteModelsWithCompositeExternalIdPattern(String id) {
		HashMap<String, Object> map = parseAndValidateObjectUID(id);
		
		String learningVersionUID = (String) map.get("learningVersionUID");
		String paramName = (String) map.get("paramName");
		Integer paramIndex = (Integer) map.get("paramIndex");
		
		if(learningVersionUID != null && learningVersionUID.isEmpty()) {
			learningVersionUID = null;
		}
		
		if(paramName != null && paramName.isEmpty()) {
			paramName = null;
		}
		
		
		if (learningVersionUID != null && !learningVersionUID.isEmpty()) {		
			LearningVersion learningVersion = getLearningVersionDao().get(learningVersionUID);
			if (learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s in LearningParam", learningVersionUID);	    								    													
			}
		} 
		
		if(paramName == null && paramIndex != null) {
			throw new GenericValidationException("Deleting of param by index is not allowed without specifiying param name");	 
		}
		
		String hql = "delete LearningParam  where ";
		
		String andStr = "";

		if (learningVersionUID != null || paramName != null) {
				
			if (learningVersionUID != null) {								
				hql = hql + " learningVersionUID = :learningVersionUID ";
				andStr = " and ";
			}
			
			if (paramName != null) {
				hql = hql + andStr + " paramName = :paramName ";
				andStr = " and ";
			}
			
			if (paramIndex != null) {
				hql = hql + andStr + " paramIndex = :paramIndex ";
			}
			
			Query q = getDao().getSession().createQuery(hql); 
			
			if (learningVersionUID != null) {		
				q.setString("learningVersionUID", learningVersionUID);			
			} 
			
			if (paramName != null) {		
				q.setString("paramName", paramName);			
			} 
			
			if (paramIndex != null) {		
				q.setInteger("paramIndex", paramIndex);			
			} 
			
			return q.executeUpdate();			
		}
						
		return 0;		
	}

	/**
	 * Returns all learningParams by learning version
	 */	
	public List<LearningParam> getAllLearningParamsByLearningVersion(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		List<LearningParam> params = this.getDao().findWithNamedQuery("LearningParam.custom.findByLearningVersion", map);
		return params;
	}
	
}
