/*****************************************************************
 * 
 * AbstractBo is the abstract base class for all Business Objects. 
 * 
 * The AbstractBo is Generic implementation based on
 * <ul>
 * <li><DTO> (Data Transfer Object) the DTO Class of the corresponding resource representation </li>
 * <li><M> the Class of the corresponding Hibernate "model" (aka POJO) object</li>
 * <li><PK> the Class of the corresponding Hibernate model object's Primary Key</li>
 * </ul>
 * 
 * The base class provides the following capabilities that can be used or derived by the children
 * <ul>
 * 		<li>
 * 			CORE Methods to List, Search Create, Read, Update, Delete of the DTO Objects
 * 		</li>
 *		<li>
 * 			Support Methods to Create, Read, Update, Delete of the Model Objects (used by CORE Methods)
 * 		</li>
 *		<li>
 * 			Optional Methods to List Models, readModelWithPK, updateModel for use in specialized derived classes
 * 		</li> 
 *		<li>
 * 			Abstract methods for children to setId for the model and transferId from old model to new model
 * 		</li>
 *		<li>
 * 			Optional Base implementations of beforeAction and afterAction methods that enable children to intercept and perform additional
 *          tasks before or after ACTION_SAVE, ACTION_UPDATE, ACTION_REMOVE
 * 		</li>
 *		<li>
 * 			Optional Base implementations of toModelAdditionalMappings and toDTOAdditionalMappings methods that perform additional model/DTO translations
 * 		</li>
 * </ul>
 * 
 * 
 *
 * @author $Author: satya.dhanushkodi $ 
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.beans.Introspector;
import java.io.Serializable;   

import static org.reflections.ReflectionUtils.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;






import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.AbstractMapperSupport;
import com.aktana.learning.api.bo.special.SysParameterBo;
import com.aktana.learning.api.dto.AktanaSyncDTO;
import com.aktana.learning.api.dto.AktanaSyncResponseDTO;
import com.aktana.learning.api.dto.IExternallyIdentifiableDTO;
import com.aktana.learning.api.dto.IMarkableForDeleteDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.ISoftDeletable;
import com.aktana.learning.persistence.models.impl.S3PartialPaths;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractBo<DTO, M, PK extends Serializable> {

	/** Action Ids passed to the beforeAction and afterAction methods that conveys the intent of the caller */	
	public static final String ACTION_SAVE = "save";
	public static final String ACTION_UPDATE = "update";
	public static final String ACTION_REMOVE = "remove";


	private static final Set<Class<?>> SIMPLE_TYPES = getSimpleTypes();

	
	/** Class type. Primarily used for the LOGGER */
	private final Class<?> type;

	/** resource representation DTO class */
    private final Class<DTO> dtoClazz;

	/** Simple Class Name of the DTO class that represents the resource */
    private final String dtoName;

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** ObjectMapper user for JSON conversion */
    private ObjectMapper mapper;

    /** DAO object used to interact with the database for the particular Model and PK*/    private GenericDao<M, PK> dao;

    /** A mapper class that enables converting DTO to Model and vice versa **/
    private AbstractMapperSupport<M, DTO> converter;
    
	/** Simple Class Name of the POJO class that represents the resource */
    private final String modelName;
        
    private String apiVersion;
    
    
    DateFormat dateTimeFormat;
    
    /** configured methods by types **/
    /** possible types are: "validate_akt", "derive_akt", "beforeaction_akt", "afteraction_akt" 
     * The method methods will end with one of the types above
     * The constructor will read the configured methods and save it.
     * The derived methods will check if a configured method is available and override the configured method if present
     **/    
    HashMap<String, List<Method>> configuredMethods;
    
    
	/** Sole constructor with the derived BO class, the resource representation class (DTO), hibernate model class (POJO) and a converter from/to map from DTO <-> POJO */
    /**
     * 
     * @param type is the actual derived BO implementation class
     * @param dtoClazz is the class of the resource representation DTO 
     * @param modelClazz is the Class of the corresponding Hibernate "model" (aka POJO) object 
     * @param converter is the Mapper to transform DTO -> POJO and POJO -> DTO
     */    
	public AbstractBo(Class<?> type, Class<DTO> dtoClazz, Class<M> modelClazz, AbstractMapperSupport<M,DTO> converter) {
		// derived Bo
		this.type = type;

		this.dtoClazz = dtoClazz;
		
		// remove the last three characters (DTO) of the class name and uses that as the dtoName
		this.dtoName = this.dtoClazz.getSimpleName().substring(0, this.dtoClazz.getSimpleName().length()-3);

		// LOGGER specific to the derived implementation
		LOGGER = LoggerFactory.getLogger(this.type);

		mapper = new ObjectMapper();
		
		dao = new GenericDaoImpl<M, PK>(modelClazz);
		
		this.modelName = modelClazz.getSimpleName();
		
		this.converter = converter;		

		this.configuredMethods = this.findConfiguredMethods(type);
		
		dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}

	
	/**
	 * Returns an interface to convert the Model to DTO and vice versa
	 * 
	 * @return AbstractMapperSupport<M, DTO> 
	 */
	private AbstractMapperSupport<M, DTO> getConverter() {
		return converter;
	}
	

	/**
	 * Returns a DAO class for a Model (POJO) and Primary Key (PK) classes. The Model and PK classes are assumed to have been annotated for persistence
	 * 
	 * @return GenericDao<M, PK> - a DAO class
	 */
	protected GenericDao<M, PK> getDao() {
    	dao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
    	
    	return dao;
	}

	
	/**
	 * Returns a simple name that can be used to report errors and warnings
	 * 
	 * @return String - DTO Name 
	 */	
	protected String getDtoName() {
		return dtoName;
	}

	
	/**
	 * Support method Given a list of model objects uses the converter to convert it into a list of dto objects
	 * 
	 * @param modellist containing a List of model objects
	 * @return dtolist containing a list of DTO objects
	 */
	protected List<DTO> toDTO(List<M> modellist) {
		List<DTO> list = new ArrayList<DTO>();

		// convert model to DTO
    	for (M model : modellist) {
    		list.add(this.toDTO(model));
    	}

    	return list;
	}
	

	
	/**
	 * Core method for listing DTO objects. Method restricts the maximum number of objects returned to 100
	 * 
	 * This is a CORE method that used by derived Bo to listObjects in a REST endpoint
	 * 
	 * @return a list of DTO objects.
	 */
	public List<DTO> listLimitedObjects() {
    	GenericDao<M, PK> modelDao =  getDao();    	

    	List<M> modellist = modelDao.findPaginated(0, 100);

    	List<DTO> list = toDTO(modellist);

    	return list;
	}

	
	/**
	 * Optional method for listing Model objects. Method restricts the maximum number of objects returned to 100
	 *
	 * This is an optional method the may be used by derived classes for customizing implementations
	 * 
	 * @return a list of Model objects.
	 */
	public List<M> listLimitedModels() {
    	GenericDao<M, PK> modelDao =  getDao();    	

    	List<M> modellist = modelDao.findPaginated(0, 100);

    	return modellist;
	}
	
	
	/**
	 * Optional method for listing all Model objects.
	 *
	 * This is an optional method the may be used by derived classes for customizing implementations
	 * 
	 * @return a list of Model objects.
	 */
	public List<M> listAllModels() {
    	GenericDao<M, PK> modelDao =  getDao();    	

    	List<M> modellist = modelDao.findAll();

    	return modellist;
	}
	
	/**
	 * Optional method for listing all DTO objects.
	 * 
	 * This is a optional method that used by derived Bo to listObjects in a REST endpoint
	 * 
	 * @return a list all DTO objects.
	 */
	public List<DTO> listAllObjects() {
    	GenericDao<M, PK> modelDao =  getDao();    	

    	List<M> modellist = modelDao.findAll();

    	List<DTO> list = toDTO(modellist);

    	return list;
	}
	

	/**
	 * Core method for searching DTO objects.
	 * 
	 * @param offset - offset of the first row to return that can be used for paging
	 * @param limit - limit of the number of rows to return that can be used for paging
	 * @param orderby - List of DTO fields that should appear in the orderby clause of the query
	 * @param filters - Map of DTO field - values that should appear in the where clause of the query
	 * @return List of DTO objects
	 */
	public List<DTO> searchObjects(int offset, int limit, List<String> orderby, Map<String,Object> filters) {
    	
    	List<M> modellist = searchModels(offset, limit, orderby, filters);

    	List<DTO> list = toDTO(modellist);

    	return list;
	}
	

	
	/**
	 * Support method to search for POJO objects
	 * 
	 * @param offset - offset of the first row to return that can be used for paging
	 * @param limit - limit of the number of rows to return that can be used for paging
	 * @param orderby - List of DTO fields that should appear in the orderby clause of the query
	 * @param filters - Map of DTO field - values that should appear in the where clause of the query
	 * @return list of models
	 */	
	protected List<M> searchModels(int offset, int limit, List<String> orderby, Map<String,Object> filters) {
    	GenericDao<M, PK> modelDao =  getDao();    	

    	
    	// Use the DTO -> POJO converter to translate DTO fieldNames in the filter to POJO fieldNames in the filter
    	Map<String,Object> model_filters = getConverter().translateDtoFilterToModelFilter(filters);

    	// Use the DTO -> POJO converter to translate DTO fieldNames in the orderby to POJO fieldNames in the orderby    	
    	List<String> model_orderby = getConverter().translateDtoOrderbyToModelOrderby(orderby);

    	// build the criteria based on the POJO filter and POJO orderby
    	Criteria crit = buildCriteria(offset, limit, model_orderby, model_filters);
    	
    	List<M> modellist = modelDao.findByCriteria(crit);

    	return modellist;
	}

	
	/**
	 * Support method to build a hibernate criteria object based the passed in parameters for executing a criteria query
	 * 
	 * @param offset - offset of the first row to return that can be used for paging
	 * @param limit - limit of the number of rows to return that can be used for paging
	 * @param orderby - List of POJO fields that should appear in the orderby clause of the query
	 * @param filters - Map of POJO field - values that should appear in the where clause of the query
	 * @return Criteria object that can be used to execute a criteria query
	 */
	protected Criteria buildCriteria(int offset, int limit,  List<String> orderby, Map<String, Object> filters) {
		Criteria criteria = getDao().getCriteria();

		criteria.setFirstResult(offset);
		criteria.setFetchSize(limit);

		for (String field : orderby) {
			criteria.addOrder(Order.asc(field));
		}

	    for (String key: filters.keySet()) {
	    	Object strvalue = (Object) filters.get(key);
    		criteria.add(Restrictions.eq(key, strvalue));

    		/* if support is needed for multiple values in query - uncomment this
	    	String[] values = strvalue.split("|");
	    	
	    	if (values.length > 0) {
	    		for (String value : values) {
		    		criteria.add(Restrictions.eq(key, value));	    			
	    		}	    		
	    	} else {
	    	}
	    	*/
    		
	    }
				
		return criteria;		
	}
	
		
	/**
	 * Core method used for reading a DTO
	 * 
	 * @param id - objectUID for the Object to read
	 * @return DTO 
	 */
	public DTO readObject(String id) {
		DTO dto = null;
		
		M model = readModelWithExternalId(id);
		
		if (model != null) {
			dto = this.toDTO(model);
		}
		
		if (model == null) {
    		throw new GenericValidationException("Unable to read: %s with UID: %s", dtoName, id);	    								
		}
		    	
		return dto;		
	}

	
	
	/**
	 * Support method used for reading a POJO or Model
	 * 
	 * @param id - objectUID for reading the model
	 * @return POJO with the Id
	 */
	public M readModelWithExternalId(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>(); 
		map.put("externalId", id);

		return readModelWithExternalId(map);
	}
	

	/**
	 * Support method used for reading a POJO or Model when the POJO has composite keys
	 * 
	 * Note that this method expects a NamedQuery that is named as <dtoName>.findByExternalId to be defined in
	 * AkatanaNamedQueries.java. 
	 * 
	 * For example if the DTOclazz name was ProductDTO, the dtoName will be Product. The named query should be called Product.findByExternalId
	 * and the query should expect the externalId as a named parameter that is passed in by default
	 * 
	 * @param params - map holding the composite key and value pairs
	 * @return POJO with the composite Id
	 */
	public M readModelWithExternalId(HashMap<String, Object> params) {
		M model = null;

		// execute named query using externalId
    	GenericDao<M, PK> modeldao =  getDao();    	

    	String queryname =  dtoName + ".findByExternalId";
    	
    	List<M> modellist = modeldao.findWithNamedQuery(queryname,params);
		
    	if (modellist.size() > 0) {
    		model = modellist.get(0);
    	}

    	return model;
	}
	

	/**
	 * Support method used for reading a POJO or Model when the POJO has composite keys
	 * 
	 * @param modelPK
	 * @return POJO with the modelPK
	 */
	public M readModelWithPK(PK modelPK) {
		M model = getDao().get(modelPK);
		return model;
	}
	
	
	/**
	 * Support method used for searching a POJO or Model 
	 * 
	 * @param model
	 * @return a list of objects.
	 */
	public List<M> findByExample(M exampleInstance) {
		return getDao().findByExample(exampleInstance);
	}

	
	
	
	/**
	 * Optional method used by derived classes to customize before an action is performed
	 * 
	 * This method can be used in lieu of database triggers to perform some actions prior to a particular action
	 * 
	 * @param action - a String representing the action - ACTION_SAVE (save), ACTION_UPDATE (update), ACTION_REMOVE (remove). Defined as constants in AbstractBo
	 * @param dto - DTO on which the action is being performed
	 * @param model - POJO on which the action is being performed
	 * @return model - POJO which may have been modified by the method
	 */
	protected M beforeAction(String action, DTO dto, M model) {
		return model;
	}

	
	/**
	 * Optional method used by derived classes to customize after an action is performed
	 * 
	 * This method can be used in lieu of database triggers to perform some actions after a particular action
	 * 
	 * @param action - a String representing the action - ACTION_SAVE (save), ACTION_UPDATE (update), ACTION_REMOVE (remove). Defined as constants in AbstractBo
	 * @param dto - DTO on which the action is being performed
	 * @param model - POJO on which the action is being performed
	 * @return model - POJO which may have been modified by the method
	 */
	protected M afterAction(String action, DTO dto, M model) {
		return model;
	}
		
	
	/**
	 * Core method used to save a DTO
	 * 
	 * If the DTO implements IExternallyIdentifiableDTO, then this method will insert or update based on existence of the Id. Also if the object is marked for delete - then the object will be deleted 
	 * Otherwise, this method will always call insert on the object
	 * 
	 * @param obj object to be saved
	 * @return DTO
	 */
	public DTO saveObject(DTO obj) {

		if (obj instanceof IExternallyIdentifiableDTO) {		
			String id = ((IExternallyIdentifiableDTO) obj).getObjectUID();

			return updateOrSaveObject(id, obj, ACTION_SAVE);			
		} else {					
			// validate the DTO for additional constraints
			validateDTO(obj);
			
			// convert dto to model
			M model = this.toModel(obj);
	
	    	model = beforeAction(ACTION_SAVE, obj, model);
					
			model = saveModel(model);
	
	    	model = afterAction(ACTION_SAVE, obj, model);
		
	    	// create response and return
	    	DTO retobj = this.toDTO(model);
	    	
			return retobj;    	
		}
	}
	
	
	/**
	 * Support method used to save a model
	 * 
	 * @param model - POJO object
	 * @return POJO object with the updated primary key
	 */
	public M saveModel(M model) {
		try{										
			
	    	GenericDao<M, PK> modeldao =  getDao();    	
		    	
			// execute save method
	    	PK pk = modeldao.save(model);
	    	
	    	if (pk == null) {	    		
	    		throw new GenericValidationException("Unable to create: %s", this.getDtoName());	    		
	    	} 

	    	// force execution for any constraint exceptions
	    	modeldao.flush();

	    	
	    	setId(model, pk);	    	
	    	
	    } 
		catch(ConstraintViolationException e){			
			LOGGER.debug("error updating object", e);
    		throw new GenericValidationException("Unable to update: %s - %s - %s", dtoName, e.getMessage(), e.getSQLException() != null ? e.getSQLException().getMessage() : "");	    					
    	} 									
		catch(RuntimeException e){			
			LOGGER.debug("error creating object", e);
    		throw new GenericValidationException("Unable to create: %s - %s", dtoName, e.getMessage());	    					
    	} 		

    	return model;
	}


	
	/**
	 * Core method used to sync a list of DTO objects
	 * 
	 * If the DTO implements IExternallyIdentifiableDTO, then this method will insert or update based on existence of the Id. Also if the object is marked for delete - then the object will be deleted 
	 * The method will return AktanaSyncResponseObject
	 * 
	 * @param obj object to be synced
	 * @return DTO
	 */
	public AktanaSyncResponseDTO syncObject(AktanaSyncDTO<DTO> syncobj) {		
		AktanaSyncResponseDTO resp = new AktanaSyncResponseDTO();
		resp.setStatus(1);
		resp.setMessage("");
		List<DTO> objlist = syncobj.getList();

		// first process deletes		
		if (syncobj.getDelete() != null) {
			for (String delid : syncobj.getDelete()) {
				try {
					removeObject(delid);			

					resp.setDeletes(resp.getDeletes() + 1);					
				} catch (Exception e) {
					resp.setStatus(0);
					String message = resp.getMessage() + " " + e.getMessage();
					resp.setMessage(message);
					throw e;
				}	
			}
		}
		
		
		for (DTO obj : objlist) {
			try {
				saveObject(obj);
				
				resp.setUpdates(resp.getUpdates() + 1);
			} catch (Exception e) {
				resp.setStatus(0);
				String message = resp.getMessage() + " " + e.getMessage();
				resp.setMessage(message);
				throw e;
			}			
		}
		
		
		return resp;
	}
	
	
	/**
	 * Core method used to save a DTO
	 * 
	 * This will always call updateOrSaveObject method which will create the object if it does not exist or update if it exists. 
	 * If the object is marked for delete - then the object will be deleted 
	 * 
	 * @param id - objectUID of the object
	 * @param obj - object to update
	 * @return - updated object
	 */
	public DTO updateObject(String id, DTO obj) {
		return updateOrSaveObject(id, obj, ACTION_UPDATE );
	}

	
	
	/**
	 * Optional method used to update a Model
	 * 
	 * This will always call updateOrSaveModel method which will create the object if it does not exist or update if it exists. 
	 * If the object is marked for delete - then the object will be deleted 
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	public M updateModel(String id, M model) {
		return updateOrSaveModel(id, model, ACTION_UPDATE);
	}

	
	
	/**
	 * Internal method that performs updateOrSave operation depending on whether the objectUID exists or not
	 * In addition if the object is marked for Deletion (isDeletetable = true) then the object will be removed
	 * 
	 * @param id - objectUID for the object
	 * @param obj - DTO to update
	 * @param actionId - ACTION_SAVE or ACTION_UPDATE depending on whether this was called from saveObject or updateObject
	 * 
	 * @return - updated/inserted object
	 */
	private DTO updateOrSaveObject(String id, DTO obj, String actionId) {
		// validate the DTO for constraints
		validateDTO(obj);
		
		// convert dto to model
		M model = this.toModel(obj);

    	model = beforeAction(actionId, obj, model);
				
		model = updateOrSaveModel(id, model, actionId);
		
    	model = afterAction(actionId, obj, model);
    	    	
    	if (obj instanceof IMarkableForDeleteDTO) {
    		
    		if (((IMarkableForDeleteDTO) obj).getIsDeleted()) {
    			
    			LOGGER.warn("Intended action = {}. However, the {} exists has been marked for delete. Performing ACTION_REMOVE for {}", actionId, this.getDtoName(), id);
    			    							
    			DTO delobj = this.removeObject(id);
    			    	    	
    		}
    	}
		
    	DTO retobj = this.toDTO(model);
    	
		return retobj;		
	}

	
	/**
	 * Internal method that performs updateOrSave operation on the Model depending on whether the objectUID exists or not
	 * In addition if the object is marked for Deletion (isDeletetable = true) then the object will be removed
	 * 
	 * @param id - objectUID for the object
	 * @param obj - POJO to update
	 * @param actionId - ACTION_SAVE or ACTION_UPDATE depending on whether this was called from saveObject or updateObject
	 * 
	 * @return - updated/inserted object
	 */
	private M updateOrSaveModel(String id, M model, String intendedAction) {
		try{												
	    	GenericDao<M, PK> modeldao =  getDao();    	
	
	    	M existingobj = readModelWithExternalId(id);
	
	    	if (existingobj != null) {

	    		if (intendedAction.equalsIgnoreCase(ACTION_SAVE)) {
	    			LOGGER.warn("Intended action = ACTION_SAVE. However, the {} exists. Performing ACTION_UPDATE for {}", this.getDtoName(), id);
	    		}
	    			    		
		    	transferId(model, existingobj);		    	
		    	
				// execute merge method
		    	modeldao.merge(model);

		    	// force execution for any constraint exceptions
		    	modeldao.flush();
	    	} else {	    		
	    		if (intendedAction.equalsIgnoreCase(ACTION_UPDATE)) {
	    			LOGGER.warn("Intended action = ACTION_UPDATE. However, the {} does not exist. Performing ACTION_SAVE for {}", this.getDtoName(), id);
	    		}
	    		
	        	PK pk = modeldao.save(model);    			        	

		    	// force execution for any constraint exceptions	        	
		    	modeldao.flush();
		    	
	        	if (pk == null) {	    		
		    		throw new GenericValidationException("Unable to create: %s", dtoName);	    		
		    	} 	        	
		    	
		    	setId(model, pk);
	    	}
	    		    	
	    } 		
		catch(ConstraintViolationException e){			
			LOGGER.debug("error updating object", e);
    		throw new GenericValidationException("Unable to update: %s - %s - %s", dtoName, e.getMessage(), e.getSQLException() != null ? e.getSQLException().getMessage() : "");	    					
    	} 					
		catch(RuntimeException e){			
			LOGGER.debug("error updating object", e);
    		throw new GenericValidationException("Unable to update: %s - %s", dtoName, e.getMessage());	    					
    	} 			
		
		return model;		
	}

	
	/**
	 * Core method to remove an object - it will perform a softdelete or harddelete depending on interface implemented by the corresponding Model
	 * 
	 * @param id - objectUID for object
	 * @return DTO that was deleted
	 */
	public DTO removeObject(String id) {
		DTO dto = null;
				
		M existingobj = removeModel(id);
		
		if (existingobj != null) {
			dto = this.toDTO(existingobj);
		}
		
		existingobj = afterAction(ACTION_REMOVE, dto, existingobj);
    	    	
		return dto;		
	}

	
	/**
	 * Support method to remove a model - it will perform a softdelete or hardelete depending on if the model implements ISoftDeletable interface
	 * 
	 * @param id - objectUID to remove
	 * @return POJO that was deleted
	 */
	public M removeModel(String id) {
		try{															
	    	GenericDao<M, PK> modeldao =  getDao();    	
	
	    	M existingobj = readModelWithExternalId(id);

	    	if (existingobj == null) {	    		
	    		throw new GenericValidationException("No %s found with id %s", dtoName, id);	    		
	    	}
	    	
			existingobj = beforeAction(ACTION_REMOVE, null, existingobj);
	    		    	
	    	// conditional actions, based on whether the model object is subject to "soft-delete" or not

	    	if (existingobj instanceof ISoftDeletable) {
	    		// perform SOFT delete
	    		
				// set the "isDeleted" flag = true		    	
		    	setSoftDeletedFlag(existingobj);
				
				// execute merge method
		    	modeldao.merge(existingobj);

		    	// force execution for any constraint exceptions
		    	modeldao.flush();
	    	}
	    	else {
	    		// perform HARD delete
	    		modeldao.delete(existingobj);
	    		
	    		modeldao.flush();
	    	}
	    	
	    	return existingobj;
		} 
		catch(ConstraintViolationException e){			
			LOGGER.debug("error updating object", e);
    		throw new GenericValidationException("Unable to update: %s - %s - %s", dtoName, e.getMessage(), e.getSQLException() != null ? e.getSQLException().getMessage() : "");	    					
    	} 							
		catch(RuntimeException e){			
			LOGGER.debug("error deleting product", e);
    		throw new GenericValidationException("Unable to delete: %s.  Cause: %s", dtoName, e.getMessage());	    					
    	} 		
	}


	/**
	 * Core method to hard remove an object - it will perform a harddelete
	 * 
	 * @param id - objectUID for object
	 * @return DTO that was deleted
	 */
	public DTO hardRemoveObject(String id) {
		DTO dto = null;
				
		M existingobj = hardRemoveModel(id);
		
		if (existingobj != null) {
			dto = this.toDTO(existingobj);
		}
		
		existingobj = afterAction(ACTION_REMOVE, dto, existingobj);
    	    	
		return dto;		
	}

	
	/**
	 * Support method to hard remove a model - it will ignore softdeletable interface
	 * 
	 * @param id - objectUID to remove
	 * @return POJO that was deleted
	 */
	public M hardRemoveModel(String id) {
		try{															
	    	GenericDao<M, PK> modeldao =  getDao();    	
	
	    	M existingobj = readModelWithExternalId(id);

	    	if (existingobj == null) {	    		
	    		throw new GenericValidationException("No %s found with id %s", dtoName, id);	    		
	    	}
	    	
			existingobj = beforeAction(ACTION_REMOVE, null, existingobj);
	    		    	
    		// perform HARD delete
    		modeldao.delete(existingobj);
    		
    		modeldao.flush();
	    	
	    	return existingobj;
		} 
		catch(ConstraintViolationException e){			
			LOGGER.debug("error updating object", e);
    		throw new GenericValidationException("Unable to update: %s - %s - %s", dtoName, e.getMessage(), e.getSQLException() != null ? e.getSQLException().getMessage() : "");	    					
    	} 							
		catch(RuntimeException e){			
			LOGGER.debug("error deleting product", e);
    		throw new GenericValidationException("Unable to delete: %s.  Cause: %s", dtoName, e.getMessage());	    					
    	} 		
	}
	
	
	
	/**
	 * This method provides calls the provided instance of AbstractMapperSupport.toPojo(dto)
	 * to do the basic conversion, and then calls toPojoAdditionalMappings(dto,pojo) to
	 * perform any additional mapping actions that have been specified in the
	 * concrete sub-class's override of that method.
	 * 
	 * Note: this method is marked "final" to prevent overriding, and hence all
	 * customized behavior is limited to what can be done by toPojoAdditionalMappings
	 */
	protected final M toModel(DTO dto) {
		M model = null;
		
		if (this.getConverter() != null) {
			model = this.getConverter().toPojo(dto);
		}
		
		return toModelAdditionalMappings(dto, model);
	}
	
	/**
	 * This method is provided as the means to be able to customize the behavior of
	 * the toPojo(DTO) method.  This default implementation simply returns the 
	 * specified M model object..
	 * 
	 * NOTE: when overriding this method, DO NOT modify the DTO input argument.
	 */
	protected M toModelAdditionalMappings(DTO dto, M model) {
		return model;
	}

	/**
	 * This method provides calls the provided instance of AbstractMapperSupport.toDTO(model)
	 * to do the basic conversion, and then calls toDTOAdditionalMappings(dto,pojo) to
	 * perform any additional mapping actions that have been specified in the
	 * concrete sub-class's override of that method.
	 * 
	 * Note: this method is marked "final" to prevent overriding, and hence all
	 * customized behavior is limited to what can be done by toPojoAdditionalMappings
	 */
	public final DTO toDTO(M model) {
		DTO dto = null;
		if (this.getConverter() != null) {		
			dto = this.getConverter().toDTO(model);
		}
		
		return toDTOAdditionalMappings(model, dto);
	}
	
	/**
	 * This method is provided as the means to be able to customize the behavior of
	 * the toPojo(DTO) method.  This default implementation simply returns the 
	 * specified DTO object.
	 * 
	 * NOTE: when overriding this method, DO NOT modify the M model input argument.
	 */
	protected DTO toDTOAdditionalMappings(M model, DTO dto) {
		return dto;
	}

	
	
	/**
	 * Optional method that can be implemented by derived classes to perform additional validation
	 * 
	 * @param dto
	 */
	protected void validateDTO(DTO dto) {

	}

	/**
	 * Method to transfer values from the oldModel to the newModel
	 * 
	 * At a minimum the Id and CreatedAt should be transfered from oldModel to newModel such that it can be preserved
	 * 
	 * @param newModel
	 * @param oldModel
	 */
	protected abstract void transferId(M newModel, M oldModel);
	
	
	/**
	 * Method to assign the Id to the model
	 * 
	 * At a minimum the Id should be set in the model to be returned 
	 * @param newModel
	 * @param pk
	 */
	protected abstract void setId(M newModel, PK pk);
	
	/*
	 * If the model class has the Boolean "isDeleted" attribute and implements the
	 * ISoftDeletable marker interface, then this implementation should sufficient.
	 * 
	 * If, on the other hand, the model has a non-standard "soft-delete" flag of some type,
	 * the concrete Bo sub-class will need to override this method.
	 * 
	 * Examples of non-standard soft-delete flags that exist in /versiontwo code:
	 *   Visit has "status" attribute, one value of which corresponds to "removed"
	 *   VisitPattern has "removedFlag" attribute (a left-over from Aktana V1 conventions)
	 *   Action has BOTH the older "removedFlag" and the newer "isDeleted" flag; 
	 *   	both must be maintained in V2 schema, because many V2 DAO queries are still testing the old "removedFlag"
	 */
	protected void setSoftDeletedFlag(M model) {
		if (model instanceof ISoftDeletable) {
			((ISoftDeletable)model).setIsDeleted(true);
		}
	}
	
	
	/** utility method to convert an object into a JSON string */
	protected String toJSONString(Object obj) {
		String json = null;

		try {
			json = mapper.writeValueAsString(obj);
        } catch (Exception e) {
        	LOGGER.error("error converting object list",e);
		}

		return json;
	}
	

	/* is this a mock interface */
	public Boolean getIsParameterAPIMock() {
		Boolean mock = SysParameterBo.getBooleanValue("partnerAPIMock");
		if (mock == null) {
			mock = false;
		}
		return mock;
	}


	/* the version of API that is currently being invoked. Automatically set by the Resource based on path of url */
	public String getApiVersion() {
		return apiVersion;
	}


	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	
	protected Method getConfiguredMethodOverride(String method_name) {
		for (String key : configuredMethods.keySet()) {
			List<Method> methods = configuredMethods.get(key);			
			String search_name = method_name + "_" + key;

			for (Method m : methods) {
				if (m.getName().equalsIgnoreCase(search_name)) {
					return m;
				}
			}						
		}
		
		return null;		
	}

	/*
	protected void invokeAfterActionConfiguredMethod(String action,  DTO dto, M model) {	
		invokeAllActionConfiguredMethods("afteraction_akt", action, dto, model);
	}	

	protected void invokeBeforeActionConfiguredMethod(String action,  DTO dto, M model) {	
		invokeAllActionConfiguredMethods("beforeaction_akt", action, dto, model);
	}	
		
	private void invokeAllActionConfiguredMethods(String action_type, String action, DTO dto, M model) {	
		if (configuredMethods.get(action_type) != null) {
			List<Method> actionMethods = configuredMethods.get(action_type);
			for (Method m : actionMethods) {
				invokeActionConfiguredMethod(m, action_type, action, dto, model);
			}
		}		
	}	
	*/
	
	protected void invokeActionConfiguredMethod(Method m, String action_type, String action, DTO dto, M model) {	
		try {
			m.invoke(this, action, dto, model);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
        	LOGGER.error("error invoking " + action_type + " configured method",e);				
    		throw new GenericValidationException("error invoking " +  action_type + " configured method");	    			        	
		}				
	}	
	

	/*
	protected void invokeAllValidateConfiguredMethod(DTO dto) {	
		String action_type = "validate_akt";
		if (configuredMethods.get(action_type) != null) {
			List<Method> actionMethods = configuredMethods.get(action_type);
			for (Method m : actionMethods) {
				invokeValidateConfiguredMethod(m, dto);
			}
		}		
	}	
	*/
	
	protected void invokeValidateConfiguredMethod(Method m, DTO dto) {	
		String action_type = "validate_akt";		
		try {
			m.invoke(this, dto);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
        	LOGGER.error("error invoking " + action_type + " configured method",e);				
    		throw new GenericValidationException("error invoking " +  action_type + " configured method");	    			        	
		}				
	}

	/*
	protected void invokeAllDeriveConfiguredMethod(DTO dto, M model) {	
		String action_type = "derive_akt";
		if (configuredMethods.get(action_type) != null) {
			List<Method> actionMethods = configuredMethods.get(action_type);
			for (Method m : actionMethods) {
				invokeDeriveConfiguredMethod(m, dto, model);
			}
		}		
	}	
	*/
	
	protected void invokeDeriveConfiguredMethod(Method m, DTO dto, M model) {	
		String action_type = "derive_akt";
		try {
			m.invoke(this, dto, model);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
        	LOGGER.error("error invoking " + action_type + " configured method",e);				
    		throw new GenericValidationException("error invoking " +  action_type + " configured method");	    			        	
		}				
	}
	
	private HashMap<String, List<Method>> findConfiguredMethods(Class bo) {
		HashMap<String, List<Method>> methods = new HashMap<String, List<Method>>();
		String[] method_types = {"validate_akt", "derive_akt", "beforeaction_akt", "afteraction_akt"};
		 				
		Method[] allmethods = bo.getDeclaredMethods();		
		if (allmethods != null) {
			for (int i=0;i<allmethods.length;++i) {
				Method method = allmethods[i];

				for (String type : method_types) {
					if (method.getName().endsWith(type)) {
						List<Method> accumulated_methods = methods.get(type);						
						if (accumulated_methods == null) {
							accumulated_methods = new ArrayList<Method>();			
							methods.put(type, accumulated_methods);
						}
						accumulated_methods.add(method);
						break;
					}					
				}				
			}			
		}
		
		return methods;
	}
	
	
	  private static Set<Class<?>> getSimpleTypes()
	  {
	        Set<Class<?>> ret = new HashSet<Class<?>>();
	        ret.add(Boolean.class);
	        ret.add(Character.class);
	        ret.add(Byte.class);
	        ret.add(Short.class);
	        ret.add(Integer.class);
	        ret.add(Long.class);
	        ret.add(Float.class);
	        ret.add(Double.class);
	        ret.add(String.class);	        
	        ret.add(Date.class);
	        
	        
	        return ret;
	  }

	  public static boolean isSimpleType(Class<?> clazz)
	  {
	        return SIMPLE_TYPES.contains(clazz);
	  }

	  
	

	  public void validateID(String id) {
		  if(id == null || id.length() < 1) {
			  throw new GenericValidationException("ID not set"); 
		  }
		  
		  if(!id.matches("[a-zA-Z0-9_/-/~]+")) {
			  throw new GenericValidationException("ID contain illegal charecter at "+id);
		  }
	  }
	  
	  public static int getOffset(Integer page, Integer rows) {
		  if ((page != null || rows != null) && (page > 0 && rows > 0)) {
			  return (page - 1) * rows;
		  }
		  return 0; 
	  }
		 
	  public static int getLimit(Integer page, Integer rows) {
		  if ((page != null || rows != null) && (page > 0 && rows > 0)) {
			  return rows;
		  }
		  return 100;		 
	  }
    
	  public String convertDateTimeToString(Date date) { return ISODateTimeFormat.dateTimeNoMillis().print(new DateTime(date)); }

	  public S3PartialPaths getPartialPaths(String pathType) {
		  GenericDao<S3PartialPaths, String> s3PartialPathsDao = new GenericDaoImpl<S3PartialPaths, String>(S3PartialPaths.class);
		  s3PartialPathsDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		  HashMap<String, Object> pathMap = new HashMap<String, Object>();
		  pathMap.put("pathType", pathType);	
		  String namedQueryName = "S3PartialPaths.custom.findS3PartialPathsWithPathType";
		  List<S3PartialPaths> s3PartialPaths = s3PartialPathsDao.findWithNamedQuery(namedQueryName, pathMap);	
		  if(s3PartialPaths.size() < 1) throw new GenericValidationException("File type %s does not exists in S3PartialPaths table", pathType);
		  return s3PartialPaths.get(0); // cannot be more than 1, pathType is a pk of the table
	  }
}
