/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import ma.glasnost.orika.MapperFactory;

import com.aktana.learning.api.dto.useradmin.LearningUserDTO;
import com.aktana.learning.persistence.models.impl.AktanaUser;


public class LearningUserMapper extends AbstractMapperSupport<AktanaUser, LearningUserDTO>
{
	public LearningUserMapper()
	{
		super();
	}


    private static LearningUserMapper instance;
    
    public static LearningUserMapper getInstance()  {

		synchronized(LearningUserMapper.class) {
			if (instance == null) {
				instance = new LearningUserMapper();
			}
		}
    	
    	return instance;    	
    }
	
	
	@Override
	protected void initializeMappingsToDTO(MapperFactory factory)
	{
		factory.registerClassMap(factory
				.classMap(AktanaUser.class, LearningUserDTO.class)
				.field("id", "userId")
				.byDefault()
				.toClassMap());
	}

	@Override
	public Class<?> getDTOClass() {
		return LearningUserDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return AktanaUser.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) 
	{
		factory.registerClassMap(factory
				.classMap(LearningUserDTO.class, AktanaUser.class)
				.field("userId", "id")
				.byDefault()
				.toClassMap());
	}
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(AktanaUser pojo, LearningUserDTO dto)
	{
		dto.setPassword("");
	}
	
	

}
