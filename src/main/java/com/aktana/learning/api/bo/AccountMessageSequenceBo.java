/*****************************************************************
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.HashMap;
import java.util.List;  

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.persistence.models.impl.AccountMessageSequence;
import com.aktana.learning.persistence.models.impl.AccountMessageSequencePK;
import com.aktana.learning.api.bo.mapper.AccountMessageSequenceMapper;
import com.aktana.learning.api.dto.AccountMessageSequenceDTO;
import com.aktana.learning.common.exception.GenericValidationException;


/** BO class for RepAccount */
public class AccountMessageSequenceBo extends AbstractBo<AccountMessageSequenceDTO, AccountMessageSequence, AccountMessageSequencePK> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public AccountMessageSequenceBo() {
		super(AccountMessageSequenceBo.class, AccountMessageSequenceDTO.class, AccountMessageSequence.class, AccountMessageSequenceMapper.getInstance());

		
		LOGGER = LoggerFactory.getLogger(this.getClass());		
	}
	
	/** common method for listing a resource */
	public List<AccountMessageSequenceDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}

	/** common method for reading a resource */
	public AccountMessageSequenceDTO readObject(String id) {
		return super.readObject(id);
	}

	
	/** common method to save a resource */
	public AccountMessageSequenceDTO saveObject(AccountMessageSequenceDTO obj) {
		return super.saveObject(obj);
	}

	
	/** common method to update a resource */
	public AccountMessageSequenceDTO updateObject(String id, AccountMessageSequenceDTO obj) {
		return super.updateObject(id,  obj);
	}

		
	/** common method to remove a resource */
	public AccountMessageSequenceDTO removeObject(String id) {
		return super.removeObject(id);
	}
	
	
	@Override
	protected void transferId(AccountMessageSequence newModel, AccountMessageSequence oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}

	// setSoftDeletedFlag(User): no need to override

	@Override
	protected void setId(AccountMessageSequence model, AccountMessageSequencePK pk) {
		model.setId(pk);
	}
	





	


	
	
	/* helper method to read existing model object based on the composite ids */
	@Override
	public AccountMessageSequence readModelWithExternalId(String id) {
		HashMap<String, Object> map = parseAndValidateObjectUID(id);
				
		return super.readModelWithExternalId(map);		
	}

	


	private HashMap<String, Object> parseAndValidateObjectUID(String id) {
		String[] subkeys = id.split("\\~");
		HashMap<String, Object> map = new HashMap<String, Object>();
		

		if (subkeys.length != 3) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		map.put("accountUID", subkeys[0]);
		map.put("messageAlgorithmUID", subkeys[1]);
		map.put("messageUID", subkeys[2]);
		
		return map;
	}

	
	private HashMap<String, Object> parseAndValidateObjectUIDPattern(String id) {
		String[] subkeys = id.split("\\~", -1);
		HashMap<String, Object> map = new HashMap<String, Object>();

		if (subkeys.length != 3) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		if (!subkeys[0].isEmpty()) {
			map.put("accountUID", subkeys[0]);
		}
		
		if (!subkeys[1].isEmpty()) {
			map.put("messageAlgorithmUID", subkeys[1]);
		}

		if (!subkeys[2].isEmpty()) {
			map.put("messageUID", subkeys[2]);
		}


			
		return map;		
	}
	

	
}
