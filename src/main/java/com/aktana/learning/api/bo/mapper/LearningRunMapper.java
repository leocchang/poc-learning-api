/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.LearningRunDTO;
import com.aktana.learning.persistence.models.impl.LearningRun;

import ma.glasnost.orika.MapperFactory;


public class LearningRunMapper extends AbstractMapperSupport<LearningRun, LearningRunDTO>
{
    public LearningRunMapper()
    {
        super();
    }
    
    private static LearningRunMapper instance;
    
    public static LearningRunMapper getInstance()  {

		synchronized(LearningRunMapper.class) {
			if (instance == null) {
				instance = new LearningRunMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningRun.class, LearningRunDTO.class)
                .field("id", "learningRunUID")
                .field("learningBuild.id", "learningBuildUID")
                .field("runType", "runType")
                .field("executionStatus", "executionStatus")
                .field("executionDatetime", "executionDatetime")
                .field("isPublished", "isPublished")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningRunDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningRun.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningRunDTO.class, LearningRun.class)
                .field("learningRunUID", "id")
                .field("learningBuildUID", "learningBuild.id")
                .field("runType", "runType")
                .field("executionStatus", "executionStatus")
                .field("executionDatetime", "executionDatetime")
                .field("isPublished", "isPublished")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningRunDTO src, LearningRun tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningRun src, LearningRunDTO tgt)
	{
	
	}

}