/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.SimulationSegmentMapper;
import com.aktana.learning.api.dto.SimulationSegmentDTO;
import com.aktana.learning.api.dto.SimulationAccountSegmentDTO;
import com.aktana.learning.api.dto.SimulationSegmentMessageSequenceDTO;
import com.aktana.learning.api.dto.SimulationSegmentSequenceInfoDTO;
import com.aktana.learning.api.dto.SimulationAccountMessageSequenceDTO;
import com.aktana.learning.api.dto.SimulationAccountSequenceInfoDTO;
import com.aktana.learning.api.dto.MessageInfoDTO;
import com.aktana.learning.api.dto.SimulationAccountDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.SimulationSegment;
import com.aktana.learning.persistence.models.impl.SimulationAccountSegment;
import com.aktana.learning.persistence.models.impl.SimulationSegmentMessageSequence;
import com.aktana.learning.persistence.models.impl.SimulationAccountMessageSequence;
import com.aktana.learning.persistence.models.impl.SimulationMessageSequence;
import com.aktana.learning.persistence.models.impl.SimulationAccountSentEmail;
import com.aktana.learning.persistence.models.impl.ApprovedDocuments;
import com.aktana.learning.persistence.models.impl.LearningVersion;
import com.aktana.learning.api.bo.LearningVersionBo;




/** Base class for LearningRun */
public class LearningSimulationBo extends AbstractBo<SimulationSegmentDTO, SimulationSegment, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningBuild, String> learningBuildDao = null;
    private GenericDao<LearningRun, String> learningRunDao = null;
    private GenericDao<SimulationAccountSegment, String> simulationAccountSegmentDao = null;
    private GenericDao<SimulationSegmentMessageSequence, String> simulationSegmentMessageSequenceDao = null;
    private GenericDao<SimulationAccountMessageSequence, String> simulationAccountMessageSequenceDao = null;
    private GenericDao<SimulationMessageSequence, String> simulationMessageSequenceDao = null;
    private GenericDao<SimulationAccountSentEmail, String> simulationAccountSentEmailDao = null;
    private GenericDao<ApprovedDocuments, String> approvedDocumentsDao = null;

    
    /** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningSimulationBo() {
		super(LearningSimulationBo.class, SimulationSegmentDTO.class, SimulationSegment.class, SimulationSegmentMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());

		learningBuildDao = new GenericDaoImpl<LearningBuild, String>(LearningBuild.class);
		learningRunDao = new GenericDaoImpl<LearningRun, String>(LearningRun.class);
		simulationAccountSegmentDao = new GenericDaoImpl<SimulationAccountSegment, String>(SimulationAccountSegment.class);
		simulationSegmentMessageSequenceDao = new GenericDaoImpl<SimulationSegmentMessageSequence, String>(SimulationSegmentMessageSequence.class);
		simulationAccountMessageSequenceDao = new GenericDaoImpl<SimulationAccountMessageSequence, String>(SimulationAccountMessageSequence.class);
		simulationMessageSequenceDao = new GenericDaoImpl<SimulationMessageSequence, String>(SimulationMessageSequence.class);
		simulationAccountSentEmailDao = new GenericDaoImpl<SimulationAccountSentEmail, String>(SimulationAccountSentEmail.class);
		approvedDocumentsDao = new GenericDaoImpl<ApprovedDocuments, String>(ApprovedDocuments.class);

	}

	private GenericDao<LearningBuild, String> getLearningBuildDao() {
		learningBuildDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningBuildDao;
	}
	
	private GenericDao<LearningRun, String> getLearningRunDao() {
		learningRunDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningRunDao;
	}
	
	private GenericDao<SimulationAccountSegment, String> getSimulationAccountSegmentDao() {
		simulationAccountSegmentDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return simulationAccountSegmentDao;
	}
	
	private GenericDao<SimulationSegmentMessageSequence, String> getSimulationSegmentMessageSequenceDao() {
		simulationSegmentMessageSequenceDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return simulationSegmentMessageSequenceDao;
	}
	
	private GenericDao<SimulationAccountMessageSequence, String> getSimulationAccountMessageSequenceDao() {
		simulationAccountMessageSequenceDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return simulationAccountMessageSequenceDao;
	}
	
	private GenericDao<SimulationMessageSequence, String> getSimulationMessageSequenceDao() {
		simulationMessageSequenceDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return simulationMessageSequenceDao;
	}

	private GenericDao<SimulationAccountSentEmail, String> getSimulationAccountSentEmailDao() {
		simulationAccountSentEmailDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return simulationAccountSentEmailDao;
	}	
	
	private GenericDao<ApprovedDocuments, String> getApprovedDocumentsDao() {
		approvedDocumentsDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return approvedDocumentsDao;
	}	


	/** common method for listing a resource */
	@Override
	public List<SimulationSegmentDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** method to check that a learningBuildUID exists in learningBuild table */
	private LearningBuild validateBuildID (String learningBuildUID) {
	
		if(learningBuildUID == null || learningBuildUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningBuildUID %s", learningBuildUID);	    								    								
		}
		
    	LearningBuild learningBuild = getLearningBuildDao().get(learningBuildUID);
    	if (learningBuild == null) {
        	throw new GenericValidationException("Invalid learningBuildUID %s in LearningBuild", learningBuildUID);
    	}
		return learningBuild;
	}
	
	/** method to check that a segmentUID exists in SimulationSegment table */
	private void validateSegmentID(String segmentUID) {
		if(segmentUID == null || segmentUID.isEmpty()) {
			throw new GenericValidationException("Invalid segmentUID %s", segmentUID);	    								    								
		}

		if(this.readModelWithExternalId(segmentUID) == null) {
			throw new GenericValidationException("Invalid segment %s in SimulationSegment", segmentUID);
		}
	}
	
	/** common method to return latest successful learningRunUID given learningBuildUID */
	private String getLatestSuccessfulRun(LearningBuild learningBuild) {	
		HashMap<String, Object> learningBuildMap = new HashMap<String, Object>();
		learningBuildMap.put("learningBuildUID",learningBuild.getId());
		learningBuildMap.put("executionStatus", "success" );
		learningBuildMap.put("runType", "SIMULATION");
		
		String namedQueryName = "LearningRun.custom.findRunByBuildAndStatusAndRunTypeOrderByDate";
		
		List<LearningRun> learningRuns = getLearningRunDao().findWithNamedQuery(namedQueryName, learningBuildMap);
		
		if (learningRuns.size() < 1) { // no successful simulation learning runs
			learningBuildMap.put("executionStatus", "running" );
			learningRuns = getLearningRunDao().findWithNamedQuery(namedQueryName, learningBuildMap);
					
			if(learningRuns.size() < 1) { // no running simulations either 			
				learningBuildMap.put("executionStatus", "failure" );
				learningRuns = getLearningRunDao().findWithNamedQuery(namedQueryName, learningBuildMap);
				
				if(learningRuns.size() < 1) { // no failed simulations either (no simulations	
					LearningVersionBo learningVersionBo = new LearningVersionBo();
					LearningVersion learningVersion = learningVersionBo.readModelWithExternalId(learningBuild.getLearningVersion().getId());
					
					String errorMessage = "No simulation was found for the current version. \n";
					if(learningVersion.getIsDeployed()) errorMessage = errorMessage + "As the current version is deployed: the simulation is scheduled to be generated over the next date. \n";
					errorMessage = errorMessage + "You can request the simulation to be manually generated by providing the following code to ops: " + learningBuild.getId();
					throw new GenericValidationException(errorMessage);
				}			
				throw new GenericValidationException("Something went wrong. Simulation job failed");
			}
			throw new GenericValidationException("The simulation is running and an updated simulation should be available soon");
		}
		return learningRuns.get(0).getId();
	}
	
	/** method to return DTOs given IDs from `map` */
	private List<SimulationSegmentDTO> listSegmentsWithFilter(HashMap<String, Object> map, String namedQueryName) {	
		List<SimulationSegment> simulationSegments = getDao().findWithNamedQuery(namedQueryName, map);
		List<SimulationSegmentDTO> simulationSegmentDTOs = new ArrayList<SimulationSegmentDTO>();
		simulationSegmentDTOs = toDTO(simulationSegments);
		return simulationSegmentDTOs;
	}
	
	/** method for first api. Returns list of Segments given a learningBuild*/
	
	public List<SimulationSegmentDTO> listSegmentsWithBuildId(String learningBuildUID) {		
		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		String learningRunUID = getLatestSuccessfulRun(learningBuild);	
		
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	map.put("learningRunUID",learningRunUID);
		String namedQueryName = "SimulationSegment.custom.findByLearningRun";
		
		return listSegmentsWithFilter(map, namedQueryName);	
	}
	
	/** method for second api. Returns segment level data given a learningBuild and segment */
	public SimulationSegmentDTO getSegmentWithBuildIdAndSegmentId(String learningBuildUID, String segmentUID) {
		
		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		validateSegmentID(segmentUID);
		String learningRunUID = getLatestSuccessfulRun(learningBuild);	
		
    	HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);
		map.put("segmentUID", segmentUID);
		
		String namedQueryName = "SimulationSegment.custom.findByLearningRunAndSegment";
		List<SimulationSegmentDTO> simulationSegmentDTOs = listSegmentsWithFilter(map, namedQueryName);
		SimulationSegmentDTO simulationSegmentDTO = new SimulationSegmentDTO();
		if(simulationSegmentDTOs.size() < 1) {
			throw new GenericValidationException("Existing learningBuild segment pair (%s , %s) not found", learningBuildUID, segmentUID);
		}
		simulationSegmentDTO = simulationSegmentDTOs.get(0);
		return simulationSegmentDTO;
	}
	
	/** method for third api. Returns list of Account Segment data given a learningBuild and segment */
	public List<SimulationAccountSegmentDTO> listAccountSegmentsWithBuildIdAndSegmentId(String learningBuildUID, String segmentUID, String searchString, Integer page, Integer rows) {
		
		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		validateSegmentID(segmentUID);
		String learningRunUID = getLatestSuccessfulRun(learningBuild);	

		
		// validate limit and offset
		if(page == null || page <= 0 || rows == null || rows <= 0) {
			page = 1;
			rows = 100;
		}

		Integer offset = rows*(page-1);
		Integer limit = offset + rows;	
		
    	HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);
		map.put("segmentUID", segmentUID);
		
		String namedQueryName = null;
		
		if(searchString == null) { // no partial search string given
			namedQueryName = "SimulationAccountSegment.custom.findByLearningRunAndSegment";
		} else {
			map.put("accountName", searchString + "%"); // query for accountNames that begin with searchString
			namedQueryName = "SimulationAccountSegment.custom.findByLearningRunAndSegmentAndSearchString";
		}
		
		List<SimulationAccountSegment> simulationAccountSegments = getSimulationAccountSegmentDao().findWithNamedQuery(namedQueryName, map, offset, limit);
		
		
		if(simulationAccountSegments.size() < 1 && searchString == null) {
			throw new GenericValidationException("Existing learningBuild segment pair (%s , %s) not found", learningBuildUID, segmentUID);
		}
		if(simulationAccountSegments.size() < 1 && searchString != null) {
			throw new GenericValidationException("Existing learningBuild, segment, searchString pair (%s , %s, %s) not found", learningBuildUID, segmentUID, searchString);
		}
		
		List<SimulationAccountSegmentDTO> simulationAccountSegmentDTOs = new ArrayList<SimulationAccountSegmentDTO>();
		// convert to dto 
		for(SimulationAccountSegment sas : simulationAccountSegments) {
			SimulationAccountSegmentDTO dto = new SimulationAccountSegmentDTO();
	
			dto.setLearningBuildUID(learningBuildUID);
			dto.setLearningRunUID(learningRunUID);
			dto.setSegmentUID(segmentUID);
			dto.setAccountUID(sas.getId().getAccountUID());
			dto.setAccountName(sas.getAccountName());
			dto.setCreatedAt(dateTimeFormat.format(sas.getCreatedAt()));
			dto.setUpdatedAt(dateTimeFormat.format(sas.getUpdatedAt()));
			
			simulationAccountSegmentDTOs.add(dto);
		}
		return simulationAccountSegmentDTOs;
	}
	
	
	private String getApprovedDocumentsByMessageId(String messageUID) {
		
		HashMap<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("messageUID", messageUID);
		String namedQueryName = "ApprovedDocuments.custom.findByMessageUID";
		String messageName = getApprovedDocumentsDao().findWithNamedQuery(namedQueryName, messageMap).toString();
		return  messageName;
	}
	
	private List<ApprovedDocuments> getMessageInfoFromApprovedDocument(String messageUID) {
		HashMap<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("messageUID", messageUID);
		String namedQueryName = "ApprovedDocuments.custom.findMessageInfoByMessageUID";
		List<ApprovedDocuments> messageInfo= getApprovedDocumentsDao().findWithNamedQuery(namedQueryName, messageMap);
		return  messageInfo;
	}
	
	/** method for fourth api. returns sequence level data given a learningBuild and account */
	public SimulationAccountSequenceInfoDTO listSequenceWithBuildIdAndAccountId(String learningBuildUID, String accountUID) {

		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		if(accountUID == null || accountUID.isEmpty()) {
			throw new GenericValidationException("Invalid accountUID %s", accountUID);	    								    								
		}	
		
		String learningRunUID = getLatestSuccessfulRun(learningBuild);
			
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);
		map.put("accountUID", accountUID);		

		String namedQueryName = "SimulationAccountMessageSequence.custom.findByLearningRunAndAccount";
		List<SimulationAccountMessageSequence> simulationAccountMessageSequences = getSimulationAccountMessageSequenceDao().findWithNamedQuery(namedQueryName, map);
		
		if(simulationAccountMessageSequences.size() < 1) {
			throw new GenericValidationException("No finlized account message seqence for learningBuild account pair (%s , %s)", learningBuildUID, accountUID);
		}
		
		String messageSeqUID = simulationAccountMessageSequences.get(0).getId().getMessageSeqId();
		String accountId = simulationAccountMessageSequences.get(0).getId().getAccountUID();
		
		List<SimulationAccountMessageSequenceDTO> simulationAccountMessageSequenceDTOs = new ArrayList<SimulationAccountMessageSequenceDTO>();
		String sequenceNamedQueryName = "SimulationMessageSequence.custom.findBySequence";		

		HashMap<String, Object> sequenceMap = new HashMap<String, Object>();
		sequenceMap.put("messageSeqUID", messageSeqUID);
		// get modes associated with messageSeqUID
		List<SimulationMessageSequence> simulationMessageSequences = getSimulationMessageSequenceDao().findWithNamedQuery(sequenceNamedQueryName, sequenceMap);
		// convert models to DTOs
		for(SimulationMessageSequence simulationMessageSequence : simulationMessageSequences) {			
			
			String messageUID = simulationMessageSequence.getId().getMessageID();
			// get message info from SimulationAccountSentEmail table to add to dto
			HashMap<String, Object> infoMap = new HashMap<String, Object>();
			infoMap.put("messageUID", messageUID);
			infoMap.put("accountUID", accountId);
			namedQueryName = "SimulationAccountSentEmail.custom.findByLearningMessageAndAccount";
			List <SimulationAccountSentEmail> simulationAccountSentEmails = getSimulationAccountSentEmailDao().findWithNamedQuery(namedQueryName, infoMap);

			SimulationAccountMessageSequenceDTO dto = new SimulationAccountMessageSequenceDTO();

			dto.setMessageSeqUID(messageSeqUID);
			dto.setMessageSeqHash(simulationMessageSequence.getMessageSeqHash());
			dto.setMessageID(messageUID);
			dto.setSequenceOrder(simulationMessageSequence.getSequenceOrder());
			dto.setCreatedAt(dateTimeFormat.format(simulationMessageSequence.getCreatedAt()));
			dto.setUpdatedAt(dateTimeFormat.format(simulationMessageSequence.getUpdatedAt()));
			
			if(simulationAccountSentEmails.size() > 0) {
				SimulationAccountSentEmail email = simulationAccountSentEmails.get(0);
				dto.setInteractionId(email.getInteractionId());
				dto.setAccountUID(accountId);
				dto.setName(email.getEmailName());
				dto.setIsOpen(email.getIsOpen());
				dto.setIsOpen(email.getIsOpen());
				dto.setStatus(email.getStatus());
				dto.setClickCount(email.getClickCount());
				if(email.getEmailSentDate() != null) dto.setEmailSentDate(dateTimeFormat.format(email.getEmailSentDate()));
				dto.setSenderEmailID(email.getSenderEmailID());
				dto.setAccountEmailID(email.getAccountEmailID());
				if(email.getEmailLastOpenedDate() != null) dto.setEmailLastOpenedDate(dateTimeFormat.format(email.getEmailLastOpenedDate()));
				if(email.getLastClickDate() != null) dto.setLastClickDate(dateTimeFormat.format(email.getLastClickDate()));
			} else {
				List<ApprovedDocuments> messageInfo = getMessageInfoFromApprovedDocument(messageUID);
				dto.setName(messageInfo.get(0).getName());
			}
			
			simulationAccountMessageSequenceDTOs.add(dto);
		}
		
		SimulationAccountSequenceInfoDTO simulationAccountSequenceInfoDTO = new SimulationAccountSequenceInfoDTO();
		simulationAccountSequenceInfoDTO.setSequence(simulationAccountMessageSequenceDTOs);
		simulationAccountSequenceInfoDTO.setProbability(simulationAccountMessageSequences.get(0).getProbability());
		simulationAccountSequenceInfoDTO.setLearningBuildUID(learningBuildUID);
		simulationAccountSequenceInfoDTO.setLearningRunUID(learningRunUID);
		return simulationAccountSequenceInfoDTO;
	}
	
	/** method for fifth api. returns sequence level data given a learningBuild and segment */
	public SimulationSegmentSequenceInfoDTO listSequenceWithBuildIdAndSegmentId(String learningBuildUID, String segmentUID) {		
		
		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		validateSegmentID(segmentUID);
		
		String learningRunUID = getLatestSuccessfulRun(learningBuild);
				
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);
		map.put("segmentUID", segmentUID);	
	
		String namedQueryName = "SimulationSegmentMessageSequence.custom.findByLearningRunAndSegment";
		List<SimulationSegmentMessageSequence> simulationSegmentMessageSequences = getSimulationSegmentMessageSequenceDao().findWithNamedQuery(namedQueryName, map);
		
		if(simulationSegmentMessageSequences.size() < 1) {
			throw new GenericValidationException("No finalized segment message sequence for learningBuild segment pair (%s , %s)", learningBuildUID, segmentUID);
		}
		
		String messageSeqUID = simulationSegmentMessageSequences.get(0).getId().getMessageSeqUID();
		
		List<SimulationSegmentMessageSequenceDTO> simulationSegmentMessageSequenceDTOs = new ArrayList<SimulationSegmentMessageSequenceDTO>();
		String sequenceNamedQueryName = "SimulationMessageSequence.custom.findBySequence";

		HashMap<String, Object> sequenceMap = new HashMap<String, Object>();
		sequenceMap.put("messageSeqUID", messageSeqUID);
		// get modes associated with messageSeqUID
		List<SimulationMessageSequence> simulationMessageSequences = getSimulationMessageSequenceDao().findWithNamedQuery(sequenceNamedQueryName, sequenceMap);
		// convert models to DTOs
		for(SimulationMessageSequence simulationMessageSequence : simulationMessageSequences) {
			SimulationSegmentMessageSequenceDTO dto = new SimulationSegmentMessageSequenceDTO();
			
			dto.setMessageSeqUID(messageSeqUID);
			dto.setMessageSeqHash(simulationMessageSequence.getMessageSeqHash());
			dto.setMessageID(simulationMessageSequence.getId().getMessageID());
			dto.setSequenceOrder(simulationMessageSequence.getSequenceOrder());
			dto.setCreatedAt(dateTimeFormat.format(simulationMessageSequence.getCreatedAt()));
			dto.setUpdatedAt(dateTimeFormat.format(simulationMessageSequence.getUpdatedAt()));
			dto.setName(getApprovedDocumentsByMessageId(dto.getMessageID()));
			
			simulationSegmentMessageSequenceDTOs.add(dto);
		}

		SimulationSegmentSequenceInfoDTO simulationSegmentSequenceInfoDTO = new SimulationSegmentSequenceInfoDTO();
		simulationSegmentSequenceInfoDTO.setSequence(simulationSegmentMessageSequenceDTOs);
		simulationSegmentSequenceInfoDTO.setProbability(simulationSegmentMessageSequences.get(0).getProbability());
		simulationSegmentSequenceInfoDTO.setLearningBuildUID(learningBuildUID);
		simulationSegmentSequenceInfoDTO.setLearningRunUID(learningRunUID);
		return simulationSegmentSequenceInfoDTO;	
	}
	
	/** method for sixth api. returns simulationAccount level data given a learningBuild and optional searchString*/
	public List<SimulationAccountDTO> listAccountsWithBuildUID(String learningBuildUID, String searchString, Integer page, Integer rows) {
			
		LearningBuild learningBuild = validateBuildID(learningBuildUID);
		String learningRunUID = getLatestSuccessfulRun(learningBuild);	

		// validate limit and offset
		if(page == null || page <= 0 || rows == null || rows <= 0) {
			page = 1;
			rows = 100;
		}
		
		Integer offset = rows*(page-1);
		Integer limit = offset + rows;	
				
    	HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);	
		
		String namedQueryName = "SimulationAccountMessageSequence.custom.findByLearningRun";
		
		if(searchString != null) { // no partial search string given
			map.put("accountName", searchString + "%"); // query for accountNames that begin with searchString
			namedQueryName = "SimulationAccountMessageSequence.custom.findByLearningRunAndSearchString";
		}
		
		List<SimulationAccountMessageSequence> simulationAccountMessageSequences = getSimulationAccountMessageSequenceDao().findWithNamedQuery(namedQueryName, map, offset, limit);
		List<SimulationAccountDTO> simulationAccountDTOs = new ArrayList<SimulationAccountDTO>();
		for(SimulationAccountMessageSequence simulationAccountMessageSequence : simulationAccountMessageSequences ) {
			SimulationAccountDTO simulationAccountDTO = new SimulationAccountDTO();
			simulationAccountDTO.setLearningBuildUID(learningBuildUID);
			simulationAccountDTO.setLearningRunUID(learningRunUID);
			simulationAccountDTO.setAccountUID(simulationAccountMessageSequence.getId().getAccountUID());
			simulationAccountDTO.setAccountName(simulationAccountMessageSequence.getAccountName());
			simulationAccountDTOs.add(simulationAccountDTO);
		}
		return simulationAccountDTOs;
	}
		
	public MessageInfoDTO getMessageInfo(String messageUID) {
		MessageInfoDTO dto = new MessageInfoDTO();
		
		List<ApprovedDocuments> approvedDocuments = getMessageInfoFromApprovedDocument(messageUID);
				if(approvedDocuments.size() > 0) {
					dto.setMessageName(approvedDocuments.get(0).getName());
					dto.setEmailSubject(approvedDocuments.get(0).getEmailSubject());
					dto.setEmailBody(approvedDocuments.get(0).getEmailBody());
					dto.setMessageUID(messageUID);
				}	
		return dto;
	}
	
	public MessageInfoDTO getMessageInfoPerAccount(String messageUID, String accountUID) {
		MessageInfoDTO dto = new MessageInfoDTO();
		
		// get message info from SimulationAccountSentEmail table to add to dto
		HashMap<String, Object> infoMap = new HashMap<String, Object>();
		infoMap.put("messageUID", messageUID);
		infoMap.put("accountUID", accountUID);
		String namedQueryName = "SimulationAccountSentEmail.custom.findByLearningMessageAndAccount";
		List <SimulationAccountSentEmail> simulationAccountSentEmails = getSimulationAccountSentEmailDao().findWithNamedQuery(namedQueryName, infoMap);
		
		if(simulationAccountSentEmails.size() > 0) {
			SimulationAccountSentEmail email = simulationAccountSentEmails.get(0);
			dto.setMessageName(email.getEmailName());
			dto.setEmailSubject(email.getEmailSubject());
			dto.setEmailBody(email.getEmailBody());
			dto.setMessageUID(messageUID);
		}
		if(dto.getEmailBody() == null || dto.getEmailSubject() == null) {
			getMessageInfo( messageUID);
		}
		return dto;
	}
	
	/** common method for reading a resource */
	@Override
	public SimulationSegmentDTO readObject(String id) {
		return super.readObject(id);
	}


	/** common method to save a resource */
	public SimulationSegmentDTO saveObject(SimulationSegmentDTO obj) {
		return super.saveObject(obj);
	}

	/** common method to update a resource */
	@Override
	public SimulationSegmentDTO updateObject(String id, SimulationSegmentDTO obj) {
		return super.updateObject(id, obj);
	}


	/** common method to remove a resource */
	@Override
	public SimulationSegmentDTO removeObject(String id) {
		return super.removeObject(id);
	}

	@Override
	protected void transferId(SimulationSegment newModel, SimulationSegment oldModel) {
		//super.transferId(newModel, oldModel);
	}


	@Override
	protected void setId(SimulationSegment model, String pk) {
		//super.setId(model, pk);
		//model.setId(pk);
	}

	@Override
	protected SimulationSegment toModelAdditionalMappings(SimulationSegmentDTO dto, SimulationSegment model) {
		return super.toModelAdditionalMappings(dto, model);
	}


	@Override
	protected SimulationSegmentDTO toDTOAdditionalMappings(SimulationSegment model, SimulationSegmentDTO dto) {
		
		dto.setCreatedAt(dateTimeFormat.format(model.getCreatedAt()));
		dto.setUpdatedAt(dateTimeFormat.format(model.getUpdatedAt()));
		return dto;
	}


	@Override
	protected void validateDTO(SimulationSegmentDTO dto) {
		super.validateDTO(dto); 
	}


}
