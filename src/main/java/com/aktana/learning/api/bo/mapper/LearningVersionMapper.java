/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.aktana.learning.api.dto.LearningVersionInfoDTO;
import com.aktana.learning.persistence.models.impl.LearningVersion;

import ma.glasnost.orika.MapperFactory;


public class LearningVersionMapper extends AbstractMapperSupport<LearningVersion, LearningVersionInfoDTO>
{
    public LearningVersionMapper()
    {
        super();
    }
    
    private static LearningVersionMapper instance;
    
    public static LearningVersionMapper getInstance()  {

		synchronized(LearningVersionMapper.class) {
			if (instance == null) {
				instance = new LearningVersionMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningVersion.class, LearningVersionInfoDTO.class)
                .field("id", "learningVersionUID")
                .field("learningConfig.id", "learningConfigUID")
                .field("isFinalized", "isFinalized")
                .field("isDeployed", "isDeployed")
                .field("currentStartDateTime", "currentStartDateTime")
                .field("isDeleted", "isDeleted")
                 //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningVersionInfoDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningVersion.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningVersionInfoDTO.class, LearningVersion.class)
                .field("learningVersionUID", "id")
                .field("learningConfigUID", "learningConfig.id")
                .field("isFinalized", "isFinalized")
                .field("isDeployed", "isDeployed")
                .field("currentStartDateTime", "currentStartDateTime")
                .field("isDeleted", "isDeleted")
                //.byDefault()
                .toClassMap());        
      
	}
	
	


	
	@Override
	public void applyCustomMappingsToDTO(LearningVersion pojo, LearningVersionInfoDTO dto)
	{				
		
		Date currentStartDateTime = pojo.getCurrentStartDateTime();
		
		if(currentStartDateTime != null) {
			// time should be in ISO format with the offsets for the particular TimezoneId associated with the visit		
			DateTimeFormatter formatter    = ISODateTimeFormat.dateTimeNoMillis();			
			DateTime utcTime = new DateTime(currentStartDateTime);
		
			// format it in ISO
			String datestr  = formatter.print(utcTime);		
			dto.setCurrentStartDateTime(datestr);		
		}
	}
	
	@Override
	public void applyCustomMappingsToPojo(LearningVersionInfoDTO dto, LearningVersion pojo)
	{
		DateTimeFormatter parser    = ISODateTimeFormat.dateTimeParser();					
		// this date will be in UTC
		String currentStartDateTime = dto.getCurrentStartDateTime();
		if(currentStartDateTime != null) {
			DateTime utcTime = parser.parseDateTime(currentStartDateTime);		
			pojo.setCurrentStartDateTime(utcTime.toDate());
		}
		
	}

}