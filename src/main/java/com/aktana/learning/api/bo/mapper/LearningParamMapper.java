/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.persistence.models.impl.LearningParam;

import ma.glasnost.orika.MapperFactory;


public class LearningParamMapper extends AbstractMapperSupport<LearningParam, LearningParamDTO>
{
    public LearningParamMapper()
    {
        super();
    }
    
    private static LearningParamMapper instance;
    
    public static LearningParamMapper getInstance()  {

		synchronized(LearningParamMapper.class) {
			if (instance == null) {
				instance = new LearningParamMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningParam.class, LearningParamDTO.class)
                .field("paramValue", "paramValue")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningParamDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningParam.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningParamDTO.class, LearningParam.class)
                .field("paramValue", "paramValue")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningParamDTO src, LearningParam tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningParam src, LearningParamDTO tgt)
	{
	
	}

}