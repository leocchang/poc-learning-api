/*****************************************************************
 *
 * @author $Author: jacob.adicoff $
 * @version $Revision: xxx $ on $Date: 2019-06-20 15:43:17 -0700 (Thu, 20 June 2019) $ by $Author: jacob.adicoff $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;


import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aktana.learning.api.dto.OptimalLearningParamsDTO;
import com.aktana.learning.api.bo.mapper.OptimalLearningParamsMapper;
import com.aktana.learning.persistence.models.impl.OptimalLearningParams;
import com.aktana.learning.persistence.models.impl.OptimalLearningParamsPK;






/** Base class for LearningRun */
public class OptimalLearningParamsBo extends AbstractBo<OptimalLearningParamsDTO, OptimalLearningParams, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

  
    /** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public OptimalLearningParamsBo() {
		super(OptimalLearningParamsBo.class, OptimalLearningParamsDTO.class, OptimalLearningParams.class, OptimalLearningParamsMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());

	}
	
	
	
	public List<OptimalLearningParamsDTO> getOptimalLearningParams(String productUID, String channelUID, String goal ) {	
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("productUID", productUID);
		map.put("channelUID", channelUID);
		map.put("goal", goal);
		String namedQueryName = "OptimalLearningParams.custom.findByProductAndChannelAndGoal";
		return this.toDTO(this.getDao().findWithNamedQuery(namedQueryName, map));
	}


	/** common method for listing a resource */
	@Override
	public List<OptimalLearningParamsDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
		
	/** common method for reading a resource */
	@Override
	public OptimalLearningParamsDTO readObject(String id) {
		return super.readObject(id);
	}


	/** common method to save a resource */
	@Override
	public OptimalLearningParamsDTO saveObject(OptimalLearningParamsDTO obj) {
		return super.saveObject(obj);
	}

	/** common method to update a resource */
	@Override
	public OptimalLearningParamsDTO updateObject(String id, OptimalLearningParamsDTO obj) {
		return super.updateObject(id, obj);
	}


	/** common method to remove a resource */
	@Override
	public OptimalLearningParamsDTO removeObject(String id) {
		return super.removeObject(id);
	}

	@Override
	protected void transferId(OptimalLearningParams newModel, OptimalLearningParams oldModel) {
		//super.transferId(newModel, oldModel);
	}


	@Override
	protected void setId(OptimalLearningParams model, String pk) {
		//super.setId(model, pk);
		//model.setId(pk);
	}

	@Override
	protected OptimalLearningParams toModelAdditionalMappings(OptimalLearningParamsDTO dto, OptimalLearningParams model) {
		if (model == null) model = new OptimalLearningParams();
		
		OptimalLearningParamsPK optimalLearningParamsPK = new OptimalLearningParamsPK();
		optimalLearningParamsPK.setProduct(dto.getProductUID());	
		optimalLearningParamsPK.setChannel(dto.getChannelUID());
		optimalLearningParamsPK.setGoal(dto.getGoal());
		optimalLearningParamsPK.setParamName(dto.getParamName());
		model.setId(optimalLearningParamsPK);
		return model;
	}


	@Override
	protected OptimalLearningParamsDTO toDTOAdditionalMappings(OptimalLearningParams model, OptimalLearningParamsDTO dto) {	
		if(dto == null) dto = new OptimalLearningParamsDTO();
		
		dto.setProductUID(model.getId().getProduct());
		dto.setChannelUID(model.getId().getChannel());
		dto.setGoal(model.getId().getGoal());
		dto.setParamName(model.getId().getParamName());
		dto.setCreatedAt(dateTimeFormat.format(model.getCreatedAt()));
		dto.setUpdatedAt(dateTimeFormat.format(model.getUpdatedAt()));
		return dto;
	}


	@Override
	protected void validateDTO(OptimalLearningParamsDTO dto) {
		super.validateDTO(dto); 
	}


}
