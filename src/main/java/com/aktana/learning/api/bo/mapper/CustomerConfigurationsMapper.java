/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.CustomerConfigurationsDTO;
import com.aktana.learning.persistence.models.impl.CustomerConfigurations;

import ma.glasnost.orika.MapperFactory;


public class CustomerConfigurationsMapper extends AbstractMapperSupport<CustomerConfigurations, CustomerConfigurationsDTO>
{
    public CustomerConfigurationsMapper()
    {
        super();
    }
    
    private static CustomerConfigurationsMapper instance;
    
    public static CustomerConfigurationsMapper getInstance()  {

		synchronized(CustomerConfigurationsMapper.class) {
			if (instance == null) {
				instance = new CustomerConfigurationsMapper();
			}
		}
    	
    	return instance;    	
    }
    
    
    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(CustomerConfigurations.class, CustomerConfigurationsDTO.class)
                //.field("configurationType", "configurationType")
                //.field("configurationValue", "configurationValue")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return CustomerConfigurationsDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return CustomerConfigurations.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(CustomerConfigurationsDTO.class, CustomerConfigurations.class)
                //.field("configurationType", "configurationType")
                //.field("configurationValue", "configurationValue")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(CustomerConfigurationsDTO src, CustomerConfigurations tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(CustomerConfigurations src, CustomerConfigurationsDTO tgt)
	{
	
	}

}