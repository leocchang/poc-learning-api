/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import com.aktana.learning.persistence.models.impl.LearningConfig;

import ma.glasnost.orika.MapperFactory;


public class LearningConfigMapper extends AbstractMapperSupport<LearningConfig, LearningConfigInfoDTO>
{
    public LearningConfigMapper()
    {
        super();
    }
    
    private static LearningConfigMapper instance;
    
    public static LearningConfigMapper getInstance()  {

		synchronized(LearningConfigMapper.class) {
			if (instance == null) {
				instance = new LearningConfigMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningConfig.class, LearningConfigInfoDTO.class)
                .field("id", "learningConfigUID")
                .field("learningConfigName", "learningConfigName")
                .field("learningConfigDescription", "learningConfigDescription")
                .field("modelType", "modelType")
                .field("productUID", "productUID")
                .field("channelUID", "channelUID")
                .field("isPublished", "isPublished")
                .field("isDeployed", "isDeployed")
                .field("isDeleted", "isDeleted")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningConfigInfoDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningConfig.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningConfigInfoDTO.class, LearningConfig.class)
                .field("learningConfigUID", "id")
                .field("learningConfigName", "learningConfigName")
                .field("learningConfigDescription", "learningConfigDescription")
                .field("modelType", "modelType")
                .field("productUID", "productUID")
                .field("channelUID", "channelUID")
                .field("isPublished", "isPublished")
                .field("isDeployed", "isDeployed")
                .field("isDeleted", "isDeleted")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningConfigInfoDTO src, LearningConfig tgt)
	{
		//Adapter method, must be overridden to provide actual functionality

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningConfig src, LearningConfigInfoDTO tgt)
	{

	}

}