package com.aktana.learning.api.bo.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.glasnost.orika.MapperFactory;

import com.aktana.learning.api.dto.RepAccountEngagementDTO;
import com.aktana.learning.common.util.datetime.PureDate;
import com.aktana.learning.persistence.models.impl.RepAccountEngagement;



public class RepAccountEngagementMapper extends AbstractMapperSupport<RepAccountEngagement, RepAccountEngagementDTO>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RepAccountEngagementMapper.class);

    private static RepAccountEngagementMapper instance;
    
    public static RepAccountEngagementMapper getInstance()  {

		synchronized(RepAccountEngagementMapper.class) {
			if (instance == null) {
				instance = new RepAccountEngagementMapper();
			}
		}
    	
    	return instance;    	
    }
	
	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(RepAccountEngagementDTO.class, RepAccountEngagement.class)
				.fieldMap("repUID", "rep.externalId").add()
				.fieldMap("accountUID", "account.externalId").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("endDate", "endDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());

	}

	@Override
	protected void initializeMappingsToDTO(MapperFactory factory) {
		factory.registerClassMap(factory
				.classMap(RepAccountEngagement.class, RepAccountEngagementDTO.class)
				.fieldMap("rep.externalId", "repUID").add()
				.fieldMap("account.externalId", "accountUID").add()	
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.fieldMap("startDate", "startDate").converter(PureDate.AKTANA_PURE_DATE_FORMAT).add()
				//.byDefault()
				.toClassMap());	
	}


	@Override
	public void applyCustomMappingsToDTO(RepAccountEngagement pojo, RepAccountEngagementDTO dto)
	{		
		if (pojo == null) {
			LOGGER.error("toDTO: RepAccountEngagement pojo is null");
		}	
		else if (dto == null) {
			LOGGER.error("toDTO: RepAccountEngagement dto is null");
		}
		else {
			// set the dto's startDate and endDate
			if(pojo.getId().getDate() != null) {
				PureDate pureDate = new PureDate(pojo.getId().getDate());
				dto.setDate(pureDate.toString());
			}
		}
	}


	@Override
	public Class<?> getPojoClass() {
		return RepAccountEngagement.class;
	}

	@Override
	public Class<?> getDTOClass() {
		return RepAccountEngagementDTO.class;
	}

	
	@Override
	public RepAccountEngagement toPojo(RepAccountEngagementDTO dto)
	{
		RepAccountEngagement pojo = super.toPojo(dto);
		this.applyCustomMappingsToPojo(dto, pojo);
		return pojo;
	}


	@Override
	public RepAccountEngagementDTO toDTO(RepAccountEngagement pojo)
	{
		RepAccountEngagementDTO dto = super.toDTO(pojo);
		this.applyCustomMappingsToDTO(pojo, dto);
		return dto;
	}
	
}