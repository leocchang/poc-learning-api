/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.OptimalLearningParamsDTO;
import com.aktana.learning.persistence.models.impl.OptimalLearningParams;

import ma.glasnost.orika.MapperFactory;


public class OptimalLearningParamsMapper extends AbstractMapperSupport<OptimalLearningParams, OptimalLearningParamsDTO>
{
    public OptimalLearningParamsMapper()
    {
        super();
    }
    
    private static OptimalLearningParamsMapper instance;
    
    public static OptimalLearningParamsMapper getInstance()  {

		synchronized(OptimalLearningParamsMapper.class) {
			if (instance == null) {
				instance = new OptimalLearningParamsMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(OptimalLearningParams.class, OptimalLearningParamsDTO.class)
//                .field("id.productUID", "productUID")
//                .field("id.channelUID", "channelUID")
//                .field("id.goal", "goal")
//                .field("id.paramName", "paramName")
                .field("paramValue", "paramValue")
                .field("createdAt", "createdAt")
                .field("updatedAt", "updatedAt")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return OptimalLearningParamsDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return OptimalLearningParams.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(OptimalLearningParamsDTO.class, OptimalLearningParams.class)
//                .field("productUID", "id.productUID")
//                .field("channelUID", "id.channelUID")
//                .field("goal", "id.goal")
//                .field("paramName", "id.paramName")
                .field("paramValue", "paramValue")
                .field("createdAt", "createdAt")
                .field("updatedAt", "updatedAt")
                //.byDefault()
                .toClassMap());       
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(OptimalLearningParamsDTO src, OptimalLearningParams tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(OptimalLearningParams src, OptimalLearningParamsDTO tgt)
	{
	
	}

}