/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.LearningObjectListDTO;
import com.aktana.learning.persistence.models.impl.LearningObjectList;

import ma.glasnost.orika.MapperFactory;


public class LearningObjectListMapper extends AbstractMapperSupport<LearningObjectList, LearningObjectListDTO>
{
    public LearningObjectListMapper()
    {
        super();
    }
    
    private static LearningObjectListMapper instance;
    
    public static LearningObjectListMapper getInstance()  {

		synchronized(LearningObjectListMapper.class) {
			if (instance == null) {
				instance = new LearningObjectListMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(LearningObjectList.class, LearningObjectListDTO.class)
                .field("learningBuild.id", "learningBuildUID")
                .field("id.learningRun.id", "learningRunUID")
                .field("id.listType", "listType")
                .field("id.objectType", "objectType")
                .field("id.objectUID", "objectUID")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return LearningObjectListDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return LearningObjectList.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(LearningObjectListDTO.class, LearningObjectList.class)
                .field("learningBuildUID", "learningBuild.id")
                .field("learningRunUID", "id.learningRun.id")
                .field("listType", "id.listType")
                .field("objectType", "id.objectType")
                .field("objectUID", "id.objectUID")
                //.byDefault()
                .toClassMap());        
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(LearningObjectListDTO src, LearningObjectList tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(LearningObjectList src, LearningObjectListDTO tgt)
	{
	
	}

}