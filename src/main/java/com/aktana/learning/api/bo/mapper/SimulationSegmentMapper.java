/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION.</p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.bo.mapper;

import com.aktana.learning.api.dto.SimulationSegmentDTO;
import com.aktana.learning.persistence.models.impl.SimulationSegment;

import ma.glasnost.orika.MapperFactory;


public class SimulationSegmentMapper extends AbstractMapperSupport<SimulationSegment, SimulationSegmentDTO>
{
    public SimulationSegmentMapper()
    {
        super();
    }
    
    private static SimulationSegmentMapper instance;
    
    public static SimulationSegmentMapper getInstance()  {

		synchronized(SimulationSegmentMapper.class) {
			if (instance == null) {
				instance = new SimulationSegmentMapper();
			}
		}
    	
    	return instance;    	
    }
    

    @Override
    protected void initializeMappingsToDTO(MapperFactory factory)
    {
        factory.registerClassMap(factory
                .classMap(SimulationSegment.class, SimulationSegmentDTO.class)
                .field("id", "segmentUID")
                .field("learningBuild.id", "learningBuildUID")
                .field("learningRun.id", "learningRunUID")
                .field("segmentName", "segmentName")
                .field("segmentValue", "segmentValue")
                //.byDefault()
                .toClassMap());
    }

	@Override
	public Class<?> getDTOClass() {
		return SimulationSegmentDTO.class;
	}

	@Override
	public Class<?> getPojoClass() {
		return SimulationSegment.class;
	}

	@Override
	protected void initializeMappingsToPojo(MapperFactory factory) {
        factory.registerClassMap(factory
                .classMap(SimulationSegmentDTO.class, SimulationSegment.class)
                .field("segmentUID", "id")
                .field("learningBuildUID", "learningBuild.id")
                .field("learningRunUID", "learningRun.id")
                .field("segmentName", "segmentName")
                .field("segmentValue", "segmentValue")
                .field("createdAt", "createdAt")
                .field("updatedAt", "updatedAt")
                //.byDefault()
                .toClassMap());       
      
	}
	
	
	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src DTO
	 * @param tgt POJO
	 */
	public void applyCustomMappingsToPojo(SimulationSegmentDTO src, SimulationSegment tgt)
	{
		//Adapter method, must be overridden to provide actual functionality
		

	}

	/**
	 * Optional adapter method to perform additional custom mappings
	 * 
	 * @param src POJO
	 * @param tgt DTO
	 */
	public void applyCustomMappingsToDTO(SimulationSegment src, SimulationSegmentDTO tgt)
	{
	
	}

}