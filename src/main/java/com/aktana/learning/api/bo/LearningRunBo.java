/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.bo.mapper.LearningRunMapper;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.LearningRunDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.module.LearningModelHandler;
import com.aktana.learning.persistence.models.impl.AccountMessageSequence;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningFile;
import com.aktana.learning.persistence.models.impl.LearningObjectList;
import com.aktana.learning.persistence.models.impl.LearningRun;







/** Base class for LearningRun */
public class LearningRunBo extends AbstractBo<LearningRunDTO, LearningRun, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private GenericDao<LearningBuild, String> learningBuildDao = null;
    private GenericDao<LearningFile, String> learningFileDao = null;

	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningRunBo() {
		super(LearningRunBo.class, LearningRunDTO.class, LearningRun.class, LearningRunMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());

		learningBuildDao = new GenericDaoImpl<LearningBuild, String>(LearningBuild.class);
		learningFileDao = new GenericDaoImpl<LearningFile, String>(LearningFile.class);
	}

	private GenericDao<LearningBuild, String> getLearningBuildDao() {
		learningBuildDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningBuildDao;
	}

	private GenericDao<LearningFile, String> getLearningFileDao() {
		learningFileDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return learningFileDao;
	}


	/** common method for listing a resource */
	@Override
	public List<LearningRunDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}


	/** Returns a List of all LearningRun objects. */
	public List<LearningRun> findAll() {
		return getDao().findAll();
	}

	public List<LearningFileInfoDTO> getFiles(String learningRunUID) {

		if(learningRunUID == null || learningRunUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningRunUID %s", learningRunUID);
		}

		if(this.readModelWithExternalId(learningRunUID) == null) {
			throw new GenericValidationException("Existing learningRun %s not found", learningRunUID);
		}

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRunUID);

		List<LearningFile> learningFiles = this.getLearningFileDao().findWithNamedQuery("LearningFile.custom.findByLearningRun", map);
		List<LearningFileInfoDTO> learningFileDTOs = new ArrayList<LearningFileInfoDTO>();
		for(LearningFile learningFile : learningFiles) {
			LearningFileInfoDTO dto = new LearningFileInfoDTO();

			LearningBuild learningBuild = learningFile.getLearningBuild();

			dto.setLearningConfigUID(learningBuild.getLearningConfig().getId());
			dto.setLearningVersionUID(learningBuild.getLearningVersion().getId());
			dto.setLearningBuildUID(learningBuild.getId());
			dto.setLearningRunUID(learningRunUID);
			dto.setLearningFileUID(learningFile.getId());
			dto.setFileName(learningFile.getFileName());
			dto.setFileType(learningFile.getFileType());
			dto.setFileSize(learningFile.getFileSize());

			learningFileDTOs.add(dto);
		}

		return learningFileDTOs;
	}

	public List<LearningScoreDTO> getScores(String learningRunUID, Integer page, Integer rows) {
		
		if(learningRunUID == null || learningRunUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningRunUID %s", learningRunUID);
		}

		LearningRun learningRun = this.readModelWithExternalId(learningRunUID);
		if(learningRun == null) {
			throw new GenericValidationException("LearningRun not found (learningRunUID %s)", learningRunUID);
		}
		
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningRun.getLearningConfig().getModelType());
		List<LearningScoreDTO> learningScoreDTOs = learningModelHandler.getScores(learningRun, page, rows);

		return learningScoreDTOs;
	}
	
	
	public List<LearningScoreDTO> getAccountScores(String learningRunUID, String accountUID){
		
		if(learningRunUID == null || learningRunUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningRunUID %s", learningRunUID);
		}
		
		if(accountUID == null || accountUID.isEmpty()) {
			throw new GenericValidationException("Invalid accountUID %s", accountUID);
		}
		
		LearningRun learningRun = this.readModelWithExternalId(learningRunUID);
		if(learningRun == null) {
			throw new GenericValidationException("LearningRun not found (learningRunUID %s)", learningRunUID);
		}
		
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningRun.getLearningConfig().getModelType());
		List<LearningScoreDTO> learningScoreDTOs = learningModelHandler.getAccountScores(learningRun, accountUID, null, null);

		return learningScoreDTOs;
	}
	
	public List<LearningScoreDTO> getRepScores(String learningRunUID, String repUID, Integer page, Integer rows){
		
		if(learningRunUID == null || learningRunUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningRunUID %s", learningRunUID);
		}
		
		if(repUID == null || repUID.isEmpty()) {
			throw new GenericValidationException("Invalid repUID %s", repUID);
		}
		
		LearningRun learningRun = this.readModelWithExternalId(learningRunUID);
		if(learningRun == null) {
			throw new GenericValidationException("LearningRun not found (learningRunUID %s)", learningRunUID);
		}
		
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningRun.getLearningConfig().getModelType());
		List<LearningScoreDTO> learningScoreDTOs = learningModelHandler.getRepScores(learningRun, repUID, page, rows);

		return learningScoreDTOs;
	}

	/** common method for reading a resource */
	@Override
	public LearningRunDTO readObject(String id) {
		return super.readObject(id);
	}


	/** common method to save a resource */
	public LearningRunDTO saveObject(LearningRunDTO obj) {
		return super.saveObject(obj);
	}

	/** common method to update a resource */
	@Override
	public LearningRunDTO updateObject(String id, LearningRunDTO obj) {
		return super.updateObject(id, obj);
	}


	/** common method to remove a resource */
	@Override
	public LearningRunDTO removeObject(String id) {
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningRun newModel, LearningRun oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningRun model, String pk) {
		model.setId(pk);
	}

	@Override
	protected LearningRun toModelAdditionalMappings(LearningRunDTO dto, LearningRun model) {

    	if (model == null) {
    		model = new LearningRun();
    	}


    	String learningBuildUID =  dto.getLearningBuildUID();

    	LearningBuild learningBuild = getLearningBuildDao().get(learningBuildUID);
    	if (learningBuild == null) {
        	throw new GenericValidationException("Invalid learningBuildUID %s in LearningRun", learningBuildUID);
    	}

    	model.setLearningBuild(learningBuild);
    	model.setLearningVersion(learningBuild.getLearningVersion());
    	model.setLearningConfig(learningBuild.getLearningConfig());

		return model;
	}


	@Override
	protected LearningRunDTO toDTOAdditionalMappings(LearningRun model, LearningRunDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningRunDTO();
		}

		dto.setLearningRunUID(model.getId());
		dto.setLearningVersionUID(model.getLearningVersion().getId());

		Date executionStartDate = model.getExecutionDatetime();
		if(executionStartDate != null) {
			// time should be in ISO format with the offsets for the particular TimezoneId associated with the visit
			DateTimeFormatter formatter    = ISODateTimeFormat.dateTimeNoMillis();
			DateTime utcTime = new DateTime(executionStartDate);

			// format it in ISO
			String datestr  = formatter.print(utcTime);
			dto.setExecutionDatetime(datestr);
		}

		return dto;
	}


	@Override
	protected void validateDTO(LearningRunDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();

		if ((dto.getLearningRunUID() == null) || dto.getLearningRunUID().length() < 1 || dto.getLearningRunUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();

			err.setField("learningRunUID");
			err.setMessage(String.format("LearningRun UID is required and must be between 1 and 80 characters long: %s", dto.getLearningRunUID()));
			errors.add(err);
		}



		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}

	}

	/**
	 * Returns all learningRuns by learning version
	 */
	public List<LearningRun> getAllLearningRunsByLearningVersion(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("learningVersionUID", learningVersionUID);

		List<LearningRun> params = this.getDao().findWithNamedQuery("LearningRun.custom.findByLearningVersion", map);
		return params;
	}

	/**
	 * Returns all learningRuns by learning build
	 */
	public List<LearningRun> getAllLearningRunsByLearningBuild(String learningBuildUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("learningBuildUID", learningBuildUID);

		List<LearningRun> params = this.getDao().findWithNamedQuery("LearningRun.custom.findByLearningBuild", map);
		return params;
	}
	
	public List<LearningRun> getAllLearningScoringRunsByLearningBuild(String learningBuildUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("learningBuildUID", learningBuildUID);

		List<LearningRun> params = this.getDao().findWithNamedQuery("LearningRun.custom.findScoringRunByLearningBuild", map);
		return params;
	}
	

	/** 
	 * for calling with LearningRun api. gets LearningRun with string id 
	 * @param the uid of the run to delete
	 * @return the deleted learningRun
	 */
	public LearningRunDTO deleteLearningRunWithUIDwrapper(String learningRunId) {
		LearningRun learningRun = readModelWithExternalId(learningRunId);
		if(learningRun == null) {
			throw new GenericValidationException("No %s found with id %s", "LearningRun", learningRunId);	    		
		}
		return deleteLearningRunWithUID(learningRun, true);
	}

	/** delete a learningRun if allowable and clean learningRun from all other tables 
	 * @param the LearningRun to delete
	 * @param bool flag to check for deletable run
	 * @return the DTO of the deleted learningRun
	 */
	public LearningRunDTO deleteLearningRunWithUID(LearningRun learningRun, boolean checkRun) {
		LearningRunDTO learningRunDTO = new LearningRunDTO();

		// don't want to throw exceptions if calling func from LearningBuildBo
		if(checkRun) {			
			// do not delete learningRun
			if(learningRun.getExecutionStatus() != null) {
				if(learningRun.getExecutionStatus().equals("running")) {
					throw new GenericValidationException("Cannot delete learningRun: %s. learningRun is running", learningRun.getId());	    		
				}
			}
			if(learningRun.getIsPublished()) {
				throw new GenericValidationException("Cannot delete learningRun: %s. learningRun is published", learningRun.getId());	    		
			}

		}
		// now ok delete learningRun and clean from other tables
		learningRunDTO = toDTO(learningRun);
		
		// cascading delete in other tables as long as fk exists and ON DELETE CASCADE is set
		getDao().delete(learningRun);
		getDao().flush();
		return learningRunDTO;
	}



}
