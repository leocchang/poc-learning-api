/*****************************************************************
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.HashMap;
import java.util.List;  
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.persistence.models.impl.AccountProduct;
import com.aktana.learning.persistence.models.impl.AccountProductPK;
import com.aktana.learning.api.bo.mapper.AccountProductMapper;
import com.aktana.learning.api.dto.AccountProductDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.util.datetime.PureDate;


/** BO class for RepAccount */
public class AccountProductBo extends AbstractBo<AccountProductDTO, AccountProduct, AccountProductPK> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public AccountProductBo() {
		super(AccountProductBo.class, AccountProductDTO.class, AccountProduct.class, AccountProductMapper.getInstance());
		
		
		LOGGER = LoggerFactory.getLogger(this.getClass());		
	}
	
	/** common method for listing a resource */
	public List<AccountProductDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}

	/** common method for reading a resource */
	public AccountProductDTO readObject(String id) {
		return super.readObject(id);
	}

	
	/** common method to save a resource */
	public AccountProductDTO saveObject(AccountProductDTO obj) {
		return super.saveObject(obj);
	}

	
	/** common method to update a resource */
	public AccountProductDTO updateObject(String id, AccountProductDTO obj) {
		return super.updateObject(id,  obj);
	}

		
	/** common method to remove a resource */
	public AccountProductDTO removeObject(String id) {
		return super.removeObject(id);
	}
	
	
	@Override
	protected void transferId(AccountProduct newModel, AccountProduct oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}

	// setSoftDeletedFlag(User): no need to override

	@Override
	protected void setId(AccountProduct model, AccountProductPK pk) {
		model.setId(pk);
	}
	



	@Override
	protected AccountProduct toModelAdditionalMappings(AccountProductDTO dto, AccountProduct model) {
    	
    	
    	AccountProductPK pk = new AccountProductPK();    
    
    	model.setId(pk);
    	    	
		return model;
	}

	

	@Override
	protected AccountProductDTO toDTOAdditionalMappings(AccountProduct model, AccountProductDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new AccountProductDTO();
		}
		
				
		return dto;
	}
	
	
	
	/* helper method to read existing model object based on the composite ids */
	@Override
	public AccountProduct readModelWithExternalId(String id) {
		HashMap<String, Object> map = parseAndValidateObjectUID(id);
				
		return super.readModelWithExternalId(map);		
	}

	


	private HashMap<String, Object> parseAndValidateObjectUID(String id) {
		String[] subkeys = id.split("\\~");
		HashMap<String, Object> map = new HashMap<String, Object>();
		

		if (subkeys.length != 2) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		map.put("repUID", subkeys[0]);


		// startDate
		try {
			PureDate date = new PureDate(subkeys[1]);
			map.put("date", date.toString());
		} catch (Exception e) {
			throw new GenericValidationException(
					"Invalid date in composite key: %s", subkeys[1]);
		}

		
		return map;
	}

	
	private HashMap<String, Object> parseAndValidateObjectUIDPattern(String id) {
		String[] subkeys = id.split("\\~", -1);
		HashMap<String, Object> map = new HashMap<String, Object>();

		if (subkeys.length != 2) {
			throw new GenericValidationException("Invalid composite key format %s", id);	    											
		}

		if (!subkeys[0].isEmpty()) {
			map.put("repUID", subkeys[0]);
		}

		// date
		try {
			PureDate date = new PureDate(subkeys[1]);
			map.put("date", date.toJavaDate());
		} catch (Exception e) {
			throw new GenericValidationException(
					"Invalid date in composite key: %s", subkeys[1]);
		}
			
		return map;		
	}

	

}
