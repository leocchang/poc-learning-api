/*****************************************************************
 *
 * @author $Author: satya.dhanushkodi $
 * @version $Revision: 20112 $ on $Date: 2014-05-22 15:43:17 -0700 (Thu, 22 May 2014) $ by $Author: satya.dhanushkodi $
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.api.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Collection;
import java.lang.reflect.Type;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.bo.mapper.LearningConfigMapper;
import com.aktana.learning.api.dto.LearningConfigInfoDTO;
import com.aktana.learning.api.dto.LearningFileInfoDTO;
import com.aktana.learning.api.dto.CrontabDTO;
import com.aktana.learning.api.dto.LearningBuildDTO;
import com.aktana.learning.api.dto.LearningConfigDTO;
import com.aktana.learning.api.dto.LearningConfigDraftDTO;
import com.aktana.learning.api.dto.LearningParamDTO;
import com.aktana.learning.api.dto.LearningParamElementDTO;
import com.aktana.learning.api.dto.LearningRunDTO;
import com.aktana.learning.api.dto.LearningVersionInfoDTO;

import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.learningS3.LearningS3ConnectionException;
import com.aktana.learning.common.learningS3.LearningS3Connector;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.common.rundeck.RundeckConnectionException;
import com.aktana.learning.common.rundeck.RundeckConnector;
import com.aktana.learning.common.module.LearningModelHandler;
import com.aktana.learning.persistence.models.impl.LearningBuild;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningFile;
import com.aktana.learning.persistence.models.impl.LearningParam;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.LearningVersion;
import com.aktana.learning.persistence.models.impl.S3PartialPaths;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;



import com.aktana.learning.api.dto.dse.MessageAlgorithmDTO;
import com.aktana.learning.api.dto.dse.TimeToEngageAlgorithmDTO;

import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
//com.fasterxml.jackson.databind.ObjectMapper;



/** Base class for LearningConfig */
public class LearningConfigBo extends AbstractBo<LearningConfigInfoDTO, LearningConfig, String> {

	/** Logger for general logging purposes. */
    protected static Logger LOGGER;

    private LearningVersionBo learningVersionBo = null;
    private LearningBuildBo learningBuildBo = null;
    private LearningParamBo learningParamBo = null;
    private LearningRunBo learningRunBo = null;
    private LearningFileBo learningFileBo = null;
    
	/** Sole constructor with the derived BusinessObject class, the resource representation class (DTO), Hibernate Model (POJO) and Mapper class to the business Object */
	public LearningConfigBo() {
		super(LearningConfigBo.class, LearningConfigInfoDTO.class, LearningConfig.class, LearningConfigMapper.getInstance());

		LOGGER = LoggerFactory.getLogger(this.getClass());	
		
		learningVersionBo = new LearningVersionBo();
		learningBuildBo = new LearningBuildBo();
		learningParamBo = new LearningParamBo();
		learningRunBo = new LearningRunBo();
		learningFileBo = new LearningFileBo();
		
	}
	
	/** common method for listing a resource */
	@Override	
	public List<LearningConfigInfoDTO> listLimitedObjects() {
		return super.listLimitedObjects();
	}
	
	
	/** Returns a List of all LearningConfig objects. */
	public List<LearningConfig> findAll() {
		return getDao().findAll();
	}
	
	/** common method for reading a resource */
	@Override	
	public LearningConfigInfoDTO readObject(String id) {
		return super.readObject(id);		
	}

	/** common method to save a resource */
	public LearningConfigInfoDTO saveObject(LearningConfigInfoDTO obj) {				
		return super.saveObject(obj);
	}
	
	@Override
	protected LearningConfig beforeAction(String action, LearningConfigInfoDTO dto, LearningConfig model) {	
		
		if (action.equalsIgnoreCase(LearningConfigBo.ACTION_UPDATE)) {
			
			LearningConfig oldModel = this.readModelWithExternalId(dto.getLearningConfigUID());
			if(oldModel == null) {
				throw new GenericValidationException("Existing learningConfig %s not found", dto.getLearningConfigUID());
			}
			
			if(model.getLearningConfigName() == null) {
				model.setLearningConfigName(oldModel.getLearningConfigName());
			}
			
			if(model.getLearningConfigDescription() == null) {
				model.setLearningConfigDescription(oldModel.getLearningConfigDescription());
			}
			model.setChannelUID(oldModel.getChannelUID());
			model.setProductUID(oldModel.getProductUID());
			model.setModelType(oldModel.getModelType());			
			model.setIsPublished(oldModel.getIsPublished());
			model.setIsDeployed(oldModel.getIsDeployed());
			model.setIsDeleted(oldModel.getIsDeleted());
			model.setCurrentLearningBuild(oldModel.getCurrentLearningBuild());
			model.setCurrentLearningVersion(oldModel.getCurrentLearningVersion());
		}
		
		else if (action.equalsIgnoreCase(LearningConfigBo.ACTION_REMOVE)) {

			if(model.getIsPublished()) {
				throw new GenericValidationException("LearningConfig %s is published and cannot be deleted", model.getId());
			}
			
			if(model.getIsDeployed()) {
				throw new GenericValidationException("LearningConfig %s is deployed and cannot be deleted", model.getId());
			}
		}
		
		return model;
	}
	
	@Override
	protected LearningConfig afterAction(String action, LearningConfigInfoDTO dto, LearningConfig model) {	
		
		if (action.equalsIgnoreCase(LearningConfigBo.ACTION_REMOVE)) {

			// get uids to avoid concurrent modification issue
			ArrayList<String> uids = new ArrayList<String>();
			Set<LearningVersion> versions = model.getLearningVersions();
			if(versions != null) {
				for(LearningVersion learningVersion : versions) {
					uids.add(learningVersion.getId());
				}
			}
			
			for(String uid : uids) {
				learningVersionBo.removeObject(uid);
			}
		}
		
		return model;
	}
	
	/** save a new draft */
	public LearningConfigInfoDTO saveDraft(LearningConfigDraftDTO obj) {
		
		if(obj.getLearningConfigUID() == null || obj.getLearningConfigUID().isEmpty()) {
			obj.setLearningConfigUID(UUID.randomUUID().toString());
		}
		
		if(obj.getLearningVersionUID() == null || obj.getLearningVersionUID().isEmpty()) {
			obj.setLearningVersionUID(UUID.randomUUID().toString());
		}
		
		String learningConfigUID = obj.getLearningConfigUID();
		if(this.readModelWithExternalId(learningConfigUID) != null) {
			throw new GenericValidationException("LearningConfigUID %s is already used ", learningConfigUID);
		}
		
		String learningVersionUID = obj.getLearningVersionUID();
		if(learningVersionBo.readModelWithExternalId(learningVersionUID) != null) {
			throw new GenericValidationException("LearningVersionUID %s is already used ", learningVersionUID);
		}
		
		List<String> channels = Arrays.asList("VISIT_CHANNEL", "SEND_CHANNEL", "WEB_INTERACTIVE_CHANNEL");	
		if( obj.getChannelUID() != null && !channels.contains(obj.getChannelUID())) {
			throw new GenericValidationException("LearningConfig channelUID, if specified, must be one of 'VISIT_CHANNEL', 'SEND_CHANNEL' or 'WEB_INTERACTIVE_CHANNEL': "+obj.getChannelUID());
		}
		
		if( obj.getProductUID() != null && obj.getProductUID().equals("")) {
			throw new GenericValidationException("LearningConfig productUID, if specified, must not be em.");
		}		
		
		LearningConfigInfoDTO configDTO = obj.createLearningConfigInfoDTO();
		configDTO = super.saveObject(configDTO);
		
		LearningVersionInfoDTO version = new LearningVersionInfoDTO();
		version.setLearningConfigUID(learningConfigUID);
		version.setLearningVersionUID(learningVersionUID);
		version.setIsDeployed(false);
		version.setIsDeleted(false);
		version.setIsFinalized(false);
	
		learningVersionBo.saveObject(version);
		
		return configDTO;
	}
	
	private LearningVersionInfoDTO cloneVersion(String learningConfigUID, String learningVersionUID, String cloneLearningVersionUID) {
		
		LearningVersionInfoDTO versionDTO = new LearningVersionInfoDTO();
		versionDTO.setLearningConfigUID(learningConfigUID);
		versionDTO.setLearningVersionUID(cloneLearningVersionUID);
		versionDTO.setIsDeployed(false);
		versionDTO.setIsDeleted(false);
		versionDTO.setIsFinalized(false);
	
		versionDTO = learningVersionBo.saveObject(versionDTO);
		
		List<LearningParam> params = learningParamBo.getAllLearningParamsByLearningVersion(learningVersionUID);
		if(params != null && params.size() > 0) {
			for(LearningParam param : params) {
				LearningParamDTO paramDTO = new LearningParamDTO();
				paramDTO.setLearningVersionUID(cloneLearningVersionUID);
				paramDTO.setParamIndex(param.getId().getParamIndex());
				paramDTO.setParamName(param.getId().getParamName());
				paramDTO.setParamValue(param.getParamValue());
				learningParamBo.saveObject(paramDTO);
			}
		}
		
		return versionDTO;
	}
	
	private LearningConfig getCheckActiveLearningConfig(String learningConfigUID) {
		if(learningConfigUID == null || learningConfigUID.isEmpty()) {
			throw new GenericValidationException("Invalid learningConfigUID %s", learningConfigUID);	    								    								
		}
		
		LearningConfig learningConfig = readModelWithExternalId(learningConfigUID);
		if(learningConfig == null) {
			throw new GenericValidationException("LearningConfig %s not found", learningConfigUID);
		}
		if(learningConfig.getIsDeleted()) {
			throw new GenericValidationException("LearningConfig %s is deleted", learningConfigUID);
		}
		
		return learningConfig;
	}
	
	/** clone config */
	public LearningConfigInfoDTO clone(String learningConfigUID, String learningVersionUID, LearningConfigDTO obj) {
		

		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
		} else {			
			learningVersion = getLearningConfigLatestVersion(learningConfigUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Could not find latest version for learningConfigUID %s", learningConfigUID);
			}
			learningVersionUID = learningVersion.getId();
		}
		
		if(obj == null) {
			obj = new LearningConfigDTO();
		} 
		
		// create/validate clone new UIDs
		if(obj.getLearningConfigUID() == null || obj.getLearningConfigUID().isEmpty()) {
			obj.setLearningConfigUID(UUID.randomUUID().toString());
		}
		
		
		String clonedLearningConfigUID = obj.getLearningConfigUID();
		if(this.readModelWithExternalId(clonedLearningConfigUID) != null) {
			throw new GenericValidationException("Clone learningConfigUID %s is already used ", clonedLearningConfigUID);
		}

		
		LearningConfigInfoDTO configDTO = obj.createLearningConfigInfoDTO();
		
		//set missing values
		if(configDTO.getIsDeleted() == null) {
			configDTO.setIsDeleted(learningConfig.getIsDeleted());
		}
		if(configDTO.getChannelUID() == null) {
			configDTO.setChannelUID(learningConfig.getChannelUID());
		}
		if(configDTO.getLearningConfigName() == null) {
			configDTO.setLearningConfigName(learningConfig.getLearningConfigName());
		}
		if(configDTO.getLearningConfigDescription() == null) {
			configDTO.setLearningConfigDescription(learningConfig.getLearningConfigDescription());
		}
		if(configDTO.getModelType() ==  null) {
			configDTO.setModelType(learningConfig.getModelType());
		}
		if(configDTO.getProductUID() == null) {
			configDTO.setProductUID(learningConfig.getProductUID());
		}
		
		configDTO = super.saveObject(configDTO);
		
		String clonedLearningVersionUID = UUID.randomUUID().toString();
		this.cloneVersion(clonedLearningConfigUID, learningVersionUID, clonedLearningVersionUID);
		
		return configDTO;
	}
		
	/** common method to update a resource */
	@Override	
	public LearningConfigInfoDTO updateObject(String id, LearningConfigInfoDTO obj) {		
		return super.updateObject(id, obj);		
	}
	
	/** common method to remove a resource */
	@Override
	public LearningConfigInfoDTO removeObject(String id) {		
		return super.removeObject(id);
	}

	@Override
	protected void transferId(LearningConfig newModel, LearningConfig oldModel) {
		newModel.setId(oldModel.getId());
		newModel.setCreatedAt(oldModel.getCreatedAt());
	}


	@Override
	protected void setId(LearningConfig model, String pk) {
		model.setId(pk);
	}
	
	@Override
	protected LearningConfig toModelAdditionalMappings(LearningConfigInfoDTO dto, LearningConfig model) {
	
    	String currentBuildUID =  dto.getCurrentLearningBuildUID();
    	String currentVersionUID = dto.getCurrentLearningVersionUID();

    	LearningBuild build = null;
    	if(currentBuildUID != null && !currentBuildUID.isEmpty()) {
    		build = learningBuildBo.readModelWithExternalId(currentBuildUID);   		
    		if (build == null) {
        		throw new GenericValidationException("Invalid currentBuildUID %s in LearningConfig", currentBuildUID);	    								    								
        	}
    		
    		if(build.getIsDeleted()) {
    			throw new GenericValidationException("Deleted currentBuildUID %s in LearningConfig", currentBuildUID);
    		}
    	}
    	
    	LearningVersion version = null;
    	if(currentVersionUID != null && !currentVersionUID.isEmpty()) {
    		 version = learningVersionBo.readModelWithExternalId(currentVersionUID);
    		
    		if (version == null) {
        		throw new GenericValidationException("Invalid currentVersionUID %s in LearningConfig", currentVersionUID);	    								    								
        	}
    	}
    	
    	if (model == null) {
    		model = new LearningConfig();
    	}
    	
    	model.setCurrentLearningBuild(build);
    	model.setCurrentLearningVersion(version); 
   
    	    	
		return model;
	}

	@Override
	protected LearningConfigInfoDTO toDTOAdditionalMappings(LearningConfig model, LearningConfigInfoDTO dto) {
		// no default converter registered so dto will be null
		if (dto == null) {
			dto = new LearningConfigInfoDTO();
		}
		
	
		dto.setLearningConfigName(model.getLearningConfigName());
		dto.setLearningConfigDescription(model.getLearningConfigDescription());
		dto.setModelType(model.getModelType());
		dto.setProductUID(model.getProductUID());
		dto.setChannelUID(model.getChannelUID());
		dto.setIsPublished(model.getIsPublished());
		dto.setIsDeployed(model.getIsDeployed());
		dto.setIsDeleted(model.getIsDeleted());
		
		LearningBuild learningBuild = model.getCurrentLearningBuild();
		if(learningBuild != null) {
			dto.setCurrentLearningBuildUID(learningBuild.getId());
			dto.setCurrentLearningBuildUIDStatus(learningBuild.getExecutionStatus());
			//List<LearningRun> learningRuns = learningRunBo.getAllLearningRunsByLearningBuild(learningBuild.getId());
			List<LearningRun> learningRuns = learningRunBo.getAllLearningScoringRunsByLearningBuild(learningBuild.getId());
			if (learningRuns != null && !learningRuns.isEmpty()) {
				LearningRun latestRun = Collections.max(learningRuns, Comparator.comparing(c -> c.getUpdatedAt()));
				dto.setLatestRunUID(latestRun.getId());
				dto.setLatestRunUIDStatus(latestRun.getExecutionStatus());
				dto.setLatestRunUIDIsPublished(latestRun.getIsPublished());
				Date dateTime = latestRun.getCreatedAt();
				if(dateTime != null) dto.setLatestRunUIDCreatedAt(convertDateTimeToString(dateTime));
				
				dateTime = latestRun.getUpdatedAt();
				if(dateTime != null) dto.setLatestRunUIDupdatedAt(convertDateTimeToString(dateTime));
				
				dateTime = latestRun.getExecutionDatetime();
				if(dateTime != null) dto.setLatestRunUIDStartDateTime(convertDateTimeToString(dateTime));
			}
		}
		
		LearningVersion learningVersion = model.getCurrentLearningVersion();
		if(learningVersion != null) {
			dto.setCurrentLearningVersionUID(learningVersion.getId());
		}
				
		return dto;
	}
	
	@Override
	protected void validateDTO(LearningConfigInfoDTO dto) {
		super.validateDTO(dto);

		List<AktanaErrorInfo> errors = new ArrayList<AktanaErrorInfo>();
		
		if ((dto.getLearningConfigUID() == null) || dto.getLearningConfigUID().length() < 1 || dto.getLearningConfigUID().length() > 80) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningConfigUID");
			err.setMessage(String.format("LearningConfig UID is required and must be between 1 and 80 characters long: %s", dto.getLearningConfigUID()));
			errors.add(err);
		}
		
		if ((dto.getLearningConfigName() != null && (dto.getLearningConfigName().length() < 1 || dto.getLearningConfigName().length() > 80))) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningConfigName");
			err.setMessage(String.format("LearningConfig name is optional and must be between 1 and 80 characters long: %s", dto.getLearningConfigName()));
			errors.add(err);
		}
		
		if ( dto.getLearningConfigDescription() != null &&  dto.getLearningConfigDescription().length() > 255) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("learningConfigDescription");
			err.setMessage(String.format("LearningConfig description is optional and must be less than 255 characters long: %s", dto.getLearningConfigName()));
			errors.add(err);
		}
		
		List<String> channels = Arrays.asList("VISIT_CHANNEL", "SEND_CHANNEL", "WEB_INTERACTIVE_CHANNEL");	
		if( dto.getChannelUID() != null && !channels.contains(dto.getChannelUID())) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("channelUID");
			err.setMessage(String.format("LearningConfig channelUID is optional and must be one of 'VISIT_CHANNEL', 'SEND_CHANNEL' or 'WEB_INTERACTIVE_CHANNEL': "+dto.getChannelUID()));
			errors.add(err);
		}
		
		if( dto.getProductUID() != null && dto.getProductUID().equals("")) {
			AktanaErrorInfo err = new AktanaErrorInfo();
			
			err.setField("productUID");
			err.setMessage(String.format("LearningConfig channelUID is optional but must be valid if specified': "+dto.getProductUID()));
			errors.add(err);
		}
		
		if (errors.size() > 0) {
			throw new GenericValidationException(400, 0, errors, "Invalid input. See details");
		}
				
	}

	/**
	 * Returns the latest (latest createdAt) draft (isFinalized = false) version for a given config.
	 * Returns null is no draft is found.
	 */	
	private LearningVersion getLearningConfigDraftVersion(String learningConfigUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningConfigUID", learningConfigUID);
		
		List<LearningVersion> versions = learningVersionBo.getDao().findWithNamedQuery("LearningVersion.custom.findDraftsByLearningConfigOrderByCreated", map);
		
		if(versions != null && versions.size() > 0) {
			return versions.get(0);
		}
		
		return null;
	}
	
	private List<LearningVersion> getLearningConfigVersionsOrderedByCreatedAt(String learningConfigUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningConfigUID", learningConfigUID);
		
		return learningVersionBo.getDao().findWithNamedQuery("LearningVersion.custom.findVersionsByLearningConfigOrderByCreated", map);
		
	}
	
	private List<LearningBuild> getLearningVersionBuildsOrderedByCreatedAt(String learningVersionUID) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("learningVersionUID", learningVersionUID);
		
		return learningBuildBo.getDao().findWithNamedQuery("LearningBuild.custom.findBuildsByLearningVersionOrderByCreated", map);
		
	}
	
	/**
	 * Returns the latest (latest createdAt) version for a given config.
	 * Returns null is no version is found.
	 */	
	private LearningVersion getLearningConfigLatestVersion(String learningConfigUID) {
		
		List<LearningVersion> versions = this.getLearningConfigVersionsOrderedByCreatedAt(learningConfigUID);
		if(versions != null && versions.size() > 0) {
			return versions.get(0);
		}
		
		return null;
	}

/**
 * Find latest draft version, create new draft if not found. Optionally remove some/all params and add new ones.
 * 
 * @param learningConfigUID
 * @param learningVersionUID optional. if not specified use latest draft. clone latest version if draft not found. create new draft if no versions exist
 * @param params list of LearningParamElementDTO
 * @param deleteOldParamsUID  UID is "paramName~paramIndex". Omit paramIndex ("paramName~") to delete entire param. Omit paramName to delete all params ("~"). To not delete set to null.
 * @return
 */
	public List<LearningParamDTO> addParams(String learningConfigUID, String learningVersionUID, List<LearningParamElementDTO> params, String deleteOldParamsUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(learningVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
			if(learningVersion.getIsFinalized()) {
				throw new GenericValidationException("LearningVersionUID %s is finalized - cannot edit.", learningVersionUID);
			}
		} else {			
			learningVersion = this.getLearningConfigDraftVersion(learningConfigUID);
			if(learningVersion == null) {
				
				// no draft version is available - create new
				learningVersionUID = UUID.randomUUID().toString();
				LearningVersionInfoDTO versionDTO;
				
				// look for any latest version
				LearningVersion latestLearningVersion = this.getLearningConfigLatestVersion(learningConfigUID);
				if(latestLearningVersion == null) {
					
					// no versions - should not happen
					versionDTO = new LearningVersionInfoDTO();
					versionDTO.setLearningConfigUID(learningConfigUID);
					versionDTO.setLearningVersionUID(learningVersionUID);
					versionDTO.setIsDeployed(false);
					versionDTO.setIsDeleted(false);
					versionDTO.setIsFinalized(false);
					learningVersionBo.saveObject(versionDTO);
				} else {
					
					// clone latest version
					versionDTO = this.cloneVersion(learningConfigUID, latestLearningVersion.getId(), learningVersionUID);
				}
	
			} else {
				
				// draft found - use it
				learningVersionUID = learningVersion.getId();
			}
		}
		
		if(deleteOldParamsUID != null) {
			learningParamBo.deleteModelsWithCompositeExternalIdPattern(learningVersionUID+"~"+deleteOldParamsUID);
			learningParamBo.getDao().getSession().flush();
			learningParamBo.getDao().getSession().clear();
		}
		
		List<LearningParamDTO> respList = new ArrayList<LearningParamDTO>();
		if(params != null && params.size() > 0) {
			for(LearningParamElementDTO param : params) {
				respList.add(learningParamBo.saveObject(param.createLearningParam(learningVersionUID)));
			}
		}
	
		return respList;	
	}
	
	public List<LearningParamDTO> getParams(String learningConfigUID, String learningVersionUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(learningVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
		} else {			
			learningVersion = this.getLearningConfigLatestVersion(learningConfigUID);
			if(learningVersion == null) {
				throw new GenericValidationException("LearningConfigUID %s does not have a version", learningConfigUID);
			}
			learningVersionUID = learningVersion.getId();
		}
		
		List<LearningParam> learningParams = learningParamBo.getAllLearningParamsByLearningVersion(learningVersionUID);
		List<LearningParamDTO> learningParamDTOs = new ArrayList<LearningParamDTO>();
		for(LearningParam learningParam : learningParams) {
			learningParamDTOs.add(learningParamBo.toDTO(learningParam));
		}
		
		return learningParamDTOs;
	}
	
	public List<LearningBuildDTO> getBuilds(String learningConfigUID, String learningVersionUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(learningVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
		} else {			
			learningVersion = this.getLearningConfigLatestVersion(learningConfigUID);
			if(learningVersion == null) {
				throw new GenericValidationException("LearningConfigUID %s does not have a version", learningConfigUID);
			}
			learningVersionUID = learningVersion.getId();
		}
		
		List<LearningBuild> learningBuilds = learningBuildBo.getAllLearningBuildsByLearningVersion(learningVersionUID);
		List<LearningBuildDTO> learningBuildDTOs = new ArrayList<LearningBuildDTO>();
		for(LearningBuild learningBuild : learningBuilds) {
			learningBuildDTOs.add(learningBuildBo.toDTO(learningBuild));
		}
		
		return learningBuildDTOs;
	}
	
	public List<LearningRunDTO> getRuns(String learningConfigUID, String learningVersionUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(learningVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
		} else {			
			learningVersion = this.getLearningConfigLatestVersion(learningConfigUID);
			if(learningVersion == null) {
				throw new GenericValidationException("LearningConfigUID %s does not have a version", learningConfigUID);
			}
			learningVersionUID = learningVersion.getId();
		}
		
		List<LearningRun> learningRuns = learningRunBo.getAllLearningRunsByLearningVersion(learningVersionUID);
		List<LearningRunDTO> learningRunDTOs = new ArrayList<LearningRunDTO>();
		for(LearningRun learningRun : learningRuns) {
			learningRunDTOs.add(learningRunBo.toDTO(learningRun));
		}
		
		return learningRunDTOs;
	}
	
	public List<LearningFileInfoDTO> getFiles(String learningConfigUID, String learningVersionUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion learningVersion = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			learningVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(learningVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(learningVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			String learningVersionConfigUID = learningVersion.getLearningConfig().getId();
			if(!learningConfigUID.equals(learningVersionConfigUID)) {
				throw new GenericValidationException("LearningConfigUID %s does not match config %s of learningVersionUID %s", learningVersionUID, learningVersionConfigUID, learningVersionUID);
			}
			
		} else {			
			learningVersion = this.getLearningConfigLatestVersion(learningConfigUID);
			if(learningVersion == null) {
				throw new GenericValidationException("LearningConfigUID %s does not have a version", learningConfigUID);
			}
			learningVersionUID = learningVersion.getId();
		}
		
		List<LearningFile> learningFiles = learningFileBo.getAllLearningFilesByLearningVersion(learningVersionUID);
		List<LearningFileInfoDTO> learningFileDTOs = new ArrayList<LearningFileInfoDTO>();
		for(LearningFile learningFile : learningFiles) {
			learningFileDTOs.add(learningFileBo.toDTO(learningFile));
		}
		
		return learningFileDTOs;
	}
	
	public List<LearningVersionInfoDTO> getVersions(String learningConfigUID) {
		
		getCheckActiveLearningConfig(learningConfigUID);
		
		List<LearningVersion> versions = this.getLearningConfigVersionsOrderedByCreatedAt(learningConfigUID);
		List<LearningVersionInfoDTO> dtos = new ArrayList<LearningVersionInfoDTO>();
		if(versions != null && versions.size() > 0) {
			for(LearningVersion version : versions) {
				dtos.add(learningVersionBo.toDTO(version));
			}
		}
		
		return dtos;
	}
	
	public LearningConfigInfoDTO publish(String learningConfigUID) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		if(!learningConfig.getIsDeployed()) {
			throw new GenericValidationException("LearningConfigUID %s must be deployed first", learningConfigUID);
		}
		
		if(learningConfig.getIsPublished()) {
			throw new GenericValidationException("LearningConfigUID %s already published", learningConfigUID);
		}

		//do model specific publish (e.g. call DSE)
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningConfig.getModelType());
		learningModelHandler.publish(learningConfig);
		
		// set published
		learningConfig.setIsPublished(true);
		learningConfig = this.saveModel(learningConfig);								
	
		return this.toDTO(learningConfig);
	}
	
	public LearningConfigInfoDTO unpublish(String learningConfigUID) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		if(!learningConfig.getIsPublished()) {
			throw new GenericValidationException("LearningConfigUID %s is not published", learningConfigUID);
		}

		//do model specific unpublish (e.g. call DSE)
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningConfig.getModelType());
		learningModelHandler.unpublish(learningConfig);

		// set published
		learningConfig.setIsPublished(false);
		learningConfig = this.saveModel(learningConfig);								
	
		return this.toDTO(learningConfig);
	}
	
	public LearningBuildDTO deploy(String learningConfigUID, String learningVersionUID, String learningBuildUID) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		LearningVersion deployVersion;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			deployVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(deployVersion == null) {
				throw new GenericValidationException("Invalid learningVersionUID %s", learningVersionUID);
			}
			if(deployVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVerison %s is deleted", learningVersionUID);
			}
			
			if(!deployVersion.getLearningConfig().equals(learningConfig)) {
				throw new GenericValidationException("LearningVersionUID %s does not belong to learningConfigUID %s", learningVersionUID, learningConfigUID);
			}
			
			if(!deployVersion.getIsFinalized()) {
				throw new GenericValidationException("LearningVerison %s is not built", learningVersionUID);
			}
			
		} else {
			// get current version - if set
			deployVersion = learningConfig.getCurrentLearningVersion();
			if(deployVersion == null) {
				throw new GenericValidationException("No candidate version found for learningConfigUID %s", learningConfigUID);
			}
		}
		
		String deployVersionUID = deployVersion.getId();
		LearningBuild deployBuild = null;
		
		if(learningBuildUID != null) {
			deployBuild = learningBuildBo.readModelWithExternalId(learningBuildUID);
			if(deployBuild == null) {
				throw new GenericValidationException("Build %s not found", learningBuildUID);
			}
			
			if(deployBuild.getIsDeleted()) {
				throw new GenericValidationException("Build %s is deleted", learningBuildUID);
			}
			
			if(!deployBuild.getLearningVersion().equals(deployVersion)) {
				throw new GenericValidationException("LearningBuildUID %s does not belong to learningVersionUID %s", learningBuildUID, learningVersionUID);
			}
		} else {
			deployBuild = learningBuildBo.getLatestSuccessfullBuildByLearningVersionOrderedByExecutionDate(deployVersionUID);
			if(deployBuild == null) {
				throw new GenericValidationException("No successful build found for learningConfigUID %s learningVerison %s", learningConfigUID, deployVersionUID);
			}
		}
		
		//generate and install properties file, update objects
		
		this.deployPropertiesFile(learningConfig, deployVersion, deployBuild);
		
		return learningBuildBo.toDTO(deployBuild);
	}
	
	public LearningConfigInfoDTO undeploy(String learningConfigUID) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		if(!learningConfig.getIsDeployed()) {
			throw new GenericValidationException("LearningConfig %s is not deployed", learningConfigUID);
		}
		
		if(learningConfig.getIsPublished()) {
			throw new GenericValidationException("LearningConfig %s is published and cannot be undeployed", learningConfigUID);
		}

		//remove deployed properties file, update objects
		this.undeployPropertiesFile(learningConfig);
		
		return this.toDTO(learningConfig);
	}
	
	public LearningBuildDTO build(String learningConfigUID, String learningVersionUID, boolean deploy, boolean callRundeck) {
		return learningBuildBo.toDTO(buildModel(learningConfigUID, learningVersionUID, deploy, callRundeck));
	}

	public LearningBuild buildModel(String learningConfigUID, String learningVersionUID, boolean deploy, boolean callRundeck) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		LearningModelHandler.getCheckLearningModelTypeEnum(learningConfig.getModelType());

		
		LearningVersion buildVersion;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			buildVersion = learningVersionBo.readModelWithExternalId(learningVersionUID);
			if(buildVersion == null) {
				throw new GenericValidationException("Invalid learningVersion %s", learningVersionUID);
			}
			if(buildVersion.getIsDeleted()) {
				throw new GenericValidationException("LearningVersion %s is deleted", learningVersionUID);
			}
			if(!buildVersion.getLearningConfig().equals(learningConfig)) {
				throw new GenericValidationException("LearningVersion %s does not belong to learningConfigUID %s", learningVersionUID, learningConfigUID);
			}
			
		} else {
			// get latest version
			List<LearningVersion> versions = this.getLearningConfigVersionsOrderedByCreatedAt(learningConfigUID);
			if(versions == null || versions.size() == 0) {
				throw new GenericValidationException("No versions found for learningConfig %s", learningConfigUID);
			}		
			buildVersion = versions.get(0);
		}
		
		// get current version - if set
		LearningVersion currentVersion = learningConfig.getCurrentLearningVersion();
			
		// current version needs to be updated
		if(currentVersion == null || !buildVersion.equals(currentVersion)) {
			
			// update latest version
			buildVersion.setIsFinalized(true);
			buildVersion.setCurrentStartDateTime(new Date());
			learningVersionBo.saveModel(buildVersion);
			
			// set latest version as current
			learningConfig.setCurrentLearningVersion(buildVersion);
			learningConfig = this.saveModel(learningConfig);
		}
		
		
		LearningBuild learningBuild = doBuild(learningConfig, buildVersion, deploy, callRundeck);
		
		// set latest build as current
		learningConfig.setCurrentLearningBuild(learningBuild);
		learningConfig = this.saveModel(learningConfig);
		
		return learningBuild;
	}
	
	private String createPropertiesFileData(LearningVersion learningVersion, LearningBuild learningBuild) {
		String propertiesString = "";
		List<LearningParam> learningParams = learningParamBo.getAllLearningParamsByLearningVersion(learningVersion.getId());
		if(learningParams != null) {
			HashMap<String, String> params = new HashMap<String, String>();
			for(LearningParam learningParam : learningParams) {
				String name = learningParam.getId().getParamName();
				String value = params.get(name);
				if(value == null) {
					value = "";
				} else {
					value += ";";
				}
				value += learningParam.getParamValue();
				params.put(name, value);
			}
			params.put("modelType", learningVersion.getLearningConfig().getModelType());
			params.put("productUID", learningVersion.getLearningConfig().getProductUID());
			params.put("channelUID", learningVersion.getLearningConfig().getChannelUID());
			params.put("buildUID", learningBuild.getId());
			params.put("versionUID", learningVersion.getId());
			params.put("configUID", learningVersion.getLearningConfig().getId());
			
			for(Map.Entry<String, String> entry : params.entrySet()) propertiesString += entry.getKey() + "=" + entry.getValue() + "\n";			
		}
		return propertiesString;
	}
		
	private void updateObjectsForDeploy(LearningConfig learningConfig, LearningVersion nextDeployedLearningVersion, LearningBuild nextDeployedLearningBuild) {
		
		if(learningConfig.getIsDeployed()) {
			// this _should_ return only one build
			List<LearningBuild> learningBuilds = learningBuildBo.getAllDeployedLearningBuildsByLearningConfig(learningConfig.getId());
			if(learningBuilds != null && !learningBuilds.isEmpty()) {
				for(LearningBuild deployedLearningBuild : learningBuilds) {			
					if(!deployedLearningBuild.equals(nextDeployedLearningBuild)) {

						deployedLearningBuild.setIsDeployed(false);
						learningBuildBo.saveModel(deployedLearningBuild);
						LearningVersion deployedLearningVersion = deployedLearningBuild.getLearningVersion();

						if(!deployedLearningVersion.equals(nextDeployedLearningVersion)) {
							deployedLearningVersion.setIsDeployed(false);
							learningVersionBo.saveModel(deployedLearningVersion);
						}
					}
				}
			}
		}
		
		// update deploy build, version, and config
		if(nextDeployedLearningBuild != null && !nextDeployedLearningBuild.getIsDeployed()) {
			nextDeployedLearningBuild.setIsDeployed(true);
			learningBuildBo.saveModel(nextDeployedLearningBuild);
		}
		
		if(nextDeployedLearningVersion != null && !nextDeployedLearningVersion.getIsDeployed()) {
			nextDeployedLearningVersion.setIsDeployed(true);
			learningVersionBo.saveModel(nextDeployedLearningVersion);
		}
		
		if(learningConfig.getIsDeployed()) {
			
			// undeploy
			if(nextDeployedLearningBuild == null) {
				learningConfig.setIsDeployed(false);
				this.saveModel(learningConfig);
			}
		} else {
			
			// deploy
			if(nextDeployedLearningBuild != null) {
				learningConfig.setIsDeployed(true);
				this.saveModel(learningConfig);
			}
		}
	
	}
	
	
	private void installPropertiesFile(LearningConfig learningConfig, LearningVersion learningVersion, LearningBuild learningBuild) {
		String propertiesString = createPropertiesFileData(learningVersion, learningBuild);
		
		S3PartialPaths s3PartialPath = getPartialPaths("learningProperties");
		String targetPath = s3PartialPath.getPathPrefix() + learningBuild.getId() + s3PartialPath.getPathSuffix();
				
		LOGGER.info("Installing learning.properties file with s3 key "+targetPath);
		LOGGER.info("Learning properties content: \n"+propertiesString);
		
		try { LearningS3Connector.getInstance().callUpload(targetPath, propertiesString); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage());	}
	}
		
	private void deployPropertiesFile(LearningConfig learningConfig, LearningVersion learningVersion, LearningBuild learningBuild) {
		String propertiesString = createPropertiesFileData(learningVersion, learningBuild);

		S3PartialPaths s3PartialPath = getPartialPaths("learningPropertiesNightly");
		String targetPath = s3PartialPath.getPathPrefix() + learningConfig.getId() + s3PartialPath.getPathSuffix();
						
		LOGGER.info("Deploying learning.properties file with s3 key: " + targetPath);
		LOGGER.info("Learning properties content: \n"+propertiesString);
		
		try { LearningS3Connector.getInstance().callUpload(targetPath, propertiesString); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }
		
		this.updateObjectsForDeploy(learningConfig, learningVersion, learningBuild);
	}
	
	private void undeployPropertiesFile(LearningConfig learningConfig) {

		S3PartialPaths s3PartialPath = getPartialPaths("learningPropertiesNightly");
		String targetPath = s3PartialPath.getPathPrefix() + learningConfig.getId() + s3PartialPath.getPathSuffix();
		
		LOGGER.info("Undeploying learning.properties file with s3 key "+targetPath);
		
		try { LearningS3Connector.getInstance().callDelete(targetPath); }
		catch (LearningS3ConnectionException e) { throw new GenericValidationException(e.getMessage()); }	
		
		this.updateObjectsForDeploy(learningConfig, null, null);
	}
		
	private LearningBuild doBuild(LearningConfig learningConfig, LearningVersion learningVersion, boolean deploy, boolean callRundeck) {
		
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningConfig.getModelType());
	
		String learningBuildUID = UUID.randomUUID().toString();
		String status = learningModelHandler.isBuildRequired()?"running":"success";
		
		LearningBuild learningBuild = new LearningBuild();
		learningBuild.setLearningConfig(learningConfig);
		learningBuild.setLearningVersion(learningVersion);
		learningBuild.setId(learningBuildUID);
		learningBuild.setIsDeployed(false);
		learningBuild.setIsDeleted(false);
		learningBuild.setExecutionStatus(status);
		learningBuild.setExecutionDatetime(new Date());
		learningBuildBo.saveModel(learningBuild);
		
		//generate and install properties file
		installPropertiesFile(learningConfig, learningVersion, learningBuild);
		
		// call rundeck to build if required
		if (!callRundeck) return learningBuild;
		if(learningModelHandler.isBuildRequired()) {
			try {
				String deployStr = "-DEPLOY_ON_SUCCESS TRUE";
				String modelName = learningModelHandler.getModelName();
				String driverName = learningModelHandler.getBuildDriver();
				RundeckAPIEnum apiEnum = learningModelHandler.getRundeckAPIEnumForEMR();
				String body = "{\"argString\": \"-BUILD_UID "+learningBuild.getId()+" -CONFIG_UID "+learningConfig.getId()+" -SelectedModule "+modelName+" -SelectedDriver "+driverName+" -TASKGROUP1 performLearningrunFlow"+(deployStr == null?"":(" "+deployStr))+"\"}";
				String result = RundeckConnector.getInstance().callPost(apiEnum, body, null);
				LOGGER.info(apiEnum.name()+": "+result);
			} catch (RundeckConnectionException e) {
				learningBuild.setExecutionStatus("failed");
				learningBuildBo.saveModel(learningBuild);
				throw new GenericValidationException(e.getStatus(), "Rundeck call failed with: "+e.getMessage());	 
			}
		} else {
			if(deploy) this.deployPropertiesFile(learningConfig, learningVersion, learningBuild);
			
		}
		
		return learningBuild;
	}

	public LearningRunDTO score(String learningConfigUID, String learningVersionUID) {
		
		LearningConfig learningConfig = getCheckActiveLearningConfig(learningConfigUID);
		
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningConfig.getModelType());
		
		LearningBuild learningBuild = null;
		if(learningVersionUID != null && !learningVersionUID.isEmpty()) {
			List<LearningBuild> builds = this.getLearningVersionBuildsOrderedByCreatedAt(learningVersionUID);
			if(builds == null || builds.size() == 0) {
				throw new GenericValidationException("No builds found for learningVersionUID %s", learningVersionUID);
			}			
			learningBuild = builds.get(0);
			
		} else {			
			 learningBuild = learningConfig.getCurrentLearningBuild();
			 if(learningBuild == null) {
				 throw new GenericValidationException("No builds found for learningConfigUID %s", learningConfigUID);
			 }
		}
		
		String learningRunUID = UUID.randomUUID().toString();
		LearningRun learningRun = new LearningRun();
		learningRun.setLearningConfig(learningConfig);
		learningRun.setLearningVersion(learningBuild.getLearningVersion());
		learningRun.setLearningBuild(learningBuild);
		learningRun.setId(learningRunUID);
		learningRun.setRunType(learningConfig.getModelType());
		learningRun.setExecutionStatus("running");
		learningRun.setExecutionDatetime(new Date());
		learningRun.setIsPublished(false);
		learningRunBo.saveModel(learningRun);
		
		// Call rundeck to score.
		try {
			RundeckAPIEnum rundeckAPIEnum = learningModelHandler.getRundeckAPIEnumForEMR();
			String modelName = learningModelHandler.getModelName();
			String driverName = learningModelHandler.getScoreDriver();
			String body = "{\"argString\": \"-BUILD_UID "+learningBuild.getId()+" -RUN_UID "+learningRunUID+" -CONFIG_UID "+learningConfig.getId()+" -SelectedModule "+modelName+" -SelectedDriver "+driverName+" -TASKGROUP1 performLearningrunFlow\"}";
			String result = RundeckConnector.getInstance().callPost(rundeckAPIEnum, body, null);
			LOGGER.info(rundeckAPIEnum.name()+": "+result);
		} catch (RundeckConnectionException e) {
			
			learningRun.setExecutionStatus("failed");
			learningRunBo.saveModel(learningRun);
		
			throw new GenericValidationException(e.getStatus(), "Rundeck call failed with: "+e.getMessage());			
		}
		
		
		return learningRunBo.toDTO(learningRun);
	}

	public List<LearningConfigInfoDTO> getConfigsByType(String type, Integer page, Integer rows) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(type==null) {type="";} // type when empty selected, set to "%" to fetch all types
		map.put("modelType", "%"+type);
		Integer offSet = super.getOffset(page, rows);
		Integer limit = super.getLimit(page, rows);
		List<LearningConfig> configs = getDao().findWithNamedQuery("LearningConfig.custom.findBymodelType", map, offSet, offSet+limit);
		List<LearningConfigInfoDTO> learningConfigInfoDTOs = new ArrayList<LearningConfigInfoDTO>();
		learningConfigInfoDTOs = toDTO(configs);
		return learningConfigInfoDTOs;
	}
	
//	public CrontabDTO getJobCrontab(String learningConfigUID, String jobType) {
//		
//		CrontabDTO crontabDTO = new CrontabDTO();
//		// validate inputs and check that config is deployed
//		LearningConfig learningConfig = this.readModelWithExternalId(learningConfigUID);
//		if (learningConfig == null) throw new GenericValidationException("No LearningConfig with id: %s",learningConfigUID);			
//		if (!"build".equals(jobType) && !"score".equals(jobType)) throw new GenericValidationException("%s jobType not allowed",jobType);
//		// no job for config if undeployed. Return dto with null fields
//		if (!learningConfig.getIsDeployed()) return crontabDTO;
//
//		// get handler, and correct RundeckAPIEnum
//		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, learningConfig.getModelType());
//		RundeckAPIEnum rundeckAPIEnum = "build".equals(jobType) 
//				? learningModelHandler.getRundeckAPIEnumForBuildDefinition() : learningModelHandler.getRundeckAPIEnumForScoreDefinition();
//		
//		// fetch rundeck job definition .yaml file
//		String jobDefinitionYaml = "";
//		try {
//			jobDefinitionYaml = RundeckConnector.getInstance().callApi(rundeckAPIEnum, "GET", "application/yaml", null);
//		} catch (RundeckConnectionException e) {
//			throw new GenericValidationException(e.getStatus(), "Rundeck call failed with: "+e.getMessage());			
//		}
//
//		// convert .yaml string to java object
//		YamlReader reader = new YamlReader(jobDefinitionYaml);
//		Object jobYamlObject = new Object();
//		try {
//			jobYamlObject = reader.read();
//		} catch (Exception e) {
//			throw new GenericValidationException("cannot read job definition yaml response");
//		}
//	
//		return getJobCrontab(jobYamlObject);
//	}
	
//	private CrontabDTO getJobCrontab(Object jobYamlObject) {
//
//		// jobs yaml - deserialize to get schedule: 
//		// joblist -> jobDefMap.get("schedule") -> scheduleMap.get("time") -> timeMap.get("minutes"), timeMap.get("hours")
//		//		   -> jobDefMap.get("name")  |	-> scheduleMap.get("weekday")
//		//		   -> jobDefMap.get("id")	 |	-> scheduleMap.get("month")
//		//									 |	-> scheduleMap.get("year")
//		List joblist = (List)jobYamlObject;
//		Map jobDefMap = (Map)joblist.get(0);		
//		Map scheduleMap = (Map)jobDefMap.get("schedule");
//		
//		String jobNameString = (String)jobDefMap.get("name");
//		String jobIdString = (String)jobDefMap.get("id");
//		
//		// make dto with null schedule, if not a scheduled job, will return this dto
//		CrontabDTO crontabDTO = new CrontabDTO();
//		crontabDTO.setJobName(jobNameString);
//		crontabDTO.setJobId(jobIdString);		
//
//		// return job with null schedule
//		if (scheduleMap == null) return crontabDTO;
//		
//		Map timMap = (Map)scheduleMap.get("time");
//		Map weekdayMap = (Map)scheduleMap.get("weekday");
//		
//		// collect schedule attributes
//		String hourString = (String)timMap.get("hour");
//		String minuteString = (String)timMap.get("minute");
//		String weekdayString = (String)weekdayMap.get("day");
//
//		// make crontab string
//		// human readable: executes at  <hourString>:<minuteString>:00 every day of every month on <weekdayString> 
//		String crontabString = "0 " + minuteString + " " + hourString + " * * " + weekdayString;
//
//		crontabDTO.setCrontab(crontabString); // reset from null
//		
//		return crontabDTO;
//	}

	private List<LearningConfig> getConfigsToRebuild(String modelType) {
		Boolean isMSORebuild = modelType.equals("MSO");
		List<LearningConfig> configsToRebuild = new ArrayList<LearningConfig>();
		String algorithmDTOArrayString = "";
		try { // call dse api to get configs from messageAlg/ tteAlg table
			if(isMSORebuild) algorithmDTOArrayString = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_MESSAGE_ALGORITHM);
			else algorithmDTOArrayString = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_TTE_ALGORITHM);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}	
		// convert json array strings into java objects
		// collect all configs that need rebuilding		
		if (isMSORebuild) {
			Type messageAlgorithmDTOCollectionType = new TypeToken<Collection<MessageAlgorithmDTO>>(){}.getType();
			List<MessageAlgorithmDTO> messageAlgorithmDTOs = new ArrayList<MessageAlgorithmDTO>(new Gson().fromJson(algorithmDTOArrayString, messageAlgorithmDTOCollectionType));
			messageAlgorithmDTOs.forEach(messageAlgorithmDTO -> { if(messageAlgorithmDTO.getIsActive() && !messageAlgorithmDTO.getIsDeleted()) configsToRebuild.add(readModelWithExternalId(messageAlgorithmDTO.getMessageAlgorithmUID())); });
		} else {
			Type timeToEngageAlgorithmDTOCollectionType = new TypeToken<Collection<TimeToEngageAlgorithmDTO>>(){}.getType();
			List<TimeToEngageAlgorithmDTO> timeToEngageAlgorithmDTOs = new ArrayList<TimeToEngageAlgorithmDTO>(new Gson().fromJson(algorithmDTOArrayString, timeToEngageAlgorithmDTOCollectionType));
			timeToEngageAlgorithmDTOs.forEach(tteAlgorithmDTO -> { if(tteAlgorithmDTO.getIsActive() && !tteAlgorithmDTO.getIsDeleted()) configsToRebuild.add(readModelWithExternalId(tteAlgorithmDTO.getTteAlgorithmUID())); });
		}
		return configsToRebuild;
	}

	public List<LearningBuildDTO> rebuild(String modelType) {
		List<LearningBuild> learningBuilds = new ArrayList<LearningBuild>();
		List<LearningConfig> configsToRebuild = getConfigsToRebuild(modelType); // list of list of configs to rebuild (one list for mso, one for tte)		
		if(configsToRebuild.size() < 1) return null; // no config to rebuild for modelType			
		LearningModelHandler learningModelHandler = LearningModelHandler.getHandlerInstance(LOGGER, configsToRebuild.get(0).getModelType()); // used for api enum and driver/module names
		RundeckAPIEnum apiEnum = learningModelHandler.getRundeckAPIEnumForEMR();
		String modelName = learningModelHandler.getModelName();
		String driverName = learningModelHandler.getBuildDriver();	
		List <String> flowJsonList = new ArrayList<String>();

		configsToRebuild.forEach(learningConfig -> { // loop through each config of a modelType			
			List<LearningVersion> learningVersions = learningVersionBo.getAllDeployedLearningVersionsByLearningConfig(learningConfig);
			if(learningVersions.size() < 1) return;
			LearningVersion deployedVersion = learningVersions.get(0);
			LearningBuild learningBuild = buildModel(learningConfig.getId(), deployedVersion.getId(), true, false); // call build with flag to not execute rundeck
			learningBuilds.add(learningBuild); 
			flowJsonList.add("{moduleToRun:"+modelName+",driverToRun:"+driverName+",BUILD_UID:"+learningBuild.getId()+",CONFIG_UID:"+learningConfig.getId()+",DEPLOY_ON_SUCCESS:true}");
		});			
		String body = "{\"argString\": \"-FLOW_JSON "+String.join(",", flowJsonList)+" -TASKGROUP1 performLearningrunFlow\"}"; // concat FLOW_JSONs, add as argument
		try { // make rundeck call
			String result = RundeckConnector.getInstance().callPost(apiEnum, body, null);
			LOGGER.info(apiEnum.name()+": "+result);
		} catch (RundeckConnectionException e) {
			learningBuilds.forEach(learningBuild -> { // cleanup if rundec call failed
				learningBuild.setExecutionStatus("failed");
				learningBuildBo.saveModel(learningBuild);
			});
			String errorString = "Rundeck call failed with: "+e.getMessage() + " \n apiEnum: " + apiEnum + " \n body: " + body;
			throw new GenericValidationException(e.getStatus(), errorString);	 
		}
		return learningBuildBo.toDTO(learningBuilds);
	} 
}
