/*****************************************************************
*
* @author $Author$
* @version $Revision$ on $Date$ by $Author$
*
* Copyright (C) 2012-2014 Aktana Inc.
*
*****************************************************************/
package com.aktana.learning.api.bo.special;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.util.AktanaParameter;
import com.aktana.learning.persistence.models.impl.SysParameter;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* <p>
* SysParameterBo is intended as the primary method of support within
* the Aktana server for access to post-deployment configurable system parameters.
* <\p><p>
* Note that this is an "all-static" class. This approach was taken in order to
* optimize performance by cacheing all SysParameters.
* <\p><p>
* Example usage: String timezone = SysParameterBo.getStringValue("defaultTimezoneId");
* <\p><p>
* This BO provides two categories of methods:
* <ol>
* 	<li>A set of read-only "get*Value*" methods by which application 
* 		code can fetch the value for a specified parameter name.
* 	<li>An initial (incomplete) set of methods intended for use by
* 		a future front end screen that allows an authorized user to 
* 		to change system parameter values.
* <\ol>
* <p>
*/
public class SysParameterBo {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysParameterBo.class);
	
	private static HashMap<String,AktanaParameter> CACHED_PARAMETERS;

	private static final Map<String,String> REGISTERED_SYS_PARAMETERS = new TreeMap<String,String>();
	
	//-------------------- static block --------------------
	
	static {
		// this is to log any errors in the sysParameters db table at server start-up
		SysParameterBo.refreshCache();
		// DEVELOPERS should "register" each new sysParameter below,
		// and add a short description of how the param value is used.

		register("apitokenExpiryMinutes", 				"Number of minutes until Aktana API security token expirees");
		register("apitokenSecret", 						"String value needed to gain access to the Aktana API");
		register("defaultLanguageTag", 					"Default language for this Customer");
		register("defaultTimezoneId", 					"Default timezone for this Customer");
		register("partnerAPIMock", 						"true --> all PartnerAPI services will return static example JSON responses");
	
	
	}
	
	private static void register(String paramName, String paramDescription) {
		REGISTERED_SYS_PARAMETERS.put(paramName, paramDescription);
	}
	
	//-------------------- private constructor: is never called  --------------------
	
	private SysParameterBo() {
	}
	
	//-------------------- DAO provider --------------------
	
	private static GenericDao<SysParameter,Integer> getSysParameterDao() {
		GenericDao<SysParameter, Integer> sysParameterDao = new GenericDaoImpl<SysParameter, Integer>(SysParameter.class);
		sysParameterDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return sysParameterDao;
	}
	
	//-------------------- refreshCache --------------------

	public static synchronized void refreshCache() {
		refreshCache(false);
	}

	public static synchronized void refreshCache(Boolean doRepersistAsNeeded) {
		LOGGER.info("Initializing/refreshing cached SysParameters:");

		// Do a query to get all the current SysParameters
		List<SysParameter> sysParams = getSysParameterDao().findAll();
		
		// Insert all the SysParameters into a private/local static cache
		CACHED_PARAMETERS = new HashMap<String,AktanaParameter>();
		for (SysParameter sysParam : sysParams) {
			try {
				AktanaParameter aktParam = new AktanaParameter(sysParam);
				if (isRegistered(sysParam.getName())) {
					aktParam.recordIsRegistered();
				}
				CACHED_PARAMETERS.put(sysParam.getName(), aktParam);
				LOGGER.info("\t"+aktParam.getName()+"\t=\t"+aktParam.getValue());
			}
			catch (Exception e) {
				LOGGER.error("Database contains invalid definition for sysParameter \"{}\": {}",
						sysParam.getName(), e.getMessage());
			}
		}
	}

	
	
	
	//-------------------- Registry methods --------------------

	public static boolean isRegistered(String paramName) {
		return REGISTERED_SYS_PARAMETERS.keySet().contains(paramName);
	}
	
	public static String getDescription(String paramName) {
		return REGISTERED_SYS_PARAMETERS.get(paramName);
	}
	
	//-------------------- get value methods --------------------

	public static String getStringValue(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).stringValue() : null;
	}

	public static String[] getStringValues(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).stringValues() : null;
	}

	public static Boolean getBooleanValue(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).booleanValue() : null;
	}

	public static Boolean[] getBooleanValues(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).booleanValues() : null;
	}

	public static Integer getIntegerValue(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).integerValue() : null;
	}

	public static Integer[] getIntegerValues(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).integerValues() : null;
	}

	public static Long getLongValue(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).longValue() : null;
	}

	public static Long[] getLongValues(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).longValues() : null;
	}

	public static Double getDoubleValue(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).doubleValue() : null;
	}

	public static Double[] getDoubleValues(String paramName) {
		return CACHED_PARAMETERS.containsKey(paramName) ? CACHED_PARAMETERS.get(paramName).doubleValues() : null;
	}
	
	//-------------------- value management methods --------------------
	
	//TODO ### add a SysParameterDTO for use as interface object
	//TODO ### add List<SysParameterDTO> findAll()
	//TODO ### add other methods as needed to support Front End needs
	
	/**
	 * Updates the value for the specified parameter.  Validates the new
	 * value and returns an error message if invalid.  Returns null if
	 * no error.
	 * 
	 * @param paramName
	 * @param newValue
	 * @return String error message, or null if no error
	 */
	public static synchronized String setValue(String paramName, String newValue) {
		SysParameter sysParam = findByName(paramName);
		if (sysParam==null) {
			return "System parameter does not exist: " + paramName;
		}
		// validate the new value
		AktanaParameter aktParam = new AktanaParameter(sysParam);
		String errorMessage = aktParam.checkValue(newValue);
		if (errorMessage!=null) {
			return errorMessage;
		}
		
		// update the database
		String oldValue = sysParam.getValue();
		sysParam.setValue(newValue);
		update(sysParam);
		LOGGER.info("System parameter \"{}\" updated: old value=\"{}\", new value=\"{}\"", 
				sysParam.getName(), oldValue, newValue);

		// update the cache
		aktParam.setValue(newValue);
		CACHED_PARAMETERS.put(paramName, aktParam);
		return null;
	}

	private static SysParameter findByName(String name) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("name", name);
		SysParameter sysParam = null;
		List<SysParameter> sysParamList = getSysParameterDao()
				.findWithNamedQuery("SysParameter.findByName", map);
		if (sysParamList.size() > 0) {
			sysParam = sysParamList.get(0);
		}
		return sysParam;
	}

	private static void update(SysParameter sysParameter) {
		//TODO when needed, implement updating of the database
		throw new RuntimeException("Real-time updating of Aktana System Parameters is not yet supported");
	}
	
	public static List<AktanaParameter> getAllParamsLike(String regex) {
		List<AktanaParameter> sysParams = new ArrayList<AktanaParameter>();
		for (Entry<String, AktanaParameter> entry : CACHED_PARAMETERS.entrySet()) {
			String paramName = entry.getKey();
			if (paramName.matches(regex)) {
				AktanaParameter aktParam = entry.getValue();
				sysParams.add(aktParam);
			}
		}
		return sysParams;
	}

}
