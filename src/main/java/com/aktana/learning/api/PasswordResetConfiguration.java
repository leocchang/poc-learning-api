/*****************************************************************
 * <p>Password Reset Configuration class wraps all properties required
 * to configure this process.</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class PasswordResetConfiguration {

    @JsonProperty
    private int ttl;

    @JsonProperty
    @NotNull
    private String templatePath;

    @JsonProperty
    @NotNull
    private String sender;

    @JsonProperty
    @NotNull
    private String subject;

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
