/*****************************************************************
 * <p>Defines several constants for the whole application.</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api.allconstsandenums;

/**
 * Defines several constants for the whole application.
 * 
 * TODO ### REVIEW TO SEE WHAT CONSTS ARE NO LONGER USED and can be deleted
 */
public class Constants
{
    /**
     * The default value for the request parameter 'sizePage'.
     */
    public static final String DEFAULT_PAGE_SIZE = "-1";

    /**
     * Constant to define if user isActivated.
     */
    public static final boolean USER_ACTIVATED = true;

    public static final String PAST_SCHEDULE_PERIOD = "past";
    public static final String FUTURE_SCHEDULE_PERIOD = "future";

    public static final int KICK_ERROR_CODE = 51;

    public static final String KICK_MESSAGE = "kick";
    public static final int USER_NOT_EXISTS_ERROR_CODE = 2;
    public static final int USER_DISABLED_ERROR_CODE = 3;
    public static final String USER_NOT_EXISTS_ERROR_MESSAGE = "User not found";
    public static final String USER_DISABLED_ERROR_MESSAGE = "User not active";

    /**
     * Constant to save and retrieve userName of session;
     */
    public static String USER_NAME = "userName";

    /**
     * Constant to save and retrieve userId of session;
     */
    public static String USER_ID = "userId";

    /**
     * Constant to save and retrieve a session;
     */
    public static String SESSION_ID = "sessionId";

    /**
     * The default value for internal System User
     */
    public static String INTERNAL_SYS_USER = "internalSystemUser";
}
