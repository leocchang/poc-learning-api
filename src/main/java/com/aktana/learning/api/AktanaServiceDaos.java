/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.api;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.dropwizard.hibernate.SessionFactoryManager;

import org.hibernate.SessionFactory;


/** Singleton class to centralize access to the session factory.*/
@Singleton
public class AktanaServiceDaos
{

    private SessionFactory sessionFactory;
    
    private SessionFactoryManager manager;

    private static AktanaServiceDaos singleton;

    public static AktanaServiceDaos getInstance()
    {
    	if (singleton == null)
    		singleton = new AktanaServiceDaos();

    	return singleton;
    }

    /* Class constructor */
    public AktanaServiceDaos()
	{
    }

    /***** Getters and Setters *****/
    public void setSessionFactory(SessionFactory sf)
    {
        sessionFactory = sf;
    }

    public SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

	public SessionFactoryManager getManager() {
		return manager;
	}

	
	public void setManager(SessionFactoryManager manager) {
		this.manager = manager;
	}

    
    

}
