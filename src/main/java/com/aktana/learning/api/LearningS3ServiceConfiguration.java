package com.aktana.learning.api;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LearningS3ServiceConfiguration {
	
	@JsonProperty
    @NotNull
    private String accessKey;

    @JsonProperty
    @NotNull
    private String secretKey;
    
    @JsonProperty
    @NotNull
    private String bucketName;

    @JsonProperty
    @NotNull
    private String kmsKeyId;

	/**
     * Returns the aws access key used to connect to S3
     * @return key
     */
    public String getAccessKey() {
        return accessKey;
    }

    /**
     * Sets the aws access key used to connect to S3 
     * @param key
     */
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * Returns the aws secret key used to connect to S3
     * @return the authEndpoint
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * Sets the aws secret key used to connect to S3 
     * @param authEndpoint
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    
    /**
     * Returns the LearningS3 bucket name
     * @return the authEndpoint
     */
    public String getBucketName() {
        return bucketName;
    }

    /**
     * Sets the LearningS3 bucket name
     * @param authEndpoint
     */
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
    
    /**
     * Returns the aws kms key id
     * @return the authEndpoint
     */
    public String getKmsKeyId() {
        return kmsKeyId;
    }

    /**
     * Sets the aws kms key id
     * @param authEndpoint
     */
    public void setKmsKeyId(String kmsKeyId) {
        this.kmsKeyId = kmsKeyId;
    }

}
