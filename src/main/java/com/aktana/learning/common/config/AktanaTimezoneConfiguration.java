/*****************************************************************
 * <p>Aktana standard class that sets the API's various default timezones to "Etc/UTC",
 * (Universal Time Coordinated).  This should be done at AktanaService initialization time,
 * as well as at initialization time for all unit testing environments.</p>
 *
 * <p>Known places that should call AktanaTimezoneConfiguration.setUTC():
 * <ul>
 *   <li> AktanaService.initialize()
 * </ul/>
 * </p>
 *
 * @author $Author$
 * @version $Revision$
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.common.config;

import org.joda.time.DateTimeZone;

import java.util.TimeZone;

public class AktanaTimezoneConfiguration {

	private static final String utcTimezoneId = "Etc/UTC";

	/**
	 * The purpose of this method is to make it unnecessary to have to specify
	 * the JRE arg
	 * 			-Duser.timezone="Etc/"UTC"
	 * when starting up AktanaServic or any Aktana API test programs/environments.
	 *
	 * This method sets the java and joda default time zones both to "Etc/UTC".
	 * It is important that the java and joda default timezones are the same.
	 * And it is important that the joda default timezone is UTC, because we use dateTime.withTimeAtStartOfDay()
	 * method widely within the API application.
	 *
	 * Previously we avoided problems by always using the -Duser.timezone="Etc/"UTC" JRE startup arg,
	 * which caused both java and joda to default to the UTC timezone.  However, we have had problems
	 * enforcing that all running of Aktana API code, including all test programs, must use the
	 * -Duser.timezone="Etc/"UTC" arg.  So now our convention is that all AktanaService and all Aktana API
	 * test programs/environments must call AktanaTimezoneConfiguration.setUTC() during initialization.
	 */
	public static void setUTC() {

		TimeZone.setDefault(TimeZone.getTimeZone(utcTimezoneId));
		DateTimeZone.setDefault( DateTimeZone.forID(utcTimezoneId) );

		// not sure what other impacts this might have, but just to be consistent let's make sure that the JRE
		// variable is also set to the standard.
		System.setProperty("user.timezone", utcTimezoneId);
	}

}
