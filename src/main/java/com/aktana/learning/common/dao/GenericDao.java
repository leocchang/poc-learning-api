/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION</p>
 *
 * @author $Author: paolo.lizarazu $
 * @version $Revision: 18419 $ on $Date: 2013-10-23 12:36:52 -0700 (Wed, 23 Oct 2013) $ by $Author: paolo.lizarazu $
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.common.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import  org.joda.time.DateTime;

/**
 * Generic DAO class.
 * @author lincolns, wallacew
 * @param <T>
 * @param <PK>
 */
public interface GenericDao<M, PK extends Serializable> {

    /**
     * Persist the newInstance object into database.
     * @param newInstance to save
     * @return The identifier
     **/
    PK save(M newInstance);

    /**
     * Save or update.
     * @param transientObject to save
     */
    void saveOrUpdate(M transientObject);

    void merge(M o);

    /**
     * Retrieve a persisted object with a given id from the database.
     * @param id to load
     * @return An object of type T
     */
    M load(PK id);

    /**
     * Retrieve a persisted object with a given id from the database.
     * @param id to get
     * @return An object of type T
     */
    M get(PK id);
    /**
     * Save changes made to a persistent object.
     * @param transientObject object to update
     **/
    void update(M transientObject);

    /**
     * Remove the given object from persistent storage in the database.
     * @param persistentObject object to delete.
     **/
    void delete(M persistentObject);

    /**
     * Remove the given object from persistent storage in the database.
     * @param s Query to execute
     * @return A query object
     **/
    Query getQuery(String s);

    /** Deletes an object of a given Id. Will load the object internally so
     * consider using delete (T obj) directly.
     * @param id Id of record
     */
    void delete(PK id);


    /** Delete object from disk.
     * @param persistentObject to delete
     * @param session to use
     *
     */
    void delete(M persistentObject, Session session);

    /** Deletes an object of a given Id. Will load the object internally so consider using delete (T obj) directly.
     * @param id to delete
     * @param session to use
     */
    void delete(PK id, Session session);

    /**
     * Loads the given Object.
     * @param id to load
     * @param session to use
     * @return  an object of type T
     */
    M load(PK id, Session session);

    /**
     * Loads the given Object.
     * @param id Id to load
     * @param session to use
     * @return An object of type T
     */
    M get(PK id, Session session);

    /** Save object to disk using given session.
     * @param o to save
     * @param session to use
     * @return the id of the saved object
     *
     */
    PK save(M o, Session session);

    /** Save or update given object.
     * @param o item to save.
     * @param session to use
     *
     */
    void saveOrUpdate(M o, Session session);

    /** Update given object.
     * @param o item to update
     * @param session to use
     *
     */
    void update(M o, Session session);

    /** Refreshes the object of type T.
     * @param persistentObject to refresh
     */
    void refresh(M persistentObject);

    /**
     * Get a query handle.
     * @param s Query to use
     * @param session to use
     * @return Query object
     */
    Query getQuery(String s, Session session);

    /** FindByExample.
     * @param exampleInstance to use
     * @param excludeProperty to exclude
     * @return A list of objects
     */
    List<M> findByExample(M exampleInstance, String... excludeProperty);

    /**
     * Find all paginated with <code>startPage</code> and <code>sizePage</code>.
     * @param startPage The collection will start from.
     * @param sizePage The desired amount of elements for the result.
     * @return A list of objects
     */
    List<M> findPaginated(int startPage, int sizePage);

    /** Returns a list of objects.
     * @return list of objects
     */
    List<M> findAll();

    /** Flushes the cache of the currently-used session.
     *
     */
    void flush();

    /** Object to evict from cache.
     * @param obj Object to evict
     */
    void evict(Object obj);


    /** Hibernate wrapper.
     * @param criterion to filter.
     * @return list of objects
     */
    List<M> findByCriteria(Criterion... criterion);


    DateTime getCurrentTime();


    /**
     * Returns the number of records that meet the criteria
     * @param namedQueryName
     * @return List
     */
    public List<M> findWithNamedQuery(String namedQueryName);

    /**
     * Returns the number of records that meet the criteria
     * @param namedQueryName
     * @param parameters
     * @return List
     */
    public List<M> findWithNamedQuery(String namedQueryName, Map parameters);

    /**
     * Returns the number of records with result limit
     * @param queryName
     * @param resultLimit
     * @return List
     */
    public List<M> findWithNamedQuery(String queryName, int resultLimit);

    /**
     * Returns the number of records that meet the criteria
     * @param <T>
     * @param sql
     * @param type
     * @return List
     */
    public List<M> findByNativeQuery(String sql);

    /**
     * Returns the number of total records
     * @param namedQueryName
     * @return int
     */
    public int countTotalRecord(String namedQueryName);


    /**
     * Returns the number of total records
     * @param namedQueryName
     * @param parameters
     * @return int
     */
    public int countTotalRecord(String namedQueryName, Map parameters);


    /**
     * Returns the number of records that meet the criteria with parameter map and
     * result limit
     * @param namedQueryName
     * @param parameters
     * @param resultLimit
     * @return List
     */
    public List<M> findWithNamedQuery(String namedQueryName, Map parameters, int resultLimit);

    /**
     * Returns the number of records that will be used with lazy loading / pagination
     * @param namedQueryName
     * @param start
     * @param end
     * @return List
     */
    public List<M> findWithNamedQuery(String namedQueryName, int start, int end);


    public List<M> findWithNamedQuery(String namedQueryName, Map parameters, int start, int end);
    
    
    public Session getSession();
            
    public void setSession(Session sess);
    
    /**
     * Attempt to read using the named query. If no object is found, create the default Object and return it
     * 
     * @param namedQueryName
     * @param parameters
     * @param defaultObject
     * @return
     */
    public M findAndCreateDefaultIfMissing(String namedQueryName, Map parameters, M defaultObject);    
    
    
    
    /**
     * Create a criteria object for search
     */    
    public Criteria getCriteria();

    
    /** Hibernate wrapper.
     * @param criterion to filter.
     * @return list of objects
     */
    public List<M> findByCriteria(Criteria crit);

        
    public int executeUpdateQuery(String namedQueryName);

    
    public int executeUpdateQuery(String namedQueryName, Map parameters);    
}