/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION</p>
 *
 * @author $Author: sergio.rodriguez $
 * @version $Revision: 19043 $ on $Date: 2014-01-16 07:36:48 -0800 (Thu, 16 Jan 2014) $ by $Author: sergio.rodriguez $
 *
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.common.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

import org.joda.time.DateTime;


/**
 * A generic Hibernate Data Access Object. This implementation assumes that
 * transactions are being handled by the services layer making use of this DAO.
 *
 * The DAO makes use of the Hibernate internal thread local session management via spring.
 *
 * @author lincolns, wallacew
 *
 * @param <T>
 * @param <PK>
 */

//
public class GenericDaoImpl<M, PK extends Serializable> implements GenericDao<M, PK>
{
    /** Class type. */
    Class<M> type;

    /** Session */
	private Session session;
	
    /**
     * Default bean constructor for spring.
     */
    public GenericDaoImpl() {
            // default constructor for spring
    }

    /**
     * Constructor.
     * @param type class type
     */
    public GenericDaoImpl(Class<M> type) {
        this.type = type;
    }

        
    /**
     * Constructor.
     * @param type class type
     * @param session Session
     */
    public GenericDaoImpl(Class<M> type, Session session) {
        this.session = session;
    }

    /** Helper functions.
     * @return the currently set class
     */
    public Class<M> getPersistentClass() {
        return this.type;
    }

    /**
     * Delete persistentObject from DB.
     * @param persistentObject object to delete.
     */
    public void delete(M persistentObject) {
        getSession().delete(persistentObject);
    }

    /** Deletes an object of a given Id. Will load the object internally so consider using delete (T obj) directly
     * @param id Delete key
     */
    public void delete(PK id) {
        getSession().delete(load(id));
    }

    /**
     * Loads the given Object.
     * @param id to load
     * @return T Loaded object
     */
    @SuppressWarnings("unchecked")
    public M load(PK id) {
        return (M) getSession().load(this.type, id);
    }


    /**
     * Loads the given Object.
     * @param id Id to load
     * @return An object of type T
     */
    @SuppressWarnings("unchecked")
    public M get(PK id) {
        return (M) getSession().get(this.type, id);
    }

    /**
     * Item to save.
     * @param o object to save
     * @return PK
     */
    @SuppressWarnings("unchecked")
    public PK save(M o) {
        return (PK) getSession().save(o);
    }

   /**
     * Item to refresh.
     * @param o object to refresh
     */
    public void refresh(M o) {
        getSession().refresh(o);
    }

    /**
     * Item to saveOrUpdate.
     * @param o item to save.
     */
    public void saveOrUpdate(M o) {
        getSession().saveOrUpdate(o);
    }

    public void merge(M o)
    {
        getSession().merge(o);
    }
    /**
     * Update object.
     * @param o object to update
     */
    public void update(M o) {
        getSession().update(o);
    }


    /**
     * Sets the current session to use
     */
    public void setSession(Session session) {
		this.session = session;
	}

    /**
     * Gets the current session in use
     * @return Session object
     */
    public Session getSession() {
		return session;
//        return AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession();
    }

    /**
     * Get query.
     * @param s Query to execute.
     * @return Query object
     */
    public Query getQuery(String s) {
        return getSession().createQuery(s);
    }


    /** Delete object.
     * @param persistentObject to delete
     * @param session to use
     *
     */
    public void delete(M persistentObject, Session session) {
        session.delete(persistentObject);
    }

    /** Deletes an object of a given Id. Will load the object internally so consider using delete (T obj) directly.
     * @param id to delete
     * @param session to use
     */
    public void delete(PK id, Session session) {
        session.delete(load(id));
    }

    /**
     * Loads the given Object.
     * @param id to load
     * @param session to use
     * @return  an object of type T
     */
    @SuppressWarnings("unchecked")
    public M load(PK id, Session session) {
        return (M) session.load(this.type, id);
    }

    /**
     * Loads the given Object.
     * @param id Id to load
     * @param session Session to use
     * @return An object of type T
     */
    @SuppressWarnings("unchecked")
    public M get(PK id, Session session) {
        return (M) session.get(this.type, id);
    }

    /** Save object.
     * @param o to save
     * @param session to use
     * @return the id of the saved object
     *
     */
    @SuppressWarnings("unchecked")
    public PK save(M o, Session session) {
        return (PK) session.save(o);
    }

    /** Save Or Update object.
     * @param o to save
     * @param session to use.
     *
     */
    public void saveOrUpdate(M o, Session session) {
        session.saveOrUpdate(o);
    }

    /** Update record.
     * @param o to update
     * @param session to use
     *
     */
    public void update(M o, Session session) {
        session.update(o);
    }


    /**
     * GetQuery.
     * @param s to return
     * @param session  to use
     * @return Query object
     */
    public Query getQuery(String s, Session session) {
        return session.createQuery(s);
    }

    /** Wrapper around hibernate functions.
     * @param criterion to use
     * @return A list of matching objects
     */
    @SuppressWarnings("unchecked")
    public List<M> findByCriteria(Criterion... criterion) {

    	Criteria crit =  getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();

    }

    /**
     * We use VisitType Entity because it should be strong and light in our system,
     * we can use any other Entity if visitTypes was deleted.
     * @return Datetime with current timestamp form database
     */
    //TODO ###### Review the need for obtaining database timestamp, and this particular implementation
    @Override
    public DateTime getCurrentTime() {
        Date date = (Date) getSession().createQuery("SELECT DISTINCT(CURRENT_TIMESTAMP()) FROM VisitType")
                .uniqueResult();
        return new DateTime(date);
    }


    /** FindAll.
     * @return A list of all the objects
     */
    public List<M> findAll() {
        return findByCriteria();
    }

    /** Flushes the cache of the currently-used session.
     *
     */
    public void flush() {
        getSession().flush();
    }

    /** Object to evict from cache.
     * @param obj Object to evict
     */
    public void evict(Object obj) {
        getSession().evict(obj);
    }


    /** FindByExample.
     * @param exampleInstance to use
     * @param excludeProperty to use
     * @return List of matching objects
     */
    @SuppressWarnings("unchecked")
    public List<M> findByExample(M exampleInstance, String... excludeProperty) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Example example = Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    @SuppressWarnings("unchecked")
	public List<M> findPaginated(int startPage, int sizePage) {
    	Criteria criteria = getSession().createCriteria(getPersistentClass());
    	if (sizePage > 0) {
			criteria.setFirstResult(startPage);
	    	criteria.setMaxResults(sizePage);
		}

    	return criteria.list();
    }




    /**
     * Returns the number of records that meet the criteria
     * @param namedQueryName
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String namedQueryName) {
		Query queryNamed = getSession().getNamedQuery(namedQueryName);

		return queryNamed.list();
	}

    /**
     * Returns the number of records that meet the criteria
     * @param namedQueryName
     * @param parameters
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String namedQueryName, Map parameters) {
        return findWithNamedQuery(namedQueryName, parameters, 0);
	}

    /**
     * Returns the number of records with result limit
     * @param queryName
     * @param resultLimit
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String queryName, int resultLimit) {
		Query queryNamed = getSession().getNamedQuery(queryName);
		queryNamed.setMaxResults(resultLimit);

		return queryNamed.list();
	}

    /**
     * Returns the number of records that meet the criteria
     * @param <T>
     * @param sql
     * @param type
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findByNativeQuery(String sql) {
		Query queryNamed = getSession().createSQLQuery(sql).addEntity(type);

		return queryNamed.list();
	}

    /**
     * Returns the number of total records
     * @param namedQueryName
     * @return int
     */
    @SuppressWarnings("unchecked")
    public int countTotalRecord(String namedQueryName) {
        Query query = getSession().getNamedQuery(namedQueryName);
        Number result = (Number) query.uniqueResult();
        return result.intValue();

	}


    /**
     * Returns the number of total records
     * @param namedQueryName
     * @param parameters
     * @return int
     */
    @SuppressWarnings("unchecked")
    public int countTotalRecord(String namedQueryName, Map parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.getSession().getNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        Number result = (Number) query.uniqueResult();
        return result.intValue();
	}


    /**
     * Returns the number of records that meet the criteria with parameter map and
     * result limit
     * @param namedQueryName
     * @param parameters
     * @param resultLimit
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String namedQueryName, Map parameters, int resultLimit) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.getSession().getNamedQuery(namedQueryName);
        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
        for (Map.Entry<String, Object> entry : rawParameters) {
        	if (entry.getValue() instanceof Collection) {
        		query.setParameterList(entry.getKey(), (Collection)entry.getValue());
        	}
        	else {
        		query.setParameter(entry.getKey(), entry.getValue());
        	}
        }
        return query.list();

	}

    /**
     * Returns the number of records that will be used with lazy loading / pagination
     * @param namedQueryName
     * @param start
     * @param end
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String namedQueryName, int start, int end) {
        Query query = this.getSession().getNamedQuery(namedQueryName);
        query.setMaxResults(end - start);
        query.setFirstResult(start);
        return query.list();
	}

    /**
     * Returns the number of records that will be used with lazy loading / pagination
     * @param namedQueryName
     * @param parameters
     * @param start
     * @param end
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<M> findWithNamedQuery(String namedQueryName, Map parameters, int start, int end) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.getSession().getNamedQuery(namedQueryName);
        query.setMaxResults(end - start);
        query.setFirstResult(start);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.list();
	}
    
    
    @SuppressWarnings("unchecked")
    public M findAndCreateDefaultIfMissing(String namedQueryName, Map parameters, M defaultObject) {
    	List<M> modellist = findWithNamedQuery(namedQueryName, parameters, 1);    	

    	M model = null;    	
    	
    	if (modellist.size() > 0) {
    		model = modellist.get(0);
    	} else {
    		if (defaultObject != null) {
    			getSession().save(defaultObject);
    			getSession().refresh(defaultObject);
    			model = defaultObject;
    		}
    	}

    	return model;
    }
    
    
    public Criteria getCriteria() {
    	return getSession().createCriteria(this.getPersistentClass());
    }

    
    @SuppressWarnings("unchecked")
	public List<M> findByCriteria(Criteria crit) {
    	return crit.list();
    }

    /**
     * Returns the number of records that are affected
     * @param namedQueryName
     * @return List
     */
    @SuppressWarnings("unchecked")
    public int executeUpdateQuery(String namedQueryName) {
		Query queryNamed = getSession().getNamedQuery(namedQueryName);

		return queryNamed.executeUpdate();
	}

    /**
     * Returns the number of records that are affected
     * @param namedQueryName
     * @return List
     */
    @SuppressWarnings("unchecked")    
    public int executeUpdateQuery(String namedQueryName, Map parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.getSession().getNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
		return query.executeUpdate();
	}
    
}