package com.aktana.learning.common.exception;

import java.util.List;

public class InvalidCredentialsException extends AktanaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5873186107964695365L;

	public InvalidCredentialsException(Integer status, Integer code,
			List<AktanaErrorInfo> errors, String fmt, Object[] params) {		
		super(status, code, errors, fmt, params);
		// TODO Auto-generated constructor stub
	}
	
	public InvalidCredentialsException(Integer status, Integer code, String fmt,
			Object... params) {
		super(status, code, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public InvalidCredentialsException(Integer status, String fmt, Object... params) {
		super(status, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public InvalidCredentialsException(String fmt, Object... params) {		
		super(fmt, params);
		this.status = 401;
		// TODO Auto-generated constructor stub
	}
	
}
