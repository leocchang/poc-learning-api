package com.aktana.learning.common.exception;

import java.util.List;

public abstract class AktanaException extends RuntimeException
{	
	protected Integer status;	// HTTP return status
	protected Integer code; // internal error code
	protected String message;	// message
	List<AktanaErrorInfo> errors; // list of detailed field level errors
	
	public AktanaException(String fmt, Object... params)
	{
		this.message = String.format(fmt, params);
		this.status = 400; // bad request
		this.code = null;
		this.errors = null;
	}
	
	public AktanaException(Integer status, String fmt, Object... params)
    {
        this.message = String.format(fmt, params);
        this.status = status; 
		this.code = null;
		this.errors = null;
    }

	public AktanaException(Integer status, Integer code, String fmt, Object... params)
    {
        this.message = String.format(fmt, params);
        this.code = code;
        this.status = status;
    }
	
	public AktanaException(Integer status, Integer code, List<AktanaErrorInfo> errors, String fmt, Object... params)
    {
        this.message = String.format(fmt, params);
        this.errors = errors;
        this.status = status; 
		this.code = code;
    }
	
	public List<AktanaErrorInfo> getErrors() {
		return errors;
	}

	public void setErrors(List<AktanaErrorInfo> errors) {
		this.errors = errors;
	}

	@Override
	public String getMessage()
	{
		return message;
	}
	
	
	public Integer getStatus()
	{
	    return this.status;
	}

	public Integer getCode()
	{
	    return this.code;
	}
	
}
