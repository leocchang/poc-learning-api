package com.aktana.learning.common.exception;

import lombok.Data; 

@Data()
public class AktanaErrorInfo {
	
	public AktanaErrorInfo() {		
	}
	
	private Integer code;
	private String field;
	private String message;
}
