package com.aktana.learning.common.exception;

import java.util.List;

public class GenericValidationException extends AktanaException {

	public GenericValidationException(Integer status, Integer code,
			List<AktanaErrorInfo> errors, String fmt, Object... params) {
		super(status, code, errors, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public GenericValidationException(Integer status, Integer code, String fmt,
			Object... params) {
		super(status, code, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public GenericValidationException(Integer status, String fmt, Object... params) {
		super(status, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public GenericValidationException(String fmt, Object... params) {
		super(fmt, params);
		// TODO Auto-generated constructor stub
	}

}
