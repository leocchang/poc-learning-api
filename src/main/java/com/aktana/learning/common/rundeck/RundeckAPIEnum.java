package com.aktana.learning.common.rundeck;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RundeckAPIEnum {
	
	RUNDECK_API_INFO("info"),
	
	RUNDECK_API_ANCHOR_EMR("anchor.emr.job"),
	RUNDECK_API_REM_EMR("rem.emr.job"),
	RUNDECK_API_TTE_EMR("tte.emr.job"),
	RUNDECK_API_MSO_EMR("mso.emr.job");

	private String pathId;


	private RundeckAPIEnum(String pathId) {
		this.pathId = pathId;
	}

	@JsonValue
	public String getPathId() {
		return this.pathId;
	}
}
