package com.aktana.learning.common.rundeck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.RundeckServiceConfiguration;
import com.aktana.learning.common.dse.DSEConnector;
import com.google.common.net.HttpHeaders;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RundeckConnection {

	private RundeckServiceConfiguration config;
	
	private Logger LOGGER = LoggerFactory.getLogger(DSEConnector.class);
	
	public RundeckConnection(RundeckServiceConfiguration config) throws RundeckConnectionException {
		this.config = config;
	}
	

	
	public String callGet(RundeckAPIEnum apiEnum) throws RundeckConnectionException {
		return callApi(apiEnum, "GET", null);
	}
	
	public String callApi(RundeckAPIEnum apiEnum, String method) throws RundeckConnectionException {
		return callApi(apiEnum, method, null);
	}
	
	public String callApi(RundeckAPIEnum apiEnum, String method, String body) throws RundeckConnectionException {
		return callApi(apiEnum, method, "application/json;charset=UTF-8", body, "");
	}
	
	private Response makeCall(RundeckAPIEnum apiEnum, String method, String contentType, String body, String suffix) {
		LOGGER.info(apiEnum.name()+" "+method);
        
		String jobPath = config.getPaths().get(apiEnum.getPathId());
		if(jobPath == null) {
			throw new RundeckConnectionException(500, "Rundeck job "+apiEnum.getPathId()+" not set in learning-service.yml");
		}
		
		String path = config.getUrl()+jobPath+(suffix == null?"":(((jobPath.indexOf("?") == -1)?"?":"&")+suffix));
		LOGGER.info("path:"+path);
		LOGGER.info("body:"+body);
		
		Client client = ClientBuilder.newClient();
		// client.addFilter(new LoggingFilter(System.out));
    		WebTarget target = client.target(path);
    		Invocation.Builder invocation = target.request()
    				.accept("*/*")
    				.header("Content-Type", contentType)
    				.header( "X-Rundeck-Auth-Token", config.getKey());
		Response response;
		Entity<?> entity = Entity.entity(body, MediaType.APPLICATION_JSON);
		if(method.equalsIgnoreCase("POST")) {
			response = invocation.post(entity, Response.class);
		} else if(method.equalsIgnoreCase("GET")) {
			response = invocation.get(Response.class);
		} else if(method.equalsIgnoreCase("PUT")) {
			response = invocation.put(entity, Response.class);
		} else if(method.equalsIgnoreCase("DELETE")) {
			response = invocation.delete(Response.class);
		} else {
			throw new RundeckConnectionException(400, "Unknown method "+method);
		}
		
		return response;
	}
	
    public String callApi(RundeckAPIEnum apiEnum, String method, String contentType, String body, String suffix) {
    	
        try {
            
        	Response response = makeCall(apiEnum, method, contentType, body, suffix);
        	
        	int status = response.getStatus();
        
			if (status != 200) {
				throw new RundeckConnectionException(status, "Failed : HTTP error code : "
						+ response.getStatus() + " - " + response.toString());
			}

			LOGGER.debug("Output from Server: ");
			String result = response.readEntity(String.class);
			LOGGER.debug(result);
			return result;
        } catch (RundeckConnectionException rce){
        	rce.printStackTrace();
        	throw rce;
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new RundeckConnectionException(500, "Failed : " + e.getClass().getName() + " - " + e.getMessage());
        }
        
    }
}
