package com.aktana.learning.common.rundeck;

import java.util.List;

import com.aktana.learning.common.exception.AktanaErrorInfo;
import com.aktana.learning.common.exception.AktanaException;

public class RundeckConnectionException extends AktanaException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8527902095461499115L;

	public RundeckConnectionException(Integer status, Integer code,
			List<AktanaErrorInfo> errors, String fmt, Object... params) {
		super(status, code, errors, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public RundeckConnectionException(Integer status, Integer code, String fmt,
			Object... params) {
		super(status, code, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public RundeckConnectionException(Integer status, String fmt, Object... params) {
		super(status, fmt, params);
		// TODO Auto-generated constructor stub
	}

	public RundeckConnectionException(String fmt, Object... params) {
		super(fmt, params);
		// TODO Auto-generated constructor stub
	}
}