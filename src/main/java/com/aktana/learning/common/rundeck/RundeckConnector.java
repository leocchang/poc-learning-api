package com.aktana.learning.common.rundeck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.RundeckServiceConfiguration;

public class RundeckConnector {

	private static RundeckConnector instance;
	
	private RundeckServiceConfiguration config;
	private RundeckConnection connection;

	private Logger LOGGER = LoggerFactory.getLogger(RundeckConnector.class);

	private RundeckConnector() {
	}

	public static RundeckConnector getInstance() {
		if (instance == null) {
			instance = new RundeckConnector();
		}

		return instance;
	}

	public void initializeConnection(RundeckServiceConfiguration config) {
		this.config = config;   			
	}
	
	public RundeckServiceConfiguration getConfig() {
		return this.config;
	}

	public RundeckConnection getConnection() throws RundeckConnectionException {

		try {
			if (connection == null) {
				LOGGER.info("Connecting to {}  ...", config.getUrl());

				connection = new RundeckConnection(this.config);           	            	
			}
		} catch (RundeckConnectionException e) {
			LOGGER.error("Error when trying to get connection to rundeck: " + e.getMessage());
			connection = null;
			throw e;
		}

		return this.connection;
	}
	
	public String callPost(RundeckAPIEnum apiEnum, String body, String suffix) throws RundeckConnectionException {
		return this.getConnection().callApi(apiEnum, "POST", "application/json;charset=UTF-8", body, suffix);
	}
	
	public String callGet(RundeckAPIEnum apiEnum) throws RundeckConnectionException {
		return this.getConnection().callApi(apiEnum, "GET", "");
	}
	
	public String callApi(RundeckAPIEnum apiEnum, String method) throws RundeckConnectionException {
		return this.getConnection().callApi(apiEnum, method, null);
	}
	
	public String callApi(RundeckAPIEnum apiEnum, String method, String body) throws RundeckConnectionException {
		return this.getConnection().callApi(apiEnum, method, "application/json;charset=UTF-8", body, "");
	}
	
    public String callApi(RundeckAPIEnum apiEnum, String method, String contentType, String body) throws RundeckConnectionException {
    	return this.getConnection().callApi(apiEnum, method, contentType, body, "");
    }
}