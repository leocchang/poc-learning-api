package com.aktana.learning.common.util.deployment;

import lombok.Getter;
import lombok.Setter;

/**
 * A class to hold key information of interest from a Manifest
 */
@Getter @Setter
public class ManifestInfo {
	
	private String implementationTitle;
	private String implementationVersion;
	private String srcRevision;
	private String srcBranch;
	private String srcBranchSimple;
	
	/**
	 * Sets srcBranch.
	 * Also sets srcBranchSimple to string left of first space in srcBranch, if possible.
	 * Example:
	 * 		srcBranch = "develop * [git@bitbucket.org:aktana/aktana-partner-api.git]"
	 * 	--> srcBranchSimple = "develop"
	 */
	public void setSrcBranch(String srcBranch) {
		this.srcBranch = srcBranch;
		if (srcBranch != null) {
			String[] branchComponents = srcBranch.split(" ");
			if (branchComponents!=null && branchComponents.length>0) {
				String simpleBranch = branchComponents[0];
				if (simpleBranch!=null && !simpleBranch.trim().isEmpty()) {
					this.srcBranchSimple = simpleBranch;
				}
			}
		}
	}

}
