package com.aktana.learning.common.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility makes it easier to embed elapsed time logging into application code.
 * 
 * Example usage:
 * 		TimingLogger timer = new TimingLogger("App.methodName");
 * 		timer.logStart();
 * 		... code does initialization work...
 * 		timer.logElapsed("initialization");
 * 		... code does database queries ...
 * 		timer.logElapsed("database queries");
 * 		... code does calculations ....
 * 		timer.logElapsed("calculations");
 * 		timer.logEnd();
 * 
 * @author BobF
 *
 */
public class TimingLogger {
 
	private static final Logger LOGGER = LoggerFactory.getLogger(TimingLogger.class);

	private String codeSectionName;
	private long startTime;
	private long endTime;
	private long lastCheckpointTime;
	
	public TimingLogger(String name) {
		if (name==null || name.trim().isEmpty()) {
			this.codeSectionName = "Timer:";
		}
		else {
			this.codeSectionName = name;
		}
		this.startTime = getTime();
		this.lastCheckpointTime = getTime();
	}
	
	public void logStart() {
		LOGGER.info(codeSectionName+" start ");
		this.lastCheckpointTime = getTime();
	}
	
	public void logEnd() {
		this.endTime = getTime();
		LOGGER.info(codeSectionName+" end, total elapsed time = " + (endTime - startTime) + " ms");
	}
	
	public void logElapsed(String operationName) {
		long newCheckpointTime = getTime();
		LOGGER.info(codeSectionName+", "+operationName+" elapsed time = "+(newCheckpointTime - this.lastCheckpointTime) + " ms");
		this.lastCheckpointTime = newCheckpointTime;
	}
	
	private long getTime() {
		return new Date().getTime();
	}
	
	public long getTotalElapsed() {
		return endTime - startTime;
	}
}

