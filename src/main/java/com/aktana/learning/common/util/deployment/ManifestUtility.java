package com.aktana.learning.common.util.deployment;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;


/**
 * Utility offering all-static methods related to a deployed JAR's Manifest
 */
public class ManifestUtility {

	/**
	 * Returns a ManifestInfo object than contains key information extracted
	 * from the JAR file that contains the specified Class.
	 */
    public static ManifestInfo getInfoFromManifest(Class clazz) {

	    String className = clazz.getSimpleName() + ".class";
	    String classPath = clazz.getResource(className).toString();
	    if (!classPath.startsWith("jar")) {
	      // clazz is not from JAR.  
	      // This usually means the application is running under developer's local environment
	      return null;
	    }
	    
	    String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + 
	        "/META-INF/MANIFEST.MF";
	    Manifest manifest = null;
		try {
			manifest = new Manifest(new URL(manifestPath).openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	    Attributes attr = manifest.getMainAttributes();
	    
	    // Transfer the attributes of interest from the Manifest
	    ManifestInfo manifestInfo = new ManifestInfo();
	    
	   	manifestInfo.setImplementationTitle( attr.getValue("Implementation-Title") );
	   	manifestInfo.setImplementationVersion ( attr.getValue("Implementation-Version") );
	   	manifestInfo.setSrcRevision ( attr.getValue("SRC-Revision") );
	   	manifestInfo.setSrcBranch ( attr.getValue("SRC-Branch") );

	    return manifestInfo;
    }

}
