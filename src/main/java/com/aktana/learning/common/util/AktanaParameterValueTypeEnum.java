/*****************************************************************
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *****************************************************************/
package com.aktana.learning.common.util;

/**
 * This enum defines the set of data types supported by AktanaParameter.
 * 
 * The allowsMultipleValues() method indicates whether the parameter can have a list of values,
 * as opposed to a single scalar value.
 * 
 */
public enum AktanaParameterValueTypeEnum {
	STRING(false), 		STRINGS(true), 
	BOOLEAN(false), 	BOOLEANS(true), 
	INTEGER(false), 	INTEGERS(true), 
	LONG(false), 		LONGS(true), 
	DOUBLE(false), 		DOUBLES(true);

	private boolean allowsMultipleValues;

	private AktanaParameterValueTypeEnum(boolean allowsMultipleValues) {
		this.allowsMultipleValues = allowsMultipleValues;
	}

	public boolean allowsMultipleValues() {
		return this.allowsMultipleValues;
	}

	public boolean isStringType() {
		return this==STRING || this==STRINGS;
	}

	public boolean isNumericType() {
		return !isStringType();
	}

	public static AktanaParameterValueTypeEnum parse(String type) {
		if (type == null || type.trim().isEmpty())
			throw new RuntimeException("Null/blank parameter value type");
		type = type.toLowerCase();
		if (type.equals("string"))
			return STRING;
		else if (type.equals("string[]"))
			return STRINGS;
		else if (type.equals("boolean"))
			return BOOLEAN;
		else if (type.equals("boolean[]"))
			return BOOLEANS;
		else if (type.equals("integer") || type.equals("int"))
			return INTEGER;
		else if (type.equals("integer[]") || type.equals("int[]"))
			return INTEGERS;
		else if (type.equals("long"))
			return LONG;
		else if (type.equals("long[]"))
			return LONGS;
		else if (type.equals("double"))
			return DOUBLE;
		else if (type.equals("double[]"))
			return DOUBLES;
		else
			throw new RuntimeException("Unrecognized parameter value type: '" + type + "'");
	}

}
