package com.aktana.learning.common.util;

/**
 * A class whose static nextVal() method returns the negative integers in sequence,
 * starting with -1.
 * 
 * @author BobF
*/
public class NegativeIntegerSequence {

	private static int intValue = 0;
	
	public static int nextVal() {
		return --intValue;
	}
}
