package com.aktana.learning.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utility providing some convenience methods for working with Throwables/Exceptions.
 */
public class ExceptionUtility {

	
	/**
	 * Returns the Throwable that is the deepest in the chain of e.getCause().
	 */
	public static Throwable extractRootCause(Throwable e) {
		Throwable rootCause = e;
		while (e.getCause()!=null) {
			e = e.getCause();
			rootCause = e;
		}
		return rootCause;
	}
	
	/**
	 * Returns a string that identifies the type of exception, optional message,
	 * and top-level stack trace information.
	 */
	public static String extractExceptionTypeAndLocation(Throwable e) {
		StringBuilder sb = new StringBuilder();
		sb.append(e.getClass().getSimpleName());
		System.out.println(e.getMessage());
		if (e.getMessage()!=null) {
			sb.append(":").append(e.getMessage());
		}
		StackTraceElement[] stackTraceElements = e.getStackTrace();
		if (stackTraceElements!=null && stackTraceElements.length>0) {
			String location = stackTraceElements[0].toString();
			sb.append(" at ").append(location);
		}
		return sb.toString();
	}
	
	/**
	 * Returns a String containing the stack trace for the specified Throwable
	 */
	public static String extractStackTraceAsString(Throwable e) {
		StringWriter stackTraceWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stackTraceWriter));
		String stackTrace = stackTraceWriter.toString();
		return stackTrace;
	}

}
