package com.aktana.learning.common.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * The primary motivation for this class is to serve as a repository of all Error/Warning
 * messages that were recorded during the course of an operation (such as a DSE run)
 * in such way that a concise summary of recorded errors can be generated.
 */
public class ErrorMessageCollector {
	
	private Map<String,List<Object[]>> cacheMap;
	
	public ErrorMessageCollector() {
		this.cacheMap = new LinkedHashMap<String,List<Object[]>>();
	}
	
	/**
	 * This method uses String.format(template,objects) to format and return
	 * the final message string.  It also retains knowledge of all recorded
	 * instances, so that the reportSummary method can be called later,
	 * to provide a concise summary.
	 */
	public String record(String template, Object... objects) {
		List<Object[]> existingInstances = this.cacheMap.get(template);
		if (existingInstances == null) {
			existingInstances = new ArrayList<Object[]>();
			this.cacheMap.put(template,existingInstances);
		}
		existingInstances.add(objects);
		return String.format(template, objects);
	}
	
	public String reportSummary(String header) {
		StringBuilder sb = new StringBuilder(header+"\n");
		for (String template : this.cacheMap.keySet()) {
			List<Object[]> instances = this.cacheMap.get(template);
			String firstInstanceMessage = String.format(template, instances.get(0));
			sb.append("\t").append(firstInstanceMessage);
			if (instances.size() > 1) {
				sb.append("\t(and ").append(instances.size() - 1).append(" others like this)");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public String reportAll(String header) {
		StringBuilder sb = new StringBuilder(header+"\n");
		for (String template : this.cacheMap.keySet()) {
			List<Object[]> instances = this.cacheMap.get(template);
			for (Object[] instance : instances) {
				String message = String.format(template, instance);
				sb.append("\t").append(message).append("\n");
			}
		}
		return sb.toString();
	}
	
	/*
	 * Example usage
	 */
	public static void main(String[] args) {
		ErrorMessageCollector emc = new ErrorMessageCollector();
		
		emc.record("Exception %s occurred while processing accountId %d", "NullPointer", new Integer(1005));
		emc.record("Exception %s occurred during target-driven evaluation of accountId %d", "OperationNotSupported", new Integer(1007));
		
		for (int i=1001; i<1010; i++) {
			emc.record("Input data error for accountId=%d", new Integer(i));
		}
		
		emc.record("Exception %s occurred while processing accountId %d", "NullPointer", new Integer(1008));
		
		System.out.println( emc.reportSummary("Summary of errors:") );
		System.out.println( emc.reportAll("Complete list of errors:") );
	}

}
