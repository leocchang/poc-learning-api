/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.common.util.datetime;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

/**
 * A PureDate represents a GregorianCalendar date with no "time" portion.  
 * This class provides methods for construction and conversion among:
 * (1) org.joda.time.DateTime
 * (2) java.util.Date
 * (3) String, the Aktana server standard for which is AKTANA_PURE_DATE_FORMAT = "yyyy-MM-dd"
 * 
 * This class also provides other standardized convenience methods, such as:
 * -- isBetween(start,end)
 * --
 * 
 * A PureDate is immutable: once constructed, it cannot be altered.
 */
public final class PureDate {
	
	public static final String AKTANA_PURE_DATE_FORMAT = "yyyy-MM-dd";
	public static final String AKTANA_PURE_DATE_REGEX_PATTERN = "^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$";

	private static final String NULL_CONSTRUCTOR_ARG_ERROR = "PureDate constructor argument cannot be null";

	private DateTime jodaDate;
	private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(AKTANA_PURE_DATE_FORMAT);
	
	//---------------------------- Static conversion methods -----------------------------
	
	public static String jodaDateToString(DateTime date) {
		return (new PureDate(date)).toString();
	}
	
	public static String javaDateToString(Date date) {
		return (new PureDate(date)).toString();
	}
	
	public static DateTime jodaDateFromString(String date) {
		return (new PureDate(date)).toJodaDateTime();
	}
	
	public static Date javaDateFromString(String date) {
		return (new PureDate(date)).toJavaDate();
	}
	
	//---------------------------- Constructors -----------------------------
	
	/**
	 * Constructs a PureDate from a org.joda.time.DateTime
	 */
	public PureDate(DateTime aDateTime) {
		if (aDateTime==null) {
			throw new RuntimeException(NULL_CONSTRUCTOR_ARG_ERROR);
		}
		jodaDate = (new DateTime(aDateTime)).withTimeAtStartOfDay();
	}

	/**
	 * Constructs a PureDate from a java.util.Date
	 */
	public PureDate(Date aDate) {
		if (aDate==null) {
			throw new RuntimeException(NULL_CONSTRUCTOR_ARG_ERROR);
		}
		jodaDate = (new DateTime(aDate)).withTimeAtStartOfDay();
	}
	
	/**
	 * Constructs a String that is in AKTANA_PURE_DATE_FORMAT
	 */
	public PureDate(String yyyyMMdd) {
		if (yyyyMMdd==null) {
			throw new RuntimeException(NULL_CONSTRUCTOR_ARG_ERROR);
		}
		try {
			jodaDate = (dateTimeFormatter.parseDateTime(yyyyMMdd)).withTimeAtStartOfDay();
		}
		catch(Exception e) {
			throw new RuntimeException("PureDate(String) constructor requires date format to be " + AKTANA_PURE_DATE_FORMAT);
		}
	}
	
	//---------------------------- Converters -----------------------------
	
	/**
	 * Returns the PureDate as a org.joda.time.DateTime
	 */
	public DateTime toJodaDateTime() {
		return new DateTime(jodaDate);
	}

	/**
	 * Returns the PureDate as a java.util.Date
	 */
	public Date toJavaDate() {
		return jodaDate.toDate();
	}
	
	/**
	 * Returns the PureDate as a String that is in AKTANA_PURE_DATE_FORMAT
	 */
	public String toString() {
		return jodaDate.toString(AKTANA_PURE_DATE_FORMAT);
	}
	
	//---------------------------- isBetween -----------------------------
	
	/**
	 * Returns true iff this PureDate is between startDate and endDate (inclusive),
	 * i.e. PureDate(startDate) <= thisDate <= PureDate(endDate)
	 */
	public boolean isBetween(PureDate startDate, PureDate endDate) {
		return this.isBetween(startDate.toJodaDateTime(), endDate.toJodaDateTime());
	}
	
	/**
	 * Returns true iff this PureDate is between startDate and endDate (inclusive),
	 * i.e. PureDate(startDate) <= thisDate <= PureDate(endDate)
	 */
	public boolean isBetween(DateTime startDate, DateTime endDate) {
		return !jodaDate.withTimeAtStartOfDay().isBefore(startDate) 
				&& !jodaDate.withTimeAtStartOfDay().isAfter(endDate);
	}
	
	/**
	 * Returns true iff this PureDate is between startDate and endDate (inclusive),
	 * i.e. PureDate(startDate) <= thisDate <= PureDate(endDate)
	 */
	public boolean isBetween(Date startDate, Date endDate) {
		return this.isBetween(new PureDate(startDate), new PureDate(endDate));
	}
	
	/**
	 * Returns true iff this PureDate is between startDate and endDate (inclusive),
	 * i.e. PureDate(startDate) <= thisDate <= PureDate(endDate)
	 */
	public boolean isBetween(String startDate, String endDate) {
		return this.isBetween(new PureDate(startDate), new PureDate(endDate));
	}
	
	//---------------------------- daysBetween -----------------------------
	
	/**
	 * Returns the number of days between this PureDate and the specified PureDate.
	 * Will be positive if specified date is in the future, negative if in the past,
	 * zero if the two are equal.
	 */
	public int daysBetween(PureDate that) {
		return Days.daysBetween(this.toJodaDateTime(), that.toJodaDateTime()).getDays();
	}
		
}
