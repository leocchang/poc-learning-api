/*****************************************************************
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *****************************************************************/
package com.aktana.learning.common.util;

import static com.aktana.learning.common.util.AktanaParameterValueTypeEnum.*;

import java.util.Arrays;

import com.aktana.learning.persistence.models.impl.SysParameter;

/**
 * <p>
 * Utility class for handling named/typed generic scalar parameters. This is
 * intended for use in supporting two API areas of functionality:
 * <ul>
 * <li>SE Configuration, which involves SE parameters
 * <li>The sysParams table, which will hold scalar system parameters
 * </ul>
 * 
 * The name, valueType, minValue and maxValue are assumed to be under control of
 * the application, and validation of these fields is done during construction
 * and corresponding "set" methods. Any violations will cause a RuntimeException
 * to be thrown.<br>
 * <br>
 * 
 * The value attribute is assumed to be user-specified and of primary interest,
 * so additional methods are provided for it. You can just call the
 * setValue(String) method, but this will throw a RuntimeException if the value
 * is invalid. So its better to call the checkValue(String) method first.
 */
public class AktanaParameter implements Cloneable, Comparable {

	private static final String MULTIPLE_VALUES_SEPARATOR_REGEX = ",";
	private static final String MULTIPLE_STRING_VALUES_SEPARATOR_REGEX = ";";

	// public interface attributes
	private String name;
	private AktanaParameterValueTypeEnum valueType;
	private boolean isNullValueAllowed;
	private String minValue = null;;
	private String maxValue = null;
	private String value = null;

	// internal attributes for holding strongly-typed/parsed values
	@SuppressWarnings("rawtypes")
	private Comparable minValueObject;
	@SuppressWarnings("rawtypes")
	private Comparable maxValueObject;
	@SuppressWarnings("rawtypes")
	private Comparable[] valueObject;
	
	// internal attribute for use with SysParameters, to indicate whether this param is registered
	private boolean isRegistered = false;

	// -------------------- constructors -----------------

	public AktanaParameter() {
	}

	public AktanaParameter(String name, String valueTypeString,
			boolean isNullValueAllowed) {
		setName(name);
		setValueType(valueTypeString);
		this.isNullValueAllowed = isNullValueAllowed;
	}

	public AktanaParameter(String name, String valueTypeString,
			boolean isNullValueAllowed, String minValue, String maxValue) {
		this(name, valueTypeString, isNullValueAllowed);
		setMinValue(minValue);
		setMaxValue(maxValue);
	}

	public AktanaParameter(String name, String valueTypeString,
			boolean isNullValueAllowed, String minValue, String maxValue,
			String value) {
		this(name, valueTypeString, isNullValueAllowed, minValue, maxValue);
		setValue(value);
	}

	public AktanaParameter(SysParameter sysParam) {
		this(sysParam.getName(), sysParam.getType(), true, sysParam
				.getMinValue(), sysParam.getMaxValue(), sysParam.getValue());
	}

	// ---------------- get/set methods (except for "value") -----------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (null == name || name.trim().isEmpty()) {
			throw new RuntimeException("Null/blank parameter name");
		}
		this.name = name;
	}

	public AktanaParameterValueTypeEnum getValueType() {
		return valueType;
	}

	public void setValueType(AktanaParameterValueTypeEnum valueType) {
		this.valueType = valueType;
	}

	public void setValueType(String valueTypeString) {
		this.valueType = AktanaParameterValueTypeEnum.parse(valueTypeString);
	}

	public boolean getIsNullValueAllowed() {
		return isNullValueAllowed;
	}

	public void setIsNullValueAllowed(boolean isNullValueAllowed) {
		this.isNullValueAllowed = isNullValueAllowed;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
		this.minValueObject = parse(minValue, "minValue");
		validateMinVersusMax();
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
		this.maxValueObject = parse(maxValue, "maxValue");
		validateMinVersusMax();
	}
	
	public boolean isRegistered() {
		return this.isRegistered;
	}
	
	public void recordIsRegistered() {
		this.isRegistered = true;
	}

	// -------------------- min/max validations ------------------

	@SuppressWarnings("unchecked")
	private void validateMinVersusMax() {
		if (minValueObject != null && maxValueObject != null) {
			if (minValueObject.compareTo(maxValueObject) > 0) {
				throw new RuntimeException("minValue " + minValue
						+ " is greater than maxValue " + maxValue);
			}
		}
	}

	private void validateValue(int i) {
		if (this.valueObject[i] == null && !isNullValueAllowed)
			throw new RuntimeException(
					"null value is not allowed for this parameter");
		String errorMsg = checkValueAgainstMinMax(this.valueObject[i]);
		if (errorMsg != null)
			throw new RuntimeException(errorMsg);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String checkValueAgainstMinMax(Comparable value) {
		if (value == null)
			return null;
		else if (minValueObject != null && value.compareTo(minValueObject) < 0)
			return "value " + value + " is less than minValue " + minValue;
		else if (maxValueObject != null && value.compareTo(maxValueObject) > 0)
			return "value " + value + " is greater than maxValue " + maxValue;
		else
			return null;
	}

	// -------------------- "value" methods --------------------

	/**
	 * Returns the number of values that have been set.
	 */
	public int numberOfValues() {
		if (valueObject == null)
			return 0;
		else
			return valueObject.length;
	}

	/**
	 * Gets the "raw" String value that has been set via the constructor or via
	 * the setValue(String) method. Note that if the valueType allows multiple
	 * values, this will be a separator-delimited string. This method is value
	 * regardless of the valueType.
	 * 
	 * @return
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Gets the Object for the String value that has been set via the
	 * constructor or via the setValue(String) method. The caller is responsible
	 * to know the parameter's type and to handle the Object appropriately.
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Comparable[] getValueObject() {
		return this.valueObject;
	}

	/**
	 * Parses, validates, and sets the specified "raw" value(s) String.<br>
	 * <br>
	 * 
	 * If multiple values are specified, each will be individually validated.<br>
	 * <br>
	 * 
	 * Throws a RuntimeException upon first detection of an invalid value.
	 * 
	 * @param the
	 *            parameter value(s) to set
	 */
	public void setValue(String valueString) {
		if (valueType == null) {
			throw new RuntimeException(
					"AktanaParameter internal error: setValue(String) called but valueType is null");
		}
		this.value = valueString;
		if (!valueType.allowsMultipleValues()) {
			valueObject = createNewValueObject(1);
			this.valueObject[0] = parse(valueString, "value");
			validateValue(0);
		} else { // allowsMultipleValues
			String separatorRegex;
			if (valueType == AktanaParameterValueTypeEnum.STRING
					|| valueType == AktanaParameterValueTypeEnum.STRINGS) {
				separatorRegex = MULTIPLE_STRING_VALUES_SEPARATOR_REGEX;
			} else {
				separatorRegex = MULTIPLE_VALUES_SEPARATOR_REGEX;
			}
			if (!this.isNullValueAllowed && (valueString == null)) {
				throw new RuntimeException(
						"null value is not allowed for this parameter");
			}
			if (valueString == null) {
				// use empty string instead
				valueString = "";
			}
			String[] values = valueString.split(separatorRegex);
			valueObject = createNewValueObject(values.length);
			for (int i = 0; i < values.length; i++) {
				this.valueObject[i] = parse(values[i], "value");
				validateValue(i);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private Comparable[] createNewValueObject(int size) {
		Comparable[] newValueObject = null;
		switch (valueType) {
		case STRING:
		case STRINGS:
			newValueObject = new String[size];
			break;
		case BOOLEAN:
		case BOOLEANS:
			newValueObject = new Boolean[size];
			break;
		case INTEGER:
		case INTEGERS:
			newValueObject = new Integer[size];
			break;
		case LONG:
		case LONGS:
			newValueObject = new Long[size];
			break;
		case DOUBLE:
		case DOUBLES:
			newValueObject = new Double[size];
			break;
		default:
			throw new RuntimeException(
					"Internal error: AktanaParameter.createNewValueObject method does not yet support value type "
							+ valueType);
		}
		return newValueObject;
	}

	/**
	 * Performs a test setValue(String) call, catching any exception thrown and
	 * returning that exception's message. (Original value is retained.)
	 * 
	 * @param String
	 *            containing the parameter value to check
	 * @return error message
	 */
	@SuppressWarnings("rawtypes")
	public String checkValue(String valueString) {
		String errorMessage = null;
		String oldValue = this.value;
		Comparable[] oldValueObject = this.valueObject;
		try {
			setValue(valueString);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		} finally {
			this.value = oldValue;
			this.valueObject = oldValueObject;
		}
		return errorMessage;
	}

	// -------------- Strongly-typed "value" retrieval ----------------
	//
	// NOTE: the setValue(String) and String getValue() methods can be
	// called under any circumstances, and they are always dealing with
	// the "raw" value(s) string.
	//
	// HOWEVER, the philosophy behind the strongly-typed retrieval
	// methods below is that the caller is responsible to know the parameter's
	// valueType, and to only call the retrieval method that exactly matches
	// the parameter's valueType. To enforce this, all strongly-typed
	// retrieval methods use the same underlying methods,
	// retrieveValueObject(int,type) and retrieveValueObjects(type),
	// which perform thorough checks.

	/*
	 * Retrieves one value for the specified index
	 */
	@SuppressWarnings("rawtypes")
	private Comparable retrieveValueObject(int index,
			AktanaParameterValueTypeEnum requiredValueType) {
		// do the common checks
		doCommonRetrievalChecks(requiredValueType);
		// check the index
		if (index < 0) {
			throw new RuntimeException("Internal error: " + requiredValueType
					+ " retrieval method was called for parameter " + name
					+ " with negative index=" + index);
		}
		if (index > 0 && !valueType.allowsMultipleValues()) {
			throw new RuntimeException("Internal error: " + requiredValueType
					+ " retrieval method was called for parameter " + name
					+ " with index=" + index
					+ " but this parameter does not support multiple values");
		}
		if (index > numberOfValues()) {
			throw new RuntimeException("Internal error: " + requiredValueType
					+ " retrieval method was called for parameter " + name
					+ " with index=" + index + " but this parameter has only "
					+ numberOfValues() + " values");
		}
		// OK, finally... we're good to go
		return valueObject[index];
	}

	/*
	 * Retrieves the array of values
	 */
	@SuppressWarnings("rawtypes")
	private Comparable[] retrieveValueObjects(
			AktanaParameterValueTypeEnum requiredValueType) {
		// do the common checks
		doCommonRetrievalChecks(requiredValueType);
		// OK, finally... we're good to go
		return valueObject;
	}

	private void doCommonRetrievalChecks(
			AktanaParameterValueTypeEnum requiredValueType) {
		if (valueType != requiredValueType) {
			throw new RuntimeException("Internal error: " + requiredValueType
					+ " retrieval method was called for parameter " + name
					+ " but valueType is " + valueType);
		}
		// check for possible illegal null value (.e.g no value has been set
		// yet, but parameter does not allow null value)
		if (valueObject == null && !isNullValueAllowed) {
			throw new RuntimeException(
					"Internal error: "
							+ requiredValueType
							+ " retrieval method was called for parameter "
							+ name
							+ " but value is null, which is not allowed for this parameter");
		}
	}

	// -------------- String strongly-typed retrieval ----------------

	/**
	 * Returns the scalar String value.
	 */
	public String stringValue() {
		return (String) retrieveValueObject(0, STRING);
	}

	/**
	 * Returns the i-th of the String values.
	 */
	public String stringValue(int i) {
		return (String) retrieveValueObject(i, STRINGS);
	}

	/**
	 * Returns the array of String values.
	 */
	public String[] stringValues() {
		return (String[]) retrieveValueObjects(STRINGS);
	}

	// -------------- Boolean strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Boolean value.
	 */
	public Boolean booleanValue() {
		return (Boolean) retrieveValueObject(0, BOOLEAN);
	}

	/**
	 * Returns the i-th of the Boolean values.
	 */
	public Boolean booleanValue(int i) {
		return (Boolean) retrieveValueObject(i, BOOLEANS);
	}

	/**
	 * Returns the array of Boolean values.
	 */
	public Boolean[] booleanValues() {
		return (Boolean[]) retrieveValueObjects(BOOLEANS);
	}

	// -------------- boolean strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Boolean value, with null replaced by specified default
	 * value
	 */
	public boolean toBooleanValue(boolean defaultValue) {
		Boolean objValue = (Boolean) retrieveValueObject(0, BOOLEAN);
		if (null == objValue) {
			return defaultValue;
		}
		return (boolean) objValue;
	}

	/**
	 * Returns the i-th of the Boolean values, with nulls replaced by specified
	 * default value
	 */
	public boolean toBooleanValue(int i, boolean defaultValue) {
		Boolean objValue = (Boolean) retrieveValueObject(i, BOOLEANS);
		if (null == objValue) {
			return defaultValue;
		}
		return (boolean) objValue;
	}

	/**
	 * Returns the array of Boolean values, with nulls replaced by specified
	 * default value
	 */
	public boolean[] toBooleanValues(boolean defaultValue) {
		boolean[] values = new boolean[numberOfValues()];
		Arrays.fill(values, defaultValue);
		for (int i = 0; i < numberOfValues(); i++) {
			Boolean objValue = booleanValue(i);
			if (objValue != null) {
				values[i] = objValue;
			}
		}
		return values;
	}

	// -------------- Integer strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Integer value.
	 */
	public Integer integerValue() {
		return (Integer) retrieveValueObject(0, INTEGER);
	}

	/**
	 * Returns the i-th of the Integer values.
	 */
	public Integer integerValue(int i) {
		return (Integer) retrieveValueObject(i, INTEGERS);
	}

	/**
	 * Returns the array of Integer values.
	 */
	public Integer[] integerValues() {
		return (Integer[]) retrieveValueObjects(INTEGERS);
	}

	// -------------- int strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Integer value, with null replaced by specified default
	 * value
	 */
	public int toIntValue(int defaultValue) {
		Integer objValue = (Integer) retrieveValueObject(0, INTEGER);
		if (null == objValue) {
			return defaultValue;
		}
		return (int) objValue;
	}

	/**
	 * Returns the i-th of the Integer values, with nulls replaced by specified
	 * default value
	 */
	public int toIntValue(int i, int defaultValue) {
		Integer objValue = (Integer) retrieveValueObject(i, INTEGERS);
		if (null == objValue) {
			return defaultValue;
		}
		return (int) objValue;
	}

	/**
	 * Returns the array of Integer values, with nulls replaced by specified
	 * default value
	 */
	public int[] toIntValues(int defaultValue) {
		int[] values = new int[numberOfValues()];
		Arrays.fill(values, defaultValue);
		for (int i = 0; i < numberOfValues(); i++) {
			Integer objValue = integerValue(i);
			if (objValue != null) {
				values[i] = objValue;
			}
		}
		return values;
	}

	// -------------- Long strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Long value.
	 */
	public Long longValue() {
		return (Long) retrieveValueObject(0, LONG);
	}

	/**
	 * Returns the i-th of the Long values.
	 */
	public Long longValue(int i) {
		return (Long) retrieveValueObject(i, LONGS);
	}

	/**
	 * Returns the array of Long values.
	 */
	public Long[] longValues() {
		return (Long[]) retrieveValueObjects(LONGS);
	}

	// -------------- long strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Long value, with null replaced by specified default
	 * value
	 */
	public long toLongValue(long defaultValue) {
		Long objValue = (Long) retrieveValueObject(0, LONG);
		if (null == objValue) {
			return defaultValue;
		}
		return (long) objValue;
	}

	/**
	 * Returns the i-th of the Long values, with nulls replaced by specified
	 * default value
	 */
	public long toLongValue(int i, long defaultValue) {
		Long objValue = (Long) retrieveValueObject(i, LONGS);
		if (null == objValue) {
			return defaultValue;
		}
		return (long) objValue;
	}

	/**
	 * Returns the array of Long values, with nulls replaced by specified
	 * default value
	 */
	public long[] toLongValues(long defaultValue) {
		long[] values = new long[numberOfValues()];
		Arrays.fill(values, defaultValue);
		for (int i = 0; i < numberOfValues(); i++) {
			Long objValue = longValue(i);
			if (objValue != null) {
				values[i] = objValue;
			}
		}
		return values;
	}

	// -------------- Double strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Double value.
	 */
	public Double doubleValue() {
		return (Double) retrieveValueObject(0, DOUBLE);
	}

	/**
	 * Returns the i-th of the Double values.
	 */
	public Double doubleValue(int i) {
		return (Double) retrieveValueObject(i, DOUBLES);
	}

	/**
	 * Returns the array of Double values.
	 */
	public Double[] doubleValues() {
		return (Double[]) retrieveValueObjects(DOUBLES);
	}

	// -------------- double strongly-typed retrieval ----------------

	/**
	 * Returns the scalar Double value, with null replaced by specified default
	 * value
	 */
	public double toDoubleValue(double defaultValue) {
		Double objValue = (Double) retrieveValueObject(0, DOUBLE);
		if (null == objValue) {
			return defaultValue;
		}
		return (double) objValue;
	}

	/**
	 * Returns the i-th of the Double values, with nulls replaced by specified
	 * default value
	 */
	public double toDoubleValue(int i, double defaultValue) {
		Double objValue = (Double) retrieveValueObject(i, DOUBLES);
		if (null == objValue) {
			return defaultValue;
		}
		return (double) objValue;
	}

	/**
	 * Returns the array of Double values, with nulls replaced by specified
	 * default value
	 */
	public double[] toDoubleValues(double defaultValue) {
		double[] values = new double[numberOfValues()];
		Arrays.fill(values, defaultValue);
		for (int i = 0; i < numberOfValues(); i++) {
			Double objValue = doubleValue(i);
			if (objValue != null) {
				values[i] = objValue;
			}
		}
		return values;
	}

	// -------------------- parse ------------

	@SuppressWarnings("rawtypes")
	private Comparable parse(String value, String fieldName) {
		if (null == valueType) {
			throw new RuntimeException("parameter '" + name
					+ " type has not not been set, cannot parse value");
		}
		if (null == value || value.trim().isEmpty()) {
			// do not attempt to parse numeric types if the value is null/empty
			if (valueType.isNumericType())
				return null;
		} else {
			// trim leading/trailing spaces
			value = value.trim();
		}
		Comparable result = null;
		try {
			switch (valueType) {
			case STRING:
			case STRINGS:
				result = value;
				break;
			case BOOLEAN:
			case BOOLEANS:
				result = Boolean.parseBoolean(value);
				break;
			case INTEGER:
			case INTEGERS:
				result = Integer.parseInt(value);
				break;
			case LONG:
			case LONGS:
				result = Long.parseLong(value);
				break;
			case DOUBLE:
			case DOUBLES:
				result = Double.parseDouble(value);
				break;
			default:
				throw new RuntimeException(
						"Internal error: AktanaParameter.parse method does not yet support value type "
								+ valueType);
			}
		} catch (NumberFormatException e) {
			throw new NumberFormatException("invalid " + valueType
					+ " value for " + fieldName + ": " + value);
		}
		return result;
	}

	// -------------------- toString ------------

	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		sb.append(this.name).append(",");
		sb.append(this.valueType).append(",");
		sb.append(this.isNullValueAllowed).append(",");
		sb.append("min:").append(this.minValue).append(",");
		sb.append("max:").append(this.maxValue).append(",");
		if (value == null) {
			sb.append("value:null");
		} else {
			sb.append("value:\"").append(this.value).append("\"}");
		}
		return sb.toString();
	}

	// -------------------- main, for DEV testing ------------

	public static void main(String[] args) {
		AktanaParameter param;

		param = new AktanaParameter("AString", "string", true);
		param.setValue("AAA, BBB; CCC; ; DDD");
		System.out.println("\nParam:" + param + "\n\tValue(s):"
				+ Arrays.asList(param.getValueObject()));

		param = new AktanaParameter("MultipleStrings", "string[]", true);
		param.setValue("AAA, BBB; CCC; ; DDD");
		System.out.println("\nParam:" + param + "\n\tValue(s):"
				+ Arrays.asList(param.getValueObject()));

		param = new AktanaParameter("MultipleBooleans", "boolean[]", true);
		param.setValue("T,Yes,true,True,TRUE,,false");
		System.out.println("\nParam:" + param + "\n\tValue(s):"
				+ Arrays.asList(param.getValueObject()));

		param = new AktanaParameter("MultipleIntegers", "integer[]", true);
		param.setValue("1,2,,4");
		System.out.println("\nParam:" + param + "\n\tValue(s):"
				+ Arrays.asList(param.getValueObject()));
		int[] intVals = param.toIntValues(99);
		for (int i = 0; i < param.numberOfValues(); i++)
			System.out.println("\tintVals[" + i + "]: " + intVals[i]);

		param = new AktanaParameter("ABoolean", "boolean", false);
		param.setValue("true");
		System.out.println("\nParam:" + param + "\n\tValue(s):"
				+ Arrays.asList(param.getValueObject()));
		System.out.println("param.getValue()=" + param.getValue()); // OK
		try {
			System.out.println("param.getStringValue()=" + param.stringValue()); // wrong
																					// type
		} catch (Exception e) {
			System.out.println("Type conversion is not allowed");
		}
	}
	
	//---------------------- Cloneable --------------------
	
	@Override
	public Object clone() {
		AktanaParameter clone = new AktanaParameter();
		clone.setName(this.name);
		clone.setValueType(this.valueType);
		clone.setMinValue(this.minValue);
		clone.setMaxValue(this.maxValue);
		clone.setIsNullValueAllowed(this.isNullValueAllowed);
		if (this.value != null && !this.value.trim().isEmpty()) {
			clone.setValue(this.value);
		}
		return clone;
	}
	
	//---------------------- Comparable --------------------

	@Override
	public int compareTo(Object arg0) {
		AktanaParameter that = (AktanaParameter) arg0;
		return this.name.compareTo(that.name);
	}
}
