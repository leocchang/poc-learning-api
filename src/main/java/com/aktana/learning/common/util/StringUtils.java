package com.aktana.learning.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Provide simple convenience methods for Strings
 */
public class StringUtils {
	
	/**
	 * Returns a string of length "nchars", with "str" (shortened, if needed) in leftmost position
	 * and right-padded with spaces if needed.
	 */
	public static String leftmost(String str, int nchars) {
		String rightPadding = new String(new char[nchars]).replace("\0", " ");
		return str.concat(rightPadding).substring(0, nchars);
	}

	/**
	 * Returns a string consisting of the specified character "ch" repeated "n" times
	 */
	public static String fill(char ch, int n) {
		String result = new String(new char[n]).replace("\0", Character.toString(ch));
		return result;
	}
	
	public static String centerAndPad(String str, int minFinalWidth) {
		if (str.length() >= minFinalWidth) return str;
		
		int nPadCharsEachSide = (int) Math.ceil( 0.5d * (minFinalWidth - str.length()) );
		String leftPad = fill(' ', nPadCharsEachSide);
		String rightPad = 2*nPadCharsEachSide+str.length() > minFinalWidth ? fill(' ', nPadCharsEachSide-1) : leftPad;
		return leftPad + str + rightPad;
	}
	
	/**
	 * Returns the specified multi-line "title", with a box made of "ch" characters around it
	 */
	public static String putBoxAround(char ch, int verticalEdgeWidth, int minTitleWidth, String... titleLines) {
		if (verticalEdgeWidth <= 0) verticalEdgeWidth = 1;
		// first find  max titleLine length
		int maxLineLength = 0;
		for (String line : titleLines) {
			maxLineLength = Math.max(maxLineLength, line.length());
		}
		int standardCenteredTitleWidth = Math.max(maxLineLength, minTitleWidth);
		int totalWidth = standardCenteredTitleWidth + 2*(verticalEdgeWidth+1);

		String topAndBottomLine = fill(ch, totalWidth);
		String titleLineBorder = fill(ch, verticalEdgeWidth);
		StringBuilder sb = new StringBuilder();
		sb.append(topAndBottomLine).append("\n");
		for (String line : titleLines) {
			String centeredLine = centerAndPad(line, standardCenteredTitleWidth);
			sb.append(titleLineBorder).append(" ").append(centeredLine).append(" ").append(titleLineBorder).append("\n");
		}
		sb.append(topAndBottomLine).append("\n");
		return sb.toString();
	}
	
	public static String underline(String text, char ch) {
		StringBuilder sb = new StringBuilder(text);
		if (!text.endsWith("\n")) {
			sb.append("\n");
		}
		sb.append( fill(ch, text.length()));
		return sb.toString();
	}

	// for DEV testing only
	public static void main(String[] args) {
		System.out.println(putBoxAround('*', 5, 40, "Example Report Title"));
		System.out.println(underline("Example Section Header", '-'));
		
		System.out.println(putBoxAround('*', 5, 40, "REPORT TITLE", "Date: Jan 1, 2016"));
		}

}
