/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.common.util.datetime;

public enum DateFormats {
    ISO_FORMAT_DATE,
    ISO_FORMAT_DATE_TIME,
    ISO_FORMAT_DATE_TIME_UTC
}
