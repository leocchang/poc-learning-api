/*****************************************************************
 * <p>TODO: CLASS DESCRIPTION</p>
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 * 
 * Copyright (C) 2012-2013 Aktana Inc.
 *
 *****************************************************************/

package com.aktana.learning.common.util.datetime;

import ma.glasnost.orika.converter.builtin.DateToStringConverter;

public class DateConverterFactory {
	
    final String ISO_FORMAT_DATE_TIME = "yyyy-MM-dd'T'HH:mm";
    final String ISO_FORMAT_DATE = "yyyy-MM-dd";
    final String ISO_FORMAT_DATE_TIME_UTC = "yyyy-MM-dd'T'HH:mm:ss.000Z";
    private static DateConverterFactory instance = null;

    private DateConverterFactory() {

    }

    public static DateConverterFactory getInstance() {
        if (instance == null) {
            instance = new DateConverterFactory();
        }
        return instance;
    }

    /**
     * getDateConverter function return a DateToStringConverter using ISO_FORMAT to format dates 
     * @return DateToStringConverter 
     */
	public DateToStringConverter getDateConverter(DateFormats format)
	{
		switch  (format)
		{
		case ISO_FORMAT_DATE_TIME:
			return new DateToStringConverter(this.ISO_FORMAT_DATE_TIME);
		case ISO_FORMAT_DATE:
		    return new DateToStringConverter(this.ISO_FORMAT_DATE);
		case ISO_FORMAT_DATE_TIME_UTC:
		    return new DateToStringConverter(this.ISO_FORMAT_DATE_TIME_UTC);
		}
		return null;
	}
}
