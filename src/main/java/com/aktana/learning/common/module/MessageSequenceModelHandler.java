package com.aktana.learning.common.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.dto.AccountMessageSequenceDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.persistence.models.impl.AccountMessageSequence;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningRun;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessageSequenceModelHandler extends LearningModelHandler {

	private GenericDao<AccountMessageSequence, String> accountMessageSequenceDao = null;
	
	private static String moduleName = "sparkMessageSequence";
	private static String buildDriver = "messageSequenceDriver";
	private static String scoreDriver = "messageScoreDriver";

	
	public MessageSequenceModelHandler(Logger logger) {
		super(logger, LearningModelTypeEnum.MESSAGE_SEQUENCE_MODEL_TYPE);
		accountMessageSequenceDao = new GenericDaoImpl<AccountMessageSequence, String>(AccountMessageSequence.class);
	}
	
	private GenericDao<AccountMessageSequence, String> getAccountMessageSequenceDao() {
		accountMessageSequenceDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return accountMessageSequenceDao;
	}
	
	@Override
	public RundeckAPIEnum getRundeckAPIEnumForEMR() {
		return RundeckAPIEnum.RUNDECK_API_MSO_EMR;
	}
	
	@Override
	public String getModelName() {
		return MessageSequenceModelHandler.moduleName;
	}
	
	@Override
	public String getBuildDriver() {
		return MessageSequenceModelHandler.buildDriver;
	}
	
	@Override
	public String getScoreDriver() {
		return MessageSequenceModelHandler.scoreDriver;
	}

	@Override
	public boolean isBuildRequired() {
		return true;
	}

	@Override
	public void publish(LearningConfig learningConfig) {
		try {
			String body =  "{\"messageAlgorithmUID\":\""+learningConfig.getId()+"\","
					+"\"messageAlgorithmName\":\""+(learningConfig.getLearningConfigName())+"\","
					+ "\"messageAlgorithmDescription\":\""+(learningConfig.getLearningConfigDescription())+"\","
					+ "\"productUID\":\""+(learningConfig.getProductUID())+"\"," 
					+ "\"repActionChannel\":\""+(learningConfig.getChannelUID())+"\"," 
					+ "\"messageAlgorithmType\":\"MESSAGE_SEQUENCE_LEARNING_MODEL\"," 
					+ "\"isActive\":\"true\"," 
					+ "\"isDeleted\":\"false\"}"; 
			String result = DSEConnector.getInstance().callPost(DSEAPIEnum.DSE_API_MESSAGEALGORITHM, body);
			LOGGER.info("DSEAPIEnum.DSE_API_MESSAGEALGORITHM publish result: "+result);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
	}

	@Override
	public void unpublish(LearningConfig learningConfig) {
		try {
			String result = DSEConnector.getInstance().callDelete(DSEAPIEnum.DSE_API_MESSAGEALGORITHM, learningConfig.getId());
			LOGGER.info("DELETE DSEAPIEnum.DSE_API_MESSAGEALGORITHM: unpublish result: "+result);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
	}

	@Override
	public List<LearningScoreDTO> getLocalScores(LearningRun learningRun, Integer page, Integer rows) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		List<AccountMessageSequence> amsList;
		
		Integer offSet = super.getOffset(page, rows);
		Integer limit = super.getLimit(page, rows);
		
		amsList = this.getAccountMessageSequenceDao().findWithNamedQuery("AccountMessageSequence.custom.findByLearningRun", map, offSet, offSet+limit);
		
		return formAMSLocalScoresDTO(learningRun, amsList);

	}

	@Override
	public List<LearningScoreDTO> getPublishedScores(LearningRun learningRun, Integer page, Integer rows) {
		String amsJson;
		
		try {
			amsJson =  DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_ACCOUNTMESSAGESEQUENCE, "/run/"+learningRun.getId()+super.getPaginationString(page, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
		return  formAMSPublishedScoresDTO(amsJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows) {
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		map.put("accountUID", accountUID);
		
		List<AccountMessageSequence> amsList = this.getAccountMessageSequenceDao().findWithNamedQuery("AccountMessageSequence.custom.findByAccountUID", map);
		
		return formAMSLocalScoresDTO(learningRun, amsList);
	}

	@Override
	public List<LearningScoreDTO> getPublishedAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows) {
		String amsJson;
		
		try {
			amsJson =  DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_ACCOUNTMESSAGESEQUENCE, "/run/"+learningRun.getId()+"/account/"+accountUID+super.getPaginationString(page, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
		return formAMSPublishedScoresDTO(amsJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		throw new GenericValidationException("AccountMessageSequence do not have repUIDs.");
	}

	@Override
	public List<LearningScoreDTO> getPublishedRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		throw new GenericValidationException("AccountMessageSequence do not have repUIDs.");
	}
	
	public List<LearningScoreDTO> formAMSPublishedScoresDTO(String jsonResponse) {
		List<LearningScoreDTO> learningScoreDTOs = new ArrayList<LearningScoreDTO>();
		JSONArray amsJsonArray = new JSONArray(jsonResponse);
		for (int key = 0; key < amsJsonArray.length(); key++) {
			JSONObject amsObject = amsJsonArray.getJSONObject(key);
			
			AccountMessageSequenceDTO dto = new AccountMessageSequenceDTO();

			dto.setLearningRunUID(amsObject.getString("learningRunUID"));
			dto.setAccountUID(amsObject.getString("accountUID"));
			dto.setMessageUID(amsObject.getString("messageUID"));
			dto.setProbability(amsObject.getDouble("probability"));
			dto.setModelId(amsObject.getString("modelId"));
			dto.setIsPredict(amsObject.getBoolean("isPredict"));
			
			learningScoreDTOs.add(dto);
		}
		return learningScoreDTOs;
	}
	
	public List<LearningScoreDTO> formAMSLocalScoresDTO(LearningRun learningRun, List<AccountMessageSequence> amsList) {
		List<LearningScoreDTO> learningScoreDTOs = new ArrayList<LearningScoreDTO>();
		
		for(AccountMessageSequence accountMessageSequence : amsList) {
			AccountMessageSequenceDTO dto = new AccountMessageSequenceDTO();

			dto.setLearningRunUID(learningRun.getId());
			dto.setLearningBuildUID(accountMessageSequence.getLearningBuild().getId());
			dto.setAccountUID(accountMessageSequence.getId().getAccountUID());
			dto.setMessageUID(accountMessageSequence.getId().getMessageUID());
			dto.setProbability(accountMessageSequence.getProbability());

			learningScoreDTOs.add(dto);
		}
		return learningScoreDTOs;
	}
}
