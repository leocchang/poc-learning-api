package com.aktana.learning.common.module;

import java.util.List;

import org.slf4j.Logger;

import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningRun;

import lombok.Getter;

/**
 * Abstract class for handling learning model-specific operations.
 * Model-specific handlers extend this class and implement these model-specific operations.
 * This simplifies the operation of the main BOs and on-boarding of new models.
 */
@Getter
public abstract class LearningModelHandler {

	protected static Logger LOGGER;
	protected LearningModelTypeEnum  learningModelTypeEnum;
	
	/**
	 * Constructor
	 * 
	 * @param logger
	 * @param learningModelTypeEnum
	 */
	public LearningModelHandler(Logger logger, LearningModelTypeEnum  learningModelTypeEnum) {
		this.LOGGER = logger;
		this.learningModelTypeEnum = learningModelTypeEnum;
	}
	
	/**
	 * Instantiates a model handler based on learningModelTypeEnum
	 * 
	 * @param logger
	 * @param learningModelTypeEnum
	 * @return implementing class for abstract LearningModelHandler
	 */
	public static LearningModelHandler getHandlerInstance(Logger logger, LearningModelTypeEnum  learningModelTypeEnum) {
		switch(learningModelTypeEnum) {
		case ANCHOR_MODEL_TYPE:
			return new AnchorModelHandler(logger);
		case MESSAGE_SEQUENCE_MODEL_TYPE:
			return new MessageSequenceModelHandler(logger);
		case MESSAGE_TIMING_MODEL_TYPE:
			return new MessageTimingModelHandler(logger);
		case REP_ENGAGEMENT_MODEL_TYPE:
			return new RepEngagementModelHandler(logger);
		default:
			throw new GenericValidationException(400, "Learning Model Type "+learningModelTypeEnum+" not supported!");
		}
	}
	
	/**
	 * Parses modelType name and returns LearningModelTypeEnum (see LearningModelTypeEnum.parse())
	 * 
	 * @param modelType
	 * @return 
	 */
	public static LearningModelTypeEnum getCheckLearningModelTypeEnum(String modelType) {
		LearningModelTypeEnum modelTypeEnum = LearningModelTypeEnum.parse(modelType);
		if(modelTypeEnum == null) {
			throw new GenericValidationException("Model of type "+modelType+" is not supported at this time.");
		}
		
		return modelTypeEnum;
	}
	
	/**
	 * Instantiates a model handler based on modelType name (see LearningModelTypeEnum.parse()) 
	 * 
	 * @param logger
	 * @param modelType
	 * @return implementing class for abstract LearningModelHandler
	 */
	public static LearningModelHandler getHandlerInstance(Logger logger, String modelType) {
		LearningModelTypeEnum modelTypeEnum = getCheckLearningModelTypeEnum(modelType);
		return getHandlerInstance(logger, modelTypeEnum);
	}
	
	
	/**
	 * Returns the rundeck api enum for the model's emr job
	 * @return
	 */
	public abstract RundeckAPIEnum getRundeckAPIEnumForEMR();
	
	/**
	 * Returns the name of the model (for calling emr job)
	 * @return
	 */
	public abstract String getModelName();
	
	/**
	 * Returns the name of the model's build driver (for calling emr job)
	 * @return
	 */
	public abstract String getBuildDriver();
	
	/**
	 * Returns the name of the model's scoring driver (for calling emr job)
	 * @return
	 */
	public abstract String getScoreDriver();
	
	/**
	 * Some models do not require a separate build before scoring. 
	 *  
	 * @return true if model requires build
	 */
	public abstract boolean isBuildRequired();
	
	/**
	 * Model specific publishing tasks (e.g. call DSE API)
	 * 
	 * @param learningConfig
	 */
	public abstract void publish(LearningConfig learningConfig);
	
	/**
	 * Model specific un-publishing tasks (e.g call DSE API)
	 * 
	 * @param learningConfig
	 */
	public abstract void unpublish(LearningConfig learningConfig);
	
	/**
	 * Get scores from local Learning UI ad hoc single run
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getLocalScores(LearningRun learningRun, Integer page, Integer rows);
	
	/**
	 * Get scores from a single nightly publisher run. Normally only scores from the last few days are available in DSE.
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getPublishedScores(LearningRun learningRun, Integer page, Integer rows);
	
	/**
	 * Get scores from a single run. Will return scores from Learning or DSE DB depending of run type.
	 * Also see getPublishedScores() and getLocalScores()
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public List<LearningScoreDTO> getScores(LearningRun learningRun, Integer page, Integer rows) {
		if(learningRun.getIsPublished()) {
			return getPublishedScores(learningRun, page, rows);
		} else {
			return getLocalScores(learningRun, page, rows);
		}
	}
	
	/**
	 * Get scores for a single account from local Learning UI ad hoc single run
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getLocalAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows);
	
	/**
	 * Get scores for a single account from a single nightly publisher run. Normally only scores from the last few days are available in DSE.
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getPublishedAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows);
	
	/**
	 * Get scores for a single account from a single run. Will return scores from Learning or DSE DB depending of run type.
	 * Also see getPublishedScores() and getLocalScores()
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public List<LearningScoreDTO> getAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows) {
		if(learningRun.getIsPublished()) {
			return getPublishedAccountScores(learningRun, accountUID, page, rows);
		} else {
			return getLocalAccountScores(learningRun, accountUID, page, rows);
		}
	}
	
	
	/**
	 * Get scores for a single rep from local Learning UI ad hoc single run
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getLocalRepScores(LearningRun learningRun, String repUID, Integer page, Integer rows);
	
	/**
	 * Get scores for a single rep from a single nightly publisher run. Normally only scores from the last few days are available in DSE.
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public abstract List<LearningScoreDTO> getPublishedRepScores(LearningRun learningRun, String repUID, Integer page, Integer rows);
	
	/**
	 * Get scores for a single rep from a single run. Will return scores from Learning or DSE DB depending of run type.
	 * Also see getPublishedScores() and getLocalScores()
	 * Returns list of LearningScoreDTOs. Internal structure of DTO is based on the specific model. 
	 * @param learningRun
	 * @param start
	 * @param rows
	 * @return list of LearningScoreDTOs 
	 */
	public List<LearningScoreDTO> getRepScores(LearningRun learningRun, String repUID, Integer page, Integer rows) {
		if(learningRun.getIsPublished()) {
			return getPublishedRepScores(learningRun, repUID, page, rows);
		} else {
			return getLocalRepScores(learningRun, repUID, page, rows);
		}
	}
	
	public String getPaginationString(Integer page, Integer rows) {
		String paginatedString;
		if (page == null || rows == null) {
			paginatedString = "";
		} else {
			paginatedString = "?page="+page+"&rows="+rows;
		}
		return paginatedString;
	}
	
	public static int getOffset(Integer page, Integer rows) {
		 if ((page != null || rows != null) && (page > 0 && rows > 0)) {
			 return (page - 1) * rows;
		 }
		 return 0;
	 }
	 
	 public static int getLimit(Integer page, Integer rows) {
		 if ((page != null || rows != null) && (page > 0 && rows > 0)) {
			 return rows;
		 }
		 return 100;
	 }
}
