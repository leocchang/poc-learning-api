package com.aktana.learning.common.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.dto.AccountMessageSequenceDTO;
import com.aktana.learning.api.dto.AccountTimeToEngageDTO;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.persistence.models.impl.AccountTimeToEngage;
import com.aktana.learning.persistence.models.impl.AccountTimeToEngagePK;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningRun;

public class MessageTimingModelHandler extends LearningModelHandler {

	private GenericDao<AccountTimeToEngage, AccountTimeToEngagePK> accountTimeToEngageDao = null;
	
	private static String moduleName = "sparkMessageTiming";
	private static String buildDriver = "messageTimingDriver";
	private static String scoreDriver = "messageTimingScoreDriver";

	public MessageTimingModelHandler(Logger logger) {
		super(logger, LearningModelTypeEnum.MESSAGE_TIMING_MODEL_TYPE);
		accountTimeToEngageDao = new GenericDaoImpl<AccountTimeToEngage, AccountTimeToEngagePK>(AccountTimeToEngage.class);
	}

	private GenericDao<AccountTimeToEngage, AccountTimeToEngagePK> getAccountTimeToEngageDao() {
		accountTimeToEngageDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return accountTimeToEngageDao;
	}
	
	@Override
	public RundeckAPIEnum getRundeckAPIEnumForEMR() {
		return RundeckAPIEnum.RUNDECK_API_TTE_EMR;
	}
	
	@Override
	public String getModelName() {
		return MessageTimingModelHandler.moduleName;
	}
	
	@Override
	public String getBuildDriver() {
		return MessageTimingModelHandler.buildDriver;
	}
	
	@Override
	public String getScoreDriver() {
		return MessageTimingModelHandler.scoreDriver;
	}

	@Override
	public boolean isBuildRequired() {
		return true;
	}

	@Override
	public void publish(LearningConfig learningConfig) {
		try {
			String body =  "{\"tteAlgorithmUID\":\""+learningConfig.getId()+"\","
					+"\"tteAlgorithmName\":\""+(learningConfig.getLearningConfigName())+"\","
					+ "\"tteAlgorithmDescription\":\""+(learningConfig.getLearningConfigDescription())+"\","
					+ "\"isActive\":\"true\","
					+ "\"isDeleted\":\"false\"}";
			String result = DSEConnector.getInstance().callPost(DSEAPIEnum.DSE_API_TIMETOENGAGEALGORITHM, body);
			LOGGER.info("DSEAPIEnum.DSE_API_TIMETOENGAGEALGORITHM publish result: "+result);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());
		}
	}

	@Override
	public void unpublish(LearningConfig learningConfig) {
		try {
			String result = DSEConnector.getInstance().callDelete(DSEAPIEnum.DSE_API_TIMETOENGAGEALGORITHM, learningConfig.getId());
			LOGGER.info("DELETE DSEAPIEnum.DSE_API_TIMETOENGAGEALGORITHM: unpublish result: "+result);
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());
		}
	}

	@Override
	public List<LearningScoreDTO> getLocalScores(LearningRun learningRun, Integer start, Integer rows) {

		// validate limit and offset
		if(start == null || start <= 0 || rows == null || rows <= 0) {
			start = 1;
			rows = 100;
		}

		Integer offset = rows*(start-1);
		Integer limit = offset + rows;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		String namedQueryName = "AccountTimeToEngage.custom.findTTEscoresByLearningRun";
		return getScoreDTOs(namedQueryName, map, offset, limit, learningRun.getLearningBuild().getId());

	}

	@Override
	public List<LearningScoreDTO> getPublishedScores(LearningRun learningRun, Integer start, Integer rows) {
		String tteJson;

		try {
			tteJson =  DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_ACCOUNTTIMETOENGAGE, "/run/"+learningRun.getId()+super.getPaginationString(start, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());
		}
		return  formAMSPublishedScoresDTO(tteJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalAccountScores(LearningRun learningRun, String accountUID, Integer start, Integer rows) {

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		map.put("accountUID", accountUID);
		String namedQueryName = "AccountTimeToEngage.custom.findTTEscoresByLearningRunAndAccount";
		return getScoreDTOs(namedQueryName, map, start, rows ,learningRun.getLearningBuild().getId()); // default offset and limit
	}

	@Override
	public List<LearningScoreDTO> getPublishedAccountScores(LearningRun learningRun, String accountUID, Integer start, Integer rows) {
		String tteJson;

		try {
			tteJson =  DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_ACCOUNTTIMETOENGAGE, "/run/"+learningRun.getId()+"/account/"+accountUID+super.getPaginationString(start, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());
		}

		return formAMSPublishedScoresDTO(tteJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LearningScoreDTO> getPublishedRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		throw new GenericValidationException("TTE published scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	private List<LearningScoreDTO> getScoreDTOs(String namedQueryName, HashMap<String, Object> map, Integer offset, Integer limit, String learningBuildUID) {
		List<AccountTimeToEngage> models;

		if(offset == null || limit == null) {
			models = getAccountTimeToEngageDao().findWithNamedQuery(namedQueryName, map, offset, limit);
		} else {
			models = getAccountTimeToEngageDao().findWithNamedQuery(namedQueryName, map, offset, limit);
		}
		List<LearningScoreDTO> DTOs = new ArrayList<LearningScoreDTO>();
		for(AccountTimeToEngage model : models) {
			AccountTimeToEngageDTO dto = new AccountTimeToEngageDTO();

			dto.setLearningBuildUID(learningBuildUID);
			dto.setAccountUID(model.getId().getAccountUID());
			dto.setChannelUID(model.getId().getChannelUID());
			dto.setLearningRunUID(model.getId().getLearningRun().getId());
			dto.setMethod(model.getId().getMethod());
			dto.setPredictionDate(model.getId().getPredictionDate().toString());
			dto.setProbability(model.getProbability());

			DTOs.add(dto);
		}
		return DTOs;
	}

	private List<LearningScoreDTO> formAMSPublishedScoresDTO(String jsonResponse) {
		List<LearningScoreDTO> learningScoreDTOs = new ArrayList<LearningScoreDTO>();
		JSONArray tteJsonArray = new JSONArray(jsonResponse);
		for (int key = 0; key < tteJsonArray.length(); key++) {
			JSONObject tteObject = tteJsonArray.getJSONObject(key);

			AccountTimeToEngageDTO dto = new AccountTimeToEngageDTO();

			// commented fields do not exist in the DSE api, uncomment if added by DSE
			//dto.setLearningBuildUID(tteObject.getString("learningBuildUID"));
			dto.setAccountUID(tteObject.getString("accountUID"));
			dto.setChannelUID(tteObject.getString("repActionType"));
			dto.setLearningRunUID(tteObject.getString("learningRunUID"));
			//dto.setMethod(tteObject.getString("method"));
			dto.setProbability(tteObject.getDouble("probability"));
			dto.setPredictionDate(tteObject.getString("predictionDate"));

			learningScoreDTOs.add(dto);
		}
		return learningScoreDTOs;
	}

}
