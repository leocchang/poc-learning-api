package com.aktana.learning.common.module;

import java.util.List;

import org.slf4j.Logger;

import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningRun;

public class AnchorModelHandler extends LearningModelHandler {
	
	private static String moduleName = "sparkAnchor";
	private static String buildDriver = null;
	private static String scoreDriver = "anchorDriver";

	public AnchorModelHandler(Logger logger) {
		super(logger, LearningModelTypeEnum.ANCHOR_MODEL_TYPE);
	}
	
	@Override
	public RundeckAPIEnum getRundeckAPIEnumForEMR() {
		return RundeckAPIEnum.RUNDECK_API_ANCHOR_EMR;
	}
	
	@Override
	public String getModelName() {
		return AnchorModelHandler.moduleName;
	}
	
	@Override
	public String getBuildDriver() {
		return AnchorModelHandler.buildDriver;
	}
	
	@Override
	public String getScoreDriver() {
		return AnchorModelHandler.scoreDriver;
	}

	@Override
	public boolean isBuildRequired() {
		return false;
	}

	@Override
	public void publish(LearningConfig learningConfig) {
	}

	@Override
	public void unpublish(LearningConfig learningConfig) {
	}

	@Override
	public List<LearningScoreDTO> getScores(LearningRun learningRun, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR local scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getLocalScores(LearningRun learningRun, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR local scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getPublishedScores(LearningRun learningRun, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR published scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getLocalAccountScores(LearningRun learningRun, String accountUID, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR local scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getPublishedAccountScores(LearningRun learningRun, String accountUID, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR published scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getLocalRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR local scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

	@Override
	public List<LearningScoreDTO> getPublishedRepScores(LearningRun learningRun, String repUID, Integer start, Integer rows) {
		throw new GenericValidationException("ANCHOR published scores for LearningRun %s are not yet accessible", learningRun.getId());
	}

}
