package com.aktana.learning.common.module;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.aktana.learning.api.AktanaServiceDaos;
import com.aktana.learning.api.dto.LearningScoreDTO;
import com.aktana.learning.api.dto.RepAccountEngagementDTO;
import com.aktana.learning.common.dao.GenericDao;
import com.aktana.learning.common.dao.GenericDaoImpl;
import com.aktana.learning.common.dse.DSEAPIEnum;
import com.aktana.learning.common.dse.DSEConnectionException;
import com.aktana.learning.common.dse.DSEConnector;
import com.aktana.learning.common.exception.GenericValidationException;
import com.aktana.learning.common.rundeck.RundeckAPIEnum;
import com.aktana.learning.persistence.models.impl.LearningConfig;
import com.aktana.learning.persistence.models.impl.LearningRun;
import com.aktana.learning.persistence.models.impl.RepAccountEngagement;

public class RepEngagementModelHandler extends LearningModelHandler {
	
	private GenericDao<RepAccountEngagement, String> repAccountEngagementDao = null;
	
	private static String moduleName = "sparkEngagement";
	private static String buildDriver = null;
	private static String scoreDriver = "engagementDriver";

	public RepEngagementModelHandler(Logger logger) {
		super(logger, LearningModelTypeEnum.REP_ENGAGEMENT_MODEL_TYPE);
		repAccountEngagementDao = new GenericDaoImpl<RepAccountEngagement, String>(RepAccountEngagement.class);
	}
	
	private GenericDao<RepAccountEngagement, String> getRepAccountEngagementDao() {
		repAccountEngagementDao.setSession(AktanaServiceDaos.getInstance().getSessionFactory().getCurrentSession());
		return repAccountEngagementDao;
	}
	
	@Override
	public RundeckAPIEnum getRundeckAPIEnumForEMR() {
		return RundeckAPIEnum.RUNDECK_API_REM_EMR;
	}
	
	@Override
	public String getModelName() {
		return RepEngagementModelHandler.moduleName;
	}
	
	@Override
	public String getBuildDriver() {
		return RepEngagementModelHandler.buildDriver;
	}
	
	@Override
	public String getScoreDriver() {
		return RepEngagementModelHandler.scoreDriver;
	}

	@Override
	public boolean isBuildRequired() {
		return false;
	}

	@Override
	public void publish(LearningConfig learningConfig) {
	}
	
	@Override
	public void unpublish(LearningConfig learningConfig) {
	}
	
	@Override
	public List<LearningScoreDTO> getLocalScores(LearningRun learningRun, Integer page, Integer rows) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		
		List<RepAccountEngagement> raeList;
		
		Integer offSet = super.getOffset(page, rows);
		Integer limit = super.getLimit(page, rows);
		
		raeList = this.getRepAccountEngagementDao().findWithNamedQuery("RepAccountEngagement.custom.findByLearningRun", map, offSet, offSet+limit);
		
		return formREMLocalScoresDTO(learningRun, raeList);

	}

	@Override
	public List<LearningScoreDTO> getPublishedScores(LearningRun learningRun, Integer page, Integer rows) {
		String raeJson;
		
		try {
			raeJson = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REPACCOUNTENGAGEMENT, "/run/"+learningRun.getId()+super.getPaginationString(page, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
		return formREMPublishedScoresDTO(raeJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows) {
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		map.put("accountUID", accountUID);
		
		List<RepAccountEngagement> raeList = this.getRepAccountEngagementDao().findWithNamedQuery("RepAccountEngagement.custom.findByAccountUID", map);
		
		return formREMLocalScoresDTO(learningRun, raeList);
	}

	@Override
	public List<LearningScoreDTO> getPublishedAccountScores(LearningRun learningRun, String accountUID, Integer page, Integer rows) {
		String raeJson;
		
		try {
			raeJson =  DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REPACCOUNTENGAGEMENT, "/run/"+learningRun.getId()+"/account/"+accountUID+super.getPaginationString(page, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
		return formREMPublishedScoresDTO(raeJson);
	}

	@Override
	public List<LearningScoreDTO> getLocalRepScores(LearningRun learningRun, String repUID, Integer page, Integer rows) {
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("learningRunUID", learningRun.getId());
		map.put("repUID", repUID);
		
		List<RepAccountEngagement> raeList;
		
		Integer offSet = super.getOffset(page, rows);
		Integer limit = super.getLimit(page, rows);
		
		raeList = this.getRepAccountEngagementDao().findWithNamedQuery("RepAccountEngagement.custom.findByRepUID", map, offSet, offSet+limit);
		
		return formREMLocalScoresDTO(learningRun, raeList);
	}

	@Override
	public List<LearningScoreDTO> getPublishedRepScores(LearningRun learningRun, String repUID, Integer page, Integer rows) {
		String raeJson;
		
		try {
			raeJson = DSEConnector.getInstance().callGet(DSEAPIEnum.DSE_API_REPACCOUNTENGAGEMENT, "/run/"+learningRun.getId()+"/rep/"+repUID+super.getPaginationString(page, rows));
		} catch (DSEConnectionException e) {
			throw new GenericValidationException(500, "DSE call failed with: "+e.getMessage());	 
		}
		
		return formREMPublishedScoresDTO(raeJson);
	}
	
	public String formatDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}
	
	public List<LearningScoreDTO> formREMPublishedScoresDTO(String jsonResponse) {
		List<LearningScoreDTO> learningScoreDTOs = new ArrayList<LearningScoreDTO>();
		JSONArray raeJsonArray = new JSONArray(jsonResponse);
		for (int key = 0; key < raeJsonArray.length(); key++) {
			JSONObject raeObject = raeJsonArray.getJSONObject(key);
				
			RepAccountEngagementDTO dto = new RepAccountEngagementDTO();

			dto.setLearningRunUID(raeObject.getString("learningRunUID"));
			dto.setAccountUID(raeObject.getString("accountUID"));
			dto.setRepUID(raeObject.getString("repUID"));
			dto.setRepActionTypeId(raeObject.getInt("repActionTypeId"));
			dto.setSuggestionType(raeObject.getString("suggestionType"));
			dto.setDate(raeObject.getString("date"));
			dto.setRunDate(raeObject.getString("runDate"));
			dto.setProbability(raeObject.getDouble("probability"));
				
			learningScoreDTOs.add(dto);
		}
		return learningScoreDTOs;
	}
	
	public List<LearningScoreDTO> formREMLocalScoresDTO(LearningRun learningRun, List<RepAccountEngagement> raeList) {
		List<LearningScoreDTO> learningScoreDTOs = new ArrayList<LearningScoreDTO>();
		
		for(RepAccountEngagement repAccountEngagement : raeList) {
			RepAccountEngagementDTO dto = new RepAccountEngagementDTO();

			dto.setLearningRunUID(learningRun.getId());
			dto.setLearningBuildUID(repAccountEngagement.getLearningBuild().getId());
			dto.setAccountUID(repAccountEngagement.getId().getAccountUID());
			dto.setRepUID(repAccountEngagement.getId().getRepUID());
			dto.setRepActionTypeId(repAccountEngagement.getId().getRepActionTypeId());
			dto.setSuggestionType(repAccountEngagement.getId().getSuggestionType());
			dto.setDate(formatDate(repAccountEngagement.getId().getDate()));
			dto.setProbability(repAccountEngagement.getProbability());

			learningScoreDTOs.add(dto);
		}
		return learningScoreDTOs;
	}
}
