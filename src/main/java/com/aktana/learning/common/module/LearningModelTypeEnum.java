/*****************************************************************
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *****************************************************************/
package com.aktana.learning.common.module;

public enum LearningModelTypeEnum {
	MESSAGE_SEQUENCE_MODEL_TYPE("MSO"),
	REP_ENGAGEMENT_MODEL_TYPE("REM"),
	MESSAGE_TIMING_MODEL_TYPE("TTE"),
	ANCHOR_MODEL_TYPE("ANCHOR");
	
	private String shortName;

	private LearningModelTypeEnum(String shortName) {
		this.shortName = shortName;
	}
	
	public String getShortName() {
		return this.shortName;
	}

	public static LearningModelTypeEnum parse(String type) {
		if (type == null || type.trim().isEmpty())
			return null;

		for(LearningModelTypeEnum modelTypeEnum : LearningModelTypeEnum.values()) {
			if(modelTypeEnum.getShortName().equalsIgnoreCase(type)) return modelTypeEnum;
		}
		
		return null;
	}
	

}
