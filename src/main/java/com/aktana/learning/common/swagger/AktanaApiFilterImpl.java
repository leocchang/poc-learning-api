package com.aktana.learning.common.swagger;

import com.wordnik.swagger.core.filter.SwaggerSpecFilter;  
import com.wordnik.swagger.model.ApiDescription;
import com.wordnik.swagger.model.Operation;
import com.wordnik.swagger.model.Parameter;
import com.wordnik.swagger.model.ApiDescription;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import scala.Option;





import scala.Option;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AktanaApiFilterImpl implements SwaggerSpecFilter  {
	static Logger logger = LoggerFactory.getLogger(AktanaApiFilterImpl.class);

	Map<String, String> endpointType = null;
		
	private void init() {	//@SPECIAL KNOWLEDGE: below is where endpoints are mapped into one or more "api_type" values
		endpointType = new HashMap<String, String>();
		endpointType.put("/Token", "admin,learning");
		endpointType.put("/User", "admin,learning");
		endpointType.put("/info", "admin,learning");
		endpointType.put("/ping", "admin,learning");
		endpointType.put("/LearningConfig", "admin,learning");
		endpointType.put("/LearningVersion", "admin,learning");
		endpointType.put("/LearningParam", "admin,learning");
		endpointType.put("/LearningBuild", "admin,learning");
		endpointType.put("/LearningRun", "admin,learning");
		endpointType.put("/LearningFile", "admin,learning");
		endpointType.put("/DSE", "admin,learning");
	}
	
	public AktanaApiFilterImpl() {
		init();
	}

	@Override
	public boolean isOperationAllowed(Operation operation, ApiDescription api,
			Map<String, List<String>> params, Map<String, String> cookies,
			Map<String, List<String>> headers) {

		String path = api.path();		
		String[] patharr = path.split("/");
		String types = null;
		String version = null;
		
		if (patharr.length == 2) {
			types = endpointType.get("/"+patharr[1]);
		} else if (patharr.length >= 3) {
			if (patharr[1].matches("v\\d+(\\.\\d{1,2})?")) {
				version = patharr[1];				
				types = endpointType.get("/"+patharr[2]);				
			} else {
				types = endpointType.get("/"+patharr[1]);				
			}
		}
		
		boolean retvalue = true;
				
		retvalue = (checkKey(types, params, headers) == true) && (checkVersion(version, params, headers) == true);
				
		return retvalue;
	}

	
	@Override
	public boolean isParamAllowed(Parameter parameter, Operation operation,
			ApiDescription api, Map<String, List<String>> params,
			Map<String, String> cookies, Map<String, List<String>> headers) {
		
		Option<String> access = parameter.paramAccess();
		
		if (access.nonEmpty()) {
			if (access.get().equals("internal")) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public boolean checkKey(String types, Map<String, List<String>> params, Map<String, List<String>> headers) {
	    String keyValue = null;
	    if(params.containsKey("api_type"))
	      keyValue = params.get("api_type").get(0);
	    else {
	      if(headers.containsKey("api_type"))
	        keyValue = headers.get("api_type").get(0);
	    }
	    
	    if (keyValue != null && types != null && !keyValue.equalsIgnoreCase("all")) {
		    if(types.contains(keyValue))
			      return true;
			else
			      return false;	    	
	    } else {
	    	return true;
	    }
	    
	  }	

	


	public boolean checkVersion(String version, Map<String, List<String>> params, Map<String, List<String>> headers) {
	    String keyValue = null;
	    if(params.containsKey("api_version"))
	      keyValue = params.get("api_version").get(0);
	    else {
	      if(headers.containsKey("api_version"))
	        keyValue = headers.get("api_version").get(0);
	    }
	    
	    if (keyValue != null && version != null && !keyValue.equalsIgnoreCase("all")) {
		    if(version.contains(keyValue))
			      return true;
			else
			      return false;	    	
	    } else {
	    	return true;
	    }
	    
	  }


	/*
	@Override
	public boolean isPropertyAllowed(Model model, Property property,
			String propertyName, Map<String, List<String>> params,
			Map<String, String> cookies, Map<String, List<String>> headers) {
		// TODO Auto-generated method stub
		return true;
	}
	*/	
	
}

