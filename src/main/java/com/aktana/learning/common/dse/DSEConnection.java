package com.aktana.learning.common.dse;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.DSEServiceConfiguration;

public class DSEConnection {

	private DSEServiceConfiguration config;
	private String token;
	
	private Logger LOGGER = LoggerFactory.getLogger(DSEConnector.class);
	
	public DSEConnection(DSEServiceConfiguration config) throws DSEConnectionException {
		this.config = config;		
		this.token = login();
	}
	
	private String login() throws DSEConnectionException {
		try {
			Client client = ClientBuilder.newClient();
			
	    		WebTarget target = client.target(config.getUrl()+DSEAPIEnum.DSE_API_TOKEN.getPath());

			String input = "{\"username\":\""+config.getUsername()+"\",\"password\":\""+config.getPassword()+"\",\"secret\":\""+config.getSecret()+"\"}";

			Response response = target.request().post(Entity.json(input), Response.class);
			
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				     + response.getStatus() + " - " + response.toString());
			}

			LOGGER.debug("Output from Server: ");
			String result = response.readEntity(String.class);
			LOGGER.debug(result);
			return result;

		  } catch (Exception e) {

			e.printStackTrace();
			throw new DSEConnectionException(e.getMessage());

		  }

	}
	
	public String callApi(DSEAPIEnum apiEnum, String method) throws DSEConnectionException {
		return callApi(apiEnum, method, null);
	}
	
	public String callApi(DSEAPIEnum apiEnum, String method, String body) throws DSEConnectionException {
		return callApi(apiEnum, method, "application/json;charset=UTF-8", body, null);
	}
	
	private Response makeCall(DSEAPIEnum apiEnum, String method, String contentType, String body, String suffix) {
	 
        String appKey = "Bearer " + this.token;
    	Client client = ClientBuilder.newClient();
    	
    	if(suffix == null) {
    		suffix = "";
    	} 
    	
    	String fullUrl = config.getUrl()+apiEnum.getPath()+suffix;
    	LOGGER.info(apiEnum.name()+" "+method+" url="+fullUrl+(body == null?"":" body="+body));
    	
    		WebTarget target = client.target(fullUrl);
    		Invocation.Builder invocation = target.request().header("Content-Type", contentType).header("Authorization", appKey);
		
    		Response response;
    		Entity<?> entity = Entity.entity(body, MediaType.APPLICATION_JSON);
		if(method.equalsIgnoreCase("POST")) {
			response = invocation.post(entity, Response.class);
		} else if(method.equalsIgnoreCase("GET")) {
			response = invocation.get(Response.class);
		} else if(method.equalsIgnoreCase("PUT")) {
			response = invocation.put(entity, Response.class);
		} else if(method.equalsIgnoreCase("DELETE")) {
			response = invocation.delete(Response.class);
		} else {
			throw new RuntimeException("Unknown method "+method);
		}
		
		return response;
	}
	
    public String callApi(DSEAPIEnum apiEnum, String method, String contentType, String body, String suffix) throws DSEConnectionException {
    	
        try {
            
        	Response response = makeCall(apiEnum, method, contentType, body, suffix);
        	
        	int status = response.getStatus();
        	
        	// Unauthorized (or any error) - login and try again.
        	if(status != 200) {
        		this.token = login();
        		response = makeCall(apiEnum, method, contentType, body, suffix);     	
            	status = response.getStatus();
        	}
        	
			if (status != 200) {
				String message = response.readEntity(String.class);
				LOGGER.error("Output from Server: "+message);
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus() + " - " + response.toString() + " message: "+message);
			}

			LOGGER.debug("Output from Server: ");
			String result = response.readEntity(String.class);
			LOGGER.debug(result);
			return result;
					
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new DSEConnectionException(e.getMessage());
        }
        
    }
}
