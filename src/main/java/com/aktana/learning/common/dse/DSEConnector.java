package com.aktana.learning.common.dse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.DSEServiceConfiguration;
import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.ws.ConnectionException;



public class DSEConnector {

	private static DSEConnector instance;
	
	private DSEServiceConfiguration config;
	private DSEConnection connection;

	private Logger LOGGER = LoggerFactory.getLogger(DSEConnector.class);

	private DSEConnector() {
	}

	public static DSEConnector getInstance() {
		if (instance == null) {
			instance = new DSEConnector();
		}

		return instance;
	}

	public void initializeConnection(DSEServiceConfiguration config) {
		this.config = config;   			
	}

	public DSEServiceConfiguration getConfig() {
		return this.config;
	}
	
	public DSEConnection getConnection() throws DSEConnectionException {

		try {
			if (connection == null) {
				LOGGER.info("Connecting to {} with user {} ...", config.getUrl(), config.getUsername());

				connection = new DSEConnection(this.config);           	            	
			}
		} catch (DSEConnectionException e) {
			LOGGER.error("Error when trying to get connection to dse: " + e.getMessage());
			connection = null;
			throw e;
		}

		return this.connection;
	} 
	
	public String callPost(DSEAPIEnum apiEnum, String body) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, "POST", "application/json;charset=UTF-8", body, null);
	}
	
	public String callPost(DSEAPIEnum apiEnum, String suffix, String body) throws DSEConnectionException {
		System.out.println(body);
		return this.getConnection().callApi(apiEnum, "POST", "application/json;charset=UTF-8", body, suffix);
	}
	
	public String callGet(DSEAPIEnum apiEnum) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, "GET", null);
	}
	
	public String callGet(DSEAPIEnum apiEnum, String suffix) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, "GET", null, null, suffix);
	}
	
	public String callDelete(DSEAPIEnum apiEnum, String uid) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, "DELETE", "application/json;charset=UTF-8", null, "/"+uid);
	}
	
	public String callApi(DSEAPIEnum apiEnum, String method) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, method, null);
	}
	
	public String callApi(DSEAPIEnum apiEnum, String method, String body) throws DSEConnectionException {
		return this.getConnection().callApi(apiEnum, method, "application/json;charset=UTF-8", body, null);
	}
	
    public String callApi(DSEAPIEnum apiEnum, String method, String contentType, String body) throws DSEConnectionException {
    	return this.getConnection().callApi(apiEnum, method, contentType, body, null);
    }
}