package com.aktana.learning.common.dse;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DSEAPIEnum {

	DSE_API_TOKEN("/v3.0/Token"),
	DSE_API_ACCOUNT("/v3.0/Account"),
	DSE_API_PRODUCT("/v3.0/Product"),
	DSE_API_DSECONFIG_DIRECTORY("/v3.0/DSEConfig/directory"),
	DSE_API_DSECONFIG_REFERENCEDATA("/v3.0/DSEConfig/referencedata"),
	DSE_API_REP("/v3.0/Rep"),
	DSE_API_REPACTIONTYPE("/v3.0/RepActionType"),
	DSE_API_MESSAGEALGORITHM("/v3.0/MessageAlgorithm"),
	DSE_API_TIMETOENGAGEALGORITHM("/v3.0/TimeToEngageAlgorithm"),
	DSE_API_MESSAGESET("/v3.0/MessageSet"),
	DSE_API_MESSAGE("/v3.0/Message"),
	DSE_API_MESSAGE_UID("/v3.0/Message"),
	DSE_API_PING("/ping"),
	DSE_API_INFO("/info"),
	DSE_API_ACCOUNTMESSAGESEQUENCE("/v3.0/AccountMessageSequence"),
	DSE_API_REPACCOUNTENGAGEMENT("/v3.0/RepAccountEngagement"),
	DSE_API_ACCOUNTTIMETOENGAGE("/v3.0/AccountTimeToEngage"),
	DSE_API_DSECONFIG_GLOBALPARAMS("/v3.0/DSEConfig"),
	DSE_API_MESSAGE_ALGORITHM("/v3.0/MessageAlgorithm"),
	DSE_API_TTE_ALGORITHM("/v3.0/TimeToEngageAlgorithm"),	
	DSE_API_PARAMETER_CONTROL("/v3.0/ParameterControl");

	private String path;

	
	private DSEAPIEnum(String path) {
		this.path = path;
	}

	@JsonValue
	public String getPath() {
		return this.path;
	}


}
