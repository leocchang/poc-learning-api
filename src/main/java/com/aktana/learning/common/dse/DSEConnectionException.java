package com.aktana.learning.common.dse;

public class DSEConnectionException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8527902095461499115L;

	public DSEConnectionException() {
    }

    public DSEConnectionException(String message) {
        super(message);
    }

    public DSEConnectionException(String message, Throwable th) {
        super(message, th);
    }
}