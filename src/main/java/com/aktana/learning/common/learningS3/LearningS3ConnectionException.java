package com.aktana.learning.common.learningS3;

public class LearningS3ConnectionException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8527902095461499115L;

	public LearningS3ConnectionException() {
    }

    public LearningS3ConnectionException(String message) {
        super(message);
    }

    public LearningS3ConnectionException(String message, Throwable th) {
        super(message, th);
    }
}