package com.aktana.learning.common.learningS3;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LearningS3APIEnum {
	
	LEARNING_S3_API_HEALTHCHECK("/health"),
	LEARNING_S3_API_PROPERTIES("/learning/properties"),
	LEARNING_S3_API_PROPERTIES_NIGHTLY("/learning/properties/nightly");
	
	private String path;


	private LearningS3APIEnum(String path) {
		this.path = path;
	}

	@JsonValue
	public String getPath() {
		return this.path;
	}
	

}
