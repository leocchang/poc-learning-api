package com.aktana.learning.common.learningS3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.stream.Collectors;

import com.aktana.learning.api.DeploymentEnvironment;
import com.aktana.learning.api.LearningS3ServiceConfiguration;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;

import com.amazonaws.regions.Regions;

import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.SSEAwsKeyManagementParams;

public class LearningS3Connection {
	
	private String pathHeader =  "learning/" + DeploymentEnvironment.getEnvironment();	


	private LearningS3ServiceConfiguration config;
	
	private Logger LOGGER = LoggerFactory.getLogger(LearningS3Connector.class);
	
	
	
	public LearningS3Connection(LearningS3ServiceConfiguration config) throws LearningS3ConnectionException {
		this.config = config;
	}
	
	public String getS3PathHeader() {
		return this.pathHeader;
	}
	
	public String getFullS3path() {
		return "s3://"+config.getBucketName() +"/" + this.pathHeader;	
	}
	
	
	private String makeCall(String method, String targetObjectPath, String originObjectPath, String targetContent) throws UnsupportedEncodingException {
		
		// make credentials
		AWSCredentials credentials = new BasicAWSCredentials(
				config.getAccessKey(), 
				config.getSecretKey()
		);
		
		// consider adding region to config
		// make an s3 client with credentials
		AmazonS3 s3client = AmazonS3ClientBuilder
				.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.US_EAST_1) 
				.build();
		
		String bucketName = config.getBucketName();
		String kmsKeyId = config.getKmsKeyId(); // needed for upload and copy
		
		LOGGER.info("LearningS3 making: " + method + " request to bucket: " + bucketName);

		String result = "";
		// make s3 sdk call
		if (method.equalsIgnoreCase("TESTS3")) {
			return s3client.doesBucketExistV2(bucketName) ? "1" : "0";
		} else if(method.equalsIgnoreCase("LISTOBJECTKEYS")) {
			LOGGER.info("target object path: " + targetObjectPath);
			List<String> collect = s3client.listObjects(bucketName, targetObjectPath).getObjectSummaries()
	        		.stream().map(x -> x.getKey()).collect(Collectors.toList());
			result += String.join(";", collect);		
		} else if (method.equalsIgnoreCase("DOWNLOAD")) {	
			LOGGER.info("target object path: " + targetObjectPath);	
			result = s3client.getObjectAsString(bucketName, targetObjectPath);
		} else if(method.equalsIgnoreCase("UPLOAD")) {
			LOGGER.info("target object path: " + targetObjectPath);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(targetContent.getBytes("UTF-8"));
			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(byteArrayInputStream.available());
			s3client.putObject( new PutObjectRequest(bucketName, targetObjectPath, byteArrayInputStream, objectMetadata)
					.withSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams(kmsKeyId))
			);
		} else if(method.equalsIgnoreCase("COPY")) {	
			LOGGER.info("origin object path: " + originObjectPath);
			LOGGER.info("target object path: " + targetObjectPath);
			for(S3ObjectSummary s3ObjectSummary : s3client.listObjects(bucketName, originObjectPath).getObjectSummaries()) {
				CopyObjectRequest copyObjectRequest = new CopyObjectRequest(
						bucketName, s3ObjectSummary.getKey(), 
						bucketName, s3ObjectSummary.getKey().replace(originObjectPath, targetObjectPath)
				).withSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams(kmsKeyId));
				s3client.copyObject(copyObjectRequest);
			}
		} else if(method.equalsIgnoreCase("MOVE")) {	
			LOGGER.info("origin object path: " + originObjectPath);
			LOGGER.info("target object path: " + targetObjectPath);
			for(S3ObjectSummary s3ObjectSummary : s3client.listObjects(bucketName, originObjectPath).getObjectSummaries()) {
				CopyObjectRequest copyObjectRequest = new CopyObjectRequest(
						bucketName, s3ObjectSummary.getKey(), 
						bucketName, s3ObjectSummary.getKey().replace(originObjectPath, targetObjectPath)
				).withSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams(kmsKeyId));
				s3client.copyObject(copyObjectRequest);
				s3client.deleteObject(bucketName, s3ObjectSummary.getKey());
			}
		} else if(method.equalsIgnoreCase("DELETE")) {
			LOGGER.info("target object path: " + targetObjectPath);
			List<String> collect = s3client.listObjects(bucketName, targetObjectPath).getObjectSummaries()
	        		.stream().map(x -> x.getKey()).collect(Collectors.toList());	
			s3client.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(collect.toArray(new String[0])));			
		} else {
			throw new RuntimeException("Unknown method "+method);
		}
		return result;
	}
	
    public String callApi(String method, String targetObjectPath, String originObjectPath, String targetContent) throws LearningS3ConnectionException {	
    	
    	// prepend paths with learning/<deploymentenv>/ (i.e. "learning/dev/" or "Learning/prod/"        	
    	targetObjectPath = this.getS3PathHeader() + "/" + targetObjectPath;
    	originObjectPath = this.getS3PathHeader() + "/" + originObjectPath;
    	  	
        try {
        	String response = makeCall(method, targetObjectPath, originObjectPath, targetContent);
        	
        	// strip path header from obj keys
        	if (method.equalsIgnoreCase("LISTOBJECTKEYS")) response = response.replace(pathHeader, "");
        	
			LOGGER.debug("Output from Server: ");
			LOGGER.debug(response);
			return response;	
        
        } catch (AmazonServiceException ase) {
        	ase.printStackTrace();
        	String errorMessage = "Caught an AmazonServiceException, which means your request made it to Amazon S3, ";
        	errorMessage += "\n" + "but was rejected with an error response for some reason.";
        	errorMessage += "\n" + "Error Message:    " + ase.getMessage();
			errorMessage += "\n" + "HTTP Status Code: " + ase.getStatusCode();
			errorMessage += "\n" + "AWS Error Code:   " + ase.getErrorCode();
			errorMessage += "\n" + "Error Type:       " + ase.getErrorType();
			errorMessage += "\n" + "Request ID:       " + ase.getRequestId();
			throw new LearningS3ConnectionException(errorMessage);
        } catch (AmazonClientException ace) {
        	ace.printStackTrace();
        	String errorMessage = "Caught an AmazonClientException, which means the client encountered ";
        	errorMessage += "a serious internal problem while trying to communicate with S3, ";
        	errorMessage += "such as not being able to access the network.";
        	errorMessage += "\n" + "Error Message: " + ace.getMessage();
        	throw new LearningS3ConnectionException(errorMessage);
        } catch (UnsupportedEncodingException uee) {
        	throw new LearningS3ConnectionException("Unsupported encoding, check source");
        }
    }
}
