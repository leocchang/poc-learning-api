package com.aktana.learning.common.learningS3;

import com.aktana.learning.api.TheCustomerEnvironmentName;

import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.api.LearningS3ServiceConfiguration;



public class LearningS3Connector {

	private static LearningS3Connector instance;
	
	private LearningS3ServiceConfiguration config;
	private LearningS3Connection connection;

	private Logger LOGGER = LoggerFactory.getLogger(LearningS3Connector.class);

	private LearningS3Connector() {
	}

	public static LearningS3Connector getInstance() {
		if (instance == null) {
			instance = new LearningS3Connector();
		}

		return instance;
	}

	public void initializeConnection(LearningS3ServiceConfiguration config) {
		this.config = config;   			
	}
	
	public LearningS3ServiceConfiguration getConfig() {
		return this.config;
	}

	public LearningS3Connection getConnection() throws LearningS3ConnectionException {

		try {
			if (connection == null) {
				LOGGER.info("Connecting to s3. Bucket name is: {}  ...", config.getBucketName());

				connection = new LearningS3Connection(this.config);           	            	
			}
		} catch (LearningS3ConnectionException e) {
			LOGGER.error("Error when trying to get connection to LearningS3: " + e.getMessage());
			connection = null;
			throw e;
		}

		return this.connection;
	}

	public String callTestS3() throws LearningS3ConnectionException {
		return this.getConnection().callApi("TESTS3", null, null, null);
	}
	
	public String callListKeys() throws LearningS3ConnectionException {
		return this.callListKeys(TheCustomerEnvironmentName.getName() + "/");
	}
	
	public String callListKeys(String targetObjectPath) throws LearningS3ConnectionException {
		return this.getConnection().callApi("LISTOBJECTKEYS", targetObjectPath, null, null);
	}
	
	public String callDownload(String targetObjectPath) throws LearningS3ConnectionException {
		return this.getConnection().callApi("DOWNLOAD", targetObjectPath, null, null);
	}
	
	public String callUpload(String targetObjectPath, String targetContent) throws LearningS3ConnectionException {
		return this.getConnection().callApi("UPLOAD", targetObjectPath, null, targetContent);
	}
	
	public String callCopy(String targetObjectPath, String originObjectPath) throws LearningS3ConnectionException {
		return this.getConnection().callApi("COPY", targetObjectPath, originObjectPath, null);
	}
	
	public String callMove(String targetObjectPath, String originObjectPath) throws LearningS3ConnectionException {
		return this.getConnection().callApi("MOVE", targetObjectPath, originObjectPath, null);
	}
	
	public String callDelete(String targetObjectPath) throws LearningS3ConnectionException {
		return this.getConnection().callApi("DELETE", targetObjectPath, null, null);
	}	
}