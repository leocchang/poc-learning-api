package com.aktana.learning.custom.metadata;

import java.io.FileReader; 
import java.lang.reflect.Field; 
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aktana.learning.custom.metadata.run.AktanaLoader;
import com.esotericsoftware.yamlbeans.YamlReader;

import javassist.*;

public class AktanaServiceRun 
{
	
    private static final Logger LOGGER = LoggerFactory.getLogger(AktanaServiceRun.class);
			
	
	
	public static String readCustomer(String fname) throws Exception {
    	YamlReader reader = new YamlReader(new FileReader(fname));
    	Object object = reader.read();
    	//System.out.println(object);

    	// In v3 yml file the deploymentMetadataVersion property is at the root level
    	Map map = (Map)object;
    	String theCustomerEnvironmentName = (String) map.get("theCustomerEnvironmentName");
    	
    	theCustomerEnvironmentName = theCustomerEnvironmentName.toLowerCase();

    	return theCustomerEnvironmentName;
	}


	/*
	 * runs with the customer prefix that is passed in
	 */
    public static void run(String mainClass, String customer, String[] args) throws Exception {
        try {
        	String command = mainClass;
        	

            // set up class loader with translator
        	ClassPool pool = ClassPool.getDefault();
            pool.insertClassPath(new ClassClassPath(AktanaServiceRun.class));
            Loader loader = new AktanaLoader(pool);

            Thread.currentThread().setContextClassLoader(loader);

            loader.run(command, args);

        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }
	
	
	/*
	 * reads the customer prefix from the yml file
	 */
    public static void run(String mainClass, String[] args) throws Exception {
        if (args.length >= 2) {
            try {
            	String command = mainClass;

            	String ymlfile = null;
            	
                if (args[0].equalsIgnoreCase("db") || args[0].equalsIgnoreCase("adb")) { 	//@SPECIAL KNOWLEDGE?  Unknown purpose
                	ymlfile = args[2];
                } else {
                	ymlfile = args[1];
                }
            	            	
            	String customer = readCustomer(ymlfile);
            	

                // set up class loader with translator
            	ClassPool pool = ClassPool.getDefault();
                pool.insertClassPath(new ClassClassPath(AktanaServiceRun.class));
                Loader loader = new AktanaLoader(pool);

                Thread.currentThread().setContextClassLoader(loader);

                if (args[0].equalsIgnoreCase("db")) {                
                	if (args[1].equalsIgnoreCase("migrate")) {
                		args = addIncludeParameters(customer, args);
                	}
                }

                // invoke "main" method of target class
                //String[] pargs = new String[args.length-1];
                //System.arraycopy(args, 1, pargs, 0, pargs.length);
                loader.run(command, args);

            } catch (Throwable ex) {
                ex.printStackTrace();
            }

        } else {
            System.out.println
                ("Usage: AktanaServiceRun <command> <ymlfile>");
        }	
    }

    
    
    
    
    public static String[] addIncludeParameters(String customer, String[] oldargs) {    	
    	// check if --include is already present
    	boolean includefound = false;
    	for (int i=0;i<oldargs.length;++i) {
    		String arg = oldargs[i];
    		
    		if (arg.equalsIgnoreCase("--include")) {
    			includefound = true;
    		}
    	}
    	
    	if (includefound == true) {
    		return oldargs;
    	} 
    	
    	String[] newargs = new String[oldargs.length+2];
        System.arraycopy(oldargs, 0, newargs, 0, oldargs.length);
        
        newargs[oldargs.length] = "--include";
        newargs[oldargs.length+1] = customer;

        return newargs;    	
    }
    
    
	
    public static void main(String[] args) throws Exception {    	
    	String DEFAULT_COMMAND = "com.aktana.learning.api.AktanaService";
    	
    	run(DEFAULT_COMMAND, args);    	
    }

    public static void printClasses(ClassLoader loader) {

    	 Field f = null;
		try {
			f = ClassLoader.class.getDeclaredField("classes");
	    	f.setAccessible(true);
		} catch (NoSuchFieldException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	  try {
			Vector<Class> classes =  (Vector<Class>) f.get(loader);

			for (int i=0; i<classes.size();++i) {
				System.out.println("Class Loaded: " + classes.get(i).getName());
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }


}
