package com.aktana.learning.custom.metadata.run;

import javassist.ClassPool;

public class AktanaLoader extends javassist.Loader {

	public AktanaLoader(ClassPool pool) {
		super(pool);
	}

	@Override
	 protected java.lang.Class loadClassByDelegation(java.lang.String name) throws java.lang.ClassNotFoundException
	 {
		 Class c = null;

         if (doDelegation)
             if (name.startsWith("java.")
                 || name.startsWith("javax.")
                 || name.startsWith("sun.")
                 || name.startsWith("org.w3c.")
                 || name.startsWith("org.xml."))
                 c = delegateToParent(name);

		 return c;
	 }


}
