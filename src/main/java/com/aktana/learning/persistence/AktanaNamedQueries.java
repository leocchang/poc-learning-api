package com.aktana.learning.persistence;

import java.util.HashMap;

import javax.persistence.MappedSuperclass;  
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@MappedSuperclass
@NamedQueries({
		
	@NamedQuery(name = "SysParameter.findByName",
				query = "from SysParameter a where a.name = :name"),
		
	@NamedQuery(name="LearningConfigInfo.findByExternalId", 
				query="FROM LearningConfig l WHERE l.id = :externalId"),
	
	@NamedQuery(name="LearningConfig.custom.findBymodelType", 
				query="FROM LearningConfig l WHERE l.modelType LIKE :modelType AND l.isDeleted = false ORDER BY l.createdAt DESC"),

	@NamedQuery(name="LearningVersionInfo.findByExternalId", 
				query="FROM LearningVersion l WHERE l.id = :externalId"),
	
	@NamedQuery(name="LearningBuild.findByExternalId", 
				query="FROM LearningBuild l WHERE l.id = :externalId"),
	
	@NamedQuery(name="LearningRun.findByExternalId", 
				query="FROM LearningRun l WHERE l.id = :externalId"),
	
	@NamedQuery(name="LearningFileInfo.findByExternalId", 
				query="FROM LearningFile l WHERE l.id = :externalId"),

	@NamedQuery(name="SimulationSegment.findByExternalId", 
			query="FROM SimulationSegment l WHERE l.id = :externalId"),
	
	@NamedQuery(name = "LearningBuild.custom.findDeployedByConfig",
				query = "FROM LearningBuild l WHERE l.learningConfig.id = :learningConfigUID AND l.isDeployed = true AND l.isDeleted = false"),	
	
	@NamedQuery(name = "LearningBuild.custom.findAllByLearningVersion",
		query = "FROM LearningBuild l WHERE l.learningVersion.id = :learningVersionUID"),
	
	@NamedQuery(name = "LearningParam.findByExternalId",
				query = "FROM LearningParam l WHERE l.id.learningVersion.id = :learningVersionUID AND l.id.paramName = :paramName AND l.id.paramIndex = :paramIndex"),
	
	@NamedQuery(name = "LearningParam.custom.findByLearningVersion",
			query = "FROM LearningParam l WHERE l.id.learningVersion.id = :learningVersionUID"),

	@NamedQuery(name = "LearningVersion.custom.findDraftsByLearningConfigOrderByCreated",
			query = "FROM LearningVersion l WHERE l.id.learningConfig.id = :learningConfigUID AND l.isFinalized = false AND l.isDeleted = false  ORDER BY l.createdAt DESC"),	
	
	@NamedQuery(name = "LearningVersion.custom.findVersionsByLearningConfigOrderByCreated",
			query = "FROM LearningVersion l WHERE l.id.learningConfig.id = :learningConfigUID AND l.isDeleted = false ORDER BY l.createdAt DESC"),
	
	@NamedQuery(name = "LearningVersion.custom.findDeployedVersionsByLearningConfigOrderByCreated",
			query = "FROM LearningVersion l WHERE l.id.learningConfig.id = :learningConfigUID AND l.isDeleted = false AND l.isDeployed = true ORDER BY l.createdAt DESC"),
		
	@NamedQuery(name = "LearningVersion.custom.findFinalizedVersionsByLearningConfigOrderByCreated",
		query = "FROM LearningVersion l WHERE l.id.learningConfig.id = :learningConfigUID AND l.isDeleted = false AND l.isFinalized = true ORDER BY l.createdAt DESC"),

	@NamedQuery(name = "LearningBuild.custom.findBuildsByLearningVersionOrderByCreated",
			query = "FROM LearningBuild l WHERE l.id.learningVersion.id = :learningVersionUID AND l.id.isDeleted = false AND l.id.learningVersion.isDeleted = false ORDER BY l.createdAt DESC"),						

	@NamedQuery(name = "LearningBuild.custom.findLatestSuccessfulBuilds",
		query = "FROM LearningBuild l WHERE l.id.learningVersion.id = :learningVersionUID AND l.id.learningVersion.isDeleted = false AND l.executionStatus = 'success' AND l.isDeleted = false ORDER BY l.executionDatetime DESC"),						
	
	@NamedQuery(name = "LearningBuild.custom.findByLearningVersion",
			query = "FROM LearningBuild l WHERE l.id.learningVersion.id = :learningVersionUID AND l.isDeleted = false"),
	
	@NamedQuery(name = "LearningRun.custom.findByLearningVersion",
			query = "FROM LearningRun l WHERE l.id.learningVersion.id = :learningVersionUID"),
	
	@NamedQuery(name = "LearningRun.custom.findByLearningBuild",
			query = "FROM LearningRun l WHERE l.id.learningBuild.id = :learningBuildUID"),
	
	@NamedQuery(name = "LearningRun.custom.findScoringRunByLearningBuild",
		query = "FROM LearningRun l WHERE l.id.learningBuild.id = :learningBuildUID AND l.runType != 'SIMULATION'"),
	
	@NamedQuery(name = "LearningRun.custom.findRunByBuildAndStatusAndRunTypeOrderByDate",
		query = "FROM LearningRun l WHERE l.id.learningBuild.id = :learningBuildUID AND l.runType = :runType AND l.executionStatus = :executionStatus ORDER BY l.executionDatetime DESC"),
	
	@NamedQuery(name = "LearningFile.custom.findByLearningVersion",
			query = "FROM LearningFile l WHERE l.learningBuild.learningVersion.id = :learningVersionUID"),

	@NamedQuery(name = "LearningFile.custom.findByLearningBuild",
			query = "FROM LearningFile l WHERE l.id.learningBuild.id = :learningBuildUID"),

	@NamedQuery(name = "LearningFile.custom.findByLearningRun",
			query = "FROM LearningFile l WHERE l.id.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "AccountMessageSequence.custom.findByLearningRun",
		query = "FROM AccountMessageSequence l WHERE l.id.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "AccountMessageSequence.custom.findByAccountUID",
		query = "FROM AccountMessageSequence l WHERE l.id.learningRun.id = :learningRunUID AND l.accountUID = :accountUID"),
	
	@NamedQuery(name = "RepAccountEngagement.custom.findByLearningRun",
		query = "FROM RepAccountEngagement l WHERE l.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "RepAccountEngagement.custom.findByAccountUID",
		query = "FROM RepAccountEngagement l WHERE l.learningRun.id = :learningRunUID AND l.accountUID = :accountUID"),
	
	@NamedQuery(name = "RepAccountEngagement.custom.findByRepUID",
		query = "FROM RepAccountEngagement l WHERE l.learningRun.id = :learningRunUID AND l.repUID = :repUID"),
	
	@NamedQuery(name = "AccountProduct.custom.findByLearningRun",
		query = "FROM AccountProduct l WHERE l.id.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "SimulationSegment.custom.findByLearningRun",
		query = "FROM SimulationSegment l WHERE l.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "SimulationSegment.custom.findByLearningRunAndSegment",
		query = "FROM SimulationSegment l WHERE l.id = :segmentUID AND l.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "SimulationAccountSegment.custom.findByLearningRunAndSegment",
		query = "FROM SimulationAccountSegment l WHERE l.id.segment.id = :segmentUID AND l.id.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "SimulationAccountSegment.custom.findByLearningRunAndSegmentAndSearchString",
		query = "FROM SimulationAccountSegment l WHERE l.id.segment.id = :segmentUID AND l.id.learningRun.id = :learningRunUID AND l.accountName LIKE :accountName"),
	
	@NamedQuery(name = "SimulationSegmentMessageSequence.custom.findByLearningRunAndSegment",
		query = "FROM SimulationSegmentMessageSequence l WHERE l.id.segment.id = :segmentUID AND l.id.learningRun.id = :learningRunUID AND l.isFinalized = 1"),
		
	@NamedQuery(name = "SimulationAccountMessageSequence.custom.findByLearningRunAndAccount",
		query = "FROM SimulationAccountMessageSequence l WHERE l.id.accountUID = :accountUID AND l.id.learningRun.id = :learningRunUID AND l.isFinalized = 1"),
	
	@NamedQuery(name = "SimulationMessageSequence.custom.findBySequence",
		query = "FROM SimulationMessageSequence l WHERE l.id.messageSeqUID = :messageSeqUID"),
	
	@NamedQuery(name = "LearningObjectList.custom.findAccountByLearningRun",
		query = "FROM LearningObjectList l WHERE l.id.learningRun.id = :learningRunUID AND l.id.objectType = :objectType"),
		
	@NamedQuery(name = "AccountTimeToEngage.custom.findTTEscoresByLearningRun",
		query = "FROM AccountTimeToEngage l WHERE l.id.learningRun.id = :learningRunUID"),
	
	@NamedQuery(name = "LearningConfig.custom.findByCurrentLearningBuild",
		query = "FROM LearningConfig l WHERE l.currentLearningBuild.id = :currentLearningBuildUID"),
	
	@NamedQuery(name = "LearningConfig.custom.findByCurrentLearningVersion",
	query = "FROM LearningConfig l WHERE l.currentLearningVersion.id = :currentLearningVersionUID"),

	@NamedQuery(name = "AccountTimeToEngage.custom.findTTEscoresByLearningRunAndAccount",
		query = "FROM AccountTimeToEngage l WHERE l.id.learningRun.id = :learningRunUID AND l.id.accountUID = :accountUID"),
	
	@NamedQuery(name = "SimulationAccountSentEmail.custom.findByLearningMessageAndAccount",
		query = "FROM SimulationAccountSentEmail l WHERE l.messageUID = :messageUID AND l.accountUID = :accountUID"),
	
	@NamedQuery(name = "SimulationAccountMessageSequence.custom.findByLearningRun",
		query = "FROM SimulationAccountMessageSequence l WHERE l.id.learningRun.id = :learningRunUID AND l.isFinalized = 1"),	
	
	@NamedQuery(name = "SimulationAccountMessageSequence.custom.findByLearningRunAndSearchString",
		query = "FROM SimulationAccountMessageSequence l WHERE l.id.learningRun.id = :learningRunUID AND l.isFinalized = 1 AND l.accountName LIKE :accountName"),	

	@NamedQuery(name = "ApprovedDocuments.custom.findByMessageUID",
		query = "SELECT l.name FROM ApprovedDocuments l WHERE l.messageUID = :messageUID"),
	
	@NamedQuery(name = "ApprovedDocuments.custom.findMessageInfoByMessageUID",
	query = "FROM ApprovedDocuments l WHERE l.messageUID = :messageUID"),
	
	@NamedQuery(name = "AggRepDateLocationAccuracy.custom.findByLearningVersion",
		query = "FROM AggRepDateLocationAccuracy l WHERE l.id.learningVersion.id = :learningVersionUID"),	
	
	@NamedQuery(name = "S3PartialPaths.custom.findS3PartialPathsWithPathType",
		query = "FROM S3PartialPaths l WHERE l.pathType = :pathType"),	

	@NamedQuery(name = "OptimalLearningParams.custom.findByProductAndChannelAndGoal",
		query = "FROM OptimalLearningParams l WHERE l.id.productUID = :productUID AND l.id.channelUID = :channelUID AND l.id.goal = :goal"),	
	
	@NamedQuery(name = "RepEngagementCalculation.custom.getRepTeams",
		query = "FROM RepEngagementCalculation l where l.seConfigId = :configId group by l.repTeamUID"),
	
	@NamedQuery(name = "RepEngagementCalculation.custom.getTerritories",
		query = "FROM RepEngagementCalculation l where l.seConfigId = :configId group by l.territoryId"),
	
	@NamedQuery(name = "CustomerConfigurations.custom.findByConfigurationType",
		query = "FROM CustomerConfigurations l where l.id.configurationType = :configurationType"),
	
	@NamedQuery(name = "CustomerConfigurations.custom.findByConfigurationTypeAndValue",
		query = "FROM CustomerConfigurations l where l.id.configurationType = :configurationType AND l.id.configurationValue = :configurationValue"),
	
	@NamedQuery(name = "ChannelActionMap.custom.findByRepActionChannel",
		query = "FROM ChannelActionMap l where l.repActionChannel = :repActionChannel"),

	@NamedQuery(name = "EngagementFiltersForDSESettings.custom.findBySeConfigIdAndSuggestionType",
		query = "FROM EngagementFiltersForDSESettings l where l.id.seConfigId = :seConfigId AND l.id.suggestionType = :suggestionType"),
	
})
/**
 * This class is intended as the home for all AktanaService named/native queries.
 * 
 * Note that /version3 infrastructure for NamedQueries that are used in standard operations
 * must follow the naming convention: <POJO>.<standardOperation>, where <POJO> is the simple name
 * of the model class involves, and <standardOperation> is one of:
 * 		findByExternalId
 * 		findByName
 */
public class AktanaNamedQueries {
	
	public AktanaNamedQueries() {		
	}
}
