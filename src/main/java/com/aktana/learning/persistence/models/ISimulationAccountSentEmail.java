package com.aktana.learning.persistence.models;

import java.util.Date;


/** 
 * Object interface mapping for hibernate-handled table: sent_email_vod_c_arc.
 * @author autogenerated
 */

public interface ISimulationAccountSentEmail {

	/**
     * Return the value associated with the column: interactionId.
	 * @return A String object (this.interactionId)
	 */
	String getInteractionId();
  
    /**  
     * Set the value related to the column: interactionId.
	 * @param interactionId the interactionId value you wish to set
	 */
	void setInteractionId(final String interactionId);	
	
	/**
     * Return the value associated with the column: messageUID.
	 * @return A String object (this.messageUID)
	 */
	String getMessageUID();
  
    /**  
     * Set the value related to the column: messageUID.
	 * @param messageUID the messageUID value you wish to set
	 */
	void setMessageUID(final String messageUID);
	
	/**
     * Return the value associated with the column: accountUID.
	 * @return A String object (this.accountUID)
	 */
	String getAccountUID();
  
    /**  
     * Set the value related to the column: accountUID.
	 * @param accountUID the accountUID value you wish to set
	 */
	void setAccountUID(final String accountUID);	
	
	/**
     * Return the value associated with the column: isOpen.
	 * @return A Boolean object (this.isOpen)
	 */
	Boolean getIsOpen();
  
    /**  
     * Set the value related to the column: isOpen.
	 * @param isOpen the isOpen value you wish to set
	 */
	void setIsOpen(final Boolean isOpen);		
	
	/**
     * Return the value associated with the column: status.
	 * @return A String object (this.status)
	 */
	String getStatus();
  
    /**  
     * Set the value related to the column: status.
	 * @param status the status value you wish to set
	 */
	void setStatus(final String status);	
	
	/**
     * Return the value associated with the column: clickCount.
	 * @return A Integer object (this.clickCount)
	 */
	Integer getClickCount();
  
    /**  
     * Set the value related to the column: clickCount.
	 * @param clickCount the clickCount value you wish to set
	 */
	void setClickCount(final Integer clickCount);
	
	/**
     * Return the value associated with the column: emailSubject.
	 * @return A String object (this.emailSubject)
	 */
	String getEmailSubject();
  
    /**  
     * Set the value related to the column: emailSubject.
	 * @param emailSubject the emailSubject value you wish to set
	 */
	void setEmailSubject(final String emailSubject);
	
	/**
     * Return the value associated with the column: emailBody.
	 * @return A String object (this.emailBody)
	 */
	String getEmailBody();
  
    /**  
     * Set the value related to the column: emailBody.
	 * @param emailBody the emailBody value you wish to set
	 */
	void setEmailBody(final String emailBody);
	
	/**
     * Return the value associated with the column: emailSentDate.
	 * @return A Date object (this.emailSentDate)
	 */
	Date getEmailSentDate();
  
    /**  
     * Set the value related to the column: emailSentDate.
	 * @param emailSentDate the emailSentDate value you wish to set
	 */
	void setEmailSentDate(final Date emailSentDate);
	
	/**
     * Return the value associated with the column: senderEmailID.
	 * @return A String object (this.senderEmailID)
	 */
	String getSenderEmailID();
  
    /**  
     * Set the value related to the column: senderEmailID.
	 * @param senderEmailID the senderEmailID value you wish to set
	 */
	void setSenderEmailID(final String senderEmailID);
	
	
	/**
     * Return the value associated with the column: accountEmailID.
	 * @return A String object (this.accountEmailID)
	 */
	String getAccountEmailID();
  
    /**  
     * Set the value related to the column: accountEmailID.
	 * @param accountEmailID the accountEmailID value you wish to set
	 */
	void setAccountEmailID(final String accountEmailID);
	
	/**
     * Return the value associated with the column: emailLastOpenedDate.
	 * @return A Date object (this.emailLastOpenedDate)
	 */
	Date getEmailLastOpenedDate();
  
    /**  
     * Set the value related to the column: emailLastOpenedDate.
	 * @param emailLastOpenedDate the emailLastOpenedDate value you wish to set
	 */
	void setEmailLastOpenedDate(final Date emailLastOpenedDate);

	/**
     * Return the value associated with the column: lastClickDate.
	 * @return A Date object (this.lastClickDate)
	 */
	Date getLastClickDate();
  
    /**  
     * Set the value related to the column: lastClickDate.
	 * @param lastClickDate the lastClickDate value you wish to set
	 */
	void setLastClickDate(final Date lastClickDate);
	
	/**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	Date getCreatedAt();
  
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	void setCreatedAt(final Date createdAt);
	
	/**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	Date getUpdatedAt();
  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	void setUpdatedAt(final Date updatedAt);
	
	// end of interface
}