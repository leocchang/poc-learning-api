package com.aktana.learning.persistence.models.impl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aktana.learning.persistence.models.IEngagementFiltersForDSESettings;


/** 
 * Object mapping for hibernate-handled table: LearningParam.
 * @author autogenerated
 */

@Entity
@Table(name = "EngagementFiltersForDSESettings")
public class EngagementFiltersForDSESettings implements Cloneable, Serializable, IEngagementFiltersForDSESettings {

	/** Serial Version UID. */
	private static final long serialVersionUID = -559001494L;	

	/** Field mapping. */
	@Id
	private EngagementFiltersForDSESettingsPK id;
	
	/** Field mapping. */
	@Column( name = "repTeamUIDs" )
	private String repTeamUIDs;
	
	/** Field mapping. */
	@Column( name = "territoryIds" )
	private String territoryIds;
	
	/** Field mapping. */
	@Column( name = "startYear" )
	private Integer startYear;
	
	/** Field mapping. */
	@Column( name = "startMonth" )
	private Integer startMonth;
	
	/** Field mapping. */
	@Column( name = "repThresholdType" )
	private String repThresholdType;
	
	/** Field mapping. */
	@Column( name = "bottomPercentile" )
	private Integer bottomPercentile;
	
	/** Field mapping. */
	@Column( name = "createdAt", insertable=false, updatable=false )  
	private Date createdAt;
	
	/** Field mapping. */
	@Column( name = "updatedAt", insertable=false, updatable=false )  
	private Date updatedAt;

	/**
	 * Default constructor, mainly for hibernate use.
	 */
	public EngagementFiltersForDSESettings() {
		// Default constructor
	} 

	/** Constructor taking a given ID.
	 * @param id to set
	 */
	public EngagementFiltersForDSESettings(EngagementFiltersForDSESettingsPK id) {
		this.id = id;
	}
	
 
	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return EngagementFiltersForDSESettings.class;
	}
 

	
    /**
     * Return the value associated with the column: id.
	 * @return A LearningparamPK object (this.id)
	 */
	public EngagementFiltersForDSESettingsPK getId() {
		return this.id;		
	}
	
    /**  
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	public void setId(final EngagementFiltersForDSESettingsPK id) {
		this.id = id;
	}
	/////////////////////////////////////

	 /**
     * Return the value associated with the column: repTeamUIDs.
	 * @return A String object (this.repTeamUIDs)
	 */
	public String getRepTeamUIDs() {
		return this.repTeamUIDs;
	}
  
    /**  
     * Set the value related to the column: repTeamUIDs.
	 * @param repTeamUIDs the repTeamUIDs value you wish to set
	 */
	public void setRepTeamUIDs(final String repTeamUIDs) {
		this.repTeamUIDs = repTeamUIDs;
	}
	
    /**
     * Return the value associated with the column: territoryIds.
	 * @return A String object (this.territoryIds)
	 */ 
	public String getTerritoryIds() {
		return this.territoryIds;
	}
  
    /**  
     * Set the value related to the column: territoryIds.
	 * @param territoryIds the territoryIds value you wish to set
	 */
	public void setTerritoryIds(final String territoryIds) {
		this.territoryIds = territoryIds;
	}

    /**
     * Return the value associated with the column: startYear.
	 * @return A Integer object (this.startYear)
	 */
	public Integer getStartYear() {
		return this.startYear;
	}
	
    /**  
     * Set the value related to the column: startYear.
	 * @param startYear the startYear value you wish to set
	 */
	public void setStartYear(final Integer startYear) {
		this.startYear = startYear;
	}

    /**
     * Return the value associated with the column: startMonth.
	 * @return A Integer object (this.startMonth)
	 */
	public Integer getStartMonth() {
		return this.startMonth;
	}
	
    /**  
     * Set the value related to the column: startMonth.
	 * @param startMonth the startMonth value you wish to set
	 */
	public void setStartMonth(final Integer startMonth) {
		this.startMonth = startMonth;
	}

	 /**
     * Return the value associated with the column: repThresholdType.
	 * @return A String object (this.repThresholdType)
	 */
	public String getRepThresholdType() {
		return this.repThresholdType;
	}
  
    /**  
     * Set the value related to the column: repThresholdType.
	 * @param repThresholdType the repThresholdType value you wish to set
	 */
	public void setRepThresholdType(final String repThresholdType) {
		this.repThresholdType = repThresholdType;
	}

	/**
     * Return the value associated with the column: bottomPercentile.
	 * @return A Integer object (this.bottomPercentile)
	 */
	public Integer getBottomPercentile() {
		return this.bottomPercentile;
	}
	
    /**  
     * Set the value related to the column: bottomPercentile.
	 * @param bottomPercentile the bottomPercentile value you wish to set
	 */
	public void setBottomPercentile(final Integer bottomPercentile) {
		this.bottomPercentile = bottomPercentile;
	}



	//////////////////////////////////////
    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	@Basic( optional = true )
	public Date getCreatedAt() {
		return this.createdAt;		
	}
	
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	@Basic( optional = true )
	public Date getUpdatedAt() {
		return this.updatedAt;		
	}
  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	public void setUpdatedAt(final Date updatedAt) {
		this.updatedAt = updatedAt;
	}


   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public EngagementFiltersForDSESettings clone() throws CloneNotSupportedException {
		
        final EngagementFiltersForDSESettings copy = (EngagementFiltersForDSESettings)super.clone();

		copy.setId(this.getId());
		copy.setRepTeamUIDs(this.getRepTeamUIDs());
		copy.setTerritoryIds(this.getTerritoryIds());		
		copy.setStartYear(this.getStartYear());
		copy.setStartMonth(this.getStartMonth());
		copy.setRepThresholdType(this.getRepThresholdType());
		copy.setBottomPercentile(this.getBottomPercentile());
		copy.setCreatedAt(this.getCreatedAt());
		copy.setUpdatedAt(this.getUpdatedAt());
		return copy;
	}

	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("id: " + this.getId() + ", ");	
		sb.append("repTeamUIDs: " + this.getRepTeamUIDs() + ", ");
		sb.append("territoryIds: " + this.getTerritoryIds() + ", ");
		sb.append("startYear: " + this.getStartYear() + ", ");
		sb.append("startMonth" + this.getStartMonth() + ", ");
		sb.append("repThresholdType: " + this.getRepThresholdType() + ", ");
		sb.append("bottomPercentile: " + this.getBottomPercentile() + ", ");		
		sb.append("createdAt: " + this.getCreatedAt() + ", ");
		sb.append("updatedAt: " + this.getUpdatedAt());
		return sb.toString();		
	}


	/** Equals implementation. 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		if ( this == aThat ) {
			 return true;
		}

		if ((aThat == null) || ( !(aThat.getClass().equals(this.getClass())))) {
			 return false;
		}
	
		final EngagementFiltersForDSESettings that = (EngagementFiltersForDSESettings) aThat;

		boolean result = true;
		result = result && this.getId().equals(that.getId());
		result = result && (((getRepTeamUIDs() == null) && (that.getRepTeamUIDs() == null)) || (getRepTeamUIDs() != null && getRepTeamUIDs().equals(that.getRepTeamUIDs())));
		result = result && (((getTerritoryIds() == null) && (that.getTerritoryIds() == null)) || (getTerritoryIds() != null && getTerritoryIds().equals(that.getTerritoryIds())));
		result = result && (((getStartYear() == null) && (that.getStartYear() == null)) || (getStartYear() != null && getStartYear().equals(that.getStartYear())));
		result = result && (((getStartMonth() == null) && (that.getStartMonth() == null)) || (getStartMonth() != null && getStartMonth().equals(that.getStartMonth())));
		result = result && (((getRepThresholdType() == null) && (that.getRepThresholdType() == null)) || (getRepThresholdType() != null && getRepThresholdType().equals(that.getRepThresholdType())));
		result = result && (((getBottomPercentile() == null) && (that.getBottomPercentile() == null)) || (getBottomPercentile() != null && getBottomPercentile().equals(that.getBottomPercentile())));
		return result;
	}
	
	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
		return getId().hashCode();
	}
	

	
}
