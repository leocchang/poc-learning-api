package com.aktana.learning.persistence.models.impl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aktana.learning.persistence.models.ICustomerConfigurations;


/** 
 * Object mapping for hibernate-handled table: LearningParam.
 * @author autogenerated
 */

@Entity
@Table(name = "CustomerConfigurations")
public class CustomerConfigurations implements Cloneable, Serializable, ICustomerConfigurations {

	/** Serial Version UID. */
	private static final long serialVersionUID = -559001494L;

	

	/** Field mapping. */
	@Column( name = "createdAt", insertable=false, updatable=false )  
	private Date createdAt;

	/** Field mapping. */
	@Id 
	private CustomerConfigurationsPK id;

	/** Field mapping. */
	@Column( name = "updatedAt", insertable=false, updatable=false )  
	private Date updatedAt;

	/**
	 * Default constructor, mainly for hibernate use.
	 */
	public CustomerConfigurations() {
		// Default constructor
	} 

	/** Constructor taking a given ID.
	 * @param id to set
	 */
	public CustomerConfigurations(CustomerConfigurationsPK id) {
		this.id = id;
	}
	
 
	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return CustomerConfigurations.class;
	}
 

    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	@Basic( optional = true )
	public Date getCreatedAt() {
		return this.createdAt;
		
	}
	

  
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}
	

    /**
     * Return the value associated with the column: id.
	 * @return A CustomerConfigurationsPK object (this.id)
	 */
	public CustomerConfigurationsPK getId() {
		return this.id;
		
	}
	

  
    /**  
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	public void setId(final CustomerConfigurationsPK id) {
		this.id = id;
	}

    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	@Basic( optional = true )
	public Date getUpdatedAt() {
		return this.updatedAt;
		
	}
	

  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	public void setUpdatedAt(final Date updatedAt) {
		this.updatedAt = updatedAt;
	}


   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public CustomerConfigurations clone() throws CloneNotSupportedException {
		
        final CustomerConfigurations copy = (CustomerConfigurations)super.clone();

		copy.setCreatedAt(this.getCreatedAt());
		copy.setId(this.getId());
		copy.setUpdatedAt(this.getUpdatedAt());
		return copy;
	}
	
	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("createdAt: " + this.getCreatedAt() + ", ");
		sb.append("id: " + this.getId() + ", ");
		sb.append("updatedAt: " + this.getUpdatedAt());
		return sb.toString();		
	}


	/** Equals implementation. 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		if ( this == aThat ) {
			 return true;
		}

		if ((aThat == null) || ( !(aThat.getClass().equals(this.getClass())))) {
			 return false;
		}
	
		final CustomerConfigurations that = (CustomerConfigurations) aThat;
		return this.getId().equals(that.getId());
	}
	
	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
		return getId().hashCode();
	}
	
}
