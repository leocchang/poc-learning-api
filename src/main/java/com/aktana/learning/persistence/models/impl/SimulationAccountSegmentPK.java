package com.aktana.learning.persistence.models.impl;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.aktana.learning.persistence.models.ISimulationAccountSegmentPK;


/** 
 * @author autogenerated
 */

@Embeddable
public class SimulationAccountSegmentPK implements Cloneable, Serializable,  ISimulationAccountSegmentPK {

	/** Serial Version UID. */
	private static final long serialVersionUID = -559037898L;
	
	
	
	/** Field mapping. */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningBuildUID", nullable = false)
	private LearningBuild learningBuild;
	
	/** Field mapping. */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningRunUID", nullable = false)
	private LearningRun learningRun;
	
	/** Field mapping. */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "segmentUID", nullable = false)
	private SimulationSegment segment;
	
	/** Field mapping. */
	@Basic( optional = false )
	@Column( name = "accountUID", nullable = false)
	private String accountUID;

 
	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return SimulationAccountSegmentPK.class;
	}
 
	
	 /**
     * Return the value associated with the column: learningBuildUID.
	 * @return A LearningBuild object (this.learningBuild)
	 */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningBuildUID", nullable = false)
	public LearningBuild getLearningBuild() {
		return this.learningBuild;	
	}
	
    /**  
     * Set the value related to the column: learningBuildUID.
	 * @param learningBuild the learningBuild value you wish to set
	 */
	public void setLearningBuild(final LearningBuild learningBuild) {
		this.learningBuild = learningBuild;
	}
	
	 /**
     * Return the value associated with the column: learningRunUID.
	 * @return A learningRun object (this.learningRun)
	 */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningRunUID", nullable = false)
	public LearningRun getLearningRun() {
		return this.learningRun;		
	}
  
    /**  
     * Set the value related to the column: learningRunUID.
	 * @param learningRun the learningRun value you wish to set
	 */
	public void setLearningRun(final LearningRun learningRun) {
		this.learningRun = learningRun;
	}
	
	
    /**
     * Return the value associated with the column: segmentUID.
	 * @return A SimulationSegment object (this.segment)
	 */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "segmentUID", nullable = false)
	public SimulationSegment getSimulationSegment() {
		return this.segment;		
	}
	
    /**  
     * Set the value related to the column: segmentUID.
	 * @param segment the segment value you wish to set
	 */
	public void setSimulationSegment(final SimulationSegment segment) {
		this.segment = segment;
	}
	
	
    /**
     * Return the value associated with the column: accountUID.
	 * @return A String object (this.accountUID)
	 */
	@Basic( optional = false )
	@Column( name = "accountUID", nullable = false)
	public String getAccountUID() {
		return this.accountUID;	
	}
	 
	/**  
     * Set the value related to the column: accountUID.
	 * @param accountUID the accountUID value you wish to set
	 */
	public void setAccountUID(final String accountID) {
		this.accountUID = accountID;	
	}


   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public SimulationAccountSegmentPK clone() throws CloneNotSupportedException {

        final SimulationAccountSegmentPK copy = (SimulationAccountSegmentPK)super.clone();
        copy.setAccountUID(this.getAccountUID());
        copy.setLearningBuild(this.getLearningBuild());
        copy.setLearningRun(this.getLearningRun());
        copy.setSimulationSegment(this.getSimulationSegment());
		return copy;
	}
	


	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("learningBuild: " + this.getLearningBuild().getId() + ", ");
		sb.append("learningRun: " + this.getLearningRun().getId() + ", ");
		sb.append("segment: "+this.getSimulationSegment().getId() + ", ");
		sb.append("accountUID: " + this.getAccountUID());

		return sb.toString();		
	}


	/** Equals implementation. 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		Object proxyThat = aThat;
		
		if ( this == aThat ) {
			 return true;
		}

		if (aThat == null)  {
			 return false;
		}
		
		final SimulationAccountSegmentPK that; 
		try {
			that = (SimulationAccountSegmentPK) proxyThat;
			if ( !(that.getClassType().equals(this.getClassType()))){
				return false;
			}
		} catch (org.hibernate.ObjectNotFoundException e) {
				return false;
		} catch (ClassCastException e) {
				return false;
		}
		
		
		boolean result = true;
		result = result && (((getAccountUID() == null) && (that.getAccountUID() == null)) || (getAccountUID() != null && getAccountUID().equals(that.getAccountUID())));	
		result = result && (((getLearningBuild() == null) && (that.getLearningBuild() == null)) || (getLearningBuild() != null && getLearningBuild().getId().equals(that.getLearningBuild().getId())));	
		result = result && (((getLearningRun() == null) && (that.getLearningRun() == null)) || (getLearningRun() != null && getLearningRun().getId().equals(that.getLearningRun().getId())));	
		result = result && (((getSimulationSegment() == null) && (that.getSimulationSegment() == null)) || (getSimulationSegment() != null && getSimulationSegment().getId().equals(that.getSimulationSegment().getId())));	
		return result;
	}
	
	
	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash = hash + getAccountUID().hashCode();
		hash = hash + getLearningBuild().hashCode();
		hash = hash + getLearningRun().hashCode();
		hash = hash + getSimulationSegment().hashCode();
		return hash;
	}

}