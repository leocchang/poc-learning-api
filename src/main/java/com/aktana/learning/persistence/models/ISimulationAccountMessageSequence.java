package com.aktana.learning.persistence.models;

import java.util.Date;

import com.aktana.learning.persistence.models.impl.SimulationAccountMessageSequencePK;
/** 
 * Object interface mapping for hibernate-handled table: SimulationAccountMessageSequence.
 * @author autogenerated
 */

public interface ISimulationAccountMessageSequence {
	
	
    /**
	 * @return A SimulationAccountMessageSequencePK (this.id) 
	 */
	SimulationAccountMessageSequencePK getId();
	
    /**  
	 * @param id the id value you wish to set
	 */
	void setId(final SimulationAccountMessageSequencePK id);

	
	/**
     * Return the value associated with the column: accountName.
	 * @return A String (this.accountName)
	 */
	String getAccountName();
	
	/**  
     * Set the value related to the column: accountName.
	 * @param accountName the accountName value you wish to set
	 */
	void setAccountName(final String accountName);
	
	
	/**
     * Return the value associated with the column: probability.
	 * @return A Double (this.probability)
	 */
	Double getProbability();
		
	/**  
     * Set the value related to the column: probability.
	 * @param probability the probability value you wish to set
	 */
	void setProbability(final Double probability);

	
	/**
	 * Return the value associated with the column: isFinalized.
	 * @return A Int (this.isFinalized)
	 */
	Integer getIsFinalized();
	
	/**  
	 * Set the value related to the column: isFinalized.
	 * @param isFinalized the isFinalized value you wish to set
	 */
	void setIsFinalized(final Integer isFinalized);
	
    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	 Date getCreatedAt();
	
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	void setCreatedAt(final Date createdAt);
	
	
    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	Date getUpdatedAt();

    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	void setUpdatedAt(final Date updatedAt);
	
	// end of interface
}