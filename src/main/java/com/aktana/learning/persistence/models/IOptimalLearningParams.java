package com.aktana.learning.persistence.models;

import java.util.Date;
import com.aktana.learning.persistence.models.impl.OptimalLearningParamsPK;


/** 
 * Object interface mapping for hibernate-handled table: LearningParam.
 * @author autogenerated
 */

public interface IOptimalLearningParams {

    /**
     * Return the value associated with the column: id.
	 * @return A LearningparamPK object (this.id)
	 */
	OptimalLearningParamsPK getId();
	

    /**  
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	void setId(final OptimalLearningParamsPK id);


    /**
     * Return the value associated with the column: paramValue.
	 * @return A String object (this.paramValue)
	 */
	String getParamValue();
	
    /**  
     * Set the value related to the column: paramValue.
	 * @param paramValue the paramValue value you wish to set
	 */
	void setParamValue(final String paramValue);

	
    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	Date getCreatedAt();
  
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	void setCreatedAt(final Date createdAt);
	
    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	Date getUpdatedAt();
  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	void setUpdatedAt(final Date updatedAt);

	// end of interface
}