package com.aktana.learning.persistence.models;

import java.util.Date;
import javax.persistence.Basic;

import com.aktana.learning.persistence.models.impl.AccountTimeToEngagePK;


/** 
 * Object interface mapping for hibernate-handled table: AccountTimeToEngage.
 * @author autogenerated
 */

public interface IAccountTimeToEngage {



    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	Date getCreatedAt();
  
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	void setCreatedAt(final Date createdAt);

	
    /**
     * Return the value associated with the column: id.
	 * @return A AccountTimeToEngagePK object (this.id)
	 */
	AccountTimeToEngagePK getId();
  
    /**  
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	void setId(final AccountTimeToEngagePK id);

	
    /**
     * Return the value associated with the column: probability.
	 * @return A Double object (this.probability)
	 */
	Double getProbability();
  
    /**  
     * Set the value related to the column: probability.
	 * @param probability the probability value you wish to set
	 */
	void setProbability(final Double probability);
	
	
    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	Date getUpdatedAt();
  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	void setUpdatedAt(final Date updatedAt);

	// end of interface
}