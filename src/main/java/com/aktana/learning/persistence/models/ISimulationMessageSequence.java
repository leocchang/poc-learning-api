package com.aktana.learning.persistence.models;

import java.util.Date;

import com.aktana.learning.persistence.models.impl.SimulationMessageSequencePK;


/** 
 * Object interface mapping for hibernate-handled table: SimulationMessageSequence.
 * @author autogenerated
 */

public interface ISimulationMessageSequence {
	
	
    /**	 
	 * @return A SimulationMessageSequencePK (this.id) 
	 */
	SimulationMessageSequencePK getId();
	
    /**  
	 * @param id the id value you wish to set
	 */
	void setId(final SimulationMessageSequencePK id);
	
	
	/**
	 * Return the value associated with the column: seqHash.
	 * @return A String (this.seqHash) 
	 */
	String getMessageSeqHash();
	
	/**  
	 * Set the value related to the column: seqHash.
	 * @param seqHash the seqHash value you wish to set
	 */
	void setMessageSeqHash(final String messageSeqHash);
	
	
	/**
	 * Return the value associated with the column: sequenceOrder.
	 * @return A Int (this.sequenceOrder)
	 */
	Integer getSequenceOrder();
	
	/**  
	 * Set the value related to the column: sequenceOrder.
	 * @param sequenceOrder the sequenceOrder value you wish to set
	 */
	void setSequenceOrder(final Integer sequenceOrder);
	
    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	 Date getCreatedAt();
	
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	void setCreatedAt(final Date createdAt);
	
	
    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	Date getUpdatedAt();

    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	void setUpdatedAt(final Date updatedAt);
	
	// end of interface
}