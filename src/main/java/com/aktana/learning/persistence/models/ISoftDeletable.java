/*****************************************************************
 *
 * @author $Author$
 * @version $Revision$ on $Date$ by $Author$
 *
 * Copyright (C) 2012-2014 Aktana Inc.
 *
 *****************************************************************/
package com.aktana.learning.persistence.models;


/** 
 * "Marker" interface for any Aktana object that has Boolean attribute "isDeleted", with
 * get/set methods named "getIsDeleted" and "setIsDeleted".
 * 
 * The intention is to enable /version3 framework to automatically know whether
 * to perform "hard" or "soft" delete against a model object.  If the object
 * implements the ISoftDeletable" interface, then by default /version3 will perform
 * "soft" delete only, by setting isDeleted=true and then saving the object.
 * 
 * Notes: 
 * (1) If lombok @Data is used to generate the object's get/set methods, then the isDeleted
 * 		field must be Boolean, not boolean. If it is boolean lombok will generate
 * 		methods boolean isDeleted() and void isDeleted(boolean)
 */
public interface ISoftDeletable {
	
	public Boolean getIsDeleted();
	public void setIsDeleted(Boolean newValue);
	
}
