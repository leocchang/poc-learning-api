package com.aktana.learning.persistence.models.impl;

import com.aktana.learning.persistence.models.IAccountProduct;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/** 
 * Object mapping for hibernate-handled table: AccountProduct.
 * @author autogenerated
 */

@Entity
@Table(name = "AccountProduct")
public class AccountProduct implements Cloneable, Serializable, IAccountProduct {

	/** Serial Version UID. */
	private static final long serialVersionUID = -559037930L;

	/** Field mapping. */
	@Column( name = "createdAt", insertable=false, updatable=false ) 
	private Date createdAt;

	/** Field mapping. */
	@Id 
	private AccountProductPK id;
	
	/** Field mapping. */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningBuildUID", nullable = false )
	private LearningBuild learningBuild;
	
	/** Field mapping. */
	private Double visitProbability_akt;

	/** Field mapping. */
	private Double sendProbability_akt;
	
	/** Field mapping. */
	@Column( name = "updatedAt", insertable=false, updatable=false ) 
	private Date updatedAt;

	/**
	 * Default constructor, mainly for hibernate use.
	 */
	public AccountProduct() {
	    id = new AccountProductPK();
	    // Default constructor
	} 

	/** Constructor taking a given ID.
	 * @param id to set
	 */
	public AccountProduct(AccountProductPK id) {
		this.id = id;
	}


	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return AccountProduct.class;
	}
 

    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdat)
	 */
	@Basic( optional = true )
	@Column( name = "createdAt", insertable=false, updatable=false )  
	public Date getCreatedAt() {
		return this.createdAt;
		
	}
	

  
    /**  
     * Set the value related to the column: createdAt.
	 * @param createdat the createdAt value you wish to set
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

    /**
     * Return the value associated with the column: id.
	 * @return A AccountproductPK object (this.id)
	 */
	public AccountProductPK getId() {
		return this.id;
		
	}
	

  
    /**  
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	public void setId(final AccountProductPK id) {
		this.id = id;
	}

    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	@Basic( optional = true )
	@Column( name = "updatedAt", insertable=false, updatable=false ) 
	public Date getUpdatedAt() {
		return this.updatedAt;
		
	}
	

  
    /**  
     * Set the value related to the column: updatedAt.
	 * @param updatedat the updatedAt value you wish to set
	 */
	public void setUpdatedAt(final Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	
	 /**
     * Return the value associated with the column: learningBuild.
	 * @return A String object (this.learningBuild)
	 */
	@Basic( optional = true )
	@Column( name = "learningBuildUID", length = 80  )
	public LearningBuild getLearningBuild() {
		return this.learningBuild;
		
	}
	

  
    /**  
     * Set the value related to the column: learningBuild.
	 * @param learningBuild the learningBuild value you wish to set
	 */
	public void setLearningBuild(final LearningBuild learningBuild) {
		this.learningBuild = learningBuild;
	}
	
	 /**
     * Return the value associated with the column: visitProbability_akt.
	 * @return A String object (this.visitProbability_akt)
	 */
	@Basic( optional = true )
	@Column( name = "visitProbability_akt", length = 40  )
	public Double getVisitProbability_akt() {
		return this.visitProbability_akt;
		
	}
	

  
    /**  
     * Set the value related to the column: visitProbability_akt.
	 * @param visitProbability_akt the visitProbability_akt value you wish to set
	 */
	public void setVisitProbability_akt(final Double visitProbability_akt) {
		this.visitProbability_akt = visitProbability_akt;
	}
	
	 /**
     * Return the value associated with the column: sendProbability_akt.
	 * @return A String object (this.sendProbability_akt)
	 */
	@Basic( optional = true )
	@Column( name = "sendProbability_akt", length = 40  )
	public Double getSendProbability_akt() {
		return this.sendProbability_akt;
		
	}
	

  
    /**  
     * Set the value related to the column: sendProbability_akt.
	 * @param sendProbability_akt the sendProbability_akt value you wish to set
	 */
	public void setSendProbability_akt(final Double sendProbability_akt) {
		this.sendProbability_akt = sendProbability_akt;
	}
	

   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public AccountProduct clone() throws CloneNotSupportedException {
		
        final AccountProduct copy = (AccountProduct)super.clone();

		copy.setCreatedAt(this.getCreatedAt());
		copy.setId(this.getId());
		copy.setUpdatedAt(this.getUpdatedAt());
		return copy;
	}
	


	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("createdAt: " + this.getCreatedAt() + ", ");
		sb.append("id: " + this.getId() + ", ");
		sb.append("updatedAt: " + this.getUpdatedAt());
		return sb.toString();		
	}


	/** Equals implementation. 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		if ( this == aThat ) {
			 return true;
		}

		if ((aThat == null) || ( !(aThat.getClass().equals(this.getClass())))) {
			 return false;
		}
	
		final AccountProduct that = (AccountProduct) aThat;
		return this.getId().equals(that.getId());
	}
	
	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
		return getId().hashCode();
	}
	

	
}