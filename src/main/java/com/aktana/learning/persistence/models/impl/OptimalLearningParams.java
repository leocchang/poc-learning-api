package com.aktana.learning.persistence.models.impl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.aktana.learning.persistence.models.IOptimalLearningParams;


/**
 * Object mapping for hibernate-handled table: LearningParam.
 * @author autogenerated
 */

@Entity
@Table(name = "OptimalLearningParams")
public class OptimalLearningParams implements Cloneable, Serializable, IOptimalLearningParams {

	/** Serial Version UID. */
	private static final long serialVersionUID = -55001494L;


	
	/** Field mapping. */
	@Id
	private OptimalLearningParamsPK id;

	/** Field mapping. */
	@Basic( optional = true )
	@Column( name = "paramValue", columnDefinition="LONGTEXT" )
	private String paramValue;

	/** Field mapping. */
	@Basic( optional = true )
	@Column( name = "createdAt", insertable=false, updatable=false )
	private Date createdAt;

	/** Field mapping. */
	@Basic( optional = true )
	@Column( name = "updatedAt", insertable=false, updatable=false )
	private Date updatedAt;

	/**
	 * Default constructor, mainly for hibernate use.
	 */
	public OptimalLearningParams() {
		// Default constructor
	}

	/** Constructor taking a given ID.
	 * @param id to set
	 */
	public OptimalLearningParams(OptimalLearningParamsPK id) {
		this.id = id;
	}


	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return OptimalLearningParams.class;
	}


    /**
     * Return the value associated with the column: id.
	 * @return A LearningparamPK object (this.id)
	 */
	public OptimalLearningParamsPK getId() {
		return this.id;
	}

    /**
     * Set the value related to the column: id.
	 * @param id the id value you wish to set
	 */
	public void setId(final OptimalLearningParamsPK id) {
		this.id = id;
	}

    /**
     * Return the value associated with the column: paramValue.
	 * @return A String object (this.paramValue)
	 */
	@Basic( optional = true )
	@Column( name = "paramValue", columnDefinition="LONGTEXT" )
	public String getParamValue() {
		return this.paramValue;
	}

    /**
     * Set the value related to the column: paramValue.
	 * @param paramValue the paramValue value you wish to set
	 */
	public void setParamValue(final String paramValue) {
		this.paramValue = paramValue;
	}

    /**
     * Return the value associated with the column: createdAt.
	 * @return A Date object (this.createdAt)
	 */
	@Basic( optional = true )
	@Column( name = "createdAt", insertable=false, updatable=false )
	public Date getCreatedAt() {
		return this.createdAt;
	}

    /**
     * Set the value related to the column: createdAt.
	 * @param createdAt the createdAt value you wish to set
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

    /**
     * Return the value associated with the column: updatedAt.
	 * @return A Date object (this.updatedAt)
	 */
	@Basic( optional = true )
	@Column( name = "updatedAt", insertable=false, updatable=false )
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

    /**
     * Set the value related to the column: updatedAt.
	 * @param updatedAt the updatedAt value you wish to set
	 */
	public void setUpdatedAt(final Date updatedAt) {
		this.updatedAt = updatedAt;
	}


   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public OptimalLearningParams clone() throws CloneNotSupportedException {

        final OptimalLearningParams copy = (OptimalLearningParams)super.clone();

		copy.setId(this.getId());
		copy.setParamValue(this.getParamValue());
		copy.setCreatedAt(this.getCreatedAt());
		copy.setUpdatedAt(this.getUpdatedAt());
		return copy;
	}



	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("id: " + this.getId().toString() + ", ");
		sb.append("paramValue: " + this.getParamValue() + ", ");
		sb.append("createdAt: " + this.getCreatedAt() + ", ");
		sb.append("updatedAt: " + this.getUpdatedAt());
		return sb.toString();
	}


	/** Equals implementation.
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		if ( this == aThat ) {
			 return true;
		}

		if ((aThat == null) || ( !(aThat.getClass().equals(this.getClass())))) {
			 return false;
		}

		final OptimalLearningParams that = (OptimalLearningParams) aThat;
		return this.getId().equals(that.getId());
	}

	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
		return getId().hashCode();
	}

}
