package com.aktana.learning.persistence.models;

public interface IRepEngagementCalculationPK {
 
	 /**
     * Return the value associated with the column: Year.
	 * @return An Integer object (this.Year)
	 */
	Integer getYear();
	
    /**  
     * Set the value related to the column: Year.
	 * @param Year the Year value you wish to set
	 */
	void setYear(final Integer year);

	/**
     * Return the value associated with the column: Month.
	 * @return An Integer object (this.Month)
	 */
	Integer getMonth();
	
    /**  
     * Set the value related to the column: Month.
	 * @param Month the Month value you wish to set
	 */
	void setMonth(final Integer month);
    
	/**
     * Return the value associated with the column: repUID.
	 * @return A String object (this.repUID)
	 */
	String getRepUID();
	
    /**  
     * Set the value related to the column: repUID.
	 * @param repUID the repUID value you wish to set
	 */
	void setRepUID(final String repUID);
	
	/**
     * Return the value associated with the column: repName.
	 * @return A String object (this.repName)
	 */
	String getRepName();
	
    /**  
     * Set the value related to the column: repName.
	 * @param repName the repName value you wish to set
	 */
	void setRepName(final String repName);
	
	/**
     * Return the value associated with the column: repTeamUID.
	 * @return A String object (this.repTeamUID)
	 */
	String getRepTeamUID();
	
    /**  
     * Set the value related to the column: repTeamUID.
	 * @param repTeamUID the repTeamUID value you wish to set
	 */
	void setRepTeamUID(final String repTeamUID);
	
	/**
     * Return the value associated with the column: repTeamName.
	 * @return A String object (this.repTeamName)
	 */
	String getRepTeamName();
	
    /**  
     * Set the value related to the column: repTeamName.
	 * @param repTeamName the repTeamName value you wish to set
	 */
	void setRepTeamName(final String repTeamName);
	
	/**
     * Return the value associated with the column: seConfigId.
	 * @return A String object (this.seConfigId)
	 */
	String getSeConfigId();
	
    /**  
     * Set the value related to the column: seConfigId.
	 * @param seConfigId the seConfigId value you wish to set
	 */
	void setSeConfigId(final String seConfigId);
	
	/**
     * Return the value associated with the column: seConfigName.
	 * @return A String object (this.seConfigName)
	 */
	String getSeConfigName();
	
    /**  
     * Set the value related to the column: seConfigName.
	 * @param seConfigName the seConfigId value you wish to set
	 */
	void setSeConfigName(final String seConfigName);
	
	/**
     * Return the value associated with the column: suggestionType.
	 * @return A String object (this.suggestionType)
	 */
	String getSuggestionType();
	
    /**  
     * Set the value related to the column:suggestionType.
	 * @param suggestionType the suggestionType value you wish to set
	 */
	void setSuggestionType(final String suggestionType);
	
	/**
     * Return the value associated with the column: territoryId.
	 * @return A String object (this.territoryId)
	 */
	String getTerritoryId();
	
    /**  
     * Set the value related to the column:territoryId.
	 * @param territoryId the territoryId value you wish to set
	 */
	void setTerritoryId(final String territoryId);
	
	/**
     * Return the value associated with the column: territoryName.
	 * @return A String object (this.territoryName)
	 */
	String getTerritoryName();
	
    /**  
     * Set the value related to the column:territoryName.
	 * @param territoryName the territoryName value you wish to set
	 */
	void setTerritoryName(final String territoryName);
	
	
	/**
     * Return the value associated with the column: engagedUniqueSuggestionsCount.
	 * @return An Integer object (this.engagedUniqueSuggestionsCount)
	 */
	Integer getEngagedUniqueSuggestionsCount();
	
    /**  
     * Set the value related to the column: engagedUniqueSuggestionsCount.
	 * @param engagedUniqueSuggestionsCount the engagedUniqueSuggestionsCount value you wish to set
	 */
	void setEngagedUniqueSuggestionsCount(final Integer engagedUniqueSuggestionsCount);
	
	/**
     * Return the value associated with the column: TotalSuggestionsDeliveredTimes.
	 * @return An Integer object (this.TotalSuggestionsDeliveredTimes)
	 */
	Integer getTotalSuggestionsDeliveredTimes();
	
    /**  
     * Set the value related to the column: TotalSuggestionsDeliveredTimes.
	 * @param TotalSuggestionsDeliveredTimes the engagedUniqueSuggestionsCount value you wish to set
	 */
	void setTotalSuggestionsDeliveredTimes(final Integer totalSuggestionsDeliveredTimes);
}
