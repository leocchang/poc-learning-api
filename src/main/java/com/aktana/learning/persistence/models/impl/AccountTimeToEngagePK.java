package com.aktana.learning.persistence.models.impl;


import com.aktana.learning.persistence.models.IAccountTimeToEngagePK;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.Date;

/** 
 * Object mapping for hibernate-handled table: AccountTimeToEngage.
 * @author autogenerated
 */

@Embeddable
public class AccountTimeToEngagePK implements Cloneable, Serializable,  IAccountTimeToEngagePK {

	/** Serial Version UID. */
	private static final long serialVersionUID = -559037924L;


	
	/** Field mapping. */
	@Basic( optional = false )
	@Column( name = "accountUID", nullable = false , length = 80 )
	private String accountUID;

	
	/** Field mapping. */
	@Basic( optional = false )
	@Column( name = "channelUID", nullable = false, length = 80 )
	private String channelUID;

	
	/** Field mapping. */
	@Basic( optional = false )
	@Column( name = "method", nullable = false, length = 40  )
	private String method;
	
	
	/** Field mapping. */
	@Basic( optional = false )
	@Column( name = "predictionDate", nullable = false, insertable=false, updatable=false )
	private Date predictionDate;
	
	
	/** Field mapping. */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningRunUID", nullable = false)
	private LearningRun learningRun;

 
	/** Return the type of this class. Useful for when dealing with proxies.
	* @return Defining class.
	*/
	@Transient
	public Class<?> getClassType() {
		return AccountTimeToEngagePK.class;
	}
	
	
	 /**
     * Return the value associated with the column: learningRunUID.
	 * @return A LearningRun object (this.learningRun)
	 */
	@ManyToOne( cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY )
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@Basic( optional = false )
	@JoinColumn(name = "learningRunUID", nullable = false)
	public LearningRun getLearningRun() {
		return this.learningRun;		
	}
  
    /**  
     * Set the value related to the column: learningRunUID.
	 * @param LearningRun the LearningRun value you wish to set
	 */
	public void setLearningRun(final LearningRun learningRun) {
		this.learningRun = learningRun;
	}
 

	/**
     * Return the value associated with the column: account.
	 * @return A Account object (this.account)
	 */
	@Basic( optional = false )
	@Column( name = "accountUID", nullable = false , length = 80 )
	public String getAccountUID() {
		return this.accountUID;	
	}
	
    /**  
     * Set the value related to the column: account.
	 * @param account the account value you wish to set
	 */
	public void setAccountUID(final String accountUID) {
		this.accountUID = accountUID;
	}

	  /**
     * Return the value associated with the column: channelUID.
	 * @return A String object (this.channelUID)
	 */
	@Basic( optional = false )
	@Column( name = "channelUID", nullable = false, length = 80 )
	public String getChannelUID() {
		return this.channelUID;		
	}
  
    /**  
     * Set the value related to the column: channelUID.
	 * @param channelUID the channelUID value you wish to set
	 */
	public void setChannelUID(final String channelUID) {
		this.channelUID = channelUID;
	}
	
	
	  /**
     * Return the value associated with the column: method.
	 * @return A String object (this.method)
	 */
	@Basic( optional = false )
	@Column( name = "method", nullable = false, length = 40  )
	public String getMethod() {
		return this.method;	
	}
	
    /**  
     * Set the value related to the column: method.
	 * @param method the method value you wish to set
	 */
	public void setMethod(final String method) {
		this.method = method;
	}
	
	
	  /**
     * Return the value associated with the column: predictionDate.
	 * @return A Date object (this.predictionDate)
	 */
	@Basic( optional = false )
	@Column( name = "predictionDate", nullable = false, insertable=false, updatable=false )
	public Date getPredictionDate() {
		return this.predictionDate;	
	}
	
    /**  
     * Set the value related to the column: predictionDate.
	 * @param predictionDate the predictionDate value you wish to set
	 */
	public void setPredictionDate(final Date predictionDate) {
		this.predictionDate = predictionDate;
	}
  


   /**
    * Deep copy.
	* @return cloned object
	* @throws CloneNotSupportedException on error
    */
    @Override
    public AccountTimeToEngagePK clone() throws CloneNotSupportedException {
		
        final AccountTimeToEngagePK copy = (AccountTimeToEngagePK)super.clone();
        copy.setAccountUID(this.getAccountUID());
        copy.setLearningRun(this.getLearningRun());
        copy.setChannelUID(this.getChannelUID());
        copy.setMethod(this.getMethod());
        copy.setPredictionDate(this.getPredictionDate());
    
		return copy;
	}
	


	/** Provides toString implementation.
	 * @see java.lang.Object#toString()
	 * @return String representation of this class.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("learningRun: " + this.getLearningRun() + ", ");
		sb.append("accountUID: " + this.getAccountUID() + ", ");
		sb.append("channelUID: "+this.getChannelUID() + ", ");
		sb.append("methodUID: "+this.getMethod() + ", ");
		sb.append("predictionDate: "+this.getPredictionDate());

		return sb.toString();		
	}


	/** Equals implementation. 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param aThat Object to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(final Object aThat) {
		Object proxyThat = aThat;
		
		if ( this == aThat ) {
			 return true;
		}

		if (aThat == null)  {
			 return false;
		}
		
		final AccountTimeToEngagePK that; 
		try {
			that = (AccountTimeToEngagePK) proxyThat;
			if ( !(that.getClassType().equals(this.getClassType()))){
				return false;
			}
		} catch (org.hibernate.ObjectNotFoundException e) {
				return false;
		} catch (ClassCastException e) {
				return false;
		}
		
		
		boolean result = true;
		result = result && (((getLearningRun() == null) && (that.getLearningRun() == null)) || (getLearningRun() != null && getLearningRun().getId().equals(that.getLearningRun().getId())));	
		result = result && (((getAccountUID() == null) && (that.getAccountUID() == null)) || (getAccountUID() != null && getAccountUID().equals(that.getAccountUID())));	
		result = result && (((getChannelUID() == null) && (that.getChannelUID() == null)) || (getChannelUID() != null && getChannelUID().equals(that.getChannelUID())));	
		result = result && (((getMethod() == null) && (that.getMethod() == null)) || (getMethod() != null && getMethod().equals(that.getMethod())));	
		result = result && (((getPredictionDate() == null) && (that.getPredictionDate() == null)) || (getPredictionDate() != null && getPredictionDate().equals(that.getPredictionDate())));	
		return result;
	}
	
	/** Calculate the hashcode.
	 * @see java.lang.Object#hashCode()
	 * @return a calculated number
	 */
	@Override
	public int hashCode() {
	int hash = 0;	
		hash = hash + getLearningRun().hashCode();		
		hash = hash + getAccountUID().hashCode();
		hash = hash + getChannelUID().hashCode();
		hash = hash + getMethod().hashCode();
		hash = hash + getPredictionDate().hashCode();
	return hash;
	}
	

	
}
