#!/bin/bash -e

INIT=/mnt/learning-api.init
POPULATE_YAML_SCRIPT=/root/populateYaml.py
CONTROL_WRAPPER_SCRIPT=/usr/local/bin/lsctl

if [ ! -f $INIT ] ; then
        LEARNING_HOME=/mnt/aktana/apps/learning-service

        LOCAL=/tmp/workspace

        # Populating the Learning-API configuration YAML.
        if [[ -f $POPULATE_YAML_SCRIPT ]] ; then
                echo -e "\n>> Setting up Learning API configuration with values:"
                python $POPULATE_YAML_SCRIPT
        else
                echo "[FATAL] Cannot find the populate script at '$POPULATE_YAML_SCRIPT'."
                exit 1
        fi

        # If USE_TUNNEL is enabled create a connection.
        if $USE_TUNNEL ; then
                if [ ! -f "$SSH_KEY_PATH" ] ; then
                        echo ">> FATAL: You should set the variable 'SSH_KEY_PATH' with a valid path where your personal Private SSH Key is located."
                        exit 1
                fi

                ssh-keyscan $BRIDGE_HOSTNAME.$DOMAIN  >> /root/.ssh/known_hosts

                echo ">> Connecting to '$RDS_HOSTNAME.$DOMAIN' through '$BRIDGE_HOSTNAME.$DOMAIN' with '$USERNAME' account..."
                ssh -i $SSH_KEY_PATH -f -N -L $TARGET_PORT:$RDS_HOSTNAME.$DOMAIN:$SOURCE_PORT $USERNAME@$BRIDGE_HOSTNAME.$DOMAIN

                # Replacing DB variables.
                DB_HOST=localhost
                DB_PORT=$TARGET_PORT
        fi

        if $MIGRATE_DB ; then
                if [[ -f $CONTROL_WRAPPER_SCRIPT ]] ; then
                  echo ">> Migrating the database... "
                  $CONTROL_WRAPPER_SCRIPT migrate
                else
                  echo "ERROR: Could not migrate the DB because the control script is not at '$CONTROL_WRAPPER_SCRIPT'."
                fi
        fi

        echo ">> Creating init file..."
        touch $INIT
else
        if $USE_TUNNEL
        then
                echo ">> Re-establishing SSH Tunnel connection..."
                ssh -i $SSH_KEY_PATH -f -N -L $TARGET_PORT:$RDS_HOSTNAME.$DOMAIN:$SOURCE_PORT $USERNAME@$BRIDGE_HOSTNAME.$DOMAIN
        fi
fi
