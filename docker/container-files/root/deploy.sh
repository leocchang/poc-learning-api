#!/bin/bash -e

VERSION=

dnf clean metadata
systemctl stop $PACKAGE

if [[ ! -z $1 ]] ; then
    VERSION=$1

    # TODO: The regex should be '(r\d+)\.([a-z0-9]{7})-(.*)' but
    # for some reason bash matcher does not recognize that format."
    echo $VERSION
    if [[ ! $VERSION =~ (r\d*).([a-z0-9].*)-(.*) ]] ; then
        echo "The version provided does not match the pattern '(r\d*).([a-z0-9].*)-(.*)'. Not able to install."
        exit 1
    fi

    echo ">> Updating package: '$PACKAGE'."
    dnf -y remove $PACKAGE
    dnf -y install $PACKAGE-$VERSION
    
    echo ">> Regenerating the configuration YAML..."
    python /root/populateYaml.py 
else
    echo ">> Updating the package: '$PACKAGE'."
    dnf -y update $PACKAGE
fi

sleep 5
$MIGRATE_DB && /usr/local/bin/lsctl migrate
systemctl start $PACKAGE
