import ruamel.yaml
import sys
import os

# Function that completes the Rundeck URL with the ID provided.
def completePath( apiVersion, jobId ):
    return '/' + str(apiVersion) + '/job/' + jobId + '/run?format=json'

# Mappings
try:
        fileName = os.environ['LEARNING_API_CONFIG']
except KeyError as e:
        print "<>>> [ERROR] The Learning API config file path was not defined.\n"
        raise

try:
        print ">>> Loading and parsing YAML document located at '{}'.\n".format(fileName)
        document = file(fileName, 'r')
        yaml=ruamel.yaml.YAML()
        parsed = yaml.load(document)
except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise

# Database Properties
try:
        dbHost = os.environ["DB_HOST"]
        dbPort = os.environ["DB_PORT"]
        dbSchema = os.environ["DB_SCHEMA"]
        parsed["database"]["url"] = "jdbc:mysql://{0}:{1}/{2}?zeroDateTimeBehavior=convertToNull&characterEncoding=UTF-8".format(dbHost, dbPort, dbSchema)
except KeyError as e:
        print "<>>> [WARNING] HOST, PORT and SCHEMA values were not defined, the values by default are going to be used.\n"
else:
        print ">> Database Information:"
        print "\tDB_HOST: {}.".format(dbHost)
        print "\tDB_PORT: {}.".format(dbPort)
        print "\tDB_SCHEMA: {}.".format(dbSchema)

try:
        dbUser = os.environ['DB_USER']
        parsed["database"]["user"] = dbUser
except KeyError as e:
        print "<>>> [WARNING] Database username value is not defined, the default value is going to be used.\n"
else:
        print "\tDB_USER: {}.".format(dbUser)

try:
        dbPassword = os.environ['DB_PASS']
        parsed["database"]["password"] = dbPassword
except KeyError as e:
        print "<>>> [WARNING] Database password value is not defined, the default value is going to be used.\n"
else:
        print "\tDB_PASS: {}.".format(dbPassword)

# Application Properties
try:
        pullservicesUrl = os.environ["PULL_SERVICES_HOST"]
        parsed["pullservicesUrl"] = pullservicesUrl
except Exception as e:
        print "<>>> [WARNING] Pull services host is not defined, the default value will be used.\n"
else:
        print ">> Application Information:"
        print "\tPULL_SERVICES_HOST: {}.".format(pullservicesUrl)

try:
        metadata = os.environ["METADATA_VERSION"]
        parsed["theCustomerEnvironmentName"] = metadata
except Exception as e:
        print "<>>> [WARNING] The customer environment name (METADATA) is not defined, the default value will be used.\n"
else:
        print "\METADATA_VERSION: {}.".format(metadata)

try:
        apiPort = os.environ["LEARNING_API_PORT"]
        parsed["server"]["applicationConnectors"][0]["port"] = apiPort
except Exception as e:
        print "<>>> [WARNING] The Learning API port is not defined, the default value will be used.\n"
else:
        print "\tLEARNING_API_PORT: {}.".format(apiPort)

try:
        apiAdminPort = os.environ["LEARNING_API_ADMIN_PORT"]
        parsed["server"]["adminConnectors"][0]["port"] = apiAdminPort
except Exception as e:
        print "<>>> [WARNING] The Learning API Admin port is not defined, the default value will be used.\n"
else:
        print "\LEARNING_tAPI_ADMIN_PORT: {}.".format(apiAdminPort)

try:
        environment = os.environ["API_ADMIN_PORT"]
        parsed["server"]["adminConnectors"][0]["port"] = apiAdminPort
except Exception as e:
        print "<>>> [WARNING] The Learning API Admin port is not defined, the default value will be used.\n"
else:
        print "\tAPI_ADMIN_PORT: {}.".format(apiAdminPort)

# DSE Service
try:
        dseUser = os.environ["DSE_SERVICE_USERNAME"]
        parsed["dseService"]["username"] = dseUser
except Exception as e:
        print "<>>> [WARNING] The DSE Service API username is not defined, the default value will be used.\n"
else:
        print ">> DSE Service Information:"
        print "\tDSE_SERVICE_USERNAME: {}.".format(dseUser)

try:
        dsePass = os.environ["DSE_SERVICE_PASSWORD"]
        parsed["dseService"]["password"] = dsePass
except Exception as e:
        print "<>>> [WARNING] The DSE Service API password is not defined, the default value will be used.\n"
else:
        print "\tDSE_SERVICE_PASSWORD: {}.".format(dsePass)

try:
        dseSecret = os.environ["DSE_SERVICE_SECRET"]
        parsed["dseService"]["secret"] = dseSecret
except Exception as e:
        print "<>>> [WARNING] The DSE Service API Secret Key is not defined, the default value will be used.\n"
else:
        print "\tDSE_SERVICE_SECRET: {}.".format(dseSecret)

try:
        dseUrl = os.environ["DSE_SERVICE_URL"]
        parsed["dseService"]["url"] = repr(dseUrl)
except Exception as e:
        print "<>>> [WARNING] The DSE Service API URL is not defined, the default value will be used.\n"
else:
        print "\tDSE_SERVICE_URL: {}.".format(dseUrl)

# Rundeck Service
try:
        rundeckKey = os.environ["RUNDECK_KEY"]
        parsed["rundeckService"]["key"] = rundeckKey
except Exception as e:
        print "<>>> [WARNING] The Rundeck Service API Key is not defined, the default value will be used.\n"
else:
        print ">> Rundeck Service Information:"
        print "\tRUNDECK_KEY: {}.".format(rundeckKey)

try:
        rundeckUrl = os.environ["RUNDECK_URL"]
        parsed["rundeckService"]["url"] = repr("{}/api".format(rundeckUrl))
except Exception as e:
        print "<>>> [WARNING] The Rundeck Service URL is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_URL: {}.".format(rundeckUrl)

try:
        rundeckAPIVersion = os.environ["RUNDECK_API_VERSION"]
        pathVersion = '/' + str(rundeckAPIVersion) + '/system/info'
        parsed["rundeckService"]["paths"]["info"] = pathVersion
except Exception as e:
        print "<>>> [WARNING] The Rundeck Service API Version is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_API_VERSION: {}.".format(pathVersion)

try:
        rundeckMSOBuild = os.environ["RUNDECK_JOB_MSO_BUILD"]
        msoBuildPath = completePath(rundeckAPIVersion, rundeckMSOBuild)
        parsed["rundeckService"]["paths"]["mso.build"] = msoBuildPath
        parsed["rundeckService"]["paths"]["mso.build.definition"] = msoBuildPath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Message Sequence Build job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_MSO_BUILD: {}.".format(msoBuildPath)

try:
        rundeckMSOScore = os.environ["RUNDECK_JOB_MSO_SCORE"]
        msdScorePath = completePath(rundeckAPIVersion, rundeckMSOScore)
        parsed["rundeckService"]["paths"]["mso.score"] = msdScorePath
        parsed["rundeckService"]["paths"]["mso.score.definition"] = msdScorePath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Message Sequence score job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_MSO_SCORE: {}.".format(msdScorePath)

try:
        rundeckREMBuild = os.environ["RUNDECK_JOB_REM"]
        remPath = completePath(rundeckAPIVersion, rundeckREMBuild)
        parsed["rundeckService"]["paths"]["rem.build"] = remPath
        parsed["rundeckService"]["paths"]["rem.build.definition"] = remPath
        parsed["rundeckService"]["paths"]["rem.score"] = remPath
        parsed["rundeckService"]["paths"]["rem.score.definition"] = remPath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Engagement Model job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_REM: {}.".format(remPath)

try:
        rundeckMTBuild = os.environ["RUNDECK_JOB_TTE_BUILD"]
        mtBuildPath = completePath(rundeckAPIVersion, rundeckMTBuild)
        parsed["rundeckService"]["paths"]["tte.build"] = mtBuildPath
        parsed["rundeckService"]["paths"]["tte.build.definition"] = mtBuildPath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Message Timing Build job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_MT_BUILD: {}.".format(mtBuildPath)

try:
        rundeckMTScore = os.environ["RUNDECK_JOB_TTE_SCORE"]
        mtScorePath = completePath(rundeckAPIVersion, rundeckMTScore)
        parsed["rundeckService"]["paths"]["tte.score"] = mtScorePath
        parsed["rundeckService"]["paths"]["tte.score.definition"] = mtScorePath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Message Timing Build job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_MT_SCORE: {}.".format(mtScorePath)

try:
        rundeckAnchorBuild = os.environ["RUNDECK_JOB_ANCHOR_BUILD"]
        anchorBuildPath = completePath(rundeckAPIVersion, rundeckAnchorBuild)
        parsed["rundeckService"]["paths"]["anchor.build"] = anchorBuildPath
        parsed["rundeckService"]["paths"]["anchor.build.definition"] = anchorBuildPath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Anchor Build job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_ANCHOR_BUILD: {}.".format(anchorBuildPath)

try:
        rundeckAnchorScore = os.environ["RUNDECK_JOB_ANCHOR_SCORE"]
        anchorScorePath = completePath(rundeckAPIVersion, rundeckAnchorScore)
        parsed["rundeckService"]["paths"]["anchor.score"] = anchorScorePath
        parsed["rundeckService"]["paths"]["anchor.score.definition"] = anchorScorePath
except Exception as e:
        print "<>>> [WARNING] The Rundeck Anchor Score job is not defined, the default value will be used.\n"
else:
        print "\tRUNDECK_JOB_ANCHOR_SCORE: {}.".format(anchorScorePath)

# Container Agent Service
try:
        containerServiceKey = os.environ["LEARNING_CONTAINERSERVICE_KEY"]
        parsed["learningContainerService"]["key"] = containerServiceKey
except Exception as e:
        print "<>>> [WARNING] The Container Service API Key is not defined, the default value will be used.\n"
else:
        print ">> Container Agent Service Information:"
        print "\tLEARNING_CONTAINERSERVICE_KEY: {}.".format(containerServiceKey)

try:
        containerServiceUrl = os.environ["LEARNING_CONTAINERSERVICE_URL"]
        parsed["learningContainerService"]["url"] = repr(containerServiceUrl)
except Exception as e:
        print "<>>> [WARNING] The Container Service API URL is not defined, the default value will be used.\n"
else:
        print "\tLEARNING_CONTAINERSERVICE_URL: {}.".format(containerServiceUrl)

def tr(s):
    return s.replace('"','')

print ">>> Document to be exported:\n"
yaml.dump(parsed, sys.stdout, transform=tr)

try:
    with open(fileName, 'w') as outfile:
        yaml.dump(parsed, outfile, transform=tr)
except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise
