# Learning API Container

## Overview

The goal of this container is stand up the Aktana Learning API service in an managed environment having all the dependencies in place, also to bring a quick an safe way to deploy the service and ensure replication and version control.

Please follow the links showed below to get more information about the API.

* https://aktana.atlassian.net/wiki/spaces/MAIN/pages/154801552/Learning+API

## Container Information

### Base image

* Name: fedora-base-image.
* Version: 0.1.8.

### Build Arguments

Build arguments should be passed to docker at building time using the modifier: --build-arg key=value.

* **LEARNING_API_VERSION:** The version of the Learning API that is going to be installed. *Default value: **Empty**.*

### Environment Variables

* **PACKAGE:** Name of the RPM package of the Learning Service without version, release and extension. *Default value: **learning-service**.*
* **LEARNING_API_PORT:** Port the Learning API is going to use. *Default value: **9090**.*
* **LEARNING_API_ADMIN_PORT:** Port the API Admin is going to use. *Default value: **9191**.*
* **LEARNING_API_CONFIG:** Path where the Learning API config file is located inside the container. *Default value: **/mnt/aktana/apps/learning-service/learning-service.yml**.*
* **DB_USER:** User of the DSE database. *Default value: **appadmin**.*
* **DB_PASS:** Password of the DB User.
* **DB_HOST**: The hostname of the RDS server.
* **DB_PORT:** The port of the MySQL DB Server. *Default value: **3306**.*
* **DB_SCHEMA:** The name of the database schema used by the DSE.
* **METADATA_VERSION:** Customer metadata id. *Default value: **PFIZERUS**.*
* **PULL_SERVICES_HOST:** FQDN of the URL to access the DSE service served by this container.
* **GRADLE_VERSION:** Version of the Gradle tool to be installed. *Default value: **4.6**.*
* **DSE_SERVICE_USERNAME:** Username that can access the DSE API. *Default value: **aktanaadmin**.*
* **DSE_SERVICE_PASSWORD:** Password of the user of the DSE API. *Default value: **password!123**.*
* **DSE_SERVICE_SECRET:** DSE API Secret key. *Default value: **AktanaSecret!**.*
* **DSE_SERVICE_URL:** Full HTTPs URL of the DSE API. *Default value: **https://aktanadev.aktana.com:443/api**.*
* **RUNDECK_KEY:** API key of an authorized Rundeck user. *Default value: **gtFiN4aqWsHFnHWsTpCisNngd8X4tV26**.*
* **RUNDECK_URL:** Full HTTP URL of the Rundeck server. *Default value: **https://ops-west.aktana.com**.*
* **RUNDECK_API_VERSION:** Version of the Rundeck API. *Default value: **14**.*
* **RUNDECK_JOB_MSO_BUILD:** Rundeck Job ID of the Message Sequence Build job.
* **RUNDECK_JOB_MSO_SCORE:** Rundeck Job ID of the Message Sequence Scoring job.
* **RUNDECK_JOB_REM:** Rundeck Job ID of the Run Engagement Model job.
* **RUNDECK_JOB_TTE_BUILD:** Rundeck Job ID of the Run Timing Build Model job.
* **RUNDECK_JOB_TTE_SCORE:** Rundeck Job ID of the Run Timing Score Model job.
* **RUNDECK_JOB_ANCHOR_BUILD:** Rundeck Job ID of the Anchor Build Model job.
* **RUNDECK_JOB_ANCHOR_SCORE:** Rundeck Job ID of the Anchor Score Model job.
* **LEARNING_CONTAINERSERVICE_KEY:** Key to access the Container Agent API.
* **LEARNING_CONTAINERSERVICE_URL:** URL of the Container Agent API endpoints.
* **USE_TUNNEL:** A boolean variable, if set to 'true' will enable the connection to a specified RDS server through SSH tunnel. The information should be provided in the variables showed next. *Default value: **false**.*
* **SSH_KEY_PATH:** Path to locate the SSH key to be used to establish the SSH tunnel with a remote EC2 machine, this path should be relative to the internal file system of the container, it is not a path in the host machine. To use a private SSH key stored in the host, the directory where the key is located must be mounted as a volume to the path specified in this variable in the container i.e.: *-v /home/userX/.ssh:/root/.ssh*.
* **BRIDGE_HOSTNAME:** Hostname (not FQDN) of the proxy EC2 host to create the tunnel. *Default value: **dev**.*
* **RDS_HOSTNAME:** Hostname (not FQDN) of the RDS server to connect through the tunnel . *Default value: **devrds**.*
* **SOURCE_PORT:** Port of the database server in the RDS. *Default value: **3306**.*
* **TARGET_PORT:** Port to be used in the local machine to access the database server through the SSH tunnel. *Default value: **3307**.*
* **USERNAME:** A value username created in the EC2 server. *Default value: **aktdeployer**.*
* **DOMAIN:** Domain to be used to auto-complete the hostnames introduced in previous variables. *Default value: **aktana.com**.*
* **MIGRATE_DB:** A boolean value that enables or disables the execution of a database migration in container's launching time. *Default value: **false**.*


### Exposed Ports

The following ports are exposed by default.

* **9090** - To access the Learning API service endpoints.
* **9091** - to access the administrative page of Dropwizard service.

### Volumes Exported

The following volumes are exported by default:

* **/mnt/aktana/apps** - To expose all LEAPI files to the host.

##### Known issue

The docker compose file also exports the route `/mnt/aktana/apps/learning-service/logs` but it is mounted with **root** as the owner by default, the service will faile with **Unhandled Exception** error since the service cannot write to the logs directory since it should be owned by the **aktana** user, if this mount is used the logs directory owner should be changed manually and the service also started manually.

### Init File Sequence

If this is the first time the container is launched the following sequence will be executed:

1. Verifies the USE_TUNNEL variable, if true creates the SSH tunnel to the RDS server specified through the EC2 server as bridge.
2. Prints the DB connection, memory and service information to the standard output.
3. Clones the DEPLOY_REPO.
4. Re-populates the YAML file with DB data, Rundeck data, DSE Service data, Container Agent data.
5. Creates the init script.

If this is not the first time the container is launched:

1. Re-establish the SSH tunnel.

### Additional Files

The following files are stored at the /root directory:

* **setup-dse.sh** - This bash script pulls latest DSE build from the Artifactory, setup the configurations and permissions.

## Usage

Before to perform the following commands first you need to make sure you have logged in to Artifactory.

```bash
docker login aktanarepo-docker-local.jfrog.io
```

If SELinux is enabled execute this before launching a container:

```bash
sudo setsebool -P container_manage_cgroup 1
sudo chcon -Rt svirt_sandbox_file_t [/mount/PATH] # To be able to use a shared volume.
```

### Build Command

```bash
docker-compose build --force-rm [--build-arg LEARNING_API_VERSION=version] learning-api
```
To build an image without using the previous cache, include the option **--no-cache**.

### Publish Command

```bash
docker-compose push
```

### Docker Compose launch

```bash
docker-compose up -d
```

### Normal launch Command

```bash
docker run -d -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name=[...] --hostname=[...] -e DB_USER=[...] -e DB_PASS=[...] -e DB_HOST=[...] -e DB_PORT=[...] -e DB_SCHEMA=[...] -e METADATA_VERSION=[...] -e PULL_SERVICES_HOST=[...] -e ENVIRONMENT=[...] -e COPYSTORMSCHEMANAME=[...] -e GRADLE_VERSION=[...] -e DEPLOY_BRANCH=[...] -e LOG_STD_OUTPUT=[...] -e DSE_SERVICE_USERNAME=[...] -e DEPLOY_REPO=[...] -e DSE_SERVICE_PASSWORD=[...] -e DSE_SERVICE_SECRET=[...] -e DSE_SERVICE_URL=[...] -e RUNDECK_KEY=[...] -e RUNDECK_URL=[...] -e RUNDECK_API_VERSION=[...] -e RUNDECK_JOB_MSO_BUILD=[...] -e RUNDECK_JOB_MSO_SCORE=[...] -e RUNDECK_JOB_REM=[...]  -e RUNDECK_JOB_REM=[...] -e LEARNING_CONTAINERSERVICE_URL=[...] -e USE_TUNNEL=[...] -e SSH_KEY_PATH=[...] -e BRIDGE_HOSTNAME=[...] -e RDS_HOSTNAME=[...] -e SOURCE_PORT=[...] -e TARGET_PORT=[...] -e USERNAME=[...] -e DOMAIN=[...] -e MIGRATE_DB=[...] aktanarepo-docker-local.jfrog.io/aktana-service:[VERSION]
```

**Note:** It's a good practice create/join the container to a custom network, in that case just add the parameter: *--net [CUSTOM NETWORK]*.

##### Example of a production deployment:

```bash
docker run -d -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name="learning-api-beta" -h learning-api --net akt-network -e DB_PASS="XXXXXXXXXXXXXX" -e DB_HOST="demords.aktana.com" -e DB_SCHEMA="e2edemoeu" -e METADATA_VERSION="PFIZERUS" -e COPYSTORMSCHEMANAME="e2edemoeu_cs" -e PULL_SERVICES_HOST="e2edemoeu-ls.aktana.com" -e BRANCH="develop" aktanarepo-docker-local.jfrog.io/learning-service[:VERSION]
```

## Changelog

**2019-09-18 | v3.0.0** - Edwin Encinas <Edwin.Encinas@jalasoft.com>

* Upgrade base docker image to fedora-base-image 0.2.1.

**2019-08-01 | v2.2.6** - Viviana Camacho <viviana.camacho@jalasoft.com>

* Fixing urls on config yaml file.

**2019-07-11 | v2.2.5** - Erick Quinteros <erick.quinteros@jalasoft.com>

* Upgrade container to V17 Learning Service version.

**2019-02-04 | v2.2.4-alpha.2** - Pablo Studer <pablo.studer@jalasoft.com>

* Fix issue in populate script: Append /api to Rundeck URL.

**2018-11-03 | v2.2.4-alpha.1** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Fix issue in populate script: build and score URLs were reversed.

**2018-10-03 | v2.2.3-alpha.4** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Include the statement to regenerate the YAML file after a package update using the deploy.sh script.

**2018-10-03 | v2.2.2-alpha.3** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Add the function `String::completePath(apiVersion, jobId)` to the populateYaml.py script to generate the Rundeck partial URL of the job that execute the Learning Model.
* Update the block that generates the MySQL connection string, add the missing parameter: `?zeroDateTimeBehavior=convertToNull&characterEncoding=UTF-8`.
* Update the DSE variable replacement code blocks since variables where duplicated in a couple of blocks, specifically in *password* and *secret*.
* Update Rundeck section, add calls to the `completePath()` method for all the blocks that update rundeck jobs and Rundeck API version information.

**2018-10-02 | v2.2.1-alpha.2** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Fix error in regex expression of deploy.sh, it was unable to match the string passed as parameter.

**2018-10-01 | v2.2.0-alpha.1** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Add logic to the deploy.sh script to be able to install serveral versions of the LE service.

**2018-09-28 | v2.1.0-alpha.3** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Add *systemctl enable learning-service* missing statement.
* Update *deploy.sh* script, use systemctl instead of supervisorctl and call to *lsctl migrate*.

**2018-09-28 | v2.0.0-alpha.1** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

- Removed supervisor image, since the LE build is using a systemd unit we can use the regular fedora-base-image.
- Add Anchor build and score environment variables, also update *setup-env.sh* script to include migrate step if *MIGRATE_DB* is true, this option was missing.
- Replace ADD statement by COPY.
- Improve the build install statement.
- Update *populateYaml.py* fixing the RUNDECK_JOB_MT_BUILD and RUNDECK_JOB_MT_SCORE replacement blocks since they were exchanged. Also add RUNDECK_JOB_ANCHOR_BUILD and RUNDECK_JOB_ANCHOR_SCORE population blocks.
- Update *docker-compose.yaml* file, included networks sections; and the ANCHOR new variables.

**2018-03-25 | v1.2.1** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Move environment variable 'LEARNING_API_VERSION' to build argument (ARG).
* Surround install statement with double quotes to ensure proper value expansion.
* Build and publish develop (9999) and 17.15a images.

**2018-03-25 | v1.2.0** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Add 'LEARNING_API_CONFIG' variable to provide the location of the Learning API config file.
* Remove installation of: initiscripts, Development tools, sudo, memcached, lpsolve and bc packages.
* Fix 'populateYaml.py' script typos in substitution variables like 'userame' to 'username'.
* Add 'container_name' to the docker-compose file.
* Remove double quotes from the 'LEARNING_API_CONFIG' variable in the docker-compose file.

**2018-03-22 | v1.2.0** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Implement 'populateYaml.py' script to replace the Gradle one.
* Remove COPYSTORMSCHEMANAME and ENVIRONMENT variables since they are not required for this application.
* Remove old Gradle script to populate the config file.
* Update 'setup-env.sh' script to use the populateYaml.py script.
* Remove SDKman installation.

**2018-03-19 | v1.1.3-r1** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Remove JVM from std output in setup-env.sh.

**2018-03-19 | v1.1.3** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Remove git environment variables since we install from RPM package, and gradle populate script is embedded in the container filesystem.
* Remove JVM memory env variable since we don't set this varible in the container.

**2018-03-19 | v1.1.2** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Fix docker-compose.yaml file since the merge mixed engine with api environment variables.

**2018-03-14 | v1.1.2** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Update setup-env.sh script, move the evaluation setter to the hashbang.
* Change the init file name to 'learning-api.init'.

**2018-03-09 | v1.1.2** - Sergio Rodríguez Inclán <sergio.rodriguez@jalasoft.com>

* Update default Gradle version to 4.6.
* Create documentation.