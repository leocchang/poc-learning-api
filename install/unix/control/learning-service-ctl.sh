#!/bin/bash
#
# learning-api-ctl.sh  Process control script for the Learning API.
#
# created by : Sergio D. Rodríguez Inclán <sergio.rodriguez@jalasoft.com>
#
# created on : 2018-09-26.
#
# Copyright AKTANA (c) 2018.

# Check if we are in a MAC OSX platform.
case "`uname`" in
        Darwin*)
                if [ -z "$JAVA_HOME" ] ; then
                        JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home
                fi
                ;;
esac

# Set the api home location.
PRG="$0"
LEARNING_HOME=`dirname "$PRG"`/..

# Make if fully qualified.
LEARNING_HOME=`cd "$LEARNING_HOME" && pwd`

# Name of the JAR file.
PROG=learning-service
LEARNINGBIN=$PROG.jar
LEARNINGCONF=$PROG.yml
LEARNING=${LEARNING_HOME}/${LEARNINGBIN}
PIDFILE=${LEARNING_HOME}/$PROG.pid
RETVAL=0
STOP_TIMEOUT=${STOP_TIMEOUT-10}
LOCKFILE=${LEARNING_HOME}/$PROG.lock

# Store standard output to a log file.
LOG_STD_OUT=false

# Verify if the jar is in place.
if [ ! -f "$LEARNING_HOME/$LEARNINGBIN" ] ; then
        echo "Error: Could not find $LEARNING_HOME/$LEARNINGBIN"
        exit 1
fi

# Check for java.
if [ -z "$JAVACMD" ] ; then
        if [ -n "$JAVACMD" ] ; then
                if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
                       # IBM's JDK on AIX uses strange locations for the executables.
                       JAVACMD="$JAVA_HOME/jre/sh/java"
                else
                       JAVACMD="$JAVA_HOME/bin/java"
                fi
        else
                JAVACMD=`which java 2> /dev/null`
                if [ -z "$JAVACMD" ] ; then
                        JAVACMD=java
                fi
        fi
fi

if [ ! -x "$JAVACMD" ] ; then
        echo "Error: JAVA_HOME is not defined correctly."
        echo "  We cannot execute $JAVACMD"
        exit 1
fi

# If the installation path is not defined.
if [ -z "$LEARNING_INST" ] ; then
        LEARNING_INST=$LEARNING_HOME
fi

JETTY_OPTIONS=""

if [ -z "$AKTANA_JVM_XMX" ]
then
        JVM_OPTIONS="-Xmx2G -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp/ -verbose:gc -Duser.timezone=\"UTC\""
else
        JVM_OPTIONS="-Xmx${AKTANA_JVM_XMX}G -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp/ -verbose:gc -Duser.timezone=\"UTC\""
fi

LEARNING_OPTIONS=""
LEARNING_CONFIG=${LEARNING_HOME}/$LEARNINGCONF

LEARNING_CMD="$JAVACMD $JVM_OPTIONS $JETTY_OPTIONS $LEARNING_OPTIONS -jar $LEARNING"

# Source functions library.
. /etc/rc.d/init.d/functions

if [ -f /etc/sysconfig/$PROG.conf ]; then
        . /etc/sysconfig/$PROG.conf
fi

start() {
        echo -n $"Starting $PROG: "
        mkdir -p ${LEARNING_HOME}/logs
    	cd ${LEARNING_HOME}
        $LOG_STD_OUT && LOG_STORE="${LEARNING_HOME}"/logs/std.out || LOG_STORE=/dev/null
        nohup $LEARNING_CMD server ${LEARNING_CONFIG} >> $LOG_STORE 2>&1 &
        echo "Output redirected to $LOG_STORE"
        RETVAL=$?
        echo
        if [ $RETVAL = 0 ] ; then
                echo $! > ${PIDFILE}
                touch ${LOCKFILE}
        fi
        return $RETVAL
}

stop() {
        echo -n $"Stopping $PROG: "
        killproc -p ${PIDFILE} -d ${STOP_TIMEOUT} ${LEARNING}
        RETVAL=$?
        echo
        [ $RETVAL = 0 ] && rm -f ${LOCKFILE} ${PIDFILE}
}

reload() {
        echo -n $"Reloading $PROG: "
        if ! ${LEARNING} -t >&/dev/null; then
                RETVAL=6
                echo $"Unable to reload due to configuration syntax error"
                failure $"Unable to reload $LEARNING due to configuration syntax error"
        else
                # Force LSB behaviour from killproc
                LSB=1 killproc -p ${PIDFILE} ${LEARNING} -HUP
                RETVAL=$?
                if [ $RETVAL -eq 7 ]; then
                        failure $"Learning api shutdown"
                fi
        fi
        echo
}

migrate() {
        echo -n $"Migrating the database: "
        mkdir -p ${LEARNING_HOME}/logs
        ${LEARNING_CMD} db migrate ${LEARNING_CONFIG} > ${LEARNING_HOME}/logs/migrate.out
        RETVAL=$?
        if [ $RETVAL -ne 0 ] ; then
                failure $"Migrations failed, please see the migrations log."
        fi
        echo
}

# See how we were called
case "$1" in
        start)
                start
                ;;
        stop)
                stop
                ;;
        status)
                status -p ${PIDFILE} ${LEARNING}
                RETVAL=$?
                ;;
        restart)
                stop
                start
                ;;
        migrate)
                migrate
                ;;
esac

exit $RETVAL
