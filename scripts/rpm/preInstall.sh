#!/bin/bash

# Create Aktana user.
if ! id -u aktana >/dev/null 2>&1; then
        useradd -m aktana
fi

# Create home.
mkdir -p /mnt/aktana/apps/learning-service